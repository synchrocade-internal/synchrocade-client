﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Synchrocade.ReusableFunctions;

namespace NetplayTests
{
    [TestClass]
    public class BitShiftTests
    {
        [TestClass]
        public class addToArrayTests
        {
            [TestMethod]
            public void TestAddingNoBreak()
            {
                byte[] verificationBytes = new byte[3];
                verificationBytes[0] = (byte)0b0101_1101;
                verificationBytes[1] = (byte)0b1110_1100;
                verificationBytes[2] = (byte)0b0000_0000;
                byte[] inputBytes = new byte[3];
                inputBytes[0] = (byte)0b0101_1101;
                inputBytes[1] = (byte)0b1000_0000;
                byte byteToAdd = (byte)0b0001_1011;
                uint currentinputbit = 9;
                uint numBitsToAdd = 5;
                BitShift.AddToArray(ref inputBytes, currentinputbit, byteToAdd, numBitsToAdd,false);
                Assert.IsTrue(CheckEqual(verificationBytes, inputBytes));
            }
            [TestMethod]
            public void TestAddingNoBreakNoShift()
            {
                byte[] verificationBytes = new byte[3];
                verificationBytes[0] = (byte)0b0101_1101;
                verificationBytes[1] = (byte)0b1001_1011;
                verificationBytes[2] = (byte)0b0000_0000;
                byte[] inputBytes = new byte[3];
                inputBytes[0] = (byte)0b0101_1101;
                inputBytes[1] = (byte)0b1000_0000;
                byte byteToAdd = (byte)0b0001_1011;
                uint currentinputbit = 11;
                uint numBitsToAdd = 5;
                BitShift.AddToArray(ref inputBytes, currentinputbit, byteToAdd, numBitsToAdd,false);
                Assert.IsTrue(CheckEqual(verificationBytes, inputBytes));
            }
            [TestMethod]
            public void TestAddingWithBreak()
            {
                byte[] verificationBytes = new byte[3];
                verificationBytes[0] = (byte)0b0101_1101;
                verificationBytes[1] = (byte)0b1001_0110;
                verificationBytes[2] = (byte)0b1100_0000;
                byte[] inputBytes = new byte[3];
                inputBytes[0] = (byte)0b0101_1101;
                inputBytes[1] = (byte)0b1000_0000;
                byte byteToAdd = (byte)0b0101_1011;
                uint currentinputbit = 11;
                uint numBitsToAdd = 7;
                BitShift.AddToArray(ref inputBytes, currentinputbit, byteToAdd, numBitsToAdd,false);
                Assert.IsTrue(CheckEqual(verificationBytes, inputBytes));

            }

            [TestMethod]
            public void TestAddingNoBreakHighOrder()
            {
                byte[] verificationBytes = new byte[3];
                verificationBytes[0] = (byte)0b0101_1101;
                verificationBytes[1] = (byte)0b1110_1100;
                verificationBytes[2] = (byte)0b0000_0000;
                byte[] inputBytes = new byte[3];
                inputBytes[0] = (byte)0b0101_1101;
                inputBytes[1] = (byte)0b1000_0000;
                //byte byteToAdd = (byte)0b0001_1011;
                byte byteToAdd = (byte)0b1101_1000;
                uint currentinputbit = 9;
                uint numBitsToAdd = 5;
                BitShift.AddToArray(ref inputBytes, currentinputbit, byteToAdd, numBitsToAdd,true);
                Assert.IsTrue(CheckEqual(verificationBytes, inputBytes));
            }
            [TestMethod]
            public void TestAddingNoBreakNoShiftHighOrder()
            {
                byte[] verificationBytes = new byte[3];
                verificationBytes[0] = (byte)0b0101_1101;
                verificationBytes[1] = (byte)0b1001_1011;
                verificationBytes[2] = (byte)0b0000_0000;
                byte[] inputBytes = new byte[3];
                inputBytes[0] = (byte)0b0101_1101;
                inputBytes[1] = (byte)0b1000_0000;
                //byte byteToAdd = (byte)0b0001_1011;
                byte byteToAdd = (byte)0b1101_1000;
                uint currentinputbit = 11;
                uint numBitsToAdd = 5;
                BitShift.AddToArray(ref inputBytes, currentinputbit, byteToAdd, numBitsToAdd,true);
                Assert.IsTrue(CheckEqual(verificationBytes, inputBytes));
            }
            [TestMethod]
            public void TestAddingWithBreakHighOrder()
            {
                byte[] verificationBytes = new byte[3];
                verificationBytes[0] = (byte)0b0101_1101;
                verificationBytes[1] = (byte)0b1001_0110;
                verificationBytes[2] = (byte)0b1100_0000;
                byte[] inputBytes = new byte[3];
                inputBytes[0] = (byte)0b0101_1101;
                inputBytes[1] = (byte)0b1000_0000;
                byte byteToAdd = (byte)0b1011_0110;
                uint currentinputbit = 11;
                uint numBitsToAdd = 7;
                BitShift.AddToArray(ref inputBytes, currentinputbit, byteToAdd, numBitsToAdd,true);
                Assert.IsTrue(CheckEqual(verificationBytes, inputBytes));

            }

            [TestMethod]
            public void TestMultiAdd()
            {
                byte[] verificationBytes = new byte[5];
                verificationBytes[0] = (byte)0b0101_1101;
                verificationBytes[1] = (byte)0b1001_0110;
                verificationBytes[2] = (byte)0b1101_0101;
                verificationBytes[3] = (byte)0b1010_0101;
                verificationBytes[4] = (byte)0b1101_0100;
                byte[] inputBytes = new byte[5];
                inputBytes[0] = (byte)0b0101_1101;
                inputBytes[1] = (byte)0b1000_0000;
                byte[] byteToAdd = new byte[4];
                byteToAdd[0] = 0b1011_0110;
                byteToAdd[1] = 0b1010_1101;
                byteToAdd[2] = 0b0010_1110;
                byteToAdd[3] = 0b1010_0000;
                uint currentinputbit = 11;
                uint numBitsToAdd = 28;

                BitShift.AddToArray(ref inputBytes, currentinputbit, byteToAdd, numBitsToAdd);
                Assert.IsTrue(CheckEqual(verificationBytes, inputBytes));
            }

            [TestMethod]
            public void testFillEndSingleByte()
            {
                byte inputByte = 0b0101_0000;
                byte verificationByte = 0b0101_0000;


                verificationByte = 0b0111_1111;
                BitShift.FillByteEnd(ref inputByte, 2);
                Assert.IsTrue(inputByte == verificationByte);

                verificationByte = 0b0100_0000;
                BitShift.FillByteEnd(ref inputByte, 2, false);
                Assert.IsTrue(inputByte == verificationByte);


                //ref inputArray
                //BitShift.fillFinalArrayByteEnd(ref inputbytes, currentinputbit);

            }

            [TestMethod]
            public void testFillEndingByteArray()
            {

                byte[] inputbytes = new byte[5];
                inputbytes[0] = (byte)0b0101_1101;
                inputbytes[1] = (byte)0b1001_0110;
                inputbytes[2] = (byte)0b1101_0101;
                inputbytes[3] = (byte)0b1010_0000;

                byte[] verificationBytes = new byte[5];
                verificationBytes[0] = (byte)0b0101_1101;
                verificationBytes[1] = (byte)0b1001_0110;
                verificationBytes[2] = (byte)0b1101_0101;
                verificationBytes[3] = (byte)0b1011_1111;
                uint currentinputbit = 26;

                

                BitShift.FillFinalArrayByteEnd(ref inputbytes, currentinputbit);
                Assert.IsTrue(CheckEqual(verificationBytes, inputbytes));

                verificationBytes[3] = (byte)0b1000_0000;

                BitShift.FillFinalArrayByteEnd(ref inputbytes, currentinputbit, false);
                Assert.IsTrue(CheckEqual(verificationBytes, inputbytes));

            }
        }

        [TestClass]
        public class getFromArray
        {
            [TestMethod]
            public void testSingleByteNoBreak()
            {
                byte[] sourceArray = new byte[5];
                sourceArray[0] = (byte)0b0101_1101;
                sourceArray[1] = (byte)0b1001_0110;
                sourceArray[2] = (byte)0b1101_0101;
                sourceArray[3] = (byte)0b1010_0101;
                sourceArray[4] = (byte)0b1101_0111;

                byte[] verificationBytes = new byte[1];
                verificationBytes[0] = (byte)0b1101_0000;

                uint bitsToPull = 4;
                uint startingBit = 16; // zero indexed
                byte[] compareByte = new byte[1];
                compareByte[0] = BitShift.ReadSingleByteFromArray(sourceArray, startingBit, bitsToPull);

                Assert.IsTrue(CheckEqual(verificationBytes, compareByte));

            }

            [TestMethod]
            public void testSingleByteOverBreak()
            {
                byte[] sourceArray = new byte[5];
                sourceArray[0] = (byte)0b0101_1101;
                sourceArray[1] = (byte)0b1001_0110;
                sourceArray[2] = (byte)0b1101_0101;
                sourceArray[3] = (byte)0b1010_0101;
                sourceArray[4] = (byte)0b1101_0111;

                byte[] verificationBytes = new byte[3];
                verificationBytes[0] = (byte)0b1001_0110;
                verificationBytes[1] = (byte)0b1101_0101;
                verificationBytes[2] = (byte)0b1010_0100;

                uint bitsToPull = 22;
                uint startingBit = 8; // zero indexed
                byte[] compareByte = BitShift.ReadFromArray(sourceArray, startingBit, bitsToPull);

                Assert.IsTrue(CheckEqual(verificationBytes, compareByte));
            }

            [TestMethod]
            public void testMultiByte()
            {
                byte[] sourceArray = new byte[5];
                sourceArray[0] = (byte)0b0101_1101;
                sourceArray[1] = (byte)0b1001_0110;
                sourceArray[2] = (byte)0b1101_0101;   
                sourceArray[3] = (byte)0b1010_0101;
                sourceArray[4] = (byte)0b1101_0111;

                byte[] verificationBytes = new byte[3];
                verificationBytes[0] = (byte)0b0101_0110;
                verificationBytes[1] = (byte)0b1001_0111;
                verificationBytes[2] = (byte)0b0100_0000;

                uint bitsToPull = 18;
                uint startingBit = 18; // zero indexed
                byte[] compareByte = BitShift.ReadFromArray(sourceArray, startingBit, bitsToPull);
                
                Assert.IsTrue(CheckEqual(verificationBytes, compareByte));

            }

            [TestMethod]
            public void testMultiByteNoShift()
            {
                byte[] sourceArray = new byte[5];
                sourceArray[0] = (byte)0b0101_1101;
                sourceArray[1] = (byte)0b1001_0110;
                sourceArray[2] = (byte)0b1101_0101;
                sourceArray[3] = (byte)0b1010_0101;
                sourceArray[4] = (byte)0b1101_0111;

                byte[] verificationBytes = new byte[3];
                verificationBytes[0] = (byte)0b1001_0110;
                verificationBytes[1] = (byte)0b1101_0101;
                verificationBytes[2] = (byte)0b1010_0100;

                uint bitsToPull = 22;
                uint startingBit = 8; // zero indexed
                byte[] compareByte = BitShift.ReadFromArray(sourceArray, startingBit, bitsToPull);
                
                Assert.IsTrue(CheckEqual(verificationBytes, compareByte));
            }


            [TestMethod]
            public void testSingleBitCheck()
            {
                byte[] sourceArray = new byte[5];
                sourceArray[0] = (byte)0b0101_1101;
                sourceArray[1] = (byte)0b1001_0110;
                sourceArray[2] = (byte)0b1101_0101;
                sourceArray[3] = (byte)0b1010_0101;
                sourceArray[4] = (byte)0b1101_0111;

               
                Assert.IsTrue(BitShift.ReadBitFromArray(sourceArray, 8));
                Assert.IsFalse(BitShift.ReadBitFromArray(sourceArray, 9));
                Assert.IsFalse(BitShift.ReadBitFromArray(sourceArray, 10));
                Assert.IsTrue(BitShift.ReadBitFromArray(sourceArray, 24));
            }




        }

        private static bool CheckEqual(byte[] first, byte[] second)
        {
            if (first.Length != second.Length)
                return false;
            for (int x = 0; x < first.Length; x++)
            {
                //236 and 128
                if (first[x] != second[x])
                {
                    return false;
                }
            }
            return true;
        }
    }
}