﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Synchrocade.GameObjects.Input;
namespace NetplayTests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class InputUnitTests 
    {

        [TestMethod]
        public void testInputEncodeDecode()
        {
            InputObject firstInput = new InputObject();
            firstInput.SetButtonPressed(InputObject.Button.but3,true);
            firstInput.SetButtonPressed(InputObject.Button.but6,true);
            firstInput.SetDirectionPressed(InputObject.Direction.up, InputObject.DType.Move, true);
            firstInput.SetDirectionPressed(InputObject.Direction.right, InputObject.DType.Move, true);
            firstInput.SetDirectionPressed(InputObject.Direction.down, InputObject.DType.Aim, true);
            firstInput.SetDirectionPressed(InputObject.Direction.left, InputObject.DType.Aim, true);

            byte[] testEncoder = new byte[50];
            InputObject.EncodeDecodeOptions options = new InputObject.EncodeDecodeOptions(true, true, 8);
            uint encodeCounter = 0;
            firstInput.EncodeInputs(ref testEncoder, ref encodeCounter, options);

            InputObject secondInput = new InputObject();
            uint decodeCounter = 0;

            secondInput.DecodeInputs(testEncoder, ref decodeCounter, options);

            Assert.IsTrue(secondInput.Equals(firstInput));

        }

        [TestMethod]
        public void testManyInputs()
        {
            InputObject firstInput = new InputObject();
            firstInput.SetButtonPressed(InputObject.Button.but3, true);
            firstInput.SetButtonPressed(InputObject.Button.but6, true);
            firstInput.SetDirectionPressed(InputObject.Direction.up, InputObject.DType.Move, true);
            firstInput.SetDirectionPressed(InputObject.Direction.right, InputObject.DType.Move, true);
            firstInput.SetDirectionPressed(InputObject.Direction.down, InputObject.DType.Aim, true);
            firstInput.SetDirectionPressed(InputObject.Direction.left, InputObject.DType.Aim, true);

            InputObject thirdInput = new InputObject();
            thirdInput.SetButtonPressed(InputObject.Button.but2, true);
            thirdInput.SetButtonPressed(InputObject.Button.but4, true);
            thirdInput.SetDirectionPressed(InputObject.Direction.down, InputObject.DType.Move, true);
            InputObject.EncodeDecodeOptions options2 = new InputObject.EncodeDecodeOptions(true, false, 6);

            byte[] testEncoder = new byte[100];
            InputObject.EncodeDecodeOptions options = new InputObject.EncodeDecodeOptions(true, true, 8);
            uint encodeCounter = 0;
            uint decodeCounter = 0;

            for (int x = 0; x < 50; x++)
            {
                if (x % 2 == 0)
                {
                    firstInput.EncodeInputs(ref testEncoder, ref encodeCounter, options);
                    InputObject secondInput = new InputObject();

                    secondInput.DecodeInputs(testEncoder, ref decodeCounter, options);

                    Assert.IsTrue(secondInput.Equals(firstInput));
                }
                else
                {
                    thirdInput.EncodeInputs(ref testEncoder, ref encodeCounter, options2);
                    InputObject secondInput = new InputObject();

                    secondInput.DecodeInputs(testEncoder, ref decodeCounter, options2);

                    Assert.IsTrue(secondInput.Equals(thirdInput));

                }

            }


        }


        [TestMethod]
        public void testEncodeDirection()
        {
            InputObject firstInput = new InputObject();
            //firstInput.setDirectionPressed(InputObject.Direction.up, InputObject.DType.Move, true);
            //firstInput.setDirectionPressed(InputObject.Direction.right, InputObject.DType.Move, true);
            firstInput.SetDirectionPressed(InputObject.Direction.down, InputObject.DType.Aim, true);
            firstInput.SetDirectionPressed(InputObject.Direction.left, InputObject.DType.Aim, true);

            var outputTuple = firstInput.EncodeDirection(InputObject.DType.Aim);
            Assert.IsTrue(outputTuple.outputByte == 0b1110_0000);
            Assert.IsTrue(outputTuple.currentBit == 4);
        }

        [TestMethod]
        public void testDecodeDirection()
        {
            InputObject firstInput = new InputObject();
            byte[] inputbyte = new byte[1];
            inputbyte[0] = 0b1110_0000;
            uint currentBit = 0;

            firstInput.DecodeDirection(ref inputbyte, ref currentBit, InputObject.DType.Aim);
            Assert.IsTrue(firstInput.GetDirectionPressed(InputObject.Direction.down, InputObject.DType.Aim));
            Assert.IsTrue(firstInput.GetDirectionPressed(InputObject.Direction.left, InputObject.DType.Aim));

            Assert.IsTrue(currentBit == 4);
            
        }

    }
}
