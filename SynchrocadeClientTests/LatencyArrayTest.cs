﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Synchrocade.Networking;

namespace LatencyArrayTests
{
    [TestClass]
    public class LatencyArrayTest
    {
        [TestMethod]
        public void testAddNoOverflow()
        {
            LatencyArray testArray = new LatencyArray(5);
            testArray.AddLatencyValue(1);
            testArray.AddLatencyValue(-1);
            testArray.AddLatencyValue(3);
            Assert.IsTrue(testArray.GetMedianValue() == 1.0);
            testArray.AddLatencyValue(3);
            Assert.IsTrue(testArray.GetMedianValue() == 6.0/4);
        }

        [TestMethod]
        public void testAddWithOverflow()
        {
            LatencyArray testArray = new LatencyArray(3);
            testArray.AddLatencyValue(1);
            testArray.AddLatencyValue(-1);
            testArray.AddLatencyValue(3);
            Assert.IsTrue(testArray.GetMedianValue() == 1.0);
            testArray.AddLatencyValue(3);
            Assert.IsTrue(testArray.GetMedianValue() == 5.0 / 3);

        }
    }
}
