﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Synchrocade.TemporalContainer;

namespace NetplayTests.TemporalContainer
{
    [TestClass]
    public class CircularArrayUnitTests
    {
        [TestMethod]
        public void TestAddingManyItems()
        {
            CircularArray<int> container = new CircularArray<int>(50, 800, 0, 0);
            for (int x = 0; x <= 15000; x++)
            {
                container.Push(x);
                Assert.AreEqual(container[800 + x], x);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestAccessingPastCount()
        {
            CircularArray<int> container = new CircularArray<int>(5, 800,0,0);
            for (int x = 0; x <= 4; x++)
            {
                container.Push(x);
                Assert.AreEqual(container[800 + x], x);
            }
            Assert.AreEqual(container[800 + 4], 4);
            Assert.AreEqual(container[800 + 0], 0);
            container.Push(5);
            Assert.AreEqual(container[800 + 6], 6);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestAccessingBeforeHead()
        {
            CircularArray<int> container = new CircularArray<int>(5, 800,0,0);
            for (int x = 0; x <= 4; x++)
            {
                container.Push(x);
                Assert.AreEqual(container[800 + x], x);
            }
            Assert.AreEqual(container[800 + 4], 4);
            Assert.AreEqual(container[800 + 0], 0);
            container.Push(5);
            Assert.AreEqual(container[800 + 0], 0);
        }

        [TestMethod]
        public void TestRemoveItemsFromStart()
        {
            CircularArray<int> container = new CircularArray<int>(5, 800,0,0);
            for (int x = 0; x <= 50; x++)
            {
                container.Push(x);
                Assert.AreEqual(container[800 + x], x);
            }
            container.RemoveItemsFromStart(800 + 48);
            Assert.AreEqual(container[800 + 48], 48);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestRemoveItemsFromStartBreaksIndex()
        {
            CircularArray<int> container = new CircularArray<int>(5, 800,0,0);
            for (int x = 0; x <= 50; x++)
            {
                container.Push(x);
                Assert.AreEqual(container[800 + x], x);
            }
            container.RemoveItemsFromStart(800 + 48);
            Assert.AreEqual(container[800 + 47], 47);
        }
        [TestMethod]
        public void TestRemoveItemsFromEnd()
        {
            CircularArray<int> container = new CircularArray<int>(5, 800,0,0);
            for (int x = 0; x <= 50; x++)
            {
                container.Push(x);
                Assert.AreEqual(container[800 + x], x);
            }
            container.RemoveItemsFromEnd(800 + 47);
            Assert.AreEqual(container[800 + 47], 47);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestRemoveItemsFromEndBreaksIndex()
        {
            CircularArray<int> container = new CircularArray<int>(5, 800,0,0);
            for (int x = 0; x <= 50; x++)
            {
                container.Push(x);
                Assert.AreEqual(container[800 + x], x);
            }
            container.RemoveItemsFromEnd(800 + 47);
            Assert.AreEqual(container[800 + 48], 48);
        }
    }
}
