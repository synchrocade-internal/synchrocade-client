﻿using System;
using System.Collections.Generic;
using Ultraviolet.Content;
using Ultraviolet.UI;
using Synchrocade.Screens.GameScreens;
using Synchrocade.Screens.UI;
using Ultraviolet.Core;
using Synchrocade.GameLogics;
using Synchrocade.UpdateThread;
using Synchrocade.Networking;
using Synchrocade.Screens.UI.Lobby;
using Synchrocade.Screens.UI.Settings;
using Synchrocade.Settings;

namespace Synchrocade.Screens
{

    //copied from simple screens example

    /// <summary>
    /// Represents a service which provides instances of UI screen types upon request.
    /// </summary>
    internal sealed class UIScreenService
    {
        GameScreen? currentGameScreen;
        private readonly PheonixClient pheonixClient; //i might want to rethink if the screenservice should be the storage for global objects or not
        internal readonly SettingsContainer settings;
        internal readonly OnlineGame Game;
        
        internal bool gameScreenShouldUpdate = true;
        /// <summary>
        /// Initializes a new instance of the UIScreenService type.
        /// </summary>
        internal UIScreenService(ContentManager globalContent, OnlineGame game, bool ignorebadssl, string address, int port)
        {
            settings = SettingsContainer.LoadOrIntialize();
            this.Game = game;
            pheonixClient = new PheonixClient(globalContent.Ultraviolet, ignorebadssl, address, port);
            //Contract.Require(globalContent, nameof(globalContent));
            //Register(new LanConnectionScreen(globalContent, this));
            Register(new LanConnectionScreen(globalContent,this));
            Register(new LobbyMainScreen(globalContent, this));
            Register(new SettingsScreen(globalContent, this));
        }

        /// <summary>
        /// Gets an instance of the specified screen type.
        /// </summary>
        /// <returns>An instance of the specified screen type.</returns>
        internal T Get<T>() where T : UIScreen
        {
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
#pragma warning disable CS8603 // Possible null reference return.
            screens.TryGetValue(typeof(T), out UIScreen screen);
            return (T)screen;
#pragma warning restore CS8603 // Possible null reference return.
#pragma warning restore CS8600 // Converting null literal or possible null value to non-nullable type.
        }

        /// <summary>
        /// Registers the screen instance that is returned for the specified type.
        /// </summary>
        /// <param name="instance">The instance to return for the specified type.</param>
        private void Register<T>(T instance) where T : UIScreen
        {
            screens[typeof(T)] = instance;
        }

        // State values.
        private readonly Dictionary<Type, UIScreen> screens =
            new Dictionary<Type, UIScreen>();


        internal NetworkLoop? GetNetworkLoop()
        {
            return Game.networkLoop;
        }
        
        internal PheonixClient GetPheonixClient()
        {
            return pheonixClient;
        }
        
        internal SettingsContainer GetSettings()
        {
            return settings;
        }

        internal GameScreen? GetCurrentGameScreen()
        {
            return currentGameScreen;
        }

        public void logout()
        {
            LanConnectionScreen lanscreen = this.Get<LanConnectionScreen>();
            lanscreen.Ultraviolet.GetUI().GetScreens()
                .CloseThenOpen(lanscreen.Ultraviolet.GetUI().GetScreens().Peek(), lanscreen);
            lanscreen.Ultraviolet.GetUI().GetScreens().CloseAllExcept(lanscreen);
            pheonixClient.logout();
        }

        internal void SetCurrentGameScreen(GameScreen? currentGameScreen)
        {
            this.currentGameScreen = currentGameScreen;
        }

        internal void DestroyGameScreen()
        {
            
            currentGameScreen?.DisposeContained();
            currentGameScreen?.Dispose();
            currentGameScreen = null;
        }

        internal void UpdateGameScreen(bool stalled)
        {
            if(!(currentGameScreen is null)&&gameScreenShouldUpdate)
                currentGameScreen.RealUpdate(stalled);
        }
 
    }
}
