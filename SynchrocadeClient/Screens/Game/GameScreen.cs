﻿using Ultraviolet.Core;
using Ultraviolet;
using Ultraviolet.Content;
using Ultraviolet.Graphics;
using Ultraviolet.Graphics.Graphics2D;
using Ultraviolet.Graphics.Graphics2D.Text;
using Ultraviolet.Input;
using Ultraviolet.UI;
using Synchrocade.GameObjects;
using Synchrocade.GameLogics;
using Synchrocade.Networking;
using Synchrocade.GameObjects.Input;
using Synchrocade.UpdateThread;
using System.Diagnostics;
using System;
using System.Threading;
using Synchrocade.GameObjects;
using Serilog;
using Synchrocade.Screens.UI;

namespace Synchrocade.Screens.GameScreens
{


    internal class GameScreen : UIScreenBase
    {
        private bool disposing = false;

        //private readonly RenderTarget2D drawTarget;
        //private readonly RenderBuffer2D drawBuffer;

        private readonly ContentManager globalContent;

        private readonly bool letterbox = true;

        private readonly Stopwatch elapsedTime = new Stopwatch();
        private static Effect? effect;
        SpriteBatch spriteBatch;
        internal readonly OnlineGameLoop myGame;
        private readonly IGameLogic myGameLogic;
        private readonly UpdateThread.UpdateThread updateThread;
        private readonly OnlineGame onlineGame;
        private readonly object _memoryIntegrityLock;
        PaletteEffectSpriteBatch scalebatch;


        internal GameScreen(int players, string[] playerNames, ContentManager globalContent, UIScreenService uiScreenService, IGameLogic gamelogic, UpdateThread.UpdateThread updateThread, OnlineGame onlineGame, object memoryIntegrityLock)
            : base("Content/UI/Screens/GameScreen", "GameScreen", globalContent, uiScreenService)
        {
            this.onlineGame = onlineGame;
            this.globalContent = globalContent;
            this.updateThread = updateThread;
            this._memoryIntegrityLock = memoryIntegrityLock;
            IsOpaque = true;
            scalebatch = new PaletteEffectSpriteBatch(Ultraviolet);
            this.spriteBatch = SpriteBatch.Create();
            myGameLogic = gamelogic;
            myGame = new OnlineGameLoop(players, playerNames, 120, myGameLogic, onlineGame, uiScreenService.GetSettings() ,this.Ultraviolet, memoryIntegrityLock);
            onlineGame.networkLoop!.SetCurrentGameFrame(myGame.GetCurrentFrame());
        }

        internal void RealUpdate(bool stalled)
        {
            //the is running thing is a hack to prevent a crash by stopping the loop after the frame resumes itself due to slow shutdown time... stupid haxx
            if (updateThread.IsRunning()&&!disposing)
            {
                //run the pre-update
                myGame.PreUpdate(stalled);

                if (myGame.IsGameFinished())
                {
                    long[] scores = myGame.ReturnScores();
                    string.Join("-",scores);
                    onlineGame.networkLoop?.endGame(false,"Game Ended " + scores,true);
                }
            }
            
        }

        protected override void OnClosing()
        {
            onlineGame.networkLoop?.endGame(false,"Application Terminated",true);
            base.OnClosing();
        }
        
  
        

        private int screenWidth = 0;
        private int screenHeight = 0;

        [DebuggerNonUserCode()]
        protected override void OnDrawingBackground(UltravioletTime time, SpriteBatch spriteBatch)
        {
            


            
            /*if (time.ElapsedTime.Ticks > 85000)
            {
                Log.Debug($"ticks since last draw {time.ElapsedTime.Ticks}," +
                          $" {GC.CollectionCount(0)}, {GC.CollectionCount(1)}, {GC.CollectionCount(2)}");

            }*/


            
            
 
            
            
            if (disposing)
                return;
            if (Monitor.TryEnter(_memoryIntegrityLock, 3))
            {
                try
                {
                    //System.Console.Write(elapsedTime.ElapsedMilliseconds - lastMSCount);

                    //if(!shouldDraw) return;
                    //double updateFrameTime = (1.0 / targetUpdateRate * 1000.0);
                    //double drawFrameTime = (1.0 / targetFrameRate * 1000.0);

                    //float percentageOfFrame = (float)(((updateTime + updateFrameTime) - (drawTime + drawFrameTime)) / updateFrameTime);
                    //calculate percentage between frames
                    var graphicsDevice = Ultraviolet.GetGraphics();


                    var window = Ultraviolet.GetPlatform().Windows.GetPrimary();
                    
                    if (screenWidth != window.DrawableSize.Width && screenHeight != window.DrawableSize.Height)
                    {
                        screenWidth = window.DrawableSize.Width;
                        screenHeight = window.DrawableSize.Height;
                        SetViewportSize(screenWidth, screenHeight);
                    }
            
                    
                    int VirtualWidth = myGameLogic.NativeXResolution();
                    int VirtualHeight = myGameLogic.NativeYResolution();
                    int ActualWidth = graphicsDevice.GetViewport().Width;
                    int ActualHeight = graphicsDevice.GetViewport().Height;

                    RenderTarget2D drawTarget;
                    RenderBuffer2D drawBuffer;
                    float scalex;
                    float scaley;
                    
                    if (myGameLogic.CanDrawToScale() || (ActualWidth == VirtualWidth && ActualHeight == VirtualHeight))
                    {
                        scalex = 1.0f;
                        scaley = 1.0f;
                        drawTarget = RenderTarget2D.Create(ActualWidth, ActualHeight, RenderTargetUsage.PlatformContents);
                        drawBuffer = Texture2D.CreateRenderBuffer(RenderBufferFormat.Color, ActualWidth, ActualHeight);

                    }
                    else
                    {
                        drawTarget = RenderTarget2D.Create(myGameLogic.NativeXResolution(), myGameLogic.NativeYResolution(), RenderTargetUsage.PlatformContents);
                        drawBuffer = Texture2D.CreateRenderBuffer(RenderBufferFormat.Color, myGameLogic.NativeXResolution(), myGameLogic.NativeYResolution());

                        if (letterbox)
                        {
                            var scalefactor = Math.Min((float) ActualHeight / VirtualHeight,
                                (float) ActualWidth / VirtualWidth);
                            scalex = scalefactor;
                            scaley = scalefactor;
                        }
                        else
                        {
                            scalex = (float) ActualWidth / VirtualWidth;
                            scaley = (float) ActualHeight / VirtualHeight;
                        }
                    }
                    
                    drawTarget.Attach(drawBuffer);
                    
                    
                    //base.OnDrawing(gameTime);

                    Ultraviolet.GetGraphics().UnbindTexture(drawBuffer);
                    Ultraviolet.GetGraphics().SetRenderTarget(drawTarget);
                    Ultraviolet.GetGraphics().Clear(Color.Black);





                    scalebatch.Begin(SpriteSortMode.BackToFront, BlendState.NonPremultiplied,
                            SamplerState.AnisotropicWrap, null, null, effect);



                    myGame.Draw(scalebatch, updateThread, DrawMode.Interpolation);
                    scalebatch.End();
                    Ultraviolet.GetGraphics().SetRenderTarget(null);
                    Ultraviolet.GetGraphics().Clear(Color.Transparent);

                    scalebatch.Begin(SpriteSortMode.BackToFront, BlendState.NonPremultiplied,
                        SamplerState.AnisotropicWrap, null, null, effect);
                    scalebatch.Draw(drawBuffer, new RectangleF((ActualWidth-drawBuffer.Width*scalex)/2, (ActualHeight-drawBuffer.Height*scaley)/2, drawBuffer.Width*scalex, drawBuffer.Height*scaley), Color.Transparent);
                    scalebatch.End();
                    
                    SafeDispose.Dispose(drawBuffer);
                    SafeDispose.Dispose(drawTarget);

                    
                }
                catch (Exception e)
                {
                    onlineGame?.networkLoop?.endGame(true, e.ToString(), true);
                    Log.Fatal(e, "Exception Passed To Games Screens Draw Routine");
                }
                finally
                {
                    base.OnDrawingBackground(time, spriteBatch);
                    Monitor.Exit(_memoryIntegrityLock);
                }
            }
            base.OnDrawingBackground(time,spriteBatch);
        }

        private
void SetViewportSize(int x, int y)
        {
            var graphics = this.Ultraviolet.GetGraphics();

            //graphics.
            //graphics.GetRenderTarget()

            //graphics.PreferredBackBufferWidth = x;
            //graphics.PreferredBackBufferHeight = y;
            //graphics.ApplyChanges();

            if (letterbox)
            {
                int VirtualWidth = myGameLogic.NativeXResolution();
                int VirtualHeight = myGameLogic.NativeYResolution();
                //int ActualWidth = x;
                //int ActualHeight = y;

                var aspectRatio = (float)VirtualWidth / VirtualHeight;
                var width = x;
                var height = (int)(width / aspectRatio + 0.5f);

                if (height > y)
                {
                    height = y;
                    width = (int)(height * aspectRatio + 0.5f);
                }

                int viewportx = (x / 2) - (width / 2);
                int viewporty = (y / 2) - (height / 2);
                graphics.SetViewport(new Viewport(viewportx, viewporty, width, height));
            }
        }


        internal void LoadContent()
        {
            elapsedTime.Start();
            if(effect is null)
                effect = this.globalContent.Load<Effect>("Shaders/palette.frag");


            effect.Parameters["Palette"].SetValue(myGameLogic.Palette());

        }

        internal void DisposeContained()
        {
            effect?.Dispose();

        }

        internal OnlineGameLoop GetGameLoop()
        {
            return myGame;
        }
    }
}
