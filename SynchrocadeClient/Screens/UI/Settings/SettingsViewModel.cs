using Synchrocade.Content.Text.Global;
using Synchrocade.Screens.UI.Lobby;
using Synchrocade.Screens.UI.Panels;
using Ultraviolet.Core;
using Ultraviolet.Input;
using Ultraviolet.Platform;
using Ultraviolet.Presentation;
using Ultraviolet.Presentation.Controls;
using Synchrocade.Settings;
using Ultraviolet.Audio;
using Button = Ultraviolet.Presentation.Controls.Button;

namespace Synchrocade.Screens.UI.Settings
{
    public class SettingsViewModel
    {
        
        private SettingsScreen owner;
        private readonly TopBarPanel? topBar = null;
        public SettingsContainer settings;

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsViewModel"/> class.
        /// </summary>
        /// <param name="owner">The <see cref="SettingsScreen"/> that owns this view model.</param>
        public SettingsViewModel(SettingsScreen owner)
        {
            
            Contract.Require(owner, GlobalExceptionText.NullError);
            this.owner = owner;
            settings = owner.UIScreenService.GetSettings();
            //where do i hold the settings file to be accessible everywhere?
        }



        public void MusicChanged(DependencyObject dobj, RoutedEventData data)
        {
            var screenService = this.owner.UIScreenService;
            screenService.Game.SetMusicVolume((float)screenService.settings.MusicVolume);
        }

        /// <summary>
        /// Gets the <see cref="GameMenuScreen"/> that owns the view model.
        /// </summary>
        public SettingsScreen Owner
        {
            get { return owner; }
        }
        
        public void HandleViewOpened(DependencyObject dobj, RoutedEventData data)
        {
            topBar?.RegisterBackButton(backButtonPressed);
            RegisterEverything();
            //load the currently mapped buttons to display
        }

        public void HandleViewClosed(DependencyObject dobj, RoutedEventData data)
        {
            UnregisterEverything();
            settings.WriteSettings();
        }

        private void RegisterEverything()
        {
            owner.Ultraviolet.GetInput().GetKeyboard().KeyPressed += KeyPressed;
            owner.Ultraviolet.GetInput().GamePadRegistered += RegisterGameController;
            int numberOfPads = owner.Ultraviolet.GetInput().GetGamePadCount();
            for (int i = 0; i < numberOfPads; i++)
            {
                RegisterSingleController(i);
            }
        }

        private void UnregisterEverything()
        {
            owner.Ultraviolet.GetInput().GetKeyboard().KeyPressed -= KeyPressed;
            owner.Ultraviolet.GetInput().GamePadRegistered -= RegisterGameController;
            int numberOfPads = owner.Ultraviolet.GetInput().GetGamePadCount();
            for (int i = 0; i < numberOfPads; i++)
            {
                owner.Ultraviolet.GetInput().GetGamePadForPlayer(i).ButtonPressed -= PadButtonPressed;
            }
        }

        public void RegisterSingleController(int playerIndex)
        {
            owner.Ultraviolet.GetInput().GetGamePadForPlayer(playerIndex).ButtonPressed += PadButtonPressed;
        }
        
        private void RegisterGameController(GamePadDevice device, int playerIndex)
        {
            RegisterSingleController(playerIndex);
        }
        private void KeyPressed(IUltravioletWindow window, KeyboardDevice device, Key key, bool ctrl, bool alt, bool shift, bool repeat)
        {
            if (mapping)
            {                
                MappedButton button = new MappedButton(key);
                if (key == Key.Escape)
                {
                    button = new MappedButton(); //escape can be used to unbind buttons
                }
                RegisterMappedButton(button);
            }
        }
        
        private void PadButtonPressed(GamePadDevice gamePadDevice, GamePadButton button, bool repeat)
        {
            bool isButtonDown = owner.Ultraviolet.GetInput().GetGamePadForPlayer(gamePadDevice.PlayerIndex)
                .IsButtonDown(button);
            if (mapping)
            {                
                MappedButton mappedButton = new MappedButton(gamePadDevice.PlayerIndex,button);
                RegisterMappedButton(mappedButton);
            }
        }

        private void RegisterMappedButton(MappedButton button)
        {
            (int player, string splitButtonName) split = SplitButtonName(mappingButton!.Name);
            switch (split.splitButtonName)
            {
                case "btn-up":
                    settings.PlayerControlScheme[split.player].upButton = button;
                    break;
                case "btn-down":
                    settings.PlayerControlScheme[split.player].downButton = button;
                    break;
                case "btn-left":
                    settings.PlayerControlScheme[split.player].leftButton = button;
                    break;
                case "btn-right":
                    settings.PlayerControlScheme[split.player].rightButton = button;
                    break;
                case "btn-1":
                    settings.PlayerControlScheme[split.player].button1 = button;
                    break;
                case "btn-2":
                    settings.PlayerControlScheme[split.player].button2 = button;
                    break;
                case "btn-3":
                    settings.PlayerControlScheme[split.player].button3 = button;
                    break;
                case "btn-4":
                    settings.PlayerControlScheme[split.player].button4 = button;
                    break;
            }
            mappingButton = null;
            mapping = false;
        }

        public string LabelForButton(string buttonName)
        {
            (int player, string splitButtonName) split = SplitButtonName(buttonName);
            if (mapping)
            {
                if (buttonName == mappingButton!.Name)
                {
                    string direction = "";
                    switch (split.splitButtonName)
                    {
                        case "btn-up":
                            direction = "Up";
                            break;
                        case "btn-down":
                            direction = "Down";
                            break;
                        case "btn-left":
                            direction = "Left";
                            break;
                        case "btn-right":
                            direction = "Right";
                            break;
                        case "btn-1":
                            direction = "Button 1";
                            break;
                        case "btn-2":
                            direction = "Button 2";
                            break;
                        case "btn-3":
                            direction = "Button 3";
                            break;
                        case "btn-4":
                            direction = "Button 4";
                            break;
                        
                    }
                    return "Press desired input for " + direction;
                }
            }
            
            switch (split.splitButtonName)
            {
                case "btn-up":
                    return "Up:" + settings.PlayerControlScheme[split.player].upButton.Label();
                case "btn-down":
                    return "Down:" + settings.PlayerControlScheme[split.player].downButton.Label();
                case "btn-left":
                    return "Left:" + settings.PlayerControlScheme[split.player].leftButton.Label();
                case "btn-right":
                    return "Right:" + settings.PlayerControlScheme[split.player].rightButton.Label();
                case "btn-1":
                    return "Button 1:" + settings.PlayerControlScheme[split.player].button1.Label();
                case "btn-2":
                    return "Button 2:" + settings.PlayerControlScheme[split.player].button2.Label();
                case "btn-3":
                    return "Button 3:" + settings.PlayerControlScheme[split.player].button3.Label();
                case "btn-4":
                    return "Button 4:" + settings.PlayerControlScheme[split.player].button4.Label();
            }
            return "";
        }

        (int player, string buttonName) SplitButtonName(string buttonName)
        {
            int player = int.Parse(buttonName.Substring(0, 1));
            string splitLabel = buttonName.Substring(2);
            return (player, splitLabel);
        }
        private bool mapping = false;
        private Button? mappingButton = null;
        public void ClickMapButton(DependencyObject dobj, RoutedEventData data)
        {
            mapping = true;
            mappingButton = (Button) (data.Source);
        }
        
        private void backButtonPressed()
        {
            owner.Screens.Close(owner);
            //owner.Screens.CloseThenOpen(owner, owner.UIScreenService.Get<LobbyMainScreen>());
        }

    }
}