using System;
using Ultraviolet.Content;
using Ultraviolet.UI;

namespace Synchrocade.Screens.UI.Settings
{
    public class SettingsScreen : UIScreenBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsScreen"/> class.
        /// </summary>
        /// <param name="globalContent">The content manager with which to load globally-available assets.</param>
        /// <param name="uiScreenService">The screen service which created this screen.</param>
        internal SettingsScreen(ContentManager globalContent, UIScreenService uiScreenService)
            : base("Content/UI/Screens/Settings", "SettingsScreen", globalContent, uiScreenService)
        {

        }

        /// <inheritdoc/>
        protected override Object CreateViewModel(UIView view)
        {
            return new SettingsViewModel(this);
        }

        /// <inheritdoc/>
        protected override void OnOpening()
        {
            ResetViewModel();
            base.OnOpening();
        }
        
        
    }

}