using Synchrocade.Networking.Pheonix;
using Synchrocade.Content.Text.Global;
using Phoenix;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Newtonsoft.Json.Linq;
using Synchrocade.Networking;
using Synchrocade.Screens.UI.Panels;
using Synchrocade.Screens.UI.Shared;
using Ultraviolet.Core;
using Ultraviolet.Core.Collections;
using Ultraviolet.Core.Data;
using Ultraviolet.Input;
using Ultraviolet.Presentation;
using Ultraviolet.Presentation.Controls;
using Ultraviolet.Presentation.Input;
using STUN;
using STUN.Attributes;
using System.Net.Sockets;
using Socket = System.Net.Sockets.Socket;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using Serilog;
using Synchrocade.Core;

namespace Synchrocade.Screens.UI.Lobby
{
    public class LobbyGameViewModel
    {
        //public string playerJoinButtonLabel = "Join"; for some reason this isn't binding, and fuck if i wanna deal with it anymore
        public bool joinButtonEnabled = true;
        public bool startButtonEnabled = false;

        private readonly TopBarPanel? topBar = null;

        private readonly LobbyGameScreen owner;
        private readonly ChatPanel? chat = null;
        private NotificationSortedSet<String>? playerList;
        private readonly ListBox? playerListBox = null;

        private readonly Button? becomePlayerButton = null;

        private bool isPlayer = false;
        private int playerNum = 0;

        private int seed = 0;
        
        private int roomId;
        private int gameId;
        private Game game;
        
        private (string username, IPAddress ip, int port, STUNNATType natType)?[]? playerData;

        public LobbyGameViewModel(LobbyGameScreen owner, int roomId,int gameId)
        {
            Contract.Require(owner, GlobalExceptionText.NullError);
            this.roomId = roomId;
            this.gameId = gameId;

            this.game = GameList.GetGameForId(gameId);

            this.owner = owner;
        }
        
        public void HandleKeyDown(DependencyObject dobj, KeyboardDevice device, Key key, ModifierKeys modifiers, RoutedEventData data)
        {
            switch (key)
            {
                case Key.AppControlBack:
                {
                    owner.Ultraviolet.Host.Exit();
                    data.Handled = true;
                }
                    break;
            }
        }
        
        public void HandleViewOpened(DependencyObject dobj, RoutedEventData data)
        {
            PheonixClient client = owner.UIScreenService.GetPheonixClient();
            chat!.prepareForJoin(client,$"room:{roomId}");
            client.OnChat("update_player_list",UpdatePlayerList);
            client.OnChat("game_start", onGameStart);
            client.OnChat("player_data", playerDataRecieved);
            client.OnChat("phx_terminated",disconnected);


            chat!.finallizeJoin();
            playerList = new NotificationSortedSet<string>();
            playerListBox!.ItemsSource = playerList;
            topBar?.RegisterBackButton(backButtonPressed);

        }
        
        private void disconnected(Message message)
        {
            owner.Ultraviolet.QueueWorkItem(delegate(object o)
            {
                owner.UIScreenService.logout();
            });
        }

        private void backButtonPressed()
        {
            owner.Screens.CloseThenOpen(owner, owner.UIScreenService.Get<LobbyMainScreen>());
        }
        
        
        public void HandleViewClosed(DependencyObject dobj, RoutedEventData data)
        {
            chat?.prepareForClose();
            
            
        }
        
        
        public void printGameErrorToChat(string error)
        {
                chat?.printMessageToChat($"GAME ERROR:{error}");
        }

        //private bool starting = false; //this probably isn't enough of a solution
        private void playerDataRecieved(Message message)
        {
            //if (!starting)
             //   return;
            try
            {

                NetworkLoop networkLoop = owner.UIScreenService.GetNetworkLoop()!;

                int playerNumber = message.payload["player_number"]!.Value<int>();
                (string username, IPAddress ip, int port, STUNNATType natType) player;

                player.username = message.payload["user"]!.Value<string>()!;
                player.ip = IPAddress.Parse(message.payload["ip"]!.Value<string>()!);
                player.port = message.payload["port"]!.Value<int>();
                
                STUNNATType stuntype = STUNNATType.Unspecified;
                STUNNATType.TryParse(message.payload["stuntype"]!.ToString(),
                    out stuntype);

                player.natType = stuntype;
                
                PheonixClient pheonixClient = owner.UIScreenService.GetPheonixClient();

                playerData![playerNumber] = player;
                //exclusive or the names being equal and the playernumbers being equal, if one is true but not the other stuff is broken
                if (player.username.Equals(pheonixClient.UserName) ^ playerNumber == this.playerNum)
                {
                    throw new Exception("Mismatch in player number and playername.");
                }


                bool somePlayerDataMissing = false;
                foreach (var data in playerData)
                {
                    if (data == null)
                    {
                        somePlayerDataMissing = true;
                        break;
                    }
                }

                if (!somePlayerDataMissing)
                {
                    networkLoop!.StartPeered(gameId,seed, playerNum, playerData);
                }
                //starting = false;
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Exception in playerDataRecieved.");
            }
        }
    
        private void onGameStart(Message message)
        {
            //if (starting)
            //    return;
            //starting = true;
            try
            {
                int totalPlayers = message.payload["total_players"]!.Value<int>();
                playerData = new (string username, IPAddress ip, int port, STUNNATType natType)?[totalPlayers];

                NetworkLoop networkLoop = owner.UIScreenService.GetNetworkLoop()!;
                PheonixClient pheonixClient = owner.UIScreenService.GetPheonixClient();
                STUNQueryResult stun = networkLoop.getStun();
                Dictionary<string, object> stundata = new Dictionary<string, object>();
                stundata["ip"] = stun.PublicEndPoint.Address.ToString();
                stundata["port"] = stun.PublicEndPoint.Port.ToString();
                stundata["stuntype"] = stun.NATType;
                
                 seed = message.payload["seed"]!.Value<int>();

                pheonixClient.sendCustomChatCommand("propagate_player_data", stundata);
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Exception in onGameStart.");
                NetworkLoop networkLoop = owner.UIScreenService.GetNetworkLoop()!;
                networkLoop.endGame(true,"Exception in onGameStart.",false);
            }
        }
        



            
            
        
        
        private void UpdatePlayerList(Message message)
        {
            
            owner.Ultraviolet.QueueWorkItem(delegate(object o)
            {
                PheonixClient pheonixClient = owner.UIScreenService.GetPheonixClient();

                playerList!.Clear();
                List<string>? plist = message.payload["player_list"]?.ToObject<List<string>>();
                if(plist is null) return;
                bool amPlayer = false;
                int listCount = 0;
                
                foreach (string s in plist)
                {
                    playerList!.Add(s,s);
                    if (s == pheonixClient.UserName)
                    {
                        amPlayer = true;
                        playerNum = listCount;
                    }
                    listCount++;
                }


                if (amPlayer)
                {
                    isPlayer = true;
                    becomePlayerButton!.Content = "Leave";
                    joinButtonEnabled = true;
                    startButtonEnabled = (listCount >= 2) && playerNum == 0;
                }
                else
                {
                    joinButtonEnabled = !(listCount >= game.MaxPlayers);
                    isPlayer = false;
                    becomePlayerButton!.Content = "Join";
                }
            });
            
        }
        
        public void BecomePlayerButtonPressed(DependencyObject element, RoutedEventData data)
        {
            PheonixClient pheonixClient = owner.UIScreenService.GetPheonixClient();
            if (isPlayer)
            {
                startButtonEnabled = false;
                pheonixClient.sendCustomChatCommand("player_leave");
            }
            else
            {
                pheonixClient.sendCustomChatCommand("player_join");
            }
        }

        public void StartGameButtonPressed(DependencyObject element, RoutedEventData data)
        {
            PheonixClient pheonixClient = owner.UIScreenService.GetPheonixClient();
            pheonixClient.sendCustomChatCommand("invoke_game_start");
        }
    }
}