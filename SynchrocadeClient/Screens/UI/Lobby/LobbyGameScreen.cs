using Synchrocade.Screens;
using Synchrocade.Screens.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ultraviolet.Content;
using Ultraviolet.UI;

namespace Synchrocade.Screens.UI.Lobby
{
    public class LobbyGameScreen : UIScreenBase
    {

        private int roomId; //as string as that's easier to pull out of our transmitted dictionary
        private int gameId;
        internal LobbyGameScreen(ContentManager globalContent, UIScreenService uiScreenService, int roomId, int gameId)
            : base("Content/UI/Screens/Lobby", "LobbyGameScreen", globalContent, uiScreenService)
        {
            this.gameId = gameId;
            this.roomId = roomId;
        }

        /// <inheritdoc/>
        protected override Object CreateViewModel(UIView view)
        {
            return new LobbyGameViewModel(this, roomId, gameId);
        }

        /// <inheritdoc/>
        protected override void OnOpening()
        {
            ResetViewModel();
            base.OnOpening();
        }
    }
}