﻿using Synchrocade.Networking.Pheonix;
using Synchrocade.Content.Text.Global;
using Phoenix;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Newtonsoft.Json.Linq;
using Synchrocade.Core;
using Synchrocade.Networking;
using Synchrocade.Screens.UI.Panels;
using Synchrocade.Screens.UI.Shared;
using Ultraviolet.Core;
using Ultraviolet.Core.Collections;
using Ultraviolet.Core.Data;
using Ultraviolet.Input;
using Ultraviolet.Presentation;
using Ultraviolet.Presentation.Controls;
using Ultraviolet.Presentation.Input;

namespace Synchrocade.Screens.UI.Lobby
{
    
    public class LobbyMainViewModel
        { 
            private readonly LobbyMainScreen owner;
            private readonly ChatPanel? chat = null;
            private readonly TopBarPanel? topBar = null;
            public string RoomName = "";
            private readonly ListBox? gameList = null;
            private readonly ComboBox? onlineGameSelect = null;

            public void ClickCreateRoom(DependencyObject element, RoutedEventData data)
            {
                PheonixClient pheonixClient = owner.UIScreenService.GetPheonixClient();
                Game game = (Game) onlineGameSelect!.SelectedItem;
                Push push = pheonixClient.createRoom(RoomName,game.Id);
                push.Receive(Reply.Status.Error, onCreationError);
            }
            
            
            private void onCreationError(Reply reply)
            {
                string? error = reply.response["reason"]?.Value<string>();
                if(!(error is null))
                    chat?.printErrorToChat(error);

            }


            
            
#pragma warning disable CA1801, IDE0060 // Remove unused parameter
            public void HandleViewOpened(DependencyObject dobj, RoutedEventData data)
            {
                PheonixClient client = owner.UIScreenService.GetPheonixClient();
                chat!.prepareForJoin(client,"lobby:chat");
                client.OnGlobalLobby("game_created",gameCreated);
                client.OnGlobalLobby("phx_terminated",disconnected);

                gameList!.ItemsSource = client.GameList;
                client.OnJoinGame = joinGame;
                chat!.finallizeJoin();
                topBar?.RegisterBackButton(owner.UIScreenService.logout);
                onlineGameSelect!.ItemsSource = GameList.ListGamesAgainstLicenses(client.gamesList);
                onlineGameSelect!.SelectedIndex = 0;

            }

            private void disconnected(Message message)
            {
                owner.Ultraviolet.QueueWorkItem(delegate(object o)
                {
                    owner.UIScreenService.logout();
                });
            }
            
            
            private void joinGame(int roomId, int gameId)
            {
                
                owner.Ultraviolet.QueueWorkItem(delegate(object o) { owner.Screens.CloseThenOpen(owner,
                    new LobbyGameScreen(owner.globalContentManager, owner.UIScreenService, roomId, gameId));});

            }
            public void HandleViewClosed(DependencyObject dobj, RoutedEventData data)
            {
                chat?.prepareForClose();
                PheonixClient client = owner.UIScreenService.GetPheonixClient();
                client.OffGlobalLobby("game_created");
                client.OffGlobalLobby("phx_terminated");
                client.OnJoinGame = null;
            }
            
            private void gameCreated(Message message)
            {
                joinGame(message.payload["id"]!.Value<int>(),message.payload["game"]!.Value<int>());
            }
            
        /// <summary>
        /// Initializes a new instance of the <see cref="GameMenuViewModel"/> class.
        /// </summary>
        /// <param name="owner">The <see cref="GameMenuScreen"/> that owns this view model.</param>
        public LobbyMainViewModel(LobbyMainScreen owner)
        {
            Contract.Require(owner, GlobalExceptionText.NullError);

            this.owner = owner;
        }

        /// <summary>
        /// Gets the <see cref="GameMenuScreen"/> that owns the view model.
        /// </summary>
        public LobbyMainScreen Owner
        {
            get { return owner; }
        }



        /// <summary>
        /// Handles the <see cref="View.OpenedEvent"/> attached event.
        /// </summary>
        /// <param name="dobj">The object that raised the event.</param>
        /// <param name="data">The routed event metadata for this event invocation.</param>






        /// <summary>
        /// Handles the <see cref="Keyboard.KeyDownEvent"/> attached event for the view's topmost <see cref="Grid"/> instance.
        /// </summary>
        /// <param name="dobj">The object that raised the event.</param>
        /// <param name="device">The <see cref="KeyboardDevice"/> that raised the event.</param>
        /// <param name="key">The <see cref="Key"/> value that represents the key that was pressed.</param>
        /// <param name="modifiers">A <see cref="ModifierKeys"/> value indicating which of the key modifiers are currently active.</param>
        /// <param name="data">The routed event metadata for this event invocation.</param>
        public void HandleKeyDown(DependencyObject dobj, KeyboardDevice device, Key key, ModifierKeys modifiers, RoutedEventData data)
        {
            switch (key)
            {
                case Key.AppControlBack:
                    {
                        owner.Ultraviolet.Host.Exit();
                        data.Handled = true;
                    }
                    break;
            }
        }


        }
        

}




