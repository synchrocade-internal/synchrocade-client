﻿using Synchrocade.Screens;
using Synchrocade.Screens.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ultraviolet.Content;
using Ultraviolet.UI;

namespace Synchrocade.Screens.UI.Lobby
{
    public class LobbyMainScreen : UIScreenBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameMenuScreen"/> class.
        /// </summary>
        /// <param name="globalContent">The content manager with which to load globally-available assets.</param>
        /// <param name="uiScreenService">The screen service which created this screen.</param>
        internal LobbyMainScreen(ContentManager globalContent, UIScreenService uiScreenService)
            : base("Content/UI/Screens/Lobby", "LobbyMainScreen", globalContent, uiScreenService)
        {

        }

        /// <inheritdoc/>
        protected override Object CreateViewModel(UIView view)
        {
            return new LobbyMainViewModel(this);
        }

        /// <inheritdoc/>
        protected override void OnOpening()
        {
            ResetViewModel();
            base.OnOpening();
        }


    }
}
