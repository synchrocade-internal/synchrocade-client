using System;
using System.Collections;
using System.Collections.Generic;
using Ultraviolet.Core.Collections;

namespace Synchrocade.Screens.UI.Shared
{
    public class NotificationSortedSet<T> :  IEnumerable, INotifyCollectionChanged<Int32, T>
    {
        
        private readonly Object untypedEventSyncObject = new Object();
        private SortedList<string,T> set = new SortedList<string,T>();
        public void Add(string key,T thing)
        {
            if (!set.ContainsKey(key))
            {
                set.Add(key, thing);
                OnCollectionItemAdded(set.IndexOfKey(key), thing);
            }
        }
        public void Remove(string key)
        {

            if (set.ContainsKey(key))
            {
                OnCollectionItemRemoved(set.IndexOfKey(key),set[key]);
                set.Remove(key);
            }
        }

        public bool update(string key, T newvalue)
        {
            if (set.ContainsKey(key))
            {
                set["key"] = newvalue;
                return true;
            }
            else return false;
        }
        public void Clear()
        {
            set.Clear();
            OnCollectionReset();
        }
        

        public IEnumerator GetEnumerator()
        {
            return set.Values.GetEnumerator();
        }

        public event CollectionResetEventHandler<Int32, T>? CollectionReset;

        /// <inheritdoc/>
        public event CollectionItemAddedEventHandler<Int32, T>? CollectionItemAdded;

        /// <inheritdoc/>
        public event CollectionItemRemovedEventHandler<Int32, T>? CollectionItemRemoved;
        private CollectionResetEventHandler? untypedCollectionReset;
        private CollectionItemAddedEventHandler? untypedCollectionItemAdded;
        private CollectionItemRemovedEventHandler? untypedCollectionItemRemoved;
        event CollectionResetEventHandler INotifyCollectionChanged.CollectionReset
        {
            add { lock (untypedEventSyncObject) { untypedCollectionReset += value; } }
            remove { lock (untypedEventSyncObject) { untypedCollectionReset -= value; } }
        }

        /// <inheritdoc/>
        event CollectionItemAddedEventHandler INotifyCollectionChanged.CollectionItemAdded
        {
            add { lock (untypedEventSyncObject) { untypedCollectionItemAdded += value; } }
            remove { lock (untypedEventSyncObject) { untypedCollectionItemAdded -= value; } }
        }

        /// <inheritdoc/>
        event CollectionItemRemovedEventHandler INotifyCollectionChanged.CollectionItemRemoved
        {
            add { lock (untypedEventSyncObject) { untypedCollectionItemRemoved += value; } }
            remove { lock (untypedEventSyncObject) { untypedCollectionItemRemoved -= value; } }
        }
        protected virtual void OnCollectionReset()
        {
            CollectionReset?.Invoke(this);

            //if (suppressUntypedNotifications)
            //    return;

            untypedCollectionReset?.Invoke(this);
        }
        protected virtual void OnCollectionItemAdded(Int32 index, T value)
        {
            CollectionItemAdded?.Invoke(this, index, value);

            //if (suppressUntypedNotifications)
            //    return;

            untypedCollectionItemAdded?.Invoke(this, index, value);
        }

        /// <summary>
        /// Raises the <see cref="CollectionItemRemoved"/> event.
        /// </summary>
        /// <param name="index">The index at which the item was removed from the list.</param>
        /// <param name="value">The item that was added to the list.</param>
        protected virtual void OnCollectionItemRemoved(Int32 index, T value)
        {
            CollectionItemRemoved?.Invoke(this, index, value);

            //if (suppressUntypedNotifications)
            //    return;

            untypedCollectionItemRemoved?.Invoke(this, index, value);
        }
        
    }

}