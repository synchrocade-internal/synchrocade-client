using System;
using Microsoft.VisualBasic.CompilerServices;
using Synchrocade.Core;
using Synchrocade.GameObjects.Input;
using Ultraviolet.Presentation;
using Ultraviolet.Presentation.Controls;

namespace Synchrocade.Screens.UI.Shared
{
    public class GameDetails : IComparable 
    {
        public int id { get; set; }
        public int game { get; set; }

        public string name { get; set; }
        private Action<int, int> onJoin;
        public Button? joinButton;
        
        public void ClickJoinRoom(DependencyObject dobj, RoutedEventData data)
        {
            onJoin(id, game);
        }
        
        public GameDetails(int id, int game,string name, Action<int,int> onJoin)
        {
            this.id = id;
            this.game = game;
            this.name = name;
            this.onJoin = onJoin;
        }

        public string GetGameName()
        {
            return GameList.GetGameForId(game).Name;
        }



        public int CompareTo(object? obj)
        {
            GameDetails? otherchatname = obj as GameDetails;
            if (!(otherchatname is null))
                return string.Compare(this.name, otherchatname.name, StringComparison.Ordinal);
            else
                throw new ArgumentException("Object is not a Name");
        }
    }
}