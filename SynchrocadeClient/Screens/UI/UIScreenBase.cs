﻿using System;
using Ultraviolet.Core;
using Ultraviolet.Content;
using Ultraviolet.UI;
using Synchrocade.Content.Text.Global;
using Ultraviolet;
using Ultraviolet.Graphics.Graphics2D;

namespace Synchrocade.Screens.UI
{
    /// <summary>
    /// Represents the base class for screens in this application.
    /// </summary>
    
    public abstract class UIScreenBase : UIScreen
    {
        private static UltravioletFont? drawFont;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameScreenBase"/> class.
        /// </summary>
        /// <param name="rootDirectory">The root directory of the panel's local content manager.</param>
        /// <param name="definitionAsset">The asset path of the screen's definition file.</param>
        /// <param name="globalContent">The content manager with which to load globally-available assets.</param>
        /// <param name="uiScreenService">The screen service which created this screen.</param>
        internal UIScreenBase(String rootDirectory, String definitionAsset, ContentManager globalContent, UIScreenService uiScreenService)
            : base(rootDirectory, definitionAsset, globalContent)
        {
            Contract.Require(uiScreenService, GlobalExceptionText.NullError);
            this.globalContentManager = globalContent;
            
            drawFont ??= globalContent.Load<UltravioletFont>("Fonts\\Aileron-Regular");
            
            this.uiScreenService = uiScreenService;
        }

        /// <summary>
        /// Gets the screen service which created this screen.
        /// </summary>
        internal UIScreenService UIScreenService
        {
            get { return uiScreenService; }
        }

        protected override void OnDrawingForeground(UltravioletTime time, SpriteBatch spriteBatch)
        {
            string? musicString = this.uiScreenService.Game.musicString;
            if (musicString != null)
            {
                int width = Ultraviolet.GetGraphics().GetViewport().Width;
                int height = Ultraviolet.GetGraphics().GetViewport().Height;

                Size2 musicSize = drawFont!.GetFace(UltravioletFontStyle.Regular).MeasureString(musicString);
                spriteBatch.DrawString(drawFont,musicString , new Ultraviolet.Vector2(width - musicSize.Width -4,height - musicSize.Height -4), new Color(174, 212, 219));

            }


            base.OnDrawingForeground(time,spriteBatch);
        }

        // Property values.
        private readonly UIScreenService uiScreenService;
        
        internal ContentManager globalContentManager;

    }
}
