﻿using Synchrocade.GameLogics;
using Synchrocade.Networking;
using Synchrocade.Content.Text.Global;
using Synchrocade.Screens.UI.Lobby;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synchrocade.Core;
using Synchrocade.ReusableFunctions;
using Synchrocade.Screens.UI.Settings;
using Ultraviolet;
using Ultraviolet.Core;
using Ultraviolet.Input;
using Ultraviolet.Presentation;
using Ultraviolet.Presentation.Controls;
using Ultraviolet.Presentation.Input;

namespace Synchrocade.Screens.UI
{
    public class LanConnectionViewModel
    {
        private int _port = 9017;
        private String _portString = "9017";
        public String Port
        {
            get => _portString;
            set {
                _port = Int32.TryParse(value, out int i) ? i : _port;
                _portString = $"{_port}";
            }
        }

        public double Players = 2;
        public double MinPlayers = 2;
        public double MaxPlayers = 2;
        public string Hostname { get; set; } = "localhost";

        public string Username { get; set; } = "";
        //public string Password { get; set; } = "";
        
        private readonly PasswordBox? passwordBox = null;

        public string ErrorText { get; set; } = "";


        // Property values.
        private readonly LanConnectionScreen owner;

        // Component references.
        private readonly Grid? container = null;
        private readonly ComboBox? localGameSelect = null;
        private readonly ComboBox? lanGameSelect = null;
        
        
        
        /// <summary>
        /// Initializes a new instance of the <see cref="GameMenuViewModel"/> class.
        /// </summary>
        /// <param name="owner">The <see cref="GameMenuScreen"/> that owns this view model.</param>
        public LanConnectionViewModel(LanConnectionScreen owner)
        {
            Contract.Require(owner, GlobalExceptionText.NullError);
            
            this.owner = owner;
        }


        /// <summary>
        /// Handles the <see cref="View.OpenedEvent"/> attached event.
        /// </summary>
        /// <param name="dobj">The object that raised the event.</param>
        /// <param name="data">The routed event metadata for this event invocation.</param>
#pragma warning disable CA1801, IDE0060 // Remove unused parameter

        private List<Game>? gamesList;
        public void HandleViewOpened(DependencyObject dobj, RoutedEventData data)
        {
            passwordBox?.SetPassword("");
            container?.Focus();
            gamesList = GameList.ListGames(false, false);
            
            localGameSelect!.ItemsSource = gamesList;
            localGameSelect!.SelectedIndex = 0;
            MinPlayers = gamesList[0].SinglePlayer ? 1 : 2;
            MaxPlayers = gamesList[0].MaxPlayers;

            
            lanGameSelect!.ItemsSource = gamesList;
            lanGameSelect!.SelectedIndex = 0;
        }
        
        public void LocalGameChanged(DependencyObject element, RoutedEventData data)
        {
            if (localGameSelect?.SelectedIndex >= 0 && localGameSelect?.SelectedIndex < gamesList?.Count)
            {
                MinPlayers = gamesList![localGameSelect!.SelectedIndex].SinglePlayer ? 1 : 2;
                MaxPlayers = gamesList![localGameSelect!.SelectedIndex].MaxPlayers;
            }
        }
        

        /// <summary>
        /// Handles the Click event for the "Start" button.
        /// </summary>
        public void ClickStart(DependencyObject element, RoutedEventData data)
        {
            NetworkLoop networkLoop = owner.UIScreenService.GetNetworkLoop()!;
            int gameId = ((Game) localGameSelect!.SelectedItem).Id;
            networkLoop.StartLocal(gameId,(int)Players);
        }

        public void ClickHost(DependencyObject element, RoutedEventData data)
        {
            NetworkLoop networkLoop = owner.UIScreenService.GetNetworkLoop()!;
            int gameId = ((Game) lanGameSelect!.SelectedItem).Id;
            networkLoop.StartServer(_port,gameId);
        }
        public void ClickJoin(DependencyObject element, RoutedEventData data)
        {
            NetworkLoop networkLoop = owner.UIScreenService.GetNetworkLoop()!;
            networkLoop.JoinServer(Hostname,_port);
        }

        public void ClickLobby(DependencyObject element, RoutedEventData data)
        {
            ErrorText = "";
            PheonixClient client = owner.UIScreenService.GetPheonixClient();
            (int id, string text)? error = client.login(Username, passwordBox!.GetPassword());
            if (error is null)
            {
                owner.Screens.CloseThenOpen(owner, owner.UIScreenService.Get<LobbyMainScreen>());
            }
            else
            {
                switch(error.Value.id)
                {
                    case 5002:
                        ErrorText = "|link:https://www.synchrocade.com/users/confirm|Please confirm your email address.\nIf you need to resend the email confirmation click here.|link|";
                        break;
                    case 5003:
                        ErrorText = "|link:https://www.synchrocade.com/games/register|You must have access to at least one game to login.\nIf you need to register a key click here.|link|";
                        break;
                    default:
                        ErrorText = error.Value.text;
                        break;
                }
                
            }
            //owner.Screens.Open(owner.UIScreenService.Get<LobbyMainViewScreen>());
            //owner.Screens.Open();
        }


        public void LoginKeyPressed(DependencyObject dobj, KeyboardDevice device, Key key, ModifierKeys modifiers, RoutedEventData data)
        {
            if(key == Key.Return||key == Key.Return2||key == Key.KeypadEnter)
                ClickLobby(dobj, data);
        }

        public void ClickOptions(DependencyObject element, RoutedEventData data)
        {
            owner.Screens.OpenAbove(owner.UIScreenService.Get<SettingsScreen>(),owner);
        }
        
        /// <summary>
        /// Handles the Click event for the "Exit" button.
        /// </summary>
        public void ClickExit(DependencyObject element, RoutedEventData data)
        {
            owner.Ultraviolet.Host.Exit();
        }
#pragma warning disable CA1801, IDE0060 // Remove unused parameter


        /// <summary>
        /// Gets the <see cref="GameMenuScreen"/> that owns the view model.
        /// </summary>
        public LanConnectionScreen Owner
        {
            get { return owner; }
        }





        
        
        
        
        
        
        // link handling functions
        public Color LinkColorizer(String target, Boolean visited, Boolean hovering, Boolean active, Color currentColor)
        {
            if (hovering)
                return new Color(100, 172, 187);
            
            return new Color(174, 212, 219);
        }

        public Boolean LinkClickHandler(String target)
        {
            return LinkHandler.ExecuteLink(target);
        }

        /*public Boolean LinkStateEvaluator(String target)
        {
            return false;
        }*/
    }
}
