﻿using System;
using Ultraviolet.Content;
using Ultraviolet.UI;

namespace Synchrocade.Screens.UI
{
    public class LanConnectionScreen : UIScreenBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameMenuScreen"/> class.
        /// </summary>
        /// <param name="globalContent">The content manager with which to load globally-available assets.</param>
        /// <param name="uiScreenService">The screen service which created this screen.</param>
        internal LanConnectionScreen(ContentManager globalContent, UIScreenService uiScreenService)
            : base("Content/UI/Screens/LanConnectionScreen", "LanConnectionScreen", globalContent, uiScreenService)
        {

        }

        /// <inheritdoc/>
        protected override Object CreateViewModel(UIView view)
        {
            return new LanConnectionViewModel(this);
        }

        /// <inheritdoc/>
        protected override void OnOpening()
        {
            ResetViewModel();
            base.OnOpening();
        }
    }

}
