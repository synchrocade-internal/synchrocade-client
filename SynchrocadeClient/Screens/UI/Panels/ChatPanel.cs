using System;

using Newtonsoft.Json.Linq;
using Phoenix;
using Synchrocade.Networking;
using Synchrocade.Screens.UI.Lobby;
using Synchrocade.Screens.UI.Shared;
using Ultraviolet;
using Ultraviolet.Input;
using Ultraviolet.Presentation;
using Ultraviolet.Presentation.Controls;
using Ultraviolet.Presentation.Input;



namespace Synchrocade.Screens.UI.Panels
{
    public class ChatName : IComparable 
    {
        public string name
        { get; set; }
        
        public ChatName(String name)
        {
            this.name = name;
        }


        public int CompareTo(object? obj)
        {
            ChatName? otherchatname = obj as ChatName;
            if (!(otherchatname is null))
                return String.Compare(this.name, otherchatname.name, StringComparison.Ordinal);
            else
                throw new ArgumentException("Object is not a Name");
        }
    }
    
    [UvmlKnownType(null,"Synchrocade.Content.UI.Panels.ChatPanel.xml")]
    public class ChatPanel : ContentControl
    {
        private readonly UltravioletContext uv;
        private PheonixClient? pheonixClient;
        public ChatPanel(UltravioletContext uv, string name) : base(uv, name)
        {
            this.uv = uv;
        }
        

            public void prepareForJoin(PheonixClient pheonixClient, string roomTag)
        {
            this.pheonixClient = pheonixClient;
            chatUsers = new NotificationSortedSet<ChatName>();
            //client.joinChatChanel("room:1492");
            pheonixClient.prepareChatChannelForJoin(roomTag,chatJoins,chatLeaves);
            pheonixClient.OnChat("shout",recieveChat);
            pheonixClient.OnChat("phx_error",printErrorToChat);
            pheonixClient.OnGlobalLobby("phx_error",printErrorToChat);
            pheonixClient.OnChat("join_fail",printErrorToChat);


            connectedChatUsers!.ItemsSource = chatUsers;
            chatTextBox?.SetText("");
        }

            public void finallizeJoin()
            {
                pheonixClient!.finallizeChatJoin();
            }


            private void addChatText(string text)
            {

                string postText = text;
                if (chatTextBox?.Text.Length != 0)
                {
                    postText = "\n" + text;
                }
                bool scrolledToBottom = chatTextBox?.VerticalOffset > chatTextBox?.ExtentHeight - chatTextBox?.ViewportHeight - 50;
                chatTextBox?.AppendText(postText);

                uv.QueueWorkItem(delegate(object o)
                {
                    if (scrolledToBottom)
                    {
                        chatTextBox?.ScrollToEnd();
                    }
                });
            }
            
            private void joinFail(Message message)
            {
                addChatText($"JOIN FAILED:{message.payload["message"]}");
            }
            private void printErrorToChat(Message message)
            {
                printErrorToChat(message.payload.ToString());
            }
            
            public void printErrorToChat(string message)
            {
                addChatText($"SERVER ERROR:{message}");
            }
            
            public void printMessageToChat(string message)
            {
                addChatText(message);
            }
            

            public void prepareForClose()
            {
                pheonixClient?.OffChat("shout");
                pheonixClient?.OffChat("phx_error");
                pheonixClient?.OffGlobalLobby("phx_error");
                pheonixClient?.OffChat("join_fail");

                pheonixClient?.leaveChatChannel();
            }
        
        private void chatJoins(string key, JToken arg1, JToken arg2)
        {
            uv.QueueWorkItem(delegate(object o) { chatUsers!.Add(key,new ChatName(key)); });

        }
        private void chatLeaves(string key, JToken arg1, JToken arg2)
        {
            uv.QueueWorkItem(delegate(object o) { chatUsers!.Remove(key); });
        }
        
        
        private Channel? PheonixChannel = null;

        private void recieveChat(Message message)
        {
            addChatText($"{message.payload["name"]}: {message.payload["message"]}");
        }

        public void ChatInputKeyDown(DependencyObject dobj, KeyboardDevice device, Key key, ModifierKeys modifiers, RoutedEventData data)
        {
            if((key==Key.Return||key==Key.Return2)&&ChatInput.Length>0)
            {
                pheonixClient!.sendChatMessage(ChatInput);
                ChatInput = "";
            }
        }




        private NotificationSortedSet<ChatName>? chatUsers;

        //public string ChatText = "";
        public string ChatInput = "";

        public void HandleViewClosed(DependencyObject dobj, RoutedEventData data)
        {
            chatTextEntryBox?.Focus();
        }
        //private readonly Grid? container = null;
        private readonly TextBox? chatTextBox = null;
        private readonly TextBox? chatTextEntryBox = null;
        private readonly ListBox? connectedChatUsers = null;
    }
}