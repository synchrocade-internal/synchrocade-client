using System;
using Ultraviolet;
using Ultraviolet.Presentation;
using Ultraviolet.Presentation.Controls;

namespace Synchrocade.Screens.UI.Panels
{
    [UvmlKnownType(null,"Synchrocade.Content.UI.Panels.TopBarPanel.xml")]
    public class TopBarPanel : ContentControl
    {

        private Action? backButtonAction = null; 
        public TopBarPanel(UltravioletContext uv, string name) : base(uv, name)
        {
            
        }

        public void RegisterBackButton(Action result)
        {
            backButtonAction = result;
        }

        public void BackButtonPressed(DependencyObject element, RoutedEventData data)
        {
            backButtonAction?.Invoke();
        }
    }
}