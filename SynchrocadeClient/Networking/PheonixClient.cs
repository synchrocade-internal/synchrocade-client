using System;
using System.Collections.Generic;
using System.Net;
using System.Security;
using Newtonsoft.Json;
using System.Threading;
using Microsoft.VisualBasic.CompilerServices;
using Newtonsoft.Json.Linq;
using Synchrocade.Networking.Pheonix;
using Phoenix;
using Synchrocade.Screens;
using Synchrocade.Screens.UI.Panels;
using Synchrocade.Screens.UI.Shared;
using Ultraviolet;

namespace Synchrocade.Networking
{
    public class PheonixClient
    {
        //i need my socket and my websocket
            private Socket? socket;
        //i need a channel for the lobby system (always on and always managed, sending the updates through a game is going to be less resources than sending the entire lobby)

        private Channel? globalLobby;
        private Channel? chat;
        public NotificationSortedSet<GameDetails>? GameList { get; set; }

        private UltravioletContext context; //i use the context to marshal handling of the lobby object to the main thread as it'll be directly drawn
            //i need a channel for exactly one chat and only ever one chat, I'll send updates on this chat for in game lobbies
            
        private readonly bool ignorebadssl;
        private readonly string address;
        private readonly int port;

        public PheonixClient(UltravioletContext context, bool ignorebadssl, string address, int port)
        {
            this.context = context;
            this.ignorebadssl = ignorebadssl;
            this.address = address;
            this.port = port;
        }

        
        
        public string? UserName { get => _userName; }
        private string? _userName;
        private JObject? chatState;
        private JObject? lobbyState;
        public List<int> gamesList = new List<int>();
        public (int id, string text)? login(string user, string pass)
        {
            if (ignorebadssl)
            {
                ServicePointManager
                        .ServerCertificateValidationCallback += 
                    (sender, cert, chain, sslPolicyErrors) => true;
            }
            string protocol = "https://";

            
            string url = protocol+address+":"+port+"/api/sign_in";
            WebClient client = new WebClient();
            client.Headers[HttpRequestHeader.ContentType] = "application/json";
            Dictionary<string, string> loginData = new Dictionary<string, string>
            {
                { "user", user },
                { "pass", pass.ToString() },
                { "version",Synchrocade.Core.Version.version }
            };
            string reqString = JsonConvert.SerializeObject(loginData);
            Dictionary<string, string> responseDict;
            try
            {
                string response = client.UploadString(url,reqString);
                responseDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(response)!;

            }
            catch (Exception exception)
            {
                return (-1,"No response from server.");
            }


            if (!responseDict.ContainsKey("token"))
            {
                if (responseDict.ContainsKey("error_text") && responseDict.ContainsKey("error_id"))
                    return (Convert.ToInt32(responseDict["error_id"]), responseDict["error_text"]);
                else
                    return (-1,"Invalid response from server.");
            }
            
            string token = responseDict["token"];


            _userName = user;
            var socketFactory = new WebsocketSharpFactory(ignorebadssl);
            socket = new Socket(socketFactory);
            Dictionary<string, string> socketParams = new Dictionary<string, string>
            {
                { "token", token }
            };
            
            protocol = "wss://";

            url = protocol+address+":"+port+"/socket";
            socket.Connect(url,socketParams);
            globalLobby = socket!.MakeChannel("lobby:lobby");
            OnGlobalLobby("playable_games",HandleLicenseData);
            lobbyState = new JObject();
            RegisterLobbyPresenceObesrvers();
            globalLobby.Join();

            if (socket.state != Socket.State.Open)
            {
                return (-1,"Cannot connect to server.");
            }
            return null;
            
        }


        public void logout()
        {
            globalLobby?.Leave();
            chat?.Leave(); 
            _userName = null;
            chatState = null;
            lobbyState = null;
            socket?.Disconnect();
            socket = null;
            globalLobby = null;
            chat = null;
            GameList = null;
            chatJoins = null;
            chatLeaves = null;
            OnJoinGame = null;
        }


        private void HandleLicenseData(Message message)
        {
            JToken? licenses = message.payload["playable_games"];
            if (licenses is null)
            {
                throw new Exception("Server did not return valid subscription data.");
            }
            gamesList = new List<int>();
            
            foreach (JToken j in message.payload["playable_games"]!.Children())
            {
                int? value = j["id"]?.Value<int>();
                if (value is null)
                {
                    throw new Exception("Server did not return valid license data.");
                }
                else
                {
                    gamesList.Add(value.Value);
                }
                
            }
        }

        //this is split into two sections because I need to give a chance for custom things to register that might fire off directly on join
        public void prepareChatChannelForJoin(string topic, Action<string, JToken, JToken>? chatJoins = null, Action<string, JToken, JToken>? chatLeaves = null)
        {
            this.chatJoins = chatJoins;
            this.chatLeaves = chatLeaves;

            chat = socket!.MakeChannel(topic);
            chatState = new JObject();
            RegisterChatPresenceObesrvers();
            OnChat("license_data",HandleLicenseData);

        }
        
        public void finallizeChatJoin()
        {
            chat!.Join();
        }
        

        public void leaveChatChannel()
        {
            chat?.Leave();
            chat = null;
        }
        
        public Push createRoom(string roomName, int gameId)
        {
            if (roomName.Length > 255)
            {
                throw new ArgumentOutOfRangeException("Chat length exceeded");
            }
            var senddict = new Dictionary<string, object>
            {
                { "room_name", roomName },
                // ReSharper disable once HeapView.BoxingAllocation
                { "game_id", gameId}
            };
            return globalLobby!.Push("create", senddict);

        }


        private Action<string, JToken, JToken>? chatJoins;
        private Action<string, JToken, JToken>? chatLeaves;
        public Action<int, int>? OnJoinGame { get; set; }




        //these two likely should be combined 
        public void OnChat(string messageType, Action<Message> callback)
        {
            chat!.On(messageType, callback);
        }

        public void OffChat(string messageType)
        {
            chat?.Off(messageType);

        }
        public void OnGlobalLobby(string messageType, Action<Message> callback)
        {
            globalLobby!.On(messageType, callback);
        }
        public void OffGlobalLobby(string messageType)
        {
            globalLobby?.Off(messageType);
        }
        
        

        private void RegisterChatPresenceObesrvers()
        {
            chat!.On("presence_state",chatPresenceState);
            chat!.On("presence_diff",chatPresenceDiff);
        }
        private void RegisterLobbyPresenceObesrvers()
        {
            GameList = new NotificationSortedSet<GameDetails>();
            globalLobby!.On("presence_state",lobbyPresenceState);
            globalLobby!.On("presence_diff",lobbyPresenceDiff);
        }


        private void chatPresenceState(Message message)
        {
            chatState = Presence.syncState(chatState, message.payload,chatJoins,chatLeaves);
        }
        private void chatPresenceDiff(Message message)
        {
            chatState = Presence.syncDiff(chatState, message.payload,chatJoins,chatLeaves);
        }
        private void lobbyPresenceState(Message message)
        {
            lobbyState = Presence.syncState(lobbyState, message.payload,lobbyJoins,lobbyLeaves);
        }
        private void lobbyPresenceDiff(Message message)
        {
            lobbyState = Presence.syncDiff(lobbyState, message.payload,lobbyJoins,lobbyLeaves);
            System.Console.WriteLine(lobbyState);
        }

        private void onJoin(int roomId, int gameId)
        {
            OnJoinGame?.Invoke(roomId,gameId);
        }
        private void lobbyJoins(string key, JToken arg1, JToken details)
        {
            context.QueueWorkItem(delegate(object o)
            {
                //we're not storing anything other than the name right now so no need to updat, add will just fail because its a set
                GameList!.Add(key,new GameDetails(details["id"]!.Value<int>(),details["game"]!.Value<int>(),details["name"]!.ToString(), onJoin));
            });
        }
        private void lobbyLeaves(string key, JToken arg1, JToken details)
        {
            context.QueueWorkItem(delegate(object o)
            {
                if(!arg1["metas"]!.HasValues)
                    GameList!.Remove(key);
            });
        }

        
        public void sendChatMessage(string message)
        {
            if (message.Length > 255)
            {
                throw new ArgumentOutOfRangeException("Chat length exceeded");
            }
            var senddict = new Dictionary<string, object>
            {
                { "message", message }
            };
            chat!.Push("shout", senddict);
        }

        public void sendCustomChatCommand(string @event, Dictionary<string, object>? payload = null)
        {
            chat!.Push(@event, payload);
        }
    }
}