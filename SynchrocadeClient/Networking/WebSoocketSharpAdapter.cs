﻿#nullable disable
#pragma warning disable CA1062 // Validate arguments of public methods

using Phoenix;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;
using WebSocketSharper;

//taken wholesale from pheonix 

namespace Synchrocade.Networking.Pheonix
{
	public sealed class WebsocketSharpAdapter : IWebsocket
	{

		private readonly WebSocket ws;
		private readonly WebsocketConfiguration config;


		public WebsocketSharpAdapter(WebSocket ws, WebsocketConfiguration config)
		{

			this.ws = ws;
			this.config = config;

			ws.OnOpen += OnWebsocketOpen;
			ws.OnClose += OnWebsocketClose;
			ws.OnError += OnWebsocketError;
			ws.OnMessage += OnWebsocketMessage;
		}


		#region IWebsocket methods

		public void Connect()
		{
			ws.Connect();
		}

		public void Send(string message)
		{
			ws.SendAsync(message,null);
		}

		public void Close(ushort? code = null, string message = null)
		{
			ws.Close();
		}

		#endregion


		#region websocketsharp callbacks

		public void OnWebsocketOpen(object sender, EventArgs args)
		{
			config.onOpenCallback(this);
		}

		public void OnWebsocketClose(object sender, CloseEventArgs args)
		{
			config.onCloseCallback(this, args.Code, args.Reason);
		}

		public void OnWebsocketError(object sender, ErrorEventArgs args)
		{
			config.onErrorCallback(this, args.Message);
		}

		public void OnWebsocketMessage(object sender, MessageEventArgs args)
		{
			config.onMessageCallback(this, args.Data);
		}

		#endregion
	}

	public sealed class WebsocketSharpFactory : IWebsocketFactory
	{

		private bool ignoreBadSSL = false;

		public WebsocketSharpFactory(bool ignoreBadSSL)
		{
			this.ignoreBadSSL = ignoreBadSSL;
		}
		public IWebsocket Build(WebsocketConfiguration config)
		{

			var socket = new WebSocket(NullLogger<string>.Instance,config.uri.AbsoluteUri,false);
			
			//set up our tls protocols
			socket.SslConfiguration.EnabledSslProtocols = SslProtocols.Tls12 & SslProtocols.Tls13;

			
			if(ignoreBadSSL)
				socket.SslConfiguration.ServerCertificateValidationCallback = delegate { return true; };

				
			return new WebsocketSharpAdapter(socket, config);
		}
	}
}
#pragma warning restore CA1062 // Validate arguments of public methods
