
//changes i need to make
//this network loop needs to be able to fire VIA the update thread even before a game is loaded
//all in game (non chat) communication needed to fire up the game can happen here, the lobby SHOULD be responsible for generating seeds and saying which player is which but i don't have that yet, and we need to support lan anyway
//i should add some handshake process

//handshake
//for now let's just do 2 players, host is player 1, connector is player 2
//start method is called by host
//more players will require a mesh, which is a bit more complicated to set up, let's get 2 player online functioning first


using LiteNetLib;
using LiteNetLib.Utils;
using Synchrocade.GameObjects.Input;
using Synchrocade.ReusableFunctions;
using Synchrocade.Content.Text.Global;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using STUN;
using STUN.Attributes;
using static Synchrocade.GameObjects.Input.InputObject;

namespace Synchrocade.Networking
{
    
    enum MessageType : ushort
    {
        Ping,
        Pong,
        SendGameInfo,
        PlayerNumber,
        GameStartedOnThisEnd,
        CommenceGameInXMS,
        InputDesynchronizationCheck,
        GameStateDesynchronizationCheck,
        GameQuit,
        GameErrored,
        PunchPacket
    }

    internal class NetworkLoop
    {

        private bool peered;
        private int players;
        private int myPlayerNumber;
        private List<int>? opponentPlayerNumber;
        private EventBasedNetListener listener;
        private NetManager connection;
        private readonly NetDataWriter writer = new NetDataWriter();
        private long networkFrame = 0;
        private Action<int, string[], int, int> onConnection;
        private Action onGameStart;
        private Action<bool,string> onGameEnd;
        private Func<int, bool, ulong> onDesynchronizationCheck;
        private readonly UpdateThread.UpdateThread updateThread;
        private readonly double NativeUPS;
        private const int latencyDifferenceCheckRate = 30;
        private const int desynchronizationCheckRate = 120;
        private int gameId;

        //private STUNNATType[]? natTypes;
        // i still need this because i have to get my nat information before the array is created
        private STUNNATType myNatType = STUNNATType.Unspecified; 

        private int[]? startingPing;
        private InputList[]? inputLists;
        private EncodeDecodeOptions inputOptions;
        private long[]? opponentsLastVerifiedFrame; 
        
        private long frameDifference = 0; //the number that if added to our network frames, will give us our game frame
        private const int frameHeaderSize = 32768;


        private (string username, IPAddress ip, int port, STUNNATType natType)?[]? playerData;



        internal bool localOnly = true;
        private bool[]? connected; 

        private bool[]? gameStarted;
        private bool updatingFrameCounter = false;
        private bool isServer = false;


        private bool inCountdownToGameStart = false;
        private bool[]? waitingToConnect;
        private ushort lengthOfCountdownInMS;
        Stopwatch? connectionTimer;
        Stopwatch? countdownTimer;
        Stopwatch? pingTimer;

        private NetPeer?[]? peer;

        private long lastLatencyCheckFrame = 120;//give a small period of time before the game starts
        private int lastGamestateDesynchronizationCheckFrame = desynchronizationCheckRate / 2; // i'm going to stage this off, so i desynch check and input desynch check every other timeperiod to lower the chance of a single frame being long running

        private long[]? inputDesynchronizationNumber;
        //private long myInputDesynchronizationNumber = long.MinValue;
        //private long opponentsInputDesynchronizationNumber = long.MinValue;
        
        private ulong[]? gameStateDesynchronizationNumber;
        //private ulong myGamestateDesynchronizationNumber = ulong.MaxValue;
        //private ulong opponentsGamestateDesynchronizationNumber = ulong.MaxValue;
        private IPEndPoint? myEndPoint = null;

        private int LastInputDesynchronizationCheckFrame;

        private int randomSeed;

        internal NetworkLoop(Action<int, string[], int, int> onConnection, Action onGameStart, Action<bool, String> onGameEnd, Func<int, bool, ulong> onDesynchronizationCheck, UpdateThread.UpdateThread updateThread)
        {
            randomSeed = new System.Random().Next();
            this.onDesynchronizationCheck = onDesynchronizationCheck;
            this.onGameEnd = onGameEnd;
            this.updateThread = updateThread;
            NativeUPS = updateThread.GetUPS();
            this.onGameStart = onGameStart;
            this.onConnection = onConnection;
            listener = new EventBasedNetListener();

            connection = new NetManager(listener)
            {
                UpdateTime = 1
            };
            listener.NetworkReceiveEvent += (fromPeer, dataReader, deliveryMethod) => HandleRecieve(fromPeer, dataReader, deliveryMethod);

            listener.ConnectionRequestEvent += request => HandleConnectionRequest(request);

            listener.PeerConnectedEvent += peer => PeerConnectedEvent(peer);

            listener.PeerDisconnectedEvent += (peer,disconnectInfo) => PeerDisconnectedEvent(peer,disconnectInfo);
        }


        internal void HandleConnectionRequest(ConnectionRequest request) //happens only on "server"
        {
            Log.Information("Handle connection request");

            try
            { 
                request.AcceptIfKey("synchrocadelan");
                Log.Information("accept connection request");

            }
            catch (Exception e)
            {
                HandleException(e);
            }
        }


        private void HandleException(Exception e)
        {
            Log.Fatal(e, "Exception In Network Thread");
            endGame(true,e.Message,true);
        }


        private void PlayerDeliveredPlayerNumber(NetPeer peer, int playerNum)
        {
            Log.Information($"Connection Established with {peer.EndPoint.Address}:{peer.EndPoint.Port}, asServer:{isServer}");
            try
            {
                
                //int peerIndex = GetPlayerNumberFromPeer(peer);
                waitingToConnect![playerNum] = false;
            
                this.peer![playerNum] = peer;
                connected![playerNum] = true;
                networkFrame = 0;
                updatingFrameCounter = false;

                //need to actually launch the game now, this isn't exactly correct but can fix later
                //we need to propagate a seed before starting
                if (isServer)
                {
                    checkIfFullyConnected();
                }
            
            }
            catch (Exception e)
            {
                HandleException(e);
            }
        }


        

        private void checkIfFullyConnected()
        {
            bool fullyConncted = true;
            foreach (int index in opponentPlayerNumber!)
            {
                if (!connected![index])
                {
                    fullyConncted = false;
                }
            }
            
            if (fullyConncted)
            {
                byte[] data = new byte[8];
                BitConverter.GetBytes(randomSeed).CopyTo(data, 0);
                BitConverter.GetBytes(gameId).CopyTo(data, 4);
                connection.SendToAll(WriteMessagePacket(MessageType.SendGameInfo, data), DeliveryMethod.ReliableOrdered);
                onConnection(players, GetPlayerNamesFromPlayerData(), randomSeed,gameId);
            }
        }

        private string[] GetPlayerNamesFromPlayerData()
        {
            string[] players = new string[playerData!.Length];
            for (int i = 0; i < playerData.Length; i++)
            {
                players[i] = playerData[i]!.Value.username;
            }

            return players;
        }
        
        
        private void PeerConnectedEvent(NetPeer peer) //happens on clinent and server
        {
            try
            {
                peer.Send(WriteMessagePacket(MessageType.PlayerNumber, BitConverter.GetBytes(myPlayerNumber)), DeliveryMethod.ReliableOrdered);
            }
            catch (Exception e)
            {
                HandleException(e);
            }

        }
        
        private void PeerDisconnectedEvent(NetPeer peer, DisconnectInfo disconnectInfo) //happens on clinent and server
        {
            int peerIndex = GetPlayerNumberFromPeer(peer);
            Log.Information($"PeerDisconnectedEvent: {disconnectInfo}");
            if (waitingToConnect![peerIndex])
            {
                STUNNATType myNatType = playerData![myPlayerNumber]!.Value.natType;
                STUNNATType opponentNatType = playerData![peerIndex]!.Value.natType;
                endGame(true,$"All connection attempts failed, myNatType:{myNatType}, opponentNatType:{opponentNatType}.",false);
                waitingToConnect[peerIndex] = false;
                connectionTimer = null;
                updateThread?.Stop();
            }
            else if(connected![peerIndex])
                endGame(true,"opponent disconnected mid game",true);


        }

        public void reportGameStarted()
        {
            if (!localOnly)
            {
                gameStarted![myPlayerNumber] = true;
                connection.SendToAll(WriteMessagePacket(MessageType.GameStartedOnThisEnd, null),
                    DeliveryMethod.ReliableUnordered);
            }
        }


        public int GetPlayerNumberFromPeer(NetPeer peer)
        {
            for (int i = 0; i < players; i++)
            {
                if (myPlayerNumber != i)
                {
                    if (peer.EndPoint.Equals(this.peer![i]?.EndPoint))
                    {
                        return i;
                    }
                }
            }

            return int.MinValue;
        }
        
        internal void HandleRecieve(NetPeer fromPeer, NetPacketReader dataReader, DeliveryMethod deliveryMethod)
        {
            try
            {
                byte[] databytes = dataReader.GetRemainingBytes();
                if (!(inputLists is null))
                    ReadBytes(fromPeer, databytes, ref inputLists[GetPlayerNumberFromPeer(fromPeer)], inputOptions);
                else
                {
                    InputList fakelist = new InputList();
                    ReadBytes(fromPeer, databytes, ref fakelist, inputOptions);
                }

                dataReader.Recycle();
            }
            catch (Exception e)
            {
                HandleException(e);
            }
        }

        private int MyLastVerfiedFrame()
        {
            int minimum = int.MaxValue;
            foreach (int index in opponentPlayerNumber!)
            {
                int lvf = inputLists![index].GetLastVerifiedFrame();
                if (lvf < minimum)
                    minimum = lvf;
            }
            return minimum;
        }


        private long OpponentsLastVerifiedFrame()
        {
            long minimum = long.MaxValue;
            foreach (int index in opponentPlayerNumber!)
            {
                long lvf = opponentsLastVerifiedFrame![index];
                if (lvf < minimum)
                    minimum = lvf;
            }
            return minimum;
        }
        
        private void CalculateLatency()
        {
            
            
            int lastVerifiedFrame = MyLastVerfiedFrame();
            int myLatencyCount = 0;
            int myCountedValues = 0;

            foreach (int index in opponentPlayerNumber!)
            {
                for (long i = lastVerifiedFrame - latencyDifferenceCheckRate; i < lastVerifiedFrame; i++)
                {
                    if (i >= 0)
                    {
                        short? value = inputLists![index][(int) i].myFrameDistance;
                        if (value.HasValue)
                        {
                            myLatencyCount += (int) value;
                            myCountedValues++;
                        }
                    }
                }
            }

            //TODO:for some reason i seem to consistantly say the opponent is 10 frames ahead on each side, i assume this is a mathmatical error so i'll work around it here
            myLatencyCount += myCountedValues;

            lastLatencyCheckFrame = GetCurrentGameFrame();

            int opponentsLatencyCount = 0;
            int opponentsCountedValues = 0;

            foreach (int index in opponentPlayerNumber!)
            {
                for (long i = opponentsLastVerifiedFrame![index] - latencyDifferenceCheckRate;
                    i < opponentsLastVerifiedFrame![index];
                    i++)
                {
                    if (i >= 0)
                    {
                        short? value = inputLists![index][(int) i].opponentFrameDistance;
                        if (value.HasValue)
                        {
                            opponentsLatencyCount += (int) value;
                            opponentsCountedValues++;
                        }

                    }
                }
            }

            //prevent div by zero
            if (myCountedValues <= 0)
            {
                myCountedValues = 1;
            }

            if (opponentsCountedValues <= 0)
            {
                opponentsCountedValues = 1;
            }
            
            
            
            //then act on the latency
            //then calculate the new updates per second value we're looking for
            //first calculate our frame difference
            double reverseRateDifference =  (float)opponentsLatencyCount/(float)opponentsCountedValues - (float)myLatencyCount/(float)myCountedValues;
            
            //this is the number of frames we are looking to bring closer together, let's aim to correct 1/4th of the delay in the updates per second rate
            //this will also be happening from the other side, so we cut the gap in half ever that number of frames
            //double requiredChangePerFrame = (reverseRateDifference / 2) / latencyDifferenceCheckRate;
            double newUPS = NativeUPS + (reverseRateDifference / 4); //i just want to the UPS out, it had been before, this means it will be based on 120 second increments
            if (newUPS > NativeUPS * 1.10) //add a 10% cap to speedup
                newUPS = NativeUPS * 1.10;
            else if (newUPS < NativeUPS * .70) //and a 30% cap to slow down, if we get really far ahead we REALLY want to correct that
                newUPS = NativeUPS * .70;
            updateThread.SetUPS(newUPS);
            //Log.Debug($"latency check frame {GetCurrentGameFrame()}, opponentsLatencyCount:{opponentsLatencyCount},myLatencyCount{myLatencyCount}, newUPS{newUPS}");
            //and set it
            //then clear the calculated latency count values

        }

        private bool AllInputDesynchronizationNumbersSet()
        {
            foreach (long value in inputDesynchronizationNumber!)
            {
                if (value == long.MinValue)
                    return false;
            }

            return true;
        }
        
        private bool AllInputDesynchronizationNumbersEqual()
        {
            long? priorValue = null;
            foreach (long value in inputDesynchronizationNumber!)
            {
                if (priorValue is null)
                {
                    priorValue = value;
                    continue;
                }
                if (priorValue != value)
                    return false;
                priorValue = value;
            }
            return true;
        }

        

        internal void CheckForInputDesynchronization()
        {
            long checksum = 0;

            ushort counter = 1;
            for (long i = LastInputDesynchronizationCheckFrame + 1; i < (LastInputDesynchronizationCheckFrame + desynchronizationCheckRate); i++)
            {
                checksum += counter * inputLists![0][(int)i].input.GetInputSynchronizationTestValue();
                counter++;
                checksum += counter * inputLists![1][(int)i].input.GetInputSynchronizationTestValue();
                counter++;

            }


            
            inputDesynchronizationNumber![myPlayerNumber] = checksum;

            LastInputDesynchronizationCheckFrame += desynchronizationCheckRate;

            if (AllInputDesynchronizationNumbersSet())
            {
                ActOnInputDesynchronization();
            }

            connection.SendToAll(WriteMessagePacket(MessageType.InputDesynchronizationCheck, BitConverter.GetBytes(checksum)), DeliveryMethod.ReliableUnordered);


        }

        
        
        internal void ActOnInputDesynchronization()
        {
            if(!AllInputDesynchronizationNumbersEqual())
            {
                string allValues = "";
                foreach (var number in inputDesynchronizationNumber!)
                {
                    allValues = $"{allValues}, {number}";
                }
                
                Log.Fatal($"Synchronization Check (Inputs) Failed, verificationFrame:{LastInputDesynchronizationCheckFrame}, currentFrame:{networkFrame}, desynchronizationNumbers:{allValues}, MyIndex:{myPlayerNumber}");
                throw new InvalidOperationException(GlobalExceptionText.DesynchronizationInput);
            }
            else
            {
                Log.Verbose($"Synchronization Check (Inputs) Passed, verificationFrame:{LastInputDesynchronizationCheckFrame}, currentFrame:{networkFrame} all:{inputDesynchronizationNumber![myPlayerNumber]}");
            }

            for (int i = 0; i < inputDesynchronizationNumber.Length; i++)
            {
                inputDesynchronizationNumber[i] = long.MinValue;
            }
        }

        private bool AllGameStateDesynchronizationNumbersSet()
        {
            foreach (ulong value in gameStateDesynchronizationNumber!)
            {
                if (value == ulong.MaxValue)
                    return false;
            }

            return true;
        }
        
        private bool AllGameStateDesynchronizationNumbersEqual()
        {
            ulong? priorValue = null;
            foreach (ulong value in gameStateDesynchronizationNumber!)
            {
                if (priorValue is null)
                {
                    priorValue = value;
                    continue;
                }
                if (priorValue != value)
                    return false;
                priorValue = value;
            }
            return true;
        }
        
        
        internal void CheckForGameStateDesynchronization()
        {

            int checkframe = lastGamestateDesynchronizationCheckFrame + 1 + desynchronizationCheckRate;
            gameStateDesynchronizationNumber![myPlayerNumber] = onDesynchronizationCheck(checkframe, false);

            if(gameStateDesynchronizationNumber![myPlayerNumber] != ulong.MaxValue)
                connection.SendToAll(WriteMessagePacket(MessageType.GameStateDesynchronizationCheck, BitConverter.GetBytes(gameStateDesynchronizationNumber![myPlayerNumber])), DeliveryMethod.ReliableOrdered);

            
            if (AllGameStateDesynchronizationNumbersSet())
            {
                
                ActOnGameStateDesynchronization();
            }
            
            lastGamestateDesynchronizationCheckFrame += desynchronizationCheckRate;




        }

        internal void ActOnGameStateDesynchronization()
        {
            if (!AllGameStateDesynchronizationNumbersEqual())
            {
                int checkframe = lastGamestateDesynchronizationCheckFrame + 1 + desynchronizationCheckRate;
                gameStateDesynchronizationNumber![myPlayerNumber] = onDesynchronizationCheck(checkframe, true);

                string allValues = "";
                foreach (var number in gameStateDesynchronizationNumber!)
                {
                    allValues = $"{allValues}, {number}";
                }
                Log.Fatal($"Synchronization Check (Game State) Failed, verificationFrame: { lastGamestateDesynchronizationCheckFrame}, currentFrame: { networkFrame}, me:{gameStateDesynchronizationNumber![myPlayerNumber]},desynchronizationNumbers:{allValues},, MyIndex:{myPlayerNumber}");
                endGame(true,GlobalExceptionText.DesynchronizationGameState,false);
            }
        
            else
            {
                Log.Verbose($"Synchronization Check (Game State) Passed, verificationFrame: { lastGamestateDesynchronizationCheckFrame}, currentFrame: { networkFrame}, all:{gameStateDesynchronizationNumber![myPlayerNumber]}");
            }
            
            for (int i = 0; i < gameStateDesynchronizationNumber.Length; i++)
            {
                gameStateDesynchronizationNumber[i] = ulong.MaxValue;
            }
        }

        
        private double msToFrames(double ms)
        {
            return ms / (1.0/120*1000);
        }

        private int MaxPing()
        {
            int maximum = int.MinValue;
            foreach (int index in opponentPlayerNumber!)
            {
                int lvf = peer![index]!.Ping;
                if (lvf > maximum)
                    maximum = lvf;
            }
            return maximum;
        }
        

        internal void Update(bool stalled)
        {
            if (localOnly || ending)
                return;
            
            bool fullyConnected = true;
            bool fullyStarted = true;
            bool waitingOnAnyone = false;
            foreach (int index in opponentPlayerNumber!)
            {
                if (!connected![index])
                {
                    fullyConnected = false;
                }

                if (!gameStarted![index])
                {
                    fullyStarted = false;
                }
                
                if (waitingToConnect![index])
                {
                    waitingOnAnyone = true;
                }
            }
            
            
            if (stalled) //this might wanna be re-merged into the other code
            {

                
                if(fullyConnected && fullyStarted && !(inputLists is null))
                {
                    connection.PollEvents();
                 
                    writer.Put(WriteNormalPacket(ref inputLists![myPlayerNumber], inputOptions, stalled)); //for some reason this ends up adding an input and breaks shit
                    connection.SendToAll(writer, DeliveryMethod.Unreliable);
                    writer.Reset();

                    //TODO:i am making stalls happen way less often then they should, only when stuff goes bad, this isn't a final solution
                    if (GetCurrentGameFrame() -1 < (MyLastVerfiedFrame() + Math.Min(Math.Max(20, msToFrames(MaxPing()) * 2 + 5),50)))
                    {
                        updateThread.IsStalled = false;
                        Log.Information($"Left stall, max ping is {MaxPing()}, currentFrame is {GetCurrentGameFrame()}, lastVerifiedFrame is {MyLastVerfiedFrame()},opponentsLastVerifiedFrame is {OpponentsLastVerifiedFrame()}");

                    }
                    else
                    {
                        Log.Information($"Continuing stall, max ping is {MaxPing()}, currentFrame is {GetCurrentGameFrame()}, lastVerifiedFrame is {MyLastVerfiedFrame()},opponentsLastVerifiedFrame is {OpponentsLastVerifiedFrame()}");
                        
                    }
                    return;
                }
                

            }
            

            if (!(inputLists is null) && fullyConnected && fullyStarted)
            {
                //TODO:i am making stalls happen way less often then they should, only when stuff goes bad, this isn't a final solution
                if (GetCurrentGameFrame() > MyLastVerfiedFrame() + Math.Min(Math.Max(50, msToFrames(MaxPing()) * 4 + 10),100))
                {
                    updateThread.SetUPS(NativeUPS);
                    updateThread.IsStalled = true;
                    Log.Information($"Stalled out, max ping is {MaxPing()}, currentFrame is {GetCurrentGameFrame()}, myLastVerifiedFrame is {MyLastVerfiedFrame()}, opponentsLastVerifiedFrame is {opponentsLastVerifiedFrame}");
                }
                

                if (GetCurrentGameFrame() > lastLatencyCheckFrame + latencyDifferenceCheckRate + 1)
                {
                    CalculateLatency();
                }
                if (Math.Min(MyLastVerfiedFrame(),inputLists![myPlayerNumber].GetLastVerifiedFrame()) > LastInputDesynchronizationCheckFrame + desynchronizationCheckRate + 1)
                {
                    CheckForInputDesynchronization();
                }
                if (Math.Min(MyLastVerfiedFrame(),inputLists[myPlayerNumber].GetLastVerifiedFrame()) > lastGamestateDesynchronizationCheckFrame + desynchronizationCheckRate + 1)
                {
                    CheckForGameStateDesynchronization();
                }
            }
            if (inCountdownToGameStart)
            {
                TimeSpan ts = countdownTimer!.Elapsed;
                if (ts.TotalMilliseconds > lengthOfCountdownInMS)
                {
                    Log.Information("Game Countdown Ended, game should start.");

                    inCountdownToGameStart = false;
                    onGameStart();
                    networkFrame = 0;
                    updatingFrameCounter = true;
                    countdownTimer.Stop();
                }
            }

            if (waitingOnAnyone && peered)
            {
                TimeSpan ts = connectionTimer!.Elapsed;
                if (ts.TotalMilliseconds > 15000)
                {
                    string allValues = "";
                    foreach (var natType in playerData!)
                    {
                        allValues = $"{allValues}, {natType!.Value.natType}";
                    }
                    
                    endGame(true,$"Did not connect in time. natTypes:{allValues}, myIndex:{myPlayerNumber}",true);
                    Array.Fill(waitingToConnect!,false);
                    connectionTimer = null;
                    
                    
                }
            }


            if (ending)
                return;

            try
            {
                connection.PollEvents();
            }
            catch (System.InvalidOperationException e)
            {
                Log.Error(e.ToString());
            }
            if (ending)
                return;
            
            if(fullyConnected && fullyStarted && !(inputLists is null))
            {
                writer.Put(WriteNormalPacket(ref inputLists![myPlayerNumber], inputOptions, false));
                connection.SendToAll(writer, DeliveryMethod.Unreliable);
                writer.Reset();
            }
            if(updatingFrameCounter)
                networkFrame++;




        }


        internal void RegisterLocalInputs(ref InputList[] inputLists, InputObject[] currentInputs, InputObject.EncodeDecodeOptions inputOptions)
        {

            //if my game is local, set both inputs as newest

            //if the game is not local set only my inputs as newest
            if(localOnly)
            {
                for(int i = 0; i < players; i++)
                {
                    inputLists[i].Add(currentInputs[i], true, 0,null);
                    inputLists[i].SetNetworkedPlayer(false);
                }
                

            }
            else
            {
                inputLists[myPlayerNumber].SetNetworkedPlayer(false);

                inputLists[myPlayerNumber].Add(currentInputs[0], true, 0,null);

                foreach (int index in opponentPlayerNumber!)
                {
                    inputLists[index].SetNetworkedPlayer(true);
                    inputLists[index].PlaceDuplicatesToRequestedFrame(GameFrameFromNetworkFrame(networkFrame) + 1);
                }
            }


            this.inputLists = inputLists;
            this.inputOptions = inputOptions;
        }

        internal void StartServer(int port, int gameId)
        {
            this.peered = false;
            players = 2;
            CreateEmptyArrays(players);
            

            

            Log.Information("Started as server.");
            Log.Information($"Starting in Server Mode");
            this.gameId = gameId;
            isServer = true;
            localOnly = false;
            connection.Start(port);
            myPlayerNumber = 0;
            opponentPlayerNumber = new List<int>(){1};
            updateThread?.Start();
        }


        private void CreateEmptyArrays(int players)
        {

            if (!peered)
            {
                playerData = new (string username, IPAddress ip, int port, STUNNATType natType)?[players];

                for (int i = 0; i < players; i++)
                {
                    playerData[i] = ($"Player {i + 1}", IPAddress.None, 0, STUNNATType.Unspecified);
                }
            }
            
            startingPing = new int[players];
            Array.Fill(startingPing,int.MinValue);
            startingPing[myPlayerNumber] = 0;


            opponentsLastVerifiedFrame = new long[players];
            
            connected = new bool[players];
            connected[myPlayerNumber] = true;

            gameStarted = new bool[players];

            waitingToConnect = new bool[players];
            Array.Fill(waitingToConnect, true);

            waitingToConnect[myPlayerNumber] = false;
            
            peer = new NetPeer?[players];
            
            inputDesynchronizationNumber = new long[players];
            Array.Fill(inputDesynchronizationNumber,long.MinValue);
            gameStateDesynchronizationNumber = new ulong[players];
            Array.Fill(gameStateDesynchronizationNumber,ulong.MaxValue);

        }

        internal void StartPeered(int gameId, int seed, int myPlayerNumber, (string username, IPAddress ip, int port, STUNNATType natType)?[] playerData)
        {
            this.peered = true;
            this.players = playerData.Length;

            isServer = myPlayerNumber == 0;

            this.playerData = playerData;
            this.myPlayerNumber = myPlayerNumber;
            CreateEmptyArrays(playerData.Length);

            opponentPlayerNumber = new List<int>();
            
            this.gameId = gameId;
            this.randomSeed = seed;
            
            connection.Start(9017);
            connection.MaxConnectAttempts = 10;
            connection.ReconnectDelay = 150;

            
            connectionTimer = new Stopwatch();
            connectionTimer.Start();
            localOnly = false;

            for (int i = 0; i < playerData.Length; i++)
            {
                if (i == myPlayerNumber)
                    continue;
                
                opponentPlayerNumber.Add(i);
                
                //decide on who should host
                int myScore = myPlayerNumber;
                int opponentsScore = i;
                switch (myNatType)
                {
                    case STUNNATType.OpenInternet: //open internet gets no points so isn't listed
                        break;
                    case STUNNATType.FullCone:
                        myScore += 2; //increment by twos so player number only decides between them
                        break;
                    case STUNNATType.Restricted:
                        myScore += 4;
                        break;
                    case STUNNATType.PortRestricted:
                        myScore += 6;
                        break;
                    case STUNNATType.SymmetricUDPFirewall:
                        myScore += 8;
                        break;
                    case STUNNATType.Unspecified:
                        myScore += 10;
                        break;
                    case STUNNATType.Symmetric:
                        myScore += 12;
                        break;
                }
                switch (playerData[i]!.Value.natType)
                {
                    case STUNNATType.OpenInternet: //open internet gets no points so isn't listed
                        break;
                    case STUNNATType.FullCone:
                        opponentsScore += 2; //increment by twos so player number only decides between them
                        break;
                    case STUNNATType.Restricted:
                        opponentsScore += 4;
                        break;
                    case STUNNATType.PortRestricted:
                        opponentsScore += 6;
                        break;
                    case STUNNATType.SymmetricUDPFirewall:
                        opponentsScore += 8;
                        break;
                    case STUNNATType.Unspecified:
                        opponentsScore += 10;
                        break;
                    case STUNNATType.Symmetric:
                        opponentsScore += 12;
                        break;
                }
                bool isHost = myScore < opponentsScore;
                

                
                //this may end up being very wrong and i need to decide a server
                //and then send punch packets from it while the other attempts to connect
                byte[] prepacket = WriteMessagePacket(MessageType.PunchPacket, null);
                byte[] punchpacket = new byte[] {0,prepacket[0],prepacket[1],prepacket[2]};

                if (isHost)
                {
                    Log.Information("Started as peered connection as host.");
                    //open a port
                    //send some packets out through the port 


                    //for whatever reason the normal raw packet sending adds an extra byte to the beginning of the packet, i'll need that
                    
                    
                    IPEndPoint remoteEP = new IPEndPoint(playerData[i]!.Value.ip, playerData[i]!.Value.port);
                    using (Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
                    {
                        connection.SendUnconnectedMessage(punchpacket, remoteEP);
                        connection.SendUnconnectedMessage(punchpacket, myEndPoint);
                        connection.SendUnconnectedMessage(punchpacket, remoteEP);
                        connection.SendUnconnectedMessage(punchpacket, remoteEP);
                    }



                }
                else
                {
                    Log.Information("Started as peered connection as client.");
                    //leave this as a short little sleep for now, might move it to something queued by the loop instead to not block
                    IPEndPoint remoteEP = new IPEndPoint(playerData[i]!.Value.ip, playerData[i]!.Value.port);

                    connection.SendUnconnectedMessage(punchpacket, remoteEP);
                    connection.SendUnconnectedMessage(punchpacket, myEndPoint);
                    connection.SendUnconnectedMessage(punchpacket, remoteEP);
                    connection.SendUnconnectedMessage(punchpacket, remoteEP);
                    connection.MaxConnectAttempts = 50;
                    connection.DisconnectTimeout = 1500;
                    connection.Connect(playerData[i]!.Value.ip.ToString(), playerData[i]!.Value.port, "synchrocadelan");
                    connection.DisconnectTimeout = 5000;

                }

            }
            
            updateThread?.Start();
        }

        public STUNQueryResult getStun()
        {
            connection.Stop();
            if (!STUNUtils.TryParseHostAndPort("synchrocade.com:3478", out IPEndPoint stunEndPoint))
                throw new Exception("Failed to resolve STUN server address");

            STUNClient.ReceiveTimeout = 1000;


            using (Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
            {
                sock.Connect("synchrocade.com", 3478);
                IPEndPoint endPoint = (sock.LocalEndPoint as IPEndPoint)!;
                sock.Shutdown(SocketShutdown.Both);
                sock.Close();
                
                IPEndPoint ipEndPoint = new IPEndPoint(endPoint.Address, 9017);
                myEndPoint = ipEndPoint;


                using (Socket sock2 = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
                {
                    sock2.Bind(ipEndPoint);
                    STUNQueryResult queryResult = STUNClient.Query(sock2, stunEndPoint, STUNQueryType.ExactNAT,NATTypeDetectionRFC.Rfc3489);
                    queryResult.Socket = null;
                    sock2.Close();

                    if (queryResult.QueryError != STUNQueryError.Success)
                    {
                        Log.Debug($"Got error from stun:{queryResult.QueryError.ToString()}");
                        
                        sock2.Bind(ipEndPoint);
                        STUNQueryResult queryResult2 = STUNClient.Query(sock2, stunEndPoint, STUNQueryType.PublicIP,NATTypeDetectionRFC.Rfc3489);
                        queryResult.Socket = null;
                        sock2.Close();
                        queryResult = queryResult2;

                    }
                    else
                    {
                        myNatType = queryResult.NATType;
                        Log.Debug($"Performed stun request and found nat type:{myNatType}");

                    }

                    return queryResult;

                }

            }
        }

        internal void StartLocal(int gameId, int players)
        {
            this.peered = false;
            this.players = players;


            CreateEmptyArrays(players);

            
            Log.Information("Started local.");

            updateThread?.Start();
            Log.Information($"Starting in Local mode.");

            localOnly = true;
            this.gameId = gameId;
            onConnection(players,GetPlayerNamesFromPlayerData(), randomSeed,gameId);
            onGameStart();
        }

        internal void JoinServer(String hostname, int port)
        {
            this.peered = false;
            players = 2;

            CreateEmptyArrays(players);

            Log.Information("Started as client.");

            connection.TriggerUpdate();
            networkFrame = 0;

            Log.Information($"Starting in client mode, connecting to {hostname}:{port}");

            isServer = false;
            localOnly = false;
            myPlayerNumber = 1;
            opponentPlayerNumber = new List<int>() {0};
            connection.Start();
            connection.MaxConnectAttempts = 20;
            connection.Connect(hostname, port, "synchrocadelan");
            updateThread?.Start();

        }

        
        private bool ending = false;
        internal void endGame(bool error, String reason,bool propagate)
        {
            //if (ending)
            //    return;
            ending = true;
            Log.Information($"Game ended, isError:{error}, reason:{reason}");

            inputLists = null;
            if (!localOnly)
            {
                if (propagate)
                {
                    

                    if(error)
                        connection.SendToAll(WriteMessagePacket(MessageType.GameErrored, Encoding.ASCII.GetBytes(reason)),DeliveryMethod.ReliableOrdered);
                    else
                        connection.SendToAll(WriteMessagePacket(MessageType.GameQuit, null),DeliveryMethod.ReliableOrdered);
                }
                connection.TriggerUpdate();
                connection.DisconnectAll();
                connection.Stop();
                connection.TriggerUpdate();
                updateThread?.Stop();
            }
            onGameEnd(error,reason);
        }
        

        internal void SetCurrentGameFrame(long newGameFrame)
        {
            //gameFrame = newGameFrame;
            frameDifference = newGameFrame - networkFrame;
        }

        //used by sub functinos in order to calculated differences
        internal long GetCurrentGameFrame()
        {
            return GameFrameFromNetworkFrame(networkFrame);
        }
        

        internal long NetworkFrameFromPacketFrame(ushort packetFrame)
        {
            //figure out network frame from network frame

            //find the mod
            long mod = networkFrame / frameHeaderSize;

            //we may have issues with things rolling over
            long distanceFromCurrentFrame = networkFrame - (mod * frameHeaderSize + packetFrame);
            if (distanceFromCurrentFrame < 240 && distanceFromCurrentFrame > -240)
            {
                return (mod * frameHeaderSize + packetFrame);
            }
            mod++;
            distanceFromCurrentFrame = networkFrame - (mod * frameHeaderSize + packetFrame);
            if (distanceFromCurrentFrame < 240 && distanceFromCurrentFrame > -240)
            {
                return (mod * frameHeaderSize + packetFrame);
            }
            mod -= 2;
            distanceFromCurrentFrame = networkFrame - (mod * frameHeaderSize + packetFrame);
            if (distanceFromCurrentFrame < 240 && distanceFromCurrentFrame > -240)
            {
                return (mod * frameHeaderSize + packetFrame);
            }
            throw new ArgumentOutOfRangeException(GlobalExceptionText.DesynchronizationNetFrame);
        }
        
        private ushort GetPacketFrame()
        {
            return (ushort)(networkFrame % 32768);
        }

        internal int GameFrameFromNetworkFrame(long networkFrame)
        {
            return (int)(networkFrame + frameDifference);
        }

        internal long GameFrameFromPacketFrame(ushort packetFrame)
        {
            return GameFrameFromNetworkFrame(NetworkFrameFromPacketFrame(packetFrame));
        }




        internal void ReadBytes(NetPeer fromPeer, byte[] packet, ref InputList list, InputObject.EncodeDecodeOptions inputOptions)
        {
            uint currentBit = 0;
            Header header = new Header(packet, ref currentBit);

            int playerIndex = GetPlayerNumberFromPeer(fromPeer);
            if (header.NormalFrame)
            {
                
                opponentsLastVerifiedFrame![playerIndex] = GameFrameFromPacketFrame(header.FrameCount!.Value) - header.DistanceOffLastVerifiedFrame!.Value;
                InputPacket.ReadPacketData(this, header, packet, ref currentBit, ref list, inputOptions);
            }
            else
            {
                (MessageType message, byte[] payload) messagePacket = ReadMessagePacket(packet);
                Log.Information($"recieved packet of type:{messagePacket.message}");

                switch(messagePacket.message)
                {
                    case MessageType.GameStartedOnThisEnd:
                        gameStarted![playerIndex] = true;
                        bool allGamesStarted = true;
                        if (isServer)
                        {
                            foreach (bool gameStarted in gameStarted!)
                            {
                                if (!gameStarted)
                                {
                                    allGamesStarted = false;
                                    break;
                                }
                            }

                            if (allGamesStarted)
                            {
                                connection.SendToAll(WriteMessagePacket(MessageType.Ping, null),
                                    DeliveryMethod.ReliableUnordered);
                                pingTimer = new Stopwatch();
                                pingTimer.Start();
                            }
                        }
                        break;
                    case MessageType.PlayerNumber:
                        int player = BitConverter.ToInt32(messagePacket.payload);
                        PlayerDeliveredPlayerNumber(fromPeer,player);
                        break;
                    case MessageType.SendGameInfo:
                        if (!peered)
                        {
                            //don't let a rogue client override this if the server set it
                            randomSeed = BitConverter.ToInt32(messagePacket.payload, 0);
                            gameId = BitConverter.ToInt32(messagePacket.payload, 4);
                        }
                        onConnection(players,GetPlayerNamesFromPlayerData(), randomSeed, gameId);
                        break;
                    case MessageType.CommenceGameInXMS:
                        lengthOfCountdownInMS = (ushort)(messagePacket.payload[0] + ((ushort)messagePacket.payload[1] << 8));
                        countdownTimer = new Stopwatch();
                        countdownTimer.Start();
                        inCountdownToGameStart = true;
                        break;
                    case MessageType.Ping:
                        fromPeer.Send(WriteMessagePacket(MessageType.Pong, null), DeliveryMethod.ReliableUnordered);
                        break;
                    case MessageType.Pong:
                        TimeSpan ts = pingTimer!.Elapsed;
                        startingPing![playerIndex] = (int)ts.TotalMilliseconds;
                        Log.Information($"Ping for player {playerIndex} time is {ts.TotalMilliseconds}ms");
                        
                        bool allPinged = true;
                        foreach (int ping in startingPing!)
                        {
                            if (ping == int.MinValue)
                            {
                                allPinged = false;
                                break;
                            }
                        }
                        if (isServer && allPinged)
                        {
                            pingTimer.Stop();

                            //when i move to more players the correct wait time is, but since i only have peer it's just 150ms
                            //highestping/2 + 150ms - peerping/2
                            ushort startTime = (ushort)(startingPing[playerIndex] / 2 + 300);
                            foreach (NetPeer? peer in peer!)
                            {
                                
                                for(int i = 0; i < players; i++)
                                {
                                    if (i != myPlayerNumber)
                                    {
                                        ushort modifiedStartingTime = (ushort)(startTime - startingPing![i]);
                                        this.peer![i]!.Send(WriteMessagePacket(MessageType.CommenceGameInXMS, new byte[] { (byte)startTime, (byte)(startTime >> 8) }), DeliveryMethod.ReliableUnordered);
                                    }

                                }
                            }
                            lengthOfCountdownInMS = startTime;
                            countdownTimer = new Stopwatch();
                            countdownTimer.Start();
                            inCountdownToGameStart = true;
                        }
                        break;
                    case MessageType.InputDesynchronizationCheck:
                        inputDesynchronizationNumber![playerIndex] = BitConverter.ToInt64(messagePacket.payload, 0);
                        if (AllInputDesynchronizationNumbersSet())
                        {
                            ActOnInputDesynchronization();
                        }
                        break;
                    case MessageType.GameStateDesynchronizationCheck:
                        gameStateDesynchronizationNumber![playerIndex] = BitConverter.ToUInt64(messagePacket.payload, 0);
                        if (AllGameStateDesynchronizationNumbersSet())
                        {
                            ActOnGameStateDesynchronization();
                        }
                        break;
                    case MessageType.GameErrored:
                        endGame(true,"Error on opponent side: "+Encoding.ASCII.GetString(messagePacket.payload),false);
                        break;
                    case MessageType.GameQuit:
                        endGame(false,"Game quit on opponent side.",false);
                        break;
                    case MessageType.PunchPacket:
                        Log.Debug("Got Punch Packet");
                        break;

                }
            }

        }

        internal byte[] WriteNormalPacket(ref InputList list, InputObject.EncodeDecodeOptions inputOptions, bool stalled)
        {
            //this will need changed to reflect the size of the input packet in bytes when i have games that use more than 2
            byte[] packet = new byte[InputPacket.InputsInPacket(OpponentsLastVerifiedFrame(), (int)GetCurrentGameFrame()) * 2 + 10];
            uint currentBit = 0;
            //Header.Encode(true, ref packet, GetPacketFrame(), ref currentBit);
            //System.Console.WriteLine(getPacketFrame());
            //internal static void CreatePacketData(ref byte[] packet, ref uint currentBit, int currentGameFrame, ushort packetFrame, long myLastVerifiedFrame, long opponentsLastVerifiedFrame, InputList list, InputObject.EncodeDecodeOptions inputOptions)

            //if stalled everything that has framecounts has to be reduced by 1
            int reduction = stalled ? 1 : 0;
            InputPacket.CreatePacketData(ref packet,ref currentBit,GameFrameFromNetworkFrame(networkFrame) - reduction,(ushort)(GetPacketFrame() - reduction),MyLastVerfiedFrame(),OpponentsLastVerifiedFrame(), list, inputOptions);
            //resize the array based on the bit count
            int newSize = (int)((currentBit+1) / 8)+1;
            //Array.Resize(packet,)
            Array.Resize(ref packet, newSize);
            //add 1's to the final bit
            BitShift.FillFinalArrayByteEnd(ref packet, currentBit, true);



            return packet;
        }

        private byte[] WriteMessagePacket(MessageType message, byte[]? payload)
        {
            int payloadSize = 0;
            if (!(payload is null))
                payloadSize = payload.Length;
            byte[] returnBytes = new byte[3 + payloadSize];
            uint currentBit = 0;
            Header.EncodeMessageHeader(ref returnBytes, ref currentBit);

            returnBytes[1] = (byte)message;
            returnBytes[2] = (byte)(((ushort)message) >> 8);
            if (!(payload is null) && payload.Length > 0)
                Array.Copy(payload, 0, returnBytes, 3, payload.Length);
            return returnBytes;
        }

        private static (MessageType message, byte[] payload) ReadMessagePacket(byte[] packet)
        {
            MessageType returnMessage = (MessageType)(packet[1] + ((ushort)packet[2] << 8));

            int payloadLength = packet.Length - 3;
            byte[] returnPayload = new byte[payloadLength];
            Array.Copy(packet, 3, returnPayload,0, payloadLength);
            return (returnMessage, returnPayload);
            //i'm just gonna ignore the header because it was parsed by something else before i got here i'm sure
        }
        



        class InputPacket
            {
                //private const int inputsPerPacket = 5;

                //in the future, this should read as many packets as i have, as that will be useful for recovery, but for now that isn't neccicary
                internal static void ReadPacketData(NetworkLoop netloop, Header header, byte[] packet,  ref uint currentBit, ref InputList list, InputObject.EncodeDecodeOptions inputOptions)
                {

                        //NormalFrame { get; }  ushort? FrameCount { get; } sbyte? DistanceOffLastVerifiedFrame { get; }

                        //now set the verification time for this specific frame
                        long gameframe = netloop.GameFrameFromPacketFrame(header.FrameCount!.Value);


                        



                        long thingsToLoop = header.InputsInFrame!.Value;
                        if (gameframe < thingsToLoop)
                            thingsToLoop = gameframe + 1;
                        (InputObject input, bool verified, short? myFrameDifference, short? opponentFrameDifference)?
                            lastreadobject = null;
                        //InputObject[] readInputs = new InputObject[inputsPerPacket];
                        for (uint x = 0; x < thingsToLoop; x++)
                        {
                            int listIndex = (int) (gameframe - x);

                            if (BitShift.ReadBitFromArray(packet, currentBit))
                            {
                                InputObject readInput = new InputObject();
                                currentBit++;
                                readInput.DecodeInputs(packet, ref currentBit, inputOptions);

                                //what if i get inputs that are past the last frame i calculated?
                                //if 
                                short framedifference = (short) (netloop.GetCurrentGameFrame() - gameframe);
                                (InputObject input, bool verified, short? frameDifference, short?
                                    opponentFrameDifference) currentInput = list[listIndex];
                                if (currentInput.verified && currentInput.frameDifference.HasValue)
                                {
                                    framedifference = currentInput.frameDifference.Value;
                                }
                                //System.Console.WriteLine(gameframe);
                                //System.Console.WriteLine(framedifference);

                                list[listIndex] = (readInput, true, framedifference, null);
                                //Log.Debug($"list[{listIndex}] = (readInput, true, framedifference, null);");
                                lastreadobject = (readInput, true, framedifference, null);
                            }
                            else
                            {
                                list[listIndex] =
                                    lastreadobject!
                                        .Value; //if a bit has never been read this should throw cause somethings broke
                                currentBit++;
                            }

                        }
                        
                        
                        //this presents an odd problem, if one side gets ahead they'll be reporting frames past the index
                        //these ahead values can't be added to the array
                        int pastFrameIndex = (int) (gameframe - header.DistanceOffLastVerifiedFrame!.Value);
                        if(pastFrameIndex>=0) //&&pastFrameIndex<=gameframe should not be required, because we'll create new entries past the point, and we don't want to discard these values
                            list[pastFrameIndex] = (list[pastFrameIndex].input, false,
                                list[pastFrameIndex].myFrameDistance,
                                (short) (header.DistanceOffLastVerifiedFrame!.Value)); //marked as unverified because we don't want this to change the verification count
                    

                }

                public static byte InputsInPacket(long opponentsLastVerifiedFrame, int currentGameFrame) //will generally be positive
                {
                    long differenceFromOpponent = (currentGameFrame - opponentsLastVerifiedFrame); //will generally be positive
                    if (differenceFromOpponent < 3)
                        return 3;
                    if (differenceFromOpponent > 127)
                        return 127;
                    return (byte) differenceFromOpponent;
                }

            internal static void CreatePacketData(ref byte[] packet, ref uint currentBit, int currentGameFrame, ushort packetFrame, long myLastVerifiedFrame, long opponentsLastVerifiedFrame, InputList list, InputObject.EncodeDecodeOptions inputOptions)
            {
                
                    long myFrameDifference = (currentGameFrame - myLastVerifiedFrame); //will generally be positive
                    sbyte differenceFromOpponent = (sbyte)myFrameDifference;
                    if (myFrameDifference > 127)
                        differenceFromOpponent = 127;
                    else if (myFrameDifference < -127)
                        differenceFromOpponent = -127;
                    byte inputsInPacket = InputsInPacket(opponentsLastVerifiedFrame, currentGameFrame);

                    //ref byte[] packet, ushort packetFrame, byte inputsInFrame, sbyte distanceOffLastVerifiedFrame, ref uint currentBit
                    Header.EncodeNormalHeader(ref packet, packetFrame, differenceFromOpponent, inputsInPacket, ref currentBit);
                    //Header.EncodeNormalHeader(ref packet, GetPacketFrame(), ref currentBit);

                    InputObject? lastInput = null;
                    bool firstFrame = true;
                    long loopLength = inputsInPacket;
                    if (currentGameFrame < inputsInPacket)
                        loopLength = currentGameFrame+1;
                    for (int x = 0; x < loopLength; x++)
                    {
                        InputObject input = list[currentGameFrame - x].input;
                        if(lastInput.HasValue && !firstFrame && input.Equals(lastInput.Value))
                        {
                            currentBit++;
                        }
                        else
                        {
                            BitShift.AddToArray(ref packet, currentBit,new byte[1] {0b1000_0000}, 1);
                            currentBit++;
                            
                            input.EncodeInputs(ref packet, ref currentBit, inputOptions);
                        }
                        lastInput = input;
                        firstFrame = false;
                    }

                }





            }

            class Header
            {
                //need to rewrite all of this to use the bitstream methods
                //we need to add two things to every packet
                //a mod saying how far away we are from last verified frame up to 127 in either direction, 1 byte
                //something saying how many frames of data we are sending as we can only send up to the byte, this can be 7 bits unsigned, as even sending one second of frames would be very excessive
                    
                //a frame counter saying how many frames we'll be sending
                //long newLoopLength = gameFrame - opponentsLastVerifiedFrame;
                //add a byte saying what our last verified frame is, we'll send packets to this frame
                //sbyte loopbyte = 0;
                //    if (newLoopLength > 127)
                //    loopbyte = 127;
                //    if (newLoopLength < -127)
                //    loopbyte = -127;
                public bool NormalFrame { get; }
                public ushort? FrameCount { get; }
                internal byte? InputsInFrame { get; }
                public sbyte? DistanceOffLastVerifiedFrame { get; }

                internal Header(byte[] packet, ref uint currentBit)
                {
                    NormalFrame = BitShift.ReadBitFromArray(packet, currentBit);
                    currentBit++;
                    //1 bit success bool
                    if (NormalFrame) //we will never have any of the other sections if we're a message header
                    {
                        //framecount bits
                        byte[] framecountBits = BitShift.ReadFromArray(packet, currentBit, 15);
                        framecountBits[1] = (byte)(framecountBits[1] >> 1);
                        //framecountBits[0] = (byte) (framecountBits[0] & 0b_0111_1111);
                        
                        FrameCount = (ushort)(BitConverter.ToUInt16(framecountBits));
                        //Log.Debug($"packetFrameOut:{FrameCount}");
                        currentBit += 15;
                        DistanceOffLastVerifiedFrame = (sbyte)BitShift.ReadFromArray(packet, currentBit, 8)[0];
                        currentBit += 8;
                        
                        InputsInFrame = (byte)(BitShift.ReadFromArray(packet, currentBit, 7)[0]>>1);
                        currentBit += 7;
                    }
                }

                internal static void EncodeNormalHeader(ref byte[] packet, ushort packetFrame, sbyte distanceOffLastVerifiedFrame, byte inputsInFrame, ref uint currentBit)
                {
                    //add the normal bit
                    BitShift.AddToArray(ref packet, currentBit, 0b0000_0001, 1,false);
                    currentBit ++;
                    //add the 15 bit packet frame
                    BitShift.AddToArray(ref packet, currentBit, (byte)(packetFrame), 8,false);
                    currentBit += 8;
                    BitShift.AddToArray(ref packet, currentBit, (byte)(packetFrame>>7), 7,true);
                    currentBit += 7;
                    //add the 8 bit DistanceOffLastVerifiedFrame
                    BitShift.AddToArray(ref packet, currentBit, (byte)distanceOffLastVerifiedFrame, 8,false);
                    currentBit += 8;
                    //add the 7 bit InputsInFrame
                    BitShift.AddToArray(ref packet, currentBit, inputsInFrame, 7,false);
                    currentBit += 7;
                }

                internal static void EncodeMessageHeader(ref byte[] packet, ref uint currentBit)
                {
                    //non normal frame headers are literally just a single 0 bit, nothing required
                    byte[] headerbytes = new byte[1];
                    BitShift.AddToArray(ref packet, currentBit, headerbytes, 1);
                    currentBit += 1;
                }
            }

    }


}
