﻿using Synchrocade.TemporalContainer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synchrocade.Networking
{
    internal class LatencyArray
    {
        //to calculate target framerate, i need to know my latency over the last number of seconds
        //i also need to know my opponents average latency over the last number of seconds

        //my opponent will either pass once each frame or once every number of frames, so my opponent could never
        //provide me more then a single average that isn't declared, it's probably going to be a 1 second average

        //question, do i calculate the average for my opponent, or do i send the average, do i have any advantage to KNOWING their latency by frame
        //i want double percision on an average, i should send the number of frames behind each time, this allows me to get that average better
        //plan, get a average value every second, try to cut that difference in half every second, up to a certain point
        //need a minimum that we never try to correct, maybe 1/10th of a frame
        //need a maximum, that we never try to correct more then, maybe 20% faster
        //don't need frame by frame accuracy for rollbacks, this is by network frame

        private readonly short[] buffer;
        private ulong currentHead;
        private ulong currentLength;
        private readonly ulong size;
        internal LatencyArray(ulong size)
        {
            this.size = size;
            buffer = new short[size];
            currentHead = 0;
            currentLength = 0;
        }

        internal void AddLatencyValue(short newVal)
        {
            //if we have not fully filled the array
            if (currentLength < size)
            {
                buffer[currentLength] = newVal;
                currentLength++;
            }
            else
            {
                buffer[currentHead] = newVal;
                currentHead++;
                if (currentHead >= size)
                {
                    currentHead = 0;
                }
            }
        }

        internal double GetMedianValue()
        {
            int currentSum = 0;
            for (ulong i = 0; i < currentLength; i++)
            {
                currentSum += buffer[i];
            }
            return currentSum / (double)currentLength;
        }
    }
}