using System.IO;
using System.Text.Json;
//using System.Xml;
//using Newtonsoft.Json;
using Synchrocade.Screens.UI.Settings;
using Ultraviolet.Input;

namespace Synchrocade.Settings
{
    public class SettingsContainer
    {
        private const string _configurationFilePath = "../Config/settings.json";

        public double SoundVolume { get; set; }
        public double MusicVolume { get; set; }

        
        public ControlScheme[] PlayerControlScheme { get; set; }

        public SettingsContainer()
        {
            SoundVolume = 75;
            MusicVolume = 75;
            PlayerControlScheme = new ControlScheme[4];
            PlayerControlScheme[0] = new ControlScheme
            {
                upButton = new MappedButton(Key.Up),
                downButton = new MappedButton(Key.Down),
                leftButton = new MappedButton(Key.Left),
                rightButton = new MappedButton(Key.Right),
                button1 = new MappedButton(Key.Z),
                button2 = new MappedButton(Key.X),
                button3 = new MappedButton(Key.C),
                button4 = new MappedButton(Key.V)
            };

            PlayerControlScheme[1] = new ControlScheme
            {
                upButton = new MappedButton(Key.I),
                downButton = new MappedButton(Key.K),
                leftButton = new MappedButton(Key.J),
                rightButton = new MappedButton(Key.L),
                button1 = new MappedButton(Key.Q),
                button2 = new MappedButton(Key.W),
                button3 = new MappedButton(Key.E),
                button4 = new MappedButton(Key.R)
            };
            
            PlayerControlScheme[2] = new ControlScheme
            {
                upButton = new MappedButton(),
                downButton = new MappedButton(),
                leftButton = new MappedButton(),
                rightButton = new MappedButton(),
                button1 = new MappedButton(),
                button2 = new MappedButton(),
                button3 = new MappedButton(),
                button4 = new MappedButton()
            };
            
            PlayerControlScheme[3] = new ControlScheme
            {
                upButton = new MappedButton(),
                downButton = new MappedButton(),
                leftButton = new MappedButton(),
                rightButton = new MappedButton(),
                button1 = new MappedButton(),
                button2 = new MappedButton(),
                button3 = new MappedButton(),
                button4 = new MappedButton()
            };

        }
        
        public static SettingsContainer LoadOrIntialize()
        {
            if (!File.Exists(_configurationFilePath))
            {
                SettingsContainer returnSettings = new SettingsContainer();

                returnSettings.WriteSettings();
                return returnSettings;
            }
            else
            {
                SettingsContainer returnSettings = JsonSerializer.Deserialize<SettingsContainer>(File.ReadAllText(_configurationFilePath))!;
                if (returnSettings.PlayerControlScheme.Length != 4)
                {
                    //if we don't have the proper length of schemes, straight up return controls to default, maybe later i should do some migrations
                    returnSettings.PlayerControlScheme = new SettingsContainer().PlayerControlScheme;
                }
                return returnSettings;
            }
        }

        public void WriteSettings()
        {
            FileInfo file = new FileInfo(_configurationFilePath);
            file?.Directory?.Create();
            File.WriteAllText(_configurationFilePath, JsonSerializer.Serialize(this));
        }
        

        
        
    }

    public struct ControlScheme
    {
        public MappedButton upButton { get; set; }
        public MappedButton downButton { get; set; }
        public MappedButton leftButton { get; set; }
        public MappedButton rightButton { get; set; }
        public MappedButton button1 { get; set; }
        public MappedButton button2 { get; set; }
        public MappedButton button3 { get; set; }
        public MappedButton button4 { get; set; }

    }

    public struct MappedButton
    {
        public int? JoystickNum { get; set; }
        public Key? Key { get; set; }
        public GamePadButton? PadButton { get; set; }

        public MappedButton(Ultraviolet.Input.Key keyPress)
        {
            JoystickNum = null;
            Key = keyPress;
            PadButton = null;
        }
        
        public MappedButton(int selectedPad, Ultraviolet.Input.GamePadButton button)
        {
            JoystickNum = selectedPad;
            this.PadButton = button;
            Key = null;
        }
        
        

        public string Label()
        {
            if (JoystickNum.HasValue)
            {
                return $"GamePad{JoystickNum} {PadButton.ToString()}";
            }
            else if (Key.HasValue)
            {
                return $"Keyboard {Key.ToString()}";
            }
            else
            {
                return "Unmapped!!";
            }
            
        }
    }
    
}