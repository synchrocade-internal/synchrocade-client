using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Synchrocade.GameLogics;

namespace Synchrocade.Core
{
    public class Game 
    {
        public string Name
        { get; set; }
        public int Id
        { get; set; }
        public bool OnlineOnly
        { get; set; }
        public bool Hidden
        { get; set; }
        public bool Alpha
        { get; set; }
        public int MaxPlayers
        { get; set; }
        public bool SinglePlayer
        { get; set; }


        public string PrintedName()
        {
            string printedname = Id + ". " + Name;
            if (Alpha)
            {
                printedname += " (Alpha)";
            }

            return printedname;
        }
        public Game(int Id, string name, int maxPlayers, bool singlePlayer, bool onlineOnly, bool hidden, bool alpha)
        {
            this.Name = name;
            this.Id = Id;
            this.OnlineOnly = onlineOnly;
            this.Hidden = hidden;
            this.Alpha = alpha;
            this.SinglePlayer = singlePlayer;
            this.MaxPlayers = maxPlayers;

        }
        
    }

    internal static class GameList
    {
        internal static List<Game> ListGames(bool includeOnlineOnly, bool showHidden)
        {
            //create the initial list
            List<Game> newlist = new List<Game>();
            newlist.Add(GetGameForId(1));
            newlist.Add(GetGameForId(2));
            newlist.Add(GetGameForId(3));
            return newlist;

        }

        internal static Game GetGameForId(int id)
        {
            switch (id)
            {
                case 1:
                    return new Game(1, "SkidStar", 2, false, false, false, false);
                case 2:
                    return new Game(2, "Takeover Incorporated",4, false, false, false, false);
                case 3:
                    return new Game(3, "DropPuzzle",4, true, false, false, true);

            };
            throw new Exception("Tried to get info for a game that is not known to the program yet.");
        }
        
        
        internal static List<Game> ListGamesAgainstLicenses(List<int> licenseList)
        {
            //create the initial list
            List<Game> games = ListGames(true, false);
            List<Game> returnList = new List<Game>();

            foreach (Game game in games)
            {
                if (licenseList.Contains(game.Id))
                {
                    returnList.Add(game);
                }
            }

            return returnList;

        }
    }

}