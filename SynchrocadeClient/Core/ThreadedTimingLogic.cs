using System;
using System.Diagnostics;
using System.Threading;
using Serilog;
using Serilog.Core;
using Synchrocade.UpdateThread;
using Ultraviolet;
using Ultraviolet.Core;
namespace Synchrocade.Core
{



    /// <summary>
    /// Contains core functionality for Ultraviolet host processes.
    /// </summary>
    public sealed partial class ThreadedTimingLogic : IUltravioletTimingLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UltravioletTimingLogic"/> class.
        /// </summary>
        /// <param name="host">The Ultraviolet host.</param>
        public ThreadedTimingLogic(IUltravioletHost host)
        {
            Contract.Require(host, nameof(host));

            this.host = host;
            this.tickTimer.Start();
        }

        /// <inheritdoc/>
        public void ResetElapsed()
        {
            tickTimer.Restart();
            if (!IsFixedTimeStep)
            {
                forceElapsedTimeToZero = true;
            }
        }

        /// <inheritdoc/>
        public void RunOneTickSuspended()
        {
            var uv = host.Ultraviolet;

            UpdateSystemTimerResolution();

            uv.UpdateSuspended();
            if (InactiveSleepTime.Ticks > 0)
            {
                //Thread.Sleep(InactiveSleepTime);
            }
        }

        /// <inheritdoc/>
        public void RunOneTick()
        {
            var uv = host.Ultraviolet;

            if (SynchronizationContext.Current is UltravioletSynchronizationContext syncContext)
                syncContext.ProcessWorkItems();

            UpdateSystemTimerResolution();
            

            bool readyForFrame = true;
            var gameTicksToRun = 0;
            var timeDeltaDraw = default(TimeSpan);
            var timeDeltaUpdate = default(TimeSpan);
            SpinWait spinner = new SpinWait();

            do
            {
                var elapsedTicks = tickTimer.Elapsed.Ticks;
                tickTimer.Restart();

                accumulatedElapsedTime += elapsedTicks;



                if (IsFixedTimeStep)
                {
                    gameTicksToRun = (Int32) (accumulatedElapsedTime / TargetElapsedTime.Ticks);
                    if (gameTicksToRun > 0)
                    {
                        /*lagFrames += (gameTicksToRun == 1) ? -1 : Math.Max(0, gameTicksToRun - 1);
    
                        if (lagFrames == 0)
                            runningSlowly = false;
                        if (lagFrames > 5)
                            runningSlowly = true;
    
                        timeDeltaUpdate = TargetElapsedTime;
                        timeDeltaDraw = TimeSpan.FromTicks(gameTicksToRun * TargetElapsedTime.Ticks);*/
                        gameTicksToRun = 1;
                        timeDeltaUpdate = TimeSpan.FromTicks(accumulatedElapsedTime);
                        timeDeltaDraw = timeDeltaUpdate;
                        accumulatedElapsedTime = 0;
                        readyForFrame = true;

                    }
                    else
                    {
                        /*var frameDelay =
                            (Int32) (TargetElapsedTime.TotalMilliseconds - tickTimer.Elapsed.TotalMilliseconds);
                        if (frameDelay > 1 + systemTimerPeriod)
                        {
                            if (frameDelay > 7)
                                new System.Threading.ManualResetEventSlim(false).Wait(frameDelay - 5);


                            elapsedTicks = tickTimer.Elapsed.Ticks;
                            tickTimer.Restart();

                            accumulatedElapsedTime += elapsedTicks;

                            //if (frameDelay > 5)
                             //   Thread.Sleep(frameDelay - 4);
                        }*/

                        
                        while (((float) accumulatedElapsedTime / (float) TargetElapsedTime.Ticks) < 1)
                        {
                            spinner.SpinOnce(-1);
                            elapsedTicks = tickTimer.Elapsed.Ticks;
                            tickTimer.Restart();

                            accumulatedElapsedTime += elapsedTicks;

                        }


                        readyForFrame = false;
                    }
                }
                else
                {
                    gameTicksToRun = 1;
                    if (forceElapsedTimeToZero)
                    {
                        timeDeltaUpdate = TimeSpan.Zero;
                        forceElapsedTimeToZero = false;
                    }
                    else
                    {
                        timeDeltaUpdate = TimeSpan.FromTicks(elapsedTicks);
                        timeDeltaDraw = timeDeltaUpdate;
                    }

                    accumulatedElapsedTime = 0;
                    runningSlowly = false;
                }
            } while (!readyForFrame);



            //if (gameTicksToRun == 0)
            //    return;

            


            uv.HandleFrameStart();

            for (var i = 0; i < gameTicksToRun; i++)
            {
                var updateTime = timeTrackerUpdate.Increment(timeDeltaUpdate, runningSlowly);
                if (!UpdateContext(uv, updateTime))
                {
                    //return;
                }
            }

            if (!host.IsSuspended)
            {
                var drawTime = timeTrackerDraw.Increment(timeDeltaDraw, runningSlowly);
                using (UltravioletProfiler.Section(UltravioletProfilerSections.Draw))
                {
                    #if DEBUG
                        if (drawTime.ElapsedTime.Ticks > 85000)
                        {
                            Log.Debug($"ticks since last draw {drawTime.ElapsedTime.Ticks}," +
                                      $" {GC.CollectionCount(0)}, {GC.CollectionCount(1)}, {GC.CollectionCount(2)}");

                        }
                    #endif
                    uv.Draw(drawTime);
                }
            }

            uv.HandleFrameEnd();
        }

        /// <inheritdoc/>
        public void Cleanup()
        {

        }

        /// <summary>
        /// Gets the default value for TargetElapsedTime.
        /// </summary>
        public static TimeSpan DefaultTargetElapsedTime { get; } = TimeSpan.FromTicks(TimeSpan.TicksPerSecond / 60);

        /// <summary>
        /// Gets the default value for InactiveSleepTime.
        /// </summary>
        public static TimeSpan DefaultInactiveSleepTime { get; } = TimeSpan.FromMilliseconds(20);

        /// <summary>
        /// Gets the default value for IsFixedTimeStep.
        /// </summary>
        public static Boolean DefaultIsFixedTimeStep { get; } = true;

        /// <inheritdoc/>
        public UltravioletContext Ultraviolet
        {
            get { return host.Ultraviolet; }
        }

        /// <inheritdoc/>
        public TimeSpan TargetElapsedTime { get; set; } = DefaultTargetElapsedTime;

        /// <inheritdoc/>
        public TimeSpan InactiveSleepTime { get; set; } = DefaultInactiveSleepTime;

        /// <inheritdoc/>
        public Boolean IsFixedTimeStep { get; set; } = DefaultIsFixedTimeStep;

        /// <summary>
        /// Updates the specified context.
        /// </summary>
        /// <param name="uv">The Ultraviolet context to update.</param>
        /// <param name="time">Time elapsed since the last update.</param>
        /// <returns><see langword="true"/> if the host should continue processing; otherwise, <see langword="false"/>.</returns>
        private Boolean UpdateContext(UltravioletContext uv, UltravioletTime time)
        {
            using (UltravioletProfiler.Section(UltravioletProfilerSections.Update))
            {
                uv.Update(time);
            }
            return !uv.Disposed;
        }

        /// <summary>
        /// Updates the resolution of the system timer on platforms which require it.
        /// </summary>
        private Boolean UpdateSystemTimerResolution()
        {
            if (Ultraviolet.Platform != UltravioletPlatform.Windows)
            {
                return false;
            }

            uint desired = 5000;

            bool setresolution = true;

            _ = NativeMethods.NtQueryTimerResolution(out _, out uint max, out _);
            if (max > desired)
                desired = max;
            //System.Console.WriteLine(max);
            _ = NativeMethods.NtSetTimerResolution(in max, in setresolution, out _);

            return true;
        }

        // The Ultraviolet host.
        private readonly IUltravioletHost host;

        // Current tick state.
        private readonly UltravioletTimeTracker timeTrackerUpdate = new UltravioletTimeTracker();
        private readonly UltravioletTimeTracker timeTrackerDraw = new UltravioletTimeTracker();
        private readonly Stopwatch tickTimer = new Stopwatch();
        private Int64 accumulatedElapsedTime;
        private Int32 lagFrames;
        private Boolean runningSlowly;
        private Boolean forceElapsedTimeToZero;

        // Current system timer resolution.
        private UInt32 systemTimerPeriod;
    }

}