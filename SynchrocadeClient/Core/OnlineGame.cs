﻿using Ultraviolet.Graphics.Graphics2D;
using Ultraviolet.Graphics;
using Ultraviolet.Input;
using Ultraviolet;

using Synchrocade.GameObjects;
using System;
using Ultraviolet.Platform;
using Ultraviolet.Content;
using Ultraviolet.OpenGL;
using Synchrocade.UpdateThread;
using Synchrocade.GameLogics;
using Synchrocade.GameObjects.Input;
using Synchrocade.Networking;
using System.Diagnostics;
using Synchrocade.Screens;
using Synchrocade.Screens.GameScreens;
using Ultraviolet.Core;
using System.Runtime.InteropServices;
using System.Xml.Linq;
using Synchrocade.Screens.UI;
using Ultraviolet.Presentation;
using Ultraviolet.Presentation.Styles;
using Serilog;
using Serilog.Core;
using Synchrocade.Content.Text.Global;
using Synchrocade.Core;
using Synchrocade.GameObjects.Sound;
using Synchrocade.Screens.UI.Lobby;
using Synchrocade.Screens.UI.Panels;
using Ultraviolet.SDL2;
using Ultraviolet.UI;

namespace Synchrocade
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    /// 
    internal class OnlineGame : UltravioletApplication
    {
        private const int targetUPS = 120;
        
        //GraphicsDeviceManager graphics;
        private ContentManager? content;
        UpdateThread.UpdateThread? updateThread;
        static readonly object _memoryIntegrityLock = new object();
        static readonly object _gameScreenCreationDestructionLock = new object();
        public NetworkLoop? networkLoop;
        UIScreenService? screenService;
        private GlobalStyleSheet? globalStyleSheet;

        private readonly bool shouldCompile;
        private readonly bool shouldRun;

        //bool waitingOnScreenCreation = false;

        private readonly bool ignorebadssl;
        private readonly string address;
        private readonly int port;

        private MusicPlayer? musicPlayer;
        internal string? musicString = null;

        internal OnlineGame(bool compile, bool run, bool ignorebadssl, string address, int port) : base("Synchrocade", "Synchrocade")
        {
            shouldCompile = compile;
            shouldRun = run;
            
            this.ignorebadssl = ignorebadssl;
            this.address = address;
            this.port = port;
        }



        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void OnInitializing()
        {
            // TODO: Add your initialization logic here



            base.OnInitializing();
        }



        protected override IUltravioletTimingLogic CreateTimingLogic()
        {
            return new ThreadedTimingLogic(this);
        } 
        //[DebuggerNonUserCode()]
        protected override void OnLoadingContent()
        {
            try
            {
                


                this.TargetElapsedTime = TimeSpan.FromTicks(TimeSpan.TicksPerSecond / 120);

                int framerate = this.Ultraviolet.GetPlatform().Displays.PrimaryDisplay.DesktopDisplayMode.RefreshRate;
                Log.Debug($"framerate:{framerate}");

                if (framerate > 55 && framerate < 260)
                {
                    this.TargetElapsedTime = TimeSpan.FromTicks(TimeSpan.TicksPerSecond / framerate);
                }



                var window = this.Ultraviolet.GetPlatform().Windows.GetPrimary();
                window.SetWindowMode(WindowMode.Windowed);
                window.ClientSize = new Size2(1280,720);
                window.ChangeCompositor(SynchrocadeCompositor.Create(window));
                window.SynchronizeWithVerticalRetrace = false;

                this.Ultraviolet.GetPlatform().Windows.GetPrimary().MinimumClientSize = new Size2(1024,576);
                SynchronizeWithVerticalRetrace = false;
                IsFixedTimeStep = true;
                

                this.content = ContentManager.Create("Content");
                LoadContentManifests(content);
                
                Surface2D icon = content.Load<Surface2D>("UI/icon.png",false);
                this.Ultraviolet.GetPlatform().Windows.GetPrimary().Icon = icon;
                
                var upf = Ultraviolet.GetUI().GetPresentationFoundation();

                upf.RegisterKnownTypes(GetType().Assembly);
                if(shouldCompile)
                    upf.CompileExpressionsIfSupported("Content", CompileExpressionsFlags.IgnoreCache);
                globalStyleSheet = GlobalStyleSheet.Create();
                globalStyleSheet.Append(content, "UI/DefaultUIStyles");
                upf.SetGlobalStyleSheet(globalStyleSheet);
                

                
                upf.LoadCompiledExpressions();




                //realUpdate();
                updateThread = new UpdateThread.UpdateThread(this.RealUpdate, targetUPS, _memoryIntegrityLock);
                

                networkLoop = new NetworkLoop(DeferredScreenCreation, EnableGameScreenUpdating, GameDestruction, CheckForObjectDesynchronization, updateThread);

                screenService = new UIScreenService(content, this, ignorebadssl, address, port);

                musicPlayer = new MusicPlayer((float)screenService.settings.MusicVolume);

                //currentGameScreen = new GameScreen(content, screenService, new SkidstarLogic(), updateThread, networkLoop, _memoryIntegrityLock);

                var screen = screenService.Get<LanConnectionScreen>();
                //currentGameScreen.loadContent();
                Ultraviolet.GetUI().GetScreens().Open(screen, TimeSpan.Zero);
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Exception Passed To Game Content Loading");
                throw;
            }

        }


        internal void SetMusicVolume(float volume)
        {
            if (volume > 2)
            {
                musicPlayer?.SetVolume(volume);
                Ultraviolet.GetAudio().SongsMuted = false;
            }
            else
            {
                Ultraviolet.GetAudio().SongsMuted = true;
            }
        }


        
        
        internal ulong CheckForObjectDesynchronization(int checkFrame, bool verbose)
        {
            if(verbose)
                Log.Error($"Desynchronziation Recheck for frame {checkFrame}");
            
            
            ulong synchronizationNumber = screenService!.GetCurrentGameScreen()!.myGame.CheckForObjectDesynchronization(checkFrame,verbose);
            
            if(verbose)
                Log.Error($"Recalculated synchronization number is {synchronizationNumber}");

            return synchronizationNumber;
        }

        private void DeferredScreenCreation(int players, string[] playerNames, int RandomSeed, int gameId)
        {
            Log.Information($"Creating screen with the random seed {RandomSeed}");
            IGameLogic gameLogic;
            switch (gameId)
            {
                case 1:
                    gameLogic = new SkidstarLogic(RandomSeed);
                    break;
                case 2: 
                    gameLogic = new AcquisitionLogic(RandomSeed);
                    break;
                case 3: 
                    gameLogic = new DropPuzzleLogic(RandomSeed);
                    break;
                default:
                    throw new Exception("Tried to load unknown game at DeferredScreenCreation.");
                    break;
            }
            content?.Ultraviolet.QueueWorkItem(o => CreateGameScreen(players, playerNames, gameLogic));
        }

        private void GameDestruction(bool isError, string message)
        {
            if (screenService?.GetCurrentGameScreen() is null)
            {
                updateThread?.Stop();
                return;
            }
            screenService!.GetCurrentGameScreen()!.GetGameLoop().StopAllSoundsForShutdown();
            screenService!.gameScreenShouldUpdate = false;
            updateThread?.Stop();

            content?.Ultraviolet.QueueWorkItem(delegate(object o)
            {
                lock (_gameScreenCreationDestructionLock)
                {
                    if(Ultraviolet.GetUI().GetScreens().Contains(screenService!.GetCurrentGameScreen()))
                        Ultraviolet.GetUI().GetScreens().Close(screenService!.GetCurrentGameScreen());
                    screenService.SetCurrentGameScreen(null);
                    updateThread = new UpdateThread.UpdateThread(this.RealUpdate, targetUPS, _memoryIntegrityLock);

                    networkLoop = new NetworkLoop(DeferredScreenCreation, EnableGameScreenUpdating, GameDestruction, CheckForObjectDesynchronization, updateThread!);
                    screenService?.DestroyGameScreen();
                    if (isError)
                    {
                        if (Ultraviolet.GetUI().GetScreens().Count > 0)
                        {
                            UIScreen currentScreen = Ultraviolet.GetUI().GetScreens().Peek();
                            if (currentScreen.View?.ViewModelType == typeof(LobbyGameViewModel))
                            {
                                LobbyGameViewModel model = (LobbyGameViewModel) currentScreen.View.ViewModel;
                                model.printGameErrorToChat(message);
                            }
                        }
                    }
                }

            });



        }

        private void EnableGameScreenUpdating()
        {
            screenService!.GetCurrentGameScreen()!.GetGameLoop().GiveLocalInputsToNetLoop();
            screenService!.gameScreenShouldUpdate = true;
        }

        //for now i'm going to put the game loading code here, i am unsure here is really where it belings but
        private GameScreen CreateGameScreen(int players, string[] playerNames, IGameLogic logic)
        {
            lock (_gameScreenCreationDestructionLock)
            {
                logic.LoadContent(content!);

                GameScreen currentGameScreen = new GameScreen(players, playerNames, content!,screenService!, logic, updateThread!, this, _memoryIntegrityLock);
                currentGameScreen.LoadContent();
                screenService!.gameScreenShouldUpdate = false;
                screenService!.SetCurrentGameScreen(currentGameScreen);
                Ultraviolet.GetUI().GetScreens().Open(currentGameScreen, TimeSpan.Zero);
                //waitingOnScreenCreation = false;
                networkLoop!.reportGameStarted();
                return currentGameScreen;
            }

        }

        /// <summary>
        /// Loads the application's content manifests.
        /// </summary>
        /// <param name="content"></param>
        protected virtual void LoadContentManifests(ContentManager content)
        {
            Contract.Require(content, GlobalExceptionText.NullError);

            var uvContent = Ultraviolet.GetContent();

            var contentManifestFiles = content.GetAssetFilePathsInDirectory("Manifests");
            uvContent.Manifests.Load(contentManifestFiles);
            
        }


        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        /// <param name="stalled">If we're stalled out and should not move forward.</param>

        /// 
        //double updateTime = 0;
        //double drawTime = 0;
        //double targetFrameRate = 5;

        //double targetUpdateRate = 120;
        
        
        private readonly Stopwatch swUpdate = new Stopwatch();
        private void RealUpdate(bool stalled)
        {
            //if (swUpdate.ElapsedMilliseconds > 9)
            //    Log.Debug($"ticks since last update {swUpdate.ElapsedMilliseconds}");


            try
            {
                if (!(screenService?.GetCurrentGameScreen() is null) && screenService.gameScreenShouldUpdate && !stalled)
                {
                    screenService!.GetCurrentGameScreen()!.GetGameLoop().GiveLocalInputsToNetLoop();
                }

                //run network update
                networkLoop?.Update(stalled);


                //run gamescreen update
                screenService?.UpdateGameScreen(stalled);
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Exception Passed To Real Update");
                if(Ultraviolet.GetUI().GetScreens().Contains(screenService?.GetCurrentGameScreen()))
                    this.networkLoop?.endGame(true,e.ToString(),true);
            }
            
            swUpdate.Reset();
            swUpdate.Start();
        }
        


        //[DebuggerNonUserCode()]
        protected override void OnUpdating(UltravioletTime gameTime)
        {
            
            if(!(content is null))
                musicString = musicPlayer?.Update(Ultraviolet, content);


            //this is for the exiting for compile only runs
            if(!shouldRun)
            {
                updateThread?.Stop();
                Exit();
            }

            try
            {
                if (this.Ultraviolet.GetInput().GetKeyboard().IsKeyDown(Key.Escape))
                {
                    GameScreen? screen = screenService!.GetCurrentGameScreen();
                    if (!(screen is null))
                    {
                        if(Ultraviolet.GetUI().GetScreens().Contains(screenService.GetCurrentGameScreen()))
                            networkLoop!.endGame(false,"escape pressed",true);
                    }
                    else
                    {
                        //updateThread?.Stop();
                        //Exit();
                    }

                }

                base.OnUpdating(gameTime);
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Exception Passed To UVs Native Update Loop, very strange.");
                throw;
            }
        }

        protected override void OnShutdown()
        {
            globalStyleSheet?.Dispose();
            updateThread?.Stop();
            base.OnShutdown();
        }






        protected override UltravioletContext OnCreatingUltravioletContext()
        {
            var configuration = new SDL2UltravioletConfiguration();
            configuration.Plugins.Add(new OpenGLGraphicsPlugin());
            configuration.Plugins.Add(new PresentationFoundationPlugin());
            configuration.Plugins.Add(new Ultraviolet.FreeType2.FreeTypeFontPlugin());
            configuration.Plugins.Add(new Ultraviolet.BASS.BASSAudioPlugin());
            PopulateConfiguration(configuration);
            //PresentationFoundation.Configure(configuration);


#if DEBUG
            configuration.Debug = true;
            configuration.DebugLevels = DebugLevels.Error | DebugLevels.Warning;
            configuration.DebugCallback = (uv, level, message) =>
            {
                System.Diagnostics.Debug.WriteLine(message);
            };
#endif
            return new SDL2UltravioletContext(this, configuration);
        }
    }
}
