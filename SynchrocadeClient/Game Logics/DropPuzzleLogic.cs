
using System;
using Synchrocade.GameObjects;
using Synchrocade.GameObjects.Input;
using System.Collections.Generic;
using Synchrocade.GameObjects.UniqueGameObjects.DropPuzzleObjects;
using Ultraviolet.Content;
using Ultraviolet.Graphics;

namespace Synchrocade.GameLogics
{

    public class DropPuzzleLogic : IGameLogic
    {
        private static Texture2D? palette;
        private readonly int RandomSeed;
        IPlayerGameObject[] IGameLogic.GetInitialPlayerState(int players, string [] playerNames, OnlineGameLoop owner)
        {
            IPlayerGameObject[] returnList = new IPlayerGameObject[players];

            switch (players)
            {
                case 1:
                    returnList[0] = new DropPuzzlePlayer(playerNames[0],0,1, RandomSeed);
                    break;
                case 2:
                    returnList[0] = new DropPuzzlePlayer(playerNames[0],0,2, RandomSeed);
                    returnList[1] = new DropPuzzlePlayer(playerNames[1],1,2, RandomSeed);
                    break;
                case 3:
                    returnList[0] = new DropPuzzlePlayer(playerNames[0],0,3, RandomSeed);
                    returnList[1] = new DropPuzzlePlayer(playerNames[1],1,3, RandomSeed);
                    returnList[2] = new DropPuzzlePlayer(playerNames[2],2,3, RandomSeed);
                    break;
                case 4:
                    returnList[0] = new DropPuzzlePlayer(playerNames[0],0,4, RandomSeed);
                    returnList[1] = new DropPuzzlePlayer(playerNames[1],1,4, RandomSeed);
                    returnList[2] = new DropPuzzlePlayer(playerNames[2],2,4, RandomSeed);
                    returnList[3] = new DropPuzzlePlayer(playerNames[3],3,4, RandomSeed);
                    break;
                default:
                    throw new Exception("Acquisition Game started with invalid player count");
            }

            
            return returnList;
        }

        IManagerGameObject IGameLogic.GetInitialGameManager(int players, int numberOfRounds)
        {
            return new DropPuzzleGameManager(players);
        }


        public DropPuzzleLogic(int randomSeed)
        {
            this.RandomSeed = randomSeed;
        }
        


        void IGameLogic.LoadContent(ContentManager contentManager)
        {
            DropPuzzlePlayer.LoadContent(contentManager);
        }

        List<IGameObject> IGameLogic.GetInitialGameState(int players, OnlineGameLoop owner)
        {
            //this game will have no non player objects as the player objects will contain complete game boards
            List<IGameObject> returnList = new List<IGameObject>();
            return returnList;
        }

        bool IGameLogic.CanDrawToScale()
        {
            return false;
        }

        int IGameLogic.NativeXResolution()
        {
            return 1280;
        }

        int IGameLogic.NativeYResolution()
        {
            return 720;
        }

        InputObject.EncodeDecodeOptions IGameLogic.GetInputEncodeDecodeOptions()
        {
            return new InputObject.EncodeDecodeOptions(true, false, 3);
        }


        Texture2D? IGameLogic.Palette()
        {
            return palette;
        }
    

    }
}