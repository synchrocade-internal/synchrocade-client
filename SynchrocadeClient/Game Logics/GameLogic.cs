﻿using Ultraviolet;
using System.Collections.Generic;
using Ultraviolet.Content;
using Synchrocade.GameObjects.Input;
using Synchrocade.GameObjects;
using Ultraviolet.Graphics;

namespace Synchrocade.GameLogics
{
    internal interface IGameLogic
    {
         internal Texture2D? Palette();
         internal List<IGameObject> GetInitialGameState(int players, OnlineGameLoop owner);
         internal IPlayerGameObject[] GetInitialPlayerState(int players, string[] playerNames, OnlineGameLoop owner);

         internal IManagerGameObject GetInitialGameManager(int players, int roundsToWin);
         
         internal void LoadContent(ContentManager content);
         internal bool CanDrawToScale();
         internal int NativeXResolution();
         internal int NativeYResolution();

         internal InputObject.EncodeDecodeOptions GetInputEncodeDecodeOptions();
    }
}
