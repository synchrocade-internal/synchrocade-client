﻿using Synchrocade.GameObjects;
using Synchrocade.GameObjects.Input;
using System.Collections.Generic;
using Synchrocade.GameObjects.UniqueGameObjects.SkidstarObjects;
using Ultraviolet.Content;
using Ultraviolet.Graphics;

namespace Synchrocade.GameLogics
{
    class SkidstarLogic : IGameLogic
    {
        private static Texture2D? palette;
        private readonly int RandomSeed;
        IPlayerGameObject[] IGameLogic.GetInitialPlayerState(int players, string [] playerNames, OnlineGameLoop owner)
        {
            IPlayerGameObject[] returnList = new IPlayerGameObject[players];
            if (players >0)
                returnList[0] = new SkidstarShip(200, 200, 0,0, owner);
            if (players >1)
                returnList[1] = new SkidstarShip(1080, 520, 600,1, owner);
            
            return returnList;
        }

        IManagerGameObject IGameLogic.GetInitialGameManager(int players, int numberOfRounds)
        {
            return new SkidstarGameManager(players, numberOfRounds);
        }


        public SkidstarLogic(int randomSeed)
        {
            this.RandomSeed = randomSeed;
        }
        

        void IGameLogic.LoadContent(ContentManager content)
        {
            if(palette is null)
                palette = content.Load<Texture2D>("Unique/Skidstar/Graphics/palette");
            SkidstarShip.LoadContent(content);
            SkidstarBullet.LoadContent(content);
            SkidstarStarfield.LoadContent(content);
            SkidstarExplosionEffect.LoadContent(content);
        }

        List<IGameObject> IGameLogic.GetInitialGameState(int players, OnlineGameLoop owner)
        {
            List<IGameObject> returnList = new List<IGameObject>();
            returnList.Add(new SkidstarStarfield(RandomSeed, ((IGameLogic) this).NativeXResolution(), ((IGameLogic) this
                ).NativeYResolution()));
            return returnList;
        }

        bool IGameLogic.CanDrawToScale()
        {
            return false;
        }

        int IGameLogic.NativeXResolution()
        {
            return 1280;
        }

        int IGameLogic.NativeYResolution()
        {
            return 720;
        }

        InputObject.EncodeDecodeOptions IGameLogic.GetInputEncodeDecodeOptions()
        {
            return new InputObject.EncodeDecodeOptions(true, false, 3);
        }


        Texture2D? IGameLogic.Palette()
        {
            return palette;
        }
    }
}
