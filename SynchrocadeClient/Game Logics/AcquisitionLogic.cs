
using System;
using Synchrocade.GameObjects;
using Synchrocade.GameObjects.Input;
using System.Collections.Generic;
using Synchrocade.GameObjects.UniqueGameObjects.AcquisitionObjects;
using Synchrocade.ReusableFunctions;
using Ultraviolet.Content;
using Ultraviolet.Graphics;

namespace Synchrocade.GameLogics
{

    public class AcquisitionLogic : IGameLogic
    {
        private static Texture2D? palette;
        private readonly int RandomSeed;
        IPlayerGameObject[] IGameLogic.GetInitialPlayerState(int players, string [] playerNames, OnlineGameLoop owner)
        {
            IPlayerGameObject[] returnList = new IPlayerGameObject[players];

            switch (players)
            {
                case 1:
                    returnList[0] = new AcquisitionPlayer(playerNames[0],0,0,0, owner);
                    break;
                case 2:
                    returnList[0] = new AcquisitionPlayer(playerNames[0],0,0,0, owner);
                    returnList[1] = new AcquisitionPlayer(playerNames[1],62,32,1, owner);
                    break;
                case 3:
                    returnList[0] = new AcquisitionPlayer(playerNames[0],31,0,0, owner);
                    returnList[1] = new AcquisitionPlayer(playerNames[1],0,32,1, owner);
                    returnList[2] = new AcquisitionPlayer(playerNames[2],62,32,2, owner);
                    break;
                case 4:
                    returnList[0] = new AcquisitionPlayer(playerNames[0],0,0,0, owner);
                    returnList[1] = new AcquisitionPlayer(playerNames[1],62,0,1, owner);
                    returnList[2] = new AcquisitionPlayer(playerNames[2],0,32,2, owner);
                    returnList[3] = new AcquisitionPlayer(playerNames[3],62,32,3, owner);
                    break;
                default:
                    throw new Exception("Acquisition Game started with invalid player count");
            }

            
            return returnList;
        }

        IManagerGameObject IGameLogic.GetInitialGameManager(int players, int numberOfRounds)
        {
            return new AcquisitionGameManager(players);
        }


        public AcquisitionLogic(int randomSeed)
        {
            this.RandomSeed = randomSeed;
        }
        


        void IGameLogic.LoadContent(ContentManager contentManager)
        {
            AcquisitionPlayer.LoadContent(contentManager);
            AcquisitionGameBall.LoadContent(contentManager);
        }

        List<IGameObject> IGameLogic.GetInitialGameState(int players, OnlineGameLoop owner)
        {
            List<IGameObject> returnList = new List<IGameObject>();
            
            ClonableRandom rand = new ClonableRandom(RandomSeed);
            int topBottomDistanceFromFloor = rand.Next(3, 29);
            int topBottomDistanceFromWall = rand.Next(3, 59);

            
            int leftRightDistanceFromFloor = rand.Next(3, 29);
            int leftRightDistanceFromWall = rand.Next(3, 59);


            
            returnList.Add(new AcquisitionGameBall(topBottomDistanceFromWall,topBottomDistanceFromFloor, AcquisitionBallHeading.DownRight, owner));
            returnList.Add(new AcquisitionGameBall(62- topBottomDistanceFromWall,32 - topBottomDistanceFromFloor, AcquisitionBallHeading.UpLeft, owner));
            
            returnList.Add(new AcquisitionGameBall(leftRightDistanceFromWall,leftRightDistanceFromFloor, AcquisitionBallHeading.DownLeft, owner));
            returnList.Add(new AcquisitionGameBall(62 - leftRightDistanceFromWall,32 - leftRightDistanceFromFloor, AcquisitionBallHeading.UpRight, owner));

            
            

            

            return returnList;
        }

        bool IGameLogic.CanDrawToScale()
        {
            return true;
        }

        int IGameLogic.NativeXResolution()
        {
            return 1280;
        }

        int IGameLogic.NativeYResolution()
        {
            return 720;
        }

        InputObject.EncodeDecodeOptions IGameLogic.GetInputEncodeDecodeOptions()
        {
            return new InputObject.EncodeDecodeOptions(true, false, 0);
        }


        Texture2D? IGameLogic.Palette()
        {
            return palette;
        }
    

    }
}