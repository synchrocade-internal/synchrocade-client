﻿#version 130

uniform sampler2D Texture;
uniform sampler2D Palette;
uniform vec2 TextureSize;

in vec2 vTextureCoordinate;
in vec4 vColor;
flat in int paletteId;
flat in int shouldUsePalette;

out vec4 fColor;


void main()
{
	vec4 color = texelFetch(Texture, ivec2(vTextureCoordinate.x,TextureSize.y - vTextureCoordinate.y), 0);
	ivec2 index = textureSize(Palette,0);
	vec4 fetchedColor = texelFetch(Palette,ivec2(color.r*256,paletteId),0);
	//i use color mixing that is either exactly 0 or 1 in order to avoid any branching to make this super fast
	vec4 tempColor = mix(color, vec4(fetchedColor.rgb, color.a), shouldUsePalette);
	fColor =  mix(tempColor, vec4(vColor.rgb, tempColor.a), vColor.a);
}