﻿#version 130

uniform mat4 MatrixTransform;
uniform vec2 TextureSize;
uniform bool SrgbColor;

in vec4 uv_Position0;
in vec4 uv_TextureCoordinate0;
in vec4 uv_Color0;

out vec2 vTextureCoordinate;
out vec4 vColor;
flat out int paletteId;
flat out int shouldUsePalette;


void main()
{
	gl_Position        = uv_Position0 * MatrixTransform;
	vTextureCoordinate = vec2(uv_TextureCoordinate0.x, uv_TextureCoordinate0.y);
	paletteId = int(uv_TextureCoordinate0.z);
	shouldUsePalette = int(uv_TextureCoordinate0.w);
	vColor = uv_Color0;
}