﻿using Ultraviolet.Graphics.Graphics2D;
using System;
using System.Collections.Generic;
using Synchrocade.GameObjects;
using Synchrocade.GameObjects.Sound;

namespace Synchrocade.GameObjects
{
    internal interface IGameObject : ICloneable
    {
        int DeletedFrame();
        void Update(OnlineGameLoop caller);
        void PlaySounds(NetSoundManager soundManager, int currentFrame);
        void Draw(PaletteEffectSpriteBatch context, float percentToNextFrame, IGameObject? nextFrame);
        (bool isColliding, ICollisionDetails? details) CheckCollision(IGameObject otherObject); //see if the 2 objects are colliding
        //true if colliding
        void AddCollisionCommand(IGameObject otherObject, ICollisionDetails? details, bool performedCollisionCheck); //do the action that should be done because of the 2 objects colliding, must be able to happen in any order
        void ExecuteCollisionCommands(OnlineGameLoop caller);
        ulong CalculateDesynchronizationCheckValue(bool verbose);
        HashSet<Type> GetCollidableClasses();
    }
}
