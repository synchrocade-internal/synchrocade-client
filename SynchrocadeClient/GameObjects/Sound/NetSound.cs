﻿using Synchrocade.GameObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ultraviolet.Audio;

namespace Synchrocade.GameObjects.Sound
{
    internal class NetSound : ICloneable
    {
        internal readonly OnlineGameLoop owner;
        internal readonly SoundEffect effect;
        internal NetSoundSharedPlayState sharedPlayState = new NetSoundSharedPlayState();
        internal bool verified = true;
        internal bool looping = false;
        internal int playFrame = -1;
        internal bool isPlaying = false;
        internal float pan = 0;
        internal float volume = 1;
        internal float pitch = 0;


        private bool isPlayingLocally = false;
        //private int lastStopCommandFrame = -1;
        private int lastPlayCommandFrame = -1;

        internal void play()
        {
            isPlayingLocally = true;
            lastPlayCommandFrame = owner.GetCurrentFrame();
        }
        internal void stop()
        {
            playFrame = -1;
            isPlayingLocally = false;
            //lastStopCommandFrame = owner.GetCurrentFrame();
        }

        internal NetSound(SoundEffect effect, OnlineGameLoop owner)
        {
            this.owner = owner;
            this.effect = effect;
        }
        internal void update()
        {
            if(isPlayingLocally && lastPlayCommandFrame > sharedPlayState.finishedFrame)
                playFrame++;

            if (isPlayingLocally && lastPlayCommandFrame <= sharedPlayState.finishedFrame)
            {
                stop();
            }
        }

        internal void doPlayback(NetSoundManager soundManager)
        {
            if (isPlayingLocally && lastPlayCommandFrame > sharedPlayState.finishedFrame)
            {
                bool didPlay = soundManager.playOrContinueSound(this, owner.GetCurrentFrame());
                if (!didPlay)
                {
                    sharedPlayState.finishedFrame = owner.GetCurrentFrame();
                }
                    
            }

            if(isPlayingLocally && lastPlayCommandFrame <= sharedPlayState.finishedFrame)
            {
                stop();
            }
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
