﻿using System;
using System.Collections.Generic;
using Synchrocade.Settings;
using Ultraviolet.Audio;

namespace Synchrocade.GameObjects.Sound
{
    internal class NetSoundManager
    {
        private SettingsContainer settings;
        Dictionary<(Object handle, SoundEffect effect), (SoundEffectPlayer player, NetSound details)> soundList;

        internal NetSoundManager(SettingsContainer settings)
        {
            this.settings = settings;
            soundList = new Dictionary<(Object handle, SoundEffect effect), (SoundEffectPlayer player, NetSound details)>();
        }
        internal void unverifyAll()
        {
            foreach (KeyValuePair< (Object handle, SoundEffect), (SoundEffectPlayer player, NetSound details)> item in soundList) {
                item.Value.details.verified = false;
            }
        }

        internal void removeAllUnverified()
        {
            List<(Object handle, SoundEffect effect)> ItemsToDelete = new List<(Object handle, SoundEffect effect)>();
            foreach (KeyValuePair<(Object handle, SoundEffect), (SoundEffectPlayer player, NetSound details)> item in soundList) {
                if(item.Value.details.verified == false)
                {
                    ItemsToDelete.Add(item.Key);
                }
            }

            foreach ((Object handle, SoundEffect) key in ItemsToDelete)
            {
                soundList[key].player.Stop();
                soundList[key].details.isPlaying = false;
                soundList[key].player.Dispose();
                soundList.Remove(key);
            }
        }


        internal bool playOrContinueSound(NetSound sound, int currentFrame)
        {
            (SoundEffectPlayer player, NetSound details) workingObject;
            if (soundList.TryGetValue((sound.sharedPlayState, sound.effect), out (SoundEffectPlayer player, NetSound details) existingObject))
            {
                //if the object exists in our dictionary we're continuing it
                workingObject = existingObject;

                if (workingObject.details.looping != sound.looping)
                {
                    workingObject.details.looping = sound.looping;
                    workingObject.player.IsLooping = sound.looping;
                }

                if (workingObject.details.pitch != sound.pitch)
               {
                    workingObject.details.pitch = sound.pitch;
                    workingObject.player.Pitch = sound.pitch;
                }


                if (workingObject.details.volume != sound.volume)
                {
                    workingObject.details.volume = sound.volume;
                    workingObject.player.Volume = sound.volume;

                }

                if (workingObject.details.pan != sound.pan)
                {
                    workingObject.details.pan = sound.pan;
                    workingObject.player.Pan = sound.pan;

                }


                /*if (workingObject.details.playFrame != sound.playFrame - 1) seeking within bass appears to be unsupported
                {
                    workingObject.player.Position = calculatePositionFromFrame(workingObject);
                }*/
                workingObject.details.playFrame = sound.playFrame;


                workingObject.details.verified = true;

            }
            else
            {
                workingObject = (SoundEffectPlayer.Create(), new NetSound(sound.effect, sound.owner));
                workingObject.details.looping = sound.looping;
                workingObject.details.playFrame = sound.playFrame;


                workingObject.details.pitch = sound.pitch;


                workingObject.details.volume = sound.volume;


                workingObject.details.pan = sound.pan;
                //workingObject.player.Play(sound.effect, sound.looping);
                workingObject.player.Play(sound.effect, sound.volume * (float)(settings.SoundVolume / 100), sound.pitch, sound.pan, sound.looping);

                if (sound.playFrame > 5)
                {
                    TimeSpan caluclatedPosition = calculatePositionFromFrame(workingObject);
                    if (workingObject.player.Duration > caluclatedPosition)
                        workingObject.player.Position = calculatePositionFromFrame(workingObject);
                    else
                        workingObject.player.Stop();
                }
                else
                {
                    //
                }


                soundList.Add((sound.sharedPlayState, sound.effect), workingObject);

                workingObject.details.verified = true;

            }
            if (workingObject.player.IsPlaying == false)
            {
                sound.isPlaying = false;
                workingObject.details.verified = false;
                return false;
            }
            sound.isPlaying = true;
            return true;
        }

        private static TimeSpan calculatePositionFromFrame((SoundEffectPlayer player, NetSound details) sound)
        {
            return TimeSpan.FromSeconds(((double)sound.details.playFrame * (1f / 120f)) % sound.details.effect.Duration.TotalSeconds);
        }

        
        internal void StopAllSoundsForShutdown()
        {
            foreach (KeyValuePair< (Object handle, SoundEffect), (SoundEffectPlayer player, NetSound details)> item in soundList) {
                item.Value.player.Stop();
            }
        }
    }
}
