using System;
using System.Collections.Generic;
using System.Linq;
using Ultraviolet;
using Ultraviolet.Audio;
using Ultraviolet.Content;
using Ultraviolet.Graphics;

namespace Synchrocade.GameObjects.Sound
{

    internal static class MusicList
    {
        internal static List<(bool safe, string artist, string title, string location)>  GetMusicList()
        {
            return new List<(bool safe, string artist, string title, string location)>
            {
                (false, "Kenny690", "High Five", "/Jamendo/1132160-Kenny690-High Five.ogg"),
                (false, "Ben Drake", "16 Cracked", "/Jamendo/120999-bendrake-16 Cracked.ogg"),
                (false, "Alexey Anisimov", "Super Game", "/Jamendo/1301567-Alexey_Anisimov_(2)-Super Game.ogg"),
                (false, "Fortadelis", "Wavefunction Collapse", "/Jamendo/1105474-tomislav.ocvirek-Wavefunction Collapse.ogg"),
                (false, "Miami Sheriff", "Along the Palm's Coast", "/Jamendo/1417287-Miami_Sheriff-Along the Palm's Coast.ogg"),
                (false, "SilverBoxStudio", "Fantasia Synthwave", "/Jamendo/1572405-Mark_Dee-Fantasia Synthwave.ogg"),
                (false, "Paul Werner", "Micro Movement", "/Jamendo/1739877-PaulWernerMusic-Micro Movement.ogg"),
                (false, "SharpFourStudio", "Futuristic Dark Technology", "/Jamendo/1780025-SharpFourStudio-Futuristic Dark Technology.ogg"),
                (false, "Alexey Anisimov", "Sci-Fi Soundtrack", "/Jamendo/1792048-Alexey_Anisimov_(2)-Sci-Fi Soundtrack.ogg"),
                (false, "Cem Music Project", "Traveling Beyond the Closest Stars", "/Jamendo/1819165-Cem_Music_Project-Traveling Beyond the Closest Stars.ogg"),
                (false, "AlexZavesa", "Cinematic Epic Thriller", "/Jamendo/1847536-AlexZavesa-Cinematic Epic Thriller.ogg"),
                (false, "Cyborgjeff", "I wanna go to Hopeland", "/Jamendo/1850096-cyborgjeff-I wanna go to Hopeland.ogg"),
                (false, "Andrey Avkhimovich", "Severed Triangles", "/Jamendo/763459-Andrey_Avkhimovich-Severed Triangles.ogg"),
                (false, "LukHash", "Eighty-five", "/Jamendo/1575621-SH_MUSIC_(lukhash)-Eighty-five.ogg"),
            };
            
        }
    }
    
    
    internal class MusicPlayer
    {
        private const float SoundDevisor = 125f;

        private List<(bool safe, string artist, string title, string location)> playList;
        private int currentTrack = 0;
        private Song? currentSong;
        private SongPlayer songPlayer;
        private int frameTimer = 0;

        private float volume = 0;

        internal void SetVolume(float volume)
        {
            this.volume = volume;
            if(songPlayer.IsPlaying)
                songPlayer.Volume = volume / SoundDevisor;
        }
        
        
        
        internal MusicPlayer(float volume)
        {
            songPlayer = SongPlayer.Create();
            SetVolume(volume);
            playList = MusicList.GetMusicList();
            shufflePlaylist(false);
        }


        internal int shufflePlaylist(bool noShuffleFinalThird)
        {
            if (!noShuffleFinalThird)
            {
                Random random = new Random();
                playList = playList.OrderBy(item => random.Next()).ToList();
                return 0;
            }
            else
            {
                int secondThirdSplit = playList.Count * 2 / 3;
                
                var savedList = playList.GetRange(secondThirdSplit, playList.Count - secondThirdSplit);

                var shuffleList = playList.GetRange(0, secondThirdSplit);
                
                Random random = new Random();
                shuffleList = shuffleList.OrderBy(item => random.Next()).ToList();
                int trackCount = savedList.Count;
                
                savedList.AddRange(shuffleList);
                playList = savedList;

                return trackCount;

            }

        }


        private void PlaySong(ContentManager content, string path)
        {
            currentSong?.Dispose();
            currentSong = content.Load<Song>("Music" + path);
            songPlayer.Play(currentSong,volume / SoundDevisor,0f,0f,false);
            frameTimer = 60 * 6;
        }

        private string GetSongString((bool safe, string artist, string title, string location) input)
        {
            return $"{input.artist} - {input.title}";
        }
        
        internal string? Update(UltravioletContext uv, ContentManager content)
        {
            if (uv.GetAudio().SongsMuted)
                return null;
            
            if (currentSong == null)
            {
                PlaySong(content, playList[0]!.location);
                currentTrack = 0;
            }
            //check the current track to see if it has finished playing
            else if (songPlayer.IsPlaying == false)
            {
                //if so load the next track
                if (currentTrack + 1 < playList.Count)
                {
                    currentTrack += 1;

                }
                else
                {
                    currentTrack = shufflePlaylist(true);
                }
                PlaySong(content, playList[currentTrack]!.location);

            }

            if (frameTimer > 0)
            {
                frameTimer--;
                return GetSongString(playList[currentTrack]!);
            }

            return null;
        }


    }
}