﻿using Synchrocade.GameObjects.Input;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace Synchrocade.GameObjects.Input
{
    internal class InputList: IEnumerable<(InputObject input, byte verified, short? myFrameDistance, short? opponentFrameDistance)>
    {
        //need to decide if we store varified frames for everything, for now let's just do that
        private readonly List<(InputObject input,byte verificationCount, short? myFrameDistance, short? opponentFrameDistance)> storage = new List<(InputObject input, byte verified, short? myFrameDistance, short? opponentFrameDistance)>();
        private int lastVerifiedFrame = -1;
        private int lowestModifiedInputFrameSinceLastCall = int.MaxValue;
        private int requiredVerificationFrames = 0; //changed if a localplayer or not
        

        internal void SetNetworkedPlayer(bool isNetworkedPlayer)
        {
            //isLocalPlayer = localPlayer;
            if (!isNetworkedPlayer)
            {
                requiredVerificationFrames = 1; //i need 1 verified frame because i add objects as unverified by indexing
            }
            else
            {
                requiredVerificationFrames = 2; //i need one frame to set it, and then a second identical frame to know it's good
            }
        }
        internal int GetLastVerifiedFrame()
        {
            return lastVerifiedFrame;
        }

        internal int GetLowestModifiedInputFrameSinceLastCall()
        {
            int returnvar = lowestModifiedInputFrameSinceLastCall;
            lowestModifiedInputFrameSinceLastCall = int.MaxValue;
            return returnvar;
        }

        //if there's 0 verification count, this means that the input was generated, generated inputs should be resimulated when the inputs before them change
        internal void ResimulateAllGeneratedInputsWithNewestValues()
        {
            InputObject lastVerifiedIO;
            if (lastVerifiedFrame>=0)
                lastVerifiedIO = storage[lastVerifiedFrame].input;
            else
            {
                lastVerifiedIO = new InputObject();
            }

            for(int x = lastVerifiedFrame + 1; x<storage.Count;x++)
            {
                if(storage[x].verificationCount <= 0)
                {
                    if(!storage[x].input.Equals(lastVerifiedIO))
                    {
                        storage[x] = (lastVerifiedIO, 0, null, null);
                    }
                }
                else
                {
                    lastVerifiedIO = storage[x].input;
                }
            }
        }

        internal void SetLowestModifiedInputIfNeeded(int newval)
        {
            if(newval < lowestModifiedInputFrameSinceLastCall)
            {
                lowestModifiedInputFrameSinceLastCall = newval;
            }
        }


        internal (InputObject input, bool verified, short? myFrameDistance, short? opponentFrameDistance) this[int index]
        {
            get
            {
                PlaceDuplicatesToRequestedFrame(index);
                return (storage[index].input,storage[index].verificationCount > requiredVerificationFrames, storage[index].myFrameDistance, storage[index].opponentFrameDistance);
            }
            set
            {
                PlaceDuplicatesToRequestedFrame(index);
                //i'm loking for something too far away, so create clones up to the location
                bool alreadyVerified = false;
                if (storage[index].verificationCount >= requiredVerificationFrames)
                    alreadyVerified = true;
                if(!alreadyVerified)
                    SetLowestModifiedInputIfNeeded(index);
                if (storage[index].input.Equals(value.input))
                {

                    byte newVerificationFrames = storage[index].verificationCount;
                    if (value.verified)
                    {
                        newVerificationFrames++;

                    }
                    storage[index] = (value.input, newVerificationFrames, !(storage[index].myFrameDistance is null) ? storage[index].myFrameDistance : value.myFrameDistance, !(storage[index].opponentFrameDistance is null) ? storage[index].opponentFrameDistance : value.opponentFrameDistance);
                    if (newVerificationFrames >= requiredVerificationFrames)
                    {
                        if (!alreadyVerified)
                        {
                            UpdateLastVerifiedFrame();
                        }
                    }
                }
                else
                {
                    if (alreadyVerified)
                        return;
                    storage[index] = (value.input, (byte)(value.verified?1:0), value.myFrameDistance, value.opponentFrameDistance);
                    if (value.verified)
                    {
                        if(requiredVerificationFrames <= 1)
                            UpdateLastVerifiedFrame();
                    }
                }
               
            }
        }

        internal void PlaceDuplicatesToRequestedFrame(long checkedIndex)
        {
            if(storage.Count == 0)
            {
                Add(new InputObject(), false, null, null);
            }
            var lastFrame = storage[storage.Count - 1];
            while(storage.Count <= checkedIndex)
            {
                Add(lastFrame.input.Clone(), false, null, null);
                
            }

        }

        internal void Add(InputObject input, bool verified, short? myFrameDistance, short? opponentFrameDistance)
        {
            
            storage.Add((input:input, verificationCount: (byte)(verified?1:0), myFrameDistance: myFrameDistance, opponentFrameDistance: opponentFrameDistance));
            if (verified)
            {
                UpdateLastVerifiedFrame();
            }
        }

        internal void UpdateLastVerifiedFrame()
        {

            while (lastVerifiedFrame+1 < storage.Count && storage[lastVerifiedFrame+1].verificationCount >= requiredVerificationFrames)
            {
                lastVerifiedFrame++;
            }

        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<(InputObject input, byte verified, short? myFrameDistance, short? opponentFrameDistance)>)storage).GetEnumerator();
        }

        IEnumerator<(InputObject input, byte verified, short? myFrameDistance, short? opponentFrameDistance)> IEnumerable<(InputObject input, byte verified, short? myFrameDistance, short? opponentFrameDistance)>.GetEnumerator()
        {
            return ((IEnumerable<(InputObject input, byte verified, short? myFrameDistance, short? opponentFrameDistance)>)storage).GetEnumerator();
        }

        public (InputObject input, byte verified, short? myFrameDistance, short? opponentFrameDistance) Last()
        {
            return storage.Last();
        }
        
         
    }
}
