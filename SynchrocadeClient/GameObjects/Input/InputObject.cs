﻿
using Synchrocade.ReusableFunctions;
using System;

namespace Synchrocade.GameObjects.Input
{

    internal struct InputObject : IEquatable<InputObject>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1028:Enum Storage should be Int32", Justification = "I serialize this over the network, so it's size is important")]
        internal enum Direction : byte //up+down not allowed, left+right not allowed
        {
            up = 1,
            right = 2,
            down = 4,
            left = 8,
        }

        internal enum DType
        {
            Move,
            Aim
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1028:Enum Storage should be Int32", Justification = "I serialize this over the network, so it's size is important")]
        internal enum Button : byte
        {
            //higest order bits should be encoded as the high part of the byte, to make encode decode far easier
            but8 = 1,
            but7 = 2,
            but6 = 4,
            but5 = 8,
            but4 = 16,
            but3 = 32,
            but2 = 64,
            but1 = 128,
        }
        internal byte dmoveStorage;
        internal byte daimStorage;
        internal byte buttonStorage;

        internal int GetInputSynchronizationTestValue()
        {
            return dmoveStorage + daimStorage + buttonStorage;
        }
        internal bool GetButtonPressed(Button button)
        {
            return (buttonStorage & (byte)button) != 0;
        }
        internal void SetButtonPressed(Button button, bool newState)
        {
            if (newState)
                buttonStorage = (byte)(buttonStorage | (byte)button);
            else
                buttonStorage = (byte)(buttonStorage & ~(byte)button);
        }

        internal bool GetDirectionPressed(Direction button, DType type)
        {
            byte comparisonVar = 0;
            switch (type)
            {
                case DType.Move:
                    comparisonVar = dmoveStorage;
                    break;
                case DType.Aim:
                    comparisonVar = daimStorage;
                    break;
            }
            return (comparisonVar & (byte)button) != 0;
        }


        internal void SetDirectionPressed(Direction button, DType type, bool newState)
        {
            if (button == Direction.up && newState == true)
            {
                if (GetDirectionPressed(Direction.down,type))
                {
                    SetDirectionPressed(Direction.up, type, false);
                    SetDirectionPressed(Direction.down, type, false);
                    return;
                }
            }
            else if (button == Direction.down && newState == true)
            {
                if (GetDirectionPressed(Direction.up, type))
                {
                    SetDirectionPressed(Direction.up, type, false);
                    SetDirectionPressed(Direction.down, type, false);
                    return;
                }
            }
            else if (button == Direction.left && newState == true)
            {
                if (GetDirectionPressed(Direction.right, type))
                {
                    SetDirectionPressed(Direction.left, type, false);
                    SetDirectionPressed(Direction.right, type, false);
                    return;
                }
            }
            else if (button == Direction.right && newState == true)
            {
                if (GetDirectionPressed(Direction.left, type))
                {
                    SetDirectionPressed(Direction.left, type, false);
                    SetDirectionPressed(Direction.right, type, false);
                    return;
                }
            }

            switch (type)
            {
                case DType.Move:
                    if (newState)
                        dmoveStorage = (byte)(dmoveStorage | (byte)button);
                    else
                        dmoveStorage = (byte)(dmoveStorage & ~(byte)button);
                    break;
                case DType.Aim:
                    if (newState)
                        daimStorage = (byte)(daimStorage | (byte)button);
                    else
                        daimStorage = (byte)(daimStorage & ~(byte)button);
                    break;
            }

        }


        internal void EncodeInputs(ref byte[] inputArray, ref uint currentBit, EncodeDecodeOptions options)
        {
            if (options.DMoveEnabled)
            {
                //currently this makes the HARD ASSUMPTION that i have not placed bits, and this will need to change if i ever add anything before the movement items
                var dmoveOutputs = EncodeDirection(DType.Move);
                BitShift.AddToArray(ref inputArray, currentBit, dmoveOutputs.outputByte, dmoveOutputs.currentBit, true);
                currentBit += dmoveOutputs.currentBit;
            }
            if (options.DAimEnabled)
            {
                var daimOutputs = EncodeDirection(DType.Aim);
                BitShift.AddToArray(ref inputArray, currentBit, daimOutputs.outputByte, daimOutputs.currentBit, true);
                currentBit += daimOutputs.currentBit;
            }

            BitShift.AddToArray(ref inputArray, currentBit, buttonStorage, options.NumberOfButtons, true);
            currentBit += options.NumberOfButtons;

        }

        //after this is done, make a couple unit tests that test this entire functionality at once, and then whatever unit tests are needed to fix the issue
        internal void DecodeInputs(byte[] inputBytes, ref uint currentBit, EncodeDecodeOptions options)
        {
            //decode dmove
            if (options.DMoveEnabled)
            {
                DecodeDirection(ref inputBytes, ref currentBit, DType.Move);
            }
            if (options.DAimEnabled)
            {
                DecodeDirection(ref inputBytes, ref currentBit, DType.Aim);
            }
            buttonStorage = BitShift.ReadSingleByteFromArray(inputBytes, currentBit, options.NumberOfButtons);
            currentBit += options.NumberOfButtons;
        }

        internal void DecodeDirection(ref byte[] inputBytes, ref uint currentBit, DType type)
        {
            //if the starting bit is true, we have input
            if (!BitShift.ReadBitFromArray(inputBytes, currentBit))
            {
                currentBit++;
                return;
            }
            else
            {
                byte dmoveByte = BitShift.ReadSingleByteFromArray(inputBytes, currentBit, 4);
                currentBit += 4;
                switch (dmoveByte)
                {
                    //up 1000 
                    case (byte)0b1000_0000:
                        this.SetDirectionPressed(Direction.up, type, true);
                        break;
                    //right 1001
                    case (byte)0b1001_0000:
                        this.SetDirectionPressed(Direction.right, type, true);
                        break;

                    //down 1010
                    case (byte)0b1010_0000:
                        this.SetDirectionPressed(Direction.down, type, true);
                        break;
                    //left 1011
                    case (byte)0b1011_0000:
                        this.SetDirectionPressed(Direction.left, type, true);
                        break;
                    //up-right 1100
                    case (byte)0b1100_0000:
                        this.SetDirectionPressed(Direction.up, type, true);
                        this.SetDirectionPressed(Direction.right, type, true);

                        break;
                    //down-right 1101
                    case (byte)0b1101_0000:
                        this.SetDirectionPressed(Direction.down, type, true);
                        this.SetDirectionPressed(Direction.right, type, true);
                        break;
                    //down-left 1110
                    case (byte)0b1110_0000:
                        this.SetDirectionPressed(Direction.down, type, true);
                        this.SetDirectionPressed(Direction.left, type, true);
                        break;
                    //up-left 1111
                    case (byte)0b1111_0000:
                        this.SetDirectionPressed(Direction.up, type, true);
                        this.SetDirectionPressed(Direction.left, type, true);
                        break;
                }
            }

        }
        internal struct EncodeDecodeOptions
        {
            internal bool DMoveEnabled { get; }
            internal bool DAimEnabled { get; }
            internal uint NumberOfButtons { get; }
            internal EncodeDecodeOptions(bool dMoveEnabled, bool dAimEnabled, byte numberOfButtons)
            {
                this.DMoveEnabled = dMoveEnabled;
                this.DAimEnabled = dAimEnabled;
                this.NumberOfButtons = numberOfButtons;
            }
        }

        internal InputObject Clone()
        {
            InputObject returnObject = new InputObject
            {
                buttonStorage = buttonStorage,
                daimStorage = buttonStorage,
                dmoveStorage = buttonStorage
            };
            return returnObject;
        }


        internal (uint currentBit, byte outputByte) EncodeDirection(DType type)
        {
            //encoding of buttons

            //no buttons - 0 (nothing else)
            //up 1000
            //right 1001
            //down 1010
            //left 1011
            //up-right 1100
            //down-right 1101
            //down-left 1110
            //up-left 1111
            if (GetDirectionPressed(Direction.up,type))
            {
                if (GetDirectionPressed(Direction.left, type))
                {
                    return (4, 0b1111_0000);//up-left 1111
                }
                else if (GetDirectionPressed(Direction.right, type))
                {
                    return (4, 0b1100_0000);//up-right 1100
                }
                return (4, 0b1000_0000);//up 1000
            }
            else if (GetDirectionPressed(Direction.down, type))
            {
                if (GetDirectionPressed(Direction.left, type))
                {
                    return (4, 0b1110_0000); //down-left 1110
                }
                else if (GetDirectionPressed(Direction.right, type))
                {
                    return (4, 0b1101_0000); //down-right 1101
                }
                return (4, 0b1010_0000);//down 1010
            }
            else if (GetDirectionPressed(Direction.left, type))
            {
                return (4, 0b1011_0000);//left 1011
            }
            else if (GetDirectionPressed(Direction.right, type))
            {
                return (4, 0b1001_0000);//right 1001
            }
            else
            {
                //no inputs, which leaves the byte as a full 0, but 1 bit is used to say that this byte is empty
                return (1, 0b0000_0000);
            }
        }

        bool IEquatable<InputObject>.Equals(InputObject other)
        {
            return (other.buttonStorage == this.buttonStorage && other.daimStorage == this.daimStorage && other.dmoveStorage == this.dmoveStorage);
        }

        public override bool Equals(object obj)
        {
            return obj is InputObject && ((IEquatable<InputObject>)this).Equals((InputObject)obj);
        }

        public override int GetHashCode()
        {
            return buttonStorage + daimStorage + dmoveStorage;
        }
    }
}
