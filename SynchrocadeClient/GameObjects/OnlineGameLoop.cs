﻿using Ultraviolet.Graphics.Graphics2D;
using Synchrocade.TemporalContainer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Synchrocade.GameObjects.Input;
using Synchrocade.GameLogics;
using Synchrocade.Networking;
using Synchrocade.UpdateThread;
using Ultraviolet;
using Ultraviolet.Input;
using Synchrocade.Screens.GameScreens;
using Synchrocade.Screens;
using Synchrocade.GameObjects;
using Serilog;
using Synchrocade.GameObjects.Sound;
using Synchrocade.Settings;
using Ultraviolet.Presentation.Controls;

namespace Synchrocade.GameObjects
{
    internal class OnlineGameLoop
    {
        private readonly object _memoryIntegrityLock;

        private readonly uint size;
        private int currentFrame = 0;
        private int lastVerifiedFrame = -1; //(-1 because frame 0 is certainly not validated)
        private readonly CircularArray<IGameObject>[] playerArray;
        private readonly CircularArray<IGameObject> manager;
        InputList[] playerInputs;
        private readonly InputObject.EncodeDecodeOptions inputOptions;
        private readonly Dictionary<Type, List<CircularArray<IGameObject>>> storage;
        List<IGameObject>? itemsToAdd;
        private readonly OnlineGame onlineGame;
        private readonly UltravioletContext ultravioletContext;
        private readonly NetSoundManager soundManager;
        private SettingsContainer settings;

        private int players;

        internal OnlineGameLoop(int players, string[] playerNames, uint size, IGameLogic game, OnlineGame onlineGame, SettingsContainer settings, UltravioletContext ultravioletContext, object memoryIntegrityLock)
        {
            this.players = players;
            this.onlineGame = onlineGame;
            this.settings = settings;
            this.soundManager = new NetSoundManager(settings);
            this.ultravioletContext = ultravioletContext;
            _memoryIntegrityLock = memoryIntegrityLock;
            this.inputOptions = game.GetInputEncodeDecodeOptions();
            this.size = size;
            storage = new Dictionary<Type, List<CircularArray<IGameObject>>>();
            
            
            // ok so, storage needs to be created and organized, 
            IPlayerGameObject[] playerReferences = game.GetInitialPlayerState(players, playerNames,this);
            playerArray = new CircularArray<IGameObject>[playerReferences.Length];
            playerInputs = new InputList[playerReferences.Length];
            
            for (int x = 0; x < playerReferences.Length; x++)
            {
                playerInputs[x] = new InputList();
                playerArray[x] = AddObjectNow(playerReferences[x],currentFrame);
            }
            
            manager = AddObjectNow(game.GetInitialGameManager(players, 1),currentFrame);

            
            
            
            List<IGameObject> startingState = game.GetInitialGameState(players, this);
            foreach (IGameObject item in startingState)
            {
                AddObjectNow(item,currentFrame);
            }


        }

        internal void PreUpdate(bool stalled)
        {
            for (int i = 0; i < players; i++)
            {
                (InputObject input, byte verified, short? frameDifference, short? opponentFrameDistance) inputs =
                    playerInputs[i].Last();
                if (inputs.verified > 0)
                {
                    IPlayerGameObject player = (IPlayerGameObject) playerArray[i][currentFrame];
                    player.SetInput(inputs.input);
                    
                }
            }


            int lowestVerifiedFrame = int.MaxValue;
            int lowestChangedFrame = int.MaxValue;
            foreach(InputList inputs in playerInputs)
            {
                inputs.ResimulateAllGeneratedInputsWithNewestValues();
                int changedFrame = inputs.GetLowestModifiedInputFrameSinceLastCall();
                int verifiedFrame = inputs.GetLastVerifiedFrame();
                if (changedFrame < lowestChangedFrame)
                    lowestChangedFrame = changedFrame;
                if (verifiedFrame < lowestVerifiedFrame)
                    lowestVerifiedFrame = verifiedFrame;
            }
            //Log.Verbose($"Looking at verified frames, lowestChangedFrame:{lowestChangedFrame}, lowestVerifiedFrame:{lowestVerifiedFrame}");
            if(!this.onlineGame.networkLoop!.localOnly)
                RollbackToFrame(Math.Max(this.lastVerifiedFrame - 1, 0));//i need to roll back to the lowest verified point of LAST frame, as i could have a much higher verifed frame now

            if (!stalled)
            {
                this.soundManager.unverifyAll();
                (this as OnlineGameLoop).Update();
                PlaySounds();
                this.soundManager.removeAllUnverified(); 
                clearDeletedItems();
                
            }
            this.lastVerifiedFrame = lowestVerifiedFrame;


        }




        public bool IsGameFinished() //must be called directly after preupdate;
        {
            if(lastVerifiedFrame>0)
                return ((IManagerGameObject) manager[lastVerifiedFrame]).IsGameFinished();
            return false;
        }

        public long[] ReturnScores() //check if the game is finished first
        {
            return ((IManagerGameObject) manager[lastVerifiedFrame]).ReturnScores();
        }
        
        
        
        void PlaySounds()
        {
            foreach (KeyValuePair<Type, List<CircularArray<IGameObject>>> objectSet in storage)
            {
                foreach (CircularArray<IGameObject> item in objectSet.Value)
                {
                    item[currentFrame].PlaySounds(soundManager,currentFrame);
                }
            }
        }

        internal void StopAllSoundsForShutdown()
        {
            soundManager.StopAllSoundsForShutdown();
        }

        void Update()
        {
            itemsToAdd = new List<IGameObject>();//items are added at the start of move all
            MoveAll();
            CheckCollisions(currentFrame);
            ExecuteCollisions(currentFrame);


            foreach (KeyValuePair<Type, List<CircularArray<IGameObject>>> objectSet in storage)
            {
                foreach (CircularArray<IGameObject> item in objectSet.Value)
                {
                    item.Push((IGameObject) item[currentFrame].Clone());
                }
            }

            currentFrame++;
            lock (_memoryIntegrityLock)
            {
                foreach (IGameObject entity in itemsToAdd)
                {
                    this.AddObjectNow(entity, currentFrame);
                }
            }
        }

        //need to impliment cloning
        private void MoveAll()
        {

            foreach (KeyValuePair<Type, List<CircularArray<IGameObject>>> objectSet in storage)
            {
                foreach (CircularArray<IGameObject> item in objectSet.Value)
                {
                    item[currentFrame].Update(this);
                    //item.Push((IGameObject)item[currentFrame].Clone());
                }

            }
        }

        private void clearDeletedItems()
        {
            lock (_memoryIntegrityLock)
            {
                List<CircularArray<IGameObject>> itemsToDelete = new List<CircularArray<IGameObject>>();

            foreach (KeyValuePair<Type, List<CircularArray<IGameObject>>> objectSet in storage)
            {
                foreach (CircularArray<IGameObject> item in objectSet.Value)
                {
                    if (item[currentFrame].DeletedFrame() >= 0 && item[currentFrame].DeletedFrame() + 1 < lastVerifiedFrame)
                    {
                        itemsToDelete.Add(item);
                    }
                }

            }
                foreach (CircularArray<IGameObject> entity in itemsToDelete)
                {
                    storage[entity[currentFrame].GetType()].Remove(entity);
                }
            }
        }


        internal ulong CheckForObjectDesynchronization(int checkFrame, bool verbose)
        {
            uint counter = 0;
            ulong checksum = 0;
            long i = checkFrame;
            foreach (KeyValuePair<Type, List<CircularArray<IGameObject>>> gameLists in storage)
            {
                foreach(CircularArray<IGameObject> item in gameLists.Value)
                {
                    if (item.GetStartingFrame() < i && item[i].DeletedFrame() < 0)
                    {
                        checksum += (counter * item[i].CalculateDesynchronizationCheckValue(verbose));
                        counter++;
                    }
                }
            }
            Log.Verbose($"encountered {counter} relevant objects for desync check");
            return checksum;
        }

        internal void Draw(PaletteEffectSpriteBatch context, UpdateThread.UpdateThread updateThread, DrawMode drawMode)
        {
            

            float percentageToNextFrame = updateThread.PercentToNextFrame();

            if (percentageToNextFrame >= 1.3)
                percentageToNextFrame = 1.3f;

            //Log.Information($"currentFrame is {currentFrame}");

            foreach (KeyValuePair<Type, List<CircularArray<IGameObject>>> objectSet in storage)
            {
                Type type = objectSet.Key;
                List<CircularArray<IGameObject>> objectList = objectSet.Value;
                foreach (CircularArray<IGameObject> entity in objectList)
                {
                    if (drawMode == DrawMode.Normal)
                    {
                        entity[currentFrame].Draw(context, 0, null);
                        /*if (currentFrame - 6 >= entity.getStartingFrame())
                            entity[currentFrame - 6].draw(context, 0, null);*/
                    }
                    else if (drawMode == DrawMode.Interpolation)
                    {

                        if (currentFrame - 1 >= entity.GetStartingFrame())
                            entity[currentFrame - 1].Draw(context, percentageToNextFrame, entity[currentFrame]);
                        /*if (currentFrame - 6 >= entity.getStartingFrame())
                            entity[currentFrame - 6].draw(context, 0, null);*/
                    }
                    else if (drawMode == DrawMode.Projection)
                    {
                        if (currentFrame - 2 >= entity.GetStartingFrame())
                            entity[currentFrame - 2].Draw(context, 1 + percentageToNextFrame, entity[currentFrame - 1]);
                        /*if (currentFrame - 6 >= entity.getStartingFrame())
                            entity[currentFrame - 6].draw(context, 0, null);*/
                    }
                    else if (drawMode == DrawMode.Extrapolation)
                    {
                        //extrapolate a frame if it hasn't been done
                        IGameObject extrapolation = entity.GetExtrapolatedFrame();
                        if (extrapolation is null)
                        {
                            extrapolation = (IGameObject)entity[currentFrame].Clone();
                            extrapolation.Update(this);
                            entity.SetExtrapolatedFrame(extrapolation);
                        }
                        entity[currentFrame].Draw(context, percentageToNextFrame, extrapolation);
                    }
                    else if(drawMode == DrawMode.Comparison)
                    {

                        //entity[currentFrame].draw(context, 0, null);
                        /*if (currentFrame - 6 >= entity.getStartingFrame())
                            entity[currentFrame - 6].draw(context, 0, null);*/

                        if (currentFrame - 1 >= entity.GetStartingFrame())
                            entity[currentFrame - 1].Draw(context, 1.0f + percentageToNextFrame, entity[currentFrame]);

                        if (currentFrame - 1 >= entity.GetStartingFrame())
                            entity[currentFrame - 1].Draw(context, percentageToNextFrame, entity[currentFrame]);
                        if (currentFrame - 7 >= entity.GetStartingFrame())
                            entity[currentFrame - 6].Draw(context, 0, null);

                        /*if (currentFrame - 6 >= entity.getStartingFrame())
                            entity[currentFrame - 6].draw(context, 0, null);*/


                        //extrapolate a frame if it hasn't been done
                        /*IGameObject extrapolation = entity.getExtrapolatedFrame();
                        if (extrapolation is null)
                        {
                            extrapolation = (IGameObject)entity[currentFrame].Clone();
                            extrapolation.update(this);
                            entity.setExtrapolatedFrame(extrapolation);
                        }
                        entity[currentFrame].draw(context, percentageToNextFrame, extrapolation);*/


                                        
                            

                    }

                }

                        
            }

            
        }


        /*void GameLoop.setInputObjectForFrame(int frame, int player, InputObject inputs, bool verified)
        {
            PlayerGameObject playerObject = (PlayerGameObject)playerArray[player][frame];

            playerInputs[player][frame] = (inputs, verified);

            playerObject.setInput(inputs);
        }*/



        internal void GiveLocalInputsToNetLoop()
        {
            InputObject[] objectList = new InputObject[4];
            objectList[0] = ParseInputs(0);
            objectList[1] = ParseInputs(1);
            objectList[2] = ParseInputs(2);
            objectList[3] = ParseInputs(3);
            
            onlineGame.networkLoop!.RegisterLocalInputs(ref playerInputs, objectList, inputOptions);
        }
        protected InputObject ParseInputs(int player) 
        {
            try
            {
                
                InputObject inputs = new InputObject();

                inputs.SetDirectionPressed(InputObject.Direction.up, InputObject.DType.Move, checkButtonPressed(settings.PlayerControlScheme[player].upButton));
                inputs.SetDirectionPressed(InputObject.Direction.down, InputObject.DType.Move, checkButtonPressed(settings.PlayerControlScheme[player].downButton));
                inputs.SetDirectionPressed(InputObject.Direction.left, InputObject.DType.Move, checkButtonPressed(settings.PlayerControlScheme[player].leftButton));
                inputs.SetDirectionPressed(InputObject.Direction.right, InputObject.DType.Move, checkButtonPressed(settings.PlayerControlScheme[player].rightButton));

                if(inputOptions.NumberOfButtons>=1)
                    inputs.SetButtonPressed(InputObject.Button.but1, checkButtonPressed(settings.PlayerControlScheme[player].button1));
                if(inputOptions.NumberOfButtons>=2) 
                    inputs.SetButtonPressed(InputObject.Button.but2, checkButtonPressed(settings.PlayerControlScheme[player].button2));
                if(inputOptions.NumberOfButtons>=3)
                    inputs.SetButtonPressed(InputObject.Button.but3, checkButtonPressed(settings.PlayerControlScheme[player].button3));
                if(inputOptions.NumberOfButtons>=4)
                    inputs.SetButtonPressed(InputObject.Button.but4, checkButtonPressed(settings.PlayerControlScheme[player].button4));


                return inputs;

            }
            catch (System.ObjectDisposedException) //happens when the update thread pulls an input as the program is terminating
            {
                return new InputObject();
            }
        }


        private bool checkButtonPressed(MappedButton button)
        {
            try
            {
                if (button.JoystickNum.HasValue)
                {
                    if (button.PadButton.HasValue)
                    {
                        var joystick = this.ultravioletContext.GetInput().GetGamePadForPlayer(button.JoystickNum.Value);
                        if(!(joystick is null)) 
                            return joystick.IsButtonDown(button.PadButton.Value);
                        return false;
                    }
                }

                if (button.Key.HasValue)
                {
                    return this.ultravioletContext.GetInput().GetKeyboard().IsKeyDown(button.Key.Value);
                }
            }
            catch (Exception e)
            {
                Log.Warning(e,"Exception in inputs, probably a disconnected controller, ignored and returned as false");
                return false;
            }

            return false;
        }
        

        internal void QueueAddObject(IGameObject item)
        {

            lock (_memoryIntegrityLock)
            {
                itemsToAdd!.Add(item);
            }
        }

        CircularArray<IGameObject> AddObjectNow(IGameObject item,int frame)
        {

            lock (_memoryIntegrityLock)
            {

                Type objectType = item.GetType();

                if (!storage.ContainsKey(objectType))
                {
                    storage[objectType] = new List<CircularArray<IGameObject>>();
                }

                CircularArray<IGameObject> circularObject = new CircularArray<IGameObject>(this.size, frame, (IGameObject)item.Clone(), (IGameObject)item.Clone());
                circularObject.Push(item);
                storage[objectType].Add(circularObject);
                return circularObject;
            }
        }
        
        
        

        internal void RollbackToFrame(int frame)
        {
            
            if (frame >= currentFrame)
            {
                return;
            }
            //make locking hold for far less time
            lock (_memoryIntegrityLock)
            {
                int oldCurrentFrame = currentFrame;
                //itemsToAdd = new List<IGameObject>();
                foreach (KeyValuePair<Type, List<CircularArray<IGameObject>>> objectSet in storage)
                {
                    List<CircularArray<IGameObject>> objectList = objectSet.Value;
                    for (int i = objectList.Count - 1; i >= 0; i--)
                    {
                        CircularArray<IGameObject> objectCircularArray = objectList[i];
                        //if  the object was created after our rollback point destroy it
                        if (frame < objectCircularArray.GetStartingFrame())
                        {
                            objectList.RemoveAt(i);
                        }
                        else if (frame == objectCircularArray.GetStartingFrame())
                        {
                            objectCircularArray.RemoveItemsFromEnd(frame-1);
                            objectCircularArray.Push((IGameObject)objectCircularArray.GetFirstElementCache().Clone());
                        }
                        else
                        {
                            IGameObject clone = (IGameObject)objectCircularArray[frame-1].Clone();
                            objectCircularArray.RemoveItemsFromEnd(frame-1);
                            objectCircularArray.Push(clone);

                        }
                    }

                }
                
                itemsToAdd = new List<IGameObject>();//need to redo this as i have made it so collision commands can create objects
                currentFrame = frame;
                this.ReplayToFrame(oldCurrentFrame);


            }
        }
        internal void ReplayToFrame(int frame)
        {
            while(currentFrame<frame)
            {
                for(int i = 0; i < playerArray.Length; i++)
                {
                    IPlayerGameObject player = (IPlayerGameObject)playerArray[i][currentFrame];
                    player.SetInput(playerInputs[i][currentFrame].input);
                }
                (this as OnlineGameLoop).Update();
            }

        }

        //going through the command list, and deleting objects when they safely can be need to be done




        //this is more of a game loop thing, but it's easier to impliment here, maybe refactor later
        //this is getting left untested for now, need to test this
        void CheckCollisions(int frame)
        {
            //var returnBag = new ConcurrentBag<(IGameObject, IGameObject)>();
            //loop through dictionaries
            for (int i = 0; i < storage.Count; i++)
            {
                Type firstType = storage.ElementAt(i).Key;
                List<CircularArray<IGameObject>> firstList = storage.ElementAt(i).Value;
                if (firstList.Count >= 1)
                {

                    if (firstList.First()[frame].GetCollidableClasses().Contains(firstType))
                    {
                        CheckCollisionsForSingleList(frame, firstList);
                    }
                }
                for (int j = i + 1; j < storage.Count; j++)
                {
                    Type secondType = storage.ElementAt(j).Key;
                    List<CircularArray<IGameObject>> secondList = storage.ElementAt(j).Value;
                    if (firstList.Count >= 1)
                    {
                        if (firstList.First()[frame].GetCollidableClasses().Contains(secondType))
                        {
                            CheckCollisionsForListPair(frame, firstList, secondList);
                        }
                    }
                }
            }
            //return returnBag;
        }

        private static void CheckCollisionsForSingleList(int frame, List<CircularArray<IGameObject>> list1)
        {
            //Parallel for runs like GARBAGIO in debug, and i don't need it yet 
            //Parallel.For(0, list1.Count, (i, state) =>
            for (int i = 0; i < list1.Count; i++)
            {

                for (int j = i + 1; j < list1.Count; j++)
                {
                    if ((list1[i][frame].DeletedFrame() < 0) && (list1[j][frame].DeletedFrame() < 0)&&list1[i].GetStartingFrame()!=frame&&list1[j].GetStartingFrame()!=frame)
                    {
                        (bool isColliding, ICollisionDetails? details) checkCollisions = list1[i][frame].CheckCollision(list1[j][frame]);

                        if (checkCollisions.isColliding)
                    {

                            list1[i][frame].AddCollisionCommand(list1[j][frame], checkCollisions.details, true);
                            list1[j][frame].AddCollisionCommand(list1[i][frame], checkCollisions.details, false);
                        
                    }
                    }
                }

            }//);


        }
        
        

        private static void CheckCollisionsForListPair(int frame, List<CircularArray<IGameObject>> list1, List<CircularArray<IGameObject>> list2)
        {
            //Parallel for runs like GARBAGIO in debug, and i don't need it yet 
            //Parallel.For(0, list1.Count, (i, state) =>
            for (int i = 0; i < list1.Count; i++)
            {

                for (int j = 0; j < list2.Count; j++)
                {
                    if ((list1[i][frame].DeletedFrame() < 0) && (list2[j][frame].DeletedFrame() < 0)&&list1[i].GetStartingFrame()!=frame&&list2[j].GetStartingFrame()!=frame)
                    {
                        (bool isColliding, ICollisionDetails? details) checkCollisions = list1[i][frame].CheckCollision(list2[j][frame]);
                        if (checkCollisions.isColliding)
                        {
                            list1[i][frame].AddCollisionCommand(list2[j][frame], checkCollisions.details, true);
                            list2[j][frame].AddCollisionCommand(list1[i][frame], checkCollisions.details, false);
                        }
                    }
                }

            }//);

        }

        private void ExecuteCollisions(int frame)
        {
            foreach (KeyValuePair<Type, List<CircularArray<IGameObject>>> objectSet in storage)
            {
                Type type = objectSet.Key;
                List<CircularArray<IGameObject>> objectList = objectSet.Value;
                foreach (CircularArray<IGameObject> entity in objectList)
                {
                    if(entity[frame].DeletedFrame() < 0)
                        entity[frame].ExecuteCollisionCommands(this);
                }

            }

        }

        internal int GetCurrentFrame()
        {
            return currentFrame;
        }


        
    }
}
