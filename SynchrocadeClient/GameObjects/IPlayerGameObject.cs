﻿using Synchrocade.GameObjects.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synchrocade.GameObjects
{
    internal interface IPlayerGameObject : IGameObject
    {
        void SetInput(InputObject input);
    }
}
