﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixMath.NET;

namespace Synchrocade.GameObjects.CommonGameObjects
{
    internal class BouncingCircleCollisionDetails : ICollisionDetails
    {
        internal (Fix64 modx, Fix64 mody) positionMods;
        internal (Fix64 c1VelModX, Fix64 c1VelModY, Fix64 c2VelModX, Fix64 c2VelModY)? velocityMods;
        internal BouncingCircleCollisionDetails((Fix64 modx, Fix64 mody) positionMods, (Fix64 c1VelModX, Fix64 c1VelModY, Fix64 c2VelModX, Fix64 c2VelModY)? velocityMods)
        {
            this.positionMods = positionMods;
            this.velocityMods = velocityMods;
        }
    }
}
