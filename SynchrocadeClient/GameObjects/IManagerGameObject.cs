namespace Synchrocade.GameObjects
{
    internal interface IManagerGameObject : IGameObject
    {
        public bool IsGameFinished();
        public long[] ReturnScores();
    }
}