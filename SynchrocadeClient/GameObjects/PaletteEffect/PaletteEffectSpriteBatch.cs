﻿using Synchrocade.GameObjects.PaletteEffect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ultraviolet;
using Ultraviolet.Graphics;
using Ultraviolet.Graphics.Graphics2D;


namespace Synchrocade.GameObjects
{


    internal unsafe class PaletteEffectSpriteBatch : SpriteBatchBase<PaletteVertex, PaletteSpriteData>
    {

        
        internal PaletteEffectSpriteBatch(UltravioletContext uv, Int32 batchSize = 2048)
    : base(uv, batchSize)
        {
        }
        
        
        
        protected override void GenerateVertices(Texture2D texture, SpriteHeader[] sprites, PaletteVertex[] vertices, PaletteSpriteData[] data, int offset, int count)
        {
            var srgb = Ultraviolet.GetGraphics().CurrentRenderTargetIsSrgbEncoded;

            fixed (SpriteHeader* pSprites1 = &sprites[offset])
            fixed (PaletteSpriteData* pData1 = &data[offset])
            fixed (PaletteVertex* pVertices1 = &vertices[0])
            {
                var pSprites = pSprites1;
                var pData = pData1;
                var pVertices = pVertices1;

                for (int i = 0; i < count; i++)
                {
                    CalculateSinAndCos(pSprites->Rotation);
                    CalculateRelativeOrigin(pSprites);

                    for (int v = 0; v < 4; v++)
                    {
                        CalculatePositionAndTextureCoordinates(pSprites, v,
                            (Vector2*)&pVertices->Position, &pVertices->U, &pVertices->V);

                        pVertices->Color = pSprites->Color;
                        pVertices->X = pData->Layer;
                        pVertices->Y = pData->ShouldUsePalette?(ushort)1:(ushort)0;
                        pVertices++;
                    }

                    pSprites++;
                    pData++;
                }
            }
        }
        
        
        //generic drawing functions, maybe should be moved elsewhere
        //inspired by the pixel handler in monogame extended
        private static Texture2D? BlankTexture;
        public Texture2D WhiteTexture()
        {
            if (BlankTexture is null)
            {

                BlankTexture = Texture2D.CreateTexture(1, 1);
                BlankTexture.SetData(new[] { Color.White });
            }

            return BlankTexture;
        }

        //inspired by the drawpoint in monogame extended
        public void DrawPoint(Texture2D texture, Vector2 position, Color color, int size = 1, float layerDepth = 0, bool centered = true)
        {
            var scale = Vector2.One * size;
            Vector2 offset;
            if(centered)
            {
                offset = new Vector2(0.0f) - new Vector2(size * 0.5f);
            }
            else
            {
                offset = new Vector2(0f);
            }
            this.Draw(texture, position + offset, null, color, 0f, Vector2.Zero, scale, SpriteEffects.None, layerDepth);
        }
        //inspired by the drawpoint in monogame extended
        public void DrawLine(Texture2D texture,Vector2 point, float length, float angle, Color color, float thickness = 1f, float layerDepth = 0)
        {
            var origin = new Vector2(0f, 0.5f);
            var scale = new Vector2(length, thickness);
            this.Draw(texture, point, null, color, angle, origin, scale, SpriteEffects.None, layerDepth);
        }
        //inspired by the drawpoint in monogame extended
        public void DrawLine(Texture2D texture, Vector2 point1, Vector2 point2, Color color, float thickness = 1f, float layerDepth = 0)
        {
            var distance = Vector2.Distance(point1, point2);
            var angle = (float)Math.Atan2(point2.Y - point1.Y, point2.X - point1.X);
            this.DrawLine(texture, point1, distance, angle, color, thickness, layerDepth);
        }
        public void DrawLine(Texture2D texture, float p1X, float p1Y, float p2X, float p2Y, Color color, float thickness = 1f, float layerDepth = 0)
        {
            this.DrawLine(texture, new Vector2(p1X, p1Y), new Vector2(p2X, p2Y), color, thickness,
                layerDepth);
        }

        public void DrawRectangle(Texture2D texture, Vector2 origin, float width, float height, Color color,
            float thickness = 1f, float layerDepth = 0)
        {
            //line from origin down
            this.DrawLine(texture,origin.X,origin.Y-thickness/2,origin.X,origin.Y+height+thickness/2f,color,thickness,layerDepth);
            //line from origin right
            this.DrawLine(texture,origin.X-thickness/2f,origin.Y,origin.X+width+thickness/2f,origin.Y,color,thickness,layerDepth);
            //line from right side down
            this.DrawLine(texture,origin.X+width,origin.Y-thickness/2f,origin.X+width,origin.Y+height+thickness/2f,color,thickness,layerDepth);
            //line from bottom right
            this.DrawLine(texture,origin.X-thickness/2f,origin.Y+height,origin.X+width+thickness/2f,origin.Y+height,color,thickness,layerDepth);
        }
        
        
        public void DrawFilledRectangle(Texture2D texture, Vector2 origin, float width, float height, Color color, float layerDepth = 0)
        {
            var size = new Size2F(width, height);
            this.Draw(texture, new RectangleF(origin,size), new Rectangle?(), color, 0.0f, Vector2.Zero, SpriteEffects.None, layerDepth, default (PaletteSpriteData));
        }
        
        
    }
}
