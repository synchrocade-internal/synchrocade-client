﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ultraviolet.Graphics.Graphics2D;

namespace Synchrocade.GameObjects.PaletteEffect
{
    internal struct PaletteSpriteData
    {
        internal UInt16 Layer {get;}
        internal bool ShouldUsePalette { get; }
        internal PaletteSpriteData(UInt16 layer, bool shouldUsePalette)
        {
            Layer = layer;
            ShouldUsePalette = shouldUsePalette;
        }
    }
}
