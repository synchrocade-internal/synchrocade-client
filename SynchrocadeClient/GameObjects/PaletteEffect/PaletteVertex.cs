﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ultraviolet;
using Ultraviolet.Graphics;

namespace Synchrocade.GameObjects.PaletteEffect
{
    internal struct PaletteVertex : IVertexType
    {


        /// <summary>
        /// Gets the vertex declaration.
        /// </summary>
        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return VertexDeclaration; }
        }

        /// <summary>
        /// The vertex declaration.
        /// </summary>
        /// 
        internal static readonly VertexDeclaration VertexDeclaration = new VertexDeclaration(new[] {
            new VertexElement(0, VertexElementFormat.Vector2, VertexElementUsage.Position, 0),
            new VertexElement(sizeof(Single) * 2, VertexElementFormat.Color, VertexElementUsage.Color, 0),
            new VertexElement(sizeof(Single) * 2, VertexElementFormat.UnsignedShort4, VertexElementUsage.TextureCoordinate, 0),
            //new VertexElement(sizeof(Single) * 2 + sizeof(Int16) * 2, VertexElementFormat.UnsignedShort2, VertexElementUsage.TextureCoordinate, 1)

        });
        /// <summary>
        /// The vertex position.
        /// </summary>
        internal Vector2 Position;
        public Color Color;



        /// <summary>
        /// The u-component of the texture coordinate.
        /// </summary>
        internal UInt16 U;

        /// <summary>
        /// The v-component of the texture coordinate.
        /// </summary>
        internal UInt16 V;

        /// <summary>
        /// The vertex color.
        /// </summary>
        internal UInt16 X;
        internal UInt16 Y;

    }
}
