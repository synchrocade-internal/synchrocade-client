using System;
using System.Collections.Generic;
using System.Linq;
using FixMath.NET;
using Synchrocade.GameObjects.Input;
using Synchrocade.GameObjects.Sound;
using Synchrocade.ReusableFunctions;
using Ultraviolet;
using Ultraviolet.Content;
using Ultraviolet.Graphics.Graphics2D;
using Ultraviolet.OpenGL.Graphics.Uniforms;
using Ultraviolet.Presentation.Controls;

namespace Synchrocade.GameObjects.UniqueGameObjects.DropPuzzleObjects
{
    internal enum Phase
    {
        Drop,
        Damage,
        Break,
        BreakAnimation,
        Collapse,
        Dead
    }
    internal enum Direction
    {
        Up,
        Down,
        Left,
        Right,
        None
    }
    
    internal class DropPuzzlePlayer : IPlayerGameObject
    {
        private static UltravioletFont? drawFont = null;

        private const int TileSize = DropPuzzleConstants.tileSize;
        
        //3 colors is the max i can sustain with 4 break
        //5 colors is reasonable with 3 break
        private const int numColors = 4;
        private const int neededToBreak = 3;

        private const int boardTilesWidth = 8;
        private const int boardTilesHeight = 18;
        private const int nextStartXLocation = 96;
        private const int nextStartYLocation = -64;
        
        private DropPuzzlePieceSegment?[,] board = new DropPuzzlePieceSegment?[boardTilesWidth,boardTilesHeight];
        private InputObject input;
        private int totalPlayers;
        private int player;
        private string playerName;
        private ClonableRandom randomPieceSource;
        private ClonableRandom randomDamageSource;
        private DropPuzzlePiece? nextPiece;

        private bool isMelding = false;

        private DropPuzzlePiece? damagePrediction;
        
        private List<DropPuzzlePiece> currentPieces;
        private Phase phase;

        private int currentPieceId = 0;

        private Fix64 comboCount = new Fix64(0);
        private int damageBlockCount = 0;
        private int numberOfEnemies;
        private int? damageToSend = null;
        private int incomingDamage = 0;

        private const int breakTimerSetTime = 40;
        private int currentBreakTimerRemaining = 0;
        private List<(DropPuzzlePieceSegment segment,int x, int y)>? currentlyBreakingPieces;

        
        private int? damageToAdd = null;
        private int? updatedNumEnemies = null;

        private bool isEvenRow = false;

        private int damageCounter = 0;

        private bool needsReportDeath = false;
        
        internal DropPuzzlePlayer(string playerName, int player, int totalPlayers, int randomSeed)
        {
            this.playerName = playerName;
            this.player = player;
            this.totalPlayers = totalPlayers;
            randomPieceSource = new ClonableRandom(randomSeed);
            randomDamageSource = new ClonableRandom(randomSeed);
            GenerateNextPiece();
            GenerateNextDamage();
            phase = Phase.Drop;
            currentPieces = new List<DropPuzzlePiece>();
            numberOfEnemies = totalPlayers - 1;

        }
        
        object ICloneable.Clone()
        {
            DropPuzzlePlayer clone =  (DropPuzzlePlayer)this.MemberwiseClone();
            clone.randomDamageSource = randomDamageSource.Clone();
            clone.randomPieceSource = randomPieceSource.Clone();

            List<DropPuzzlePiece> clonedPieces = new List<DropPuzzlePiece>();
            foreach (var piece in currentPieces)
            {
                clonedPieces.Add((DropPuzzlePiece)piece.Clone());
            }
            clone.currentPieces = clonedPieces;


            if (!(currentlyBreakingPieces is null))
            {
                List<(DropPuzzlePieceSegment segment,int x, int y)> clonedFading = new List<(DropPuzzlePieceSegment segment,int x, int y)>();
                foreach (var piece in currentlyBreakingPieces)
                {
                    clonedFading.Add(piece);
                }

                clone.currentlyBreakingPieces = clonedFading;
            }


            clone.nextPiece = (DropPuzzlePiece?)nextPiece?.Clone();
            clone.damagePrediction = (DropPuzzlePiece?)damagePrediction?.Clone();

            
            clone.board = (DropPuzzlePieceSegment?[,]) board.Clone();
            return clone;
        }

        int IGameObject.DeletedFrame()
        {
            return -1;
        }



        void GenerateNextDamage()
        {
            int color1 = randomPieceSource.Next(1, numColors+1);
            int color2 = randomPieceSource.Next(1, numColors+1);
            int color3 = randomPieceSource.Next(1, numColors+1);
            int color4 = randomPieceSource.Next(1, numColors+1);

            damagePrediction = DropPuzzlePiece.ProduceDamageRow(isEvenRow, color1, color2, color3, color4);
            isEvenRow = !isEvenRow;
            damageCounter = 0;
        }
        
        void GenerateNextPiece()
        {
            //decide on a piece type
            int pieceType = randomPieceSource.Next(1, numColors+1);
            int color1 = randomPieceSource.Next(1, numColors+1);
            int color2 = randomPieceSource.Next(1, numColors+1);
            int color3 = randomPieceSource.Next(1, numColors+1);
            
            //if playing 3 in a row, don't drop a piece that is gonna auto break
            if (neededToBreak == 3 && (pieceType == 3) && color1 == color2 && color2 == color3)
            {
                GenerateNextPiece();
                return;
            }
            currentPieceId++;

            switch (pieceType)
            {
                case 1:
                    nextPiece = DropPuzzlePiece.ProduceSingle(currentPieceId, color1, nextStartXLocation, nextStartYLocation+32, false);
                    return;
                case 2:
                    nextPiece = DropPuzzlePiece.ProduceDouble(currentPieceId, color1, color2, nextStartXLocation, nextStartYLocation+32);
                    break;
                case 3:
                    nextPiece = DropPuzzlePiece.ProduceLinearTriple(currentPieceId, color1, color2, color3, nextStartXLocation, nextStartYLocation+32);
                    break;
                case 4:
                    nextPiece = DropPuzzlePiece.ProduceL(currentPieceId, color1, color2, color3, nextStartXLocation, nextStartYLocation);
                    break;
                case 5:
                    nextPiece = DropPuzzlePiece.ProduceInverseL(currentPieceId, color1, color2, color3, nextStartXLocation, nextStartYLocation);
                    break;
            }
            
        }



        
        void IGameObject.Update(OnlineGameLoop caller)
        {
            damageToAdd = null;
            updatedNumEnemies = null;
            damageToSend = null;

            if (incomingDamage >= 8)
            {
                damageCounter++;
            }
            else
            {
                damageCounter = 0;
            }
            
            switch (phase)
            {
                case Phase.Drop:
                    HandleDropPhase();
                    break;
                case Phase.Break:
                    HandleBreakPhase();
                    break;
                case Phase.BreakAnimation:
                    HandleBreakAnimationPhase();
                    break;
                case Phase.Collapse:
                    HandleCollapsePhase();;
                    break;
                case Phase.Damage:
                    HandleDamagePhase();
                    break;
                case Phase.Dead:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }


        private void HandleDropPhase()
        {
                List<DropPuzzlePiece> piecesToRemove = new List<DropPuzzlePiece>();

                //handle mergers between pieces
                //to be honest i'm thinking i might just remove the merge button
                checkForMergers();


                bool pieceBelowThreshold = false;
                for (int i = 0; i < currentPieces.Count; i++)
                {
                    DropPuzzlePiece piece = currentPieces[i];

                    
                    if (input.GetButtonPressed(InputObject.Button.but1) && !piece.rotateLeftPressed)
                    {
                        piece.rotateLeftPressed = true;

                        DropPuzzlePiece? testPiece = FindFittingPiecePosition(piece.RotateLeft(), piece);

                        if (testPiece != null)
                        {
                            piece = testPiece;
                            currentPieces[i] = piece;
                        }
                    }
                    else if (input.GetButtonPressed(InputObject.Button.but2) && !piece.rotateRightPressed)
                    {
                        piece.rotateRightPressed = true;

                        DropPuzzlePiece? testPiece = FindFittingPiecePosition(piece.RotateRight(), piece);

                        if (testPiece != null)
                        {
                            piece = testPiece;
                            currentPieces[i] = piece;
                        }
                    }

                    if (!input.GetButtonPressed(InputObject.Button.but1))
                    {
                        piece.rotateLeftPressed = false;
                    }
                    if (!input.GetButtonPressed(InputObject.Button.but2))
                    {
                        piece.rotateRightPressed = false;
                    }
                    
                    
                    bool leftpressed = false;
                    bool rightpressed = false;
                    if (input.GetDirectionPressed(InputObject.Direction.left, InputObject.DType.Move))
                    {
                        piece.desireLeft++;
                        piece.desireRight = 0;
                        leftpressed = true;
                    }
                    else if (input.GetDirectionPressed(InputObject.Direction.right, InputObject.DType.Move))
                    {
                        piece.desireRight++;
                        piece.desireLeft = 0;
                        rightpressed = true;
                    }

                    if (piece.desireLeft > 0)
                    {
                        bool nextToWall = false;
                        int wallLocation = 0;
                        if (CheckPieceCollisionWithBoard(piece, Direction.Left,false).isCollided)
                        {
                            nextToWall = true;
                            wallLocation = piece.X - piece.X % TileSize;
                            
                        }

                        if (piece.desireLeft > 32)
                        {
                            piece.X -= 4;
                        }
                        else
                        {
                            piece.X -= 2;
                        }

                        int modDistance = 32 - (piece.X % TileSize);
                        if (modDistance <= 8 && ! leftpressed)
                        {
                            piece.desireLeft = 0;
                            piece.X += modDistance;

                        }

                        if (nextToWall && wallLocation > piece.X)
                        {
                            piece.X = wallLocation;
                        }
                        


                    }
                    else if(piece.desireRight > 0)
                    {
                        bool nextToWall = false;
                        int wallLocation = 0;
                        
                        if (CheckPieceCollisionWithBoard(piece, Direction.Right,false).isCollided)
                        {
                            nextToWall = true;
                            wallLocation = piece.X - piece.X % TileSize;
                        }
                        
                        
                        if (piece.desireRight > 32)
                        {
                            piece.X += 4;
                        }
                        else
                        {
                            piece.X += 2;
                        }              
                        
                        int modDistance = piece.X % TileSize;

                        if (modDistance <= 8 && !rightpressed)
                        {
                            piece.desireRight = 0;
                            piece.X -= modDistance;
                        }
                        
                        if (nextToWall && wallLocation < piece.X)
                        {
                            piece.X = wallLocation;
                        }
                        
                    }

                    if (piece.X < 0)
                    {
                        piece.X = 0;
                        piece.desireLeft = 0;
                    } else if (piece.X + TileSize * piece.GetPieceTileWidth() > TileSize * boardTilesWidth)
                    {
                        piece.X = TileSize * boardTilesWidth - TileSize * piece.GetPieceTileWidth();
                        piece.desireRight = 0;
                    }

                    var downCollision = CheckPieceCollisionWithBoard(piece, Direction.Down,false );
                    if (downCollision.isCollided || piece.Y + piece.GetPieceTileHeight() * TileSize > TileSize * boardTilesHeight - 1)
                    {
                        if(!downCollision.withPiece)
                            piece.forgiveness--;
                        piece.Y -= piece.Y % TileSize;
                    }
                    else if (input.GetDirectionPressed(InputObject.Direction.down,InputObject.DType.Move))
                    {
                        piece.Y += 4;
                    }
                    else
                    {
                        piece.Y += 1;
                    }

                    if (piece.forgiveness < 0)
                    {
                        
                        
                        bool didAddPiece = true;


                        didAddPiece = AddPieceToBoard(piece,false);



                        if(didAddPiece)
                            piecesToRemove.Add(piece);
                        else if (phase == Phase.Dead)
                            return;
                    }
                    
                    
                    
                    
                    
                    if (piece.Y >= 0)
                        pieceBelowThreshold = true;
                    

                        
                }




                foreach (var piece in piecesToRemove)
                {
                    currentPieces.Remove(piece);
                }
 
                
                
                if (nextPiece is null && pieceBelowThreshold)
                {
                    GenerateNextPiece();
                }
                
                if (currentPieces.Count == 0)
                {
                    phase = Phase.Break;
                }
                
                
                
        }



        private void checkForMergers()
        {
            
            List<DropPuzzlePiece> piecesToRemove = new List<DropPuzzlePiece>();

            if (!input.GetButtonPressed(InputObject.Button.but3))
            {
                isMelding = false;
                return;
            }

            isMelding = true;
            for (int i = 0; i < currentPieces.Count; i++)
            {
                var left = CheckPieceCollisionWithBoard(currentPieces[i], Direction.Left,true);
                var right = CheckPieceCollisionWithBoard(currentPieces[i], Direction.Right,true);
                var down = CheckPieceCollisionWithBoard(currentPieces[i], Direction.Down,true);
                var up = CheckPieceCollisionWithBoard(currentPieces[i], Direction.Up,true);

                if ((left.isCollided && !left.withPiece) || (right.isCollided && !right.withPiece) ||
                    (down.isCollided && !down.withPiece) || (up.isCollided && !up.withPiece))
                {
                    piecesToRemove.Add(currentPieces[i]);
                }
            }

            foreach (var piece in piecesToRemove)
            {
                AddPieceToBoard(piece,true);
                currentPieces.Remove(piece);
            }
            
            /*for (int j = i; j < currentPieces.Count; j++)
            {

                const int lowerLimit = 6;
                const int upperLimit = 24;
                //if the pieces are near enough to centered
                if ((currentPieces[i].X % TileSize < lowerLimit || currentPieces[i].X % TileSize > upperLimit) &&
                    (currentPieces[j].X % TileSize < lowerLimit || currentPieces[j].X % TileSize > upperLimit))
                {
                    bool p1movedRight = false;
                    int p1x = currentPieces[i].X % TileSize;
                    if (currentPieces[i].X % TileSize > upperLimit)
                    {
                        p1x++;
                        p1movedRight = true;
                    }

                    int p1y = currentPieces[i].Y % TileSize;
                            
                            
                    int p2x = currentPieces[j].X % TileSize;
                    
                    bool p2movedRight = false;

                    if (currentPieces[j].X % TileSize > upperLimit)
                    {
                        p2x++;
                        p2movedRight = true;
                    }
                    int p2y = currentPieces[j].Y % TileSize;

                            
                    bool[,] contained = new bool[boardTilesWidth+1,boardTilesHeight+1];

                    for (int x = 0; x < currentPieces[i].grid.GetLength(0); x++)
                    {
                        for (int y = 0; y < currentPieces[i].grid.GetLength(1); y++)
                        {
                            if (currentPieces[i].grid[x, y].HasValue)
                            {
                                contained[x + p1x, y + p1y] = true;
                            }
                        }
                    }


                    
                    for (int x = 0; x < currentPieces[j].grid.GetLength(0); x++)
                    {
                        for (int y = 0; y < currentPieces[j].grid.GetLength(1); y++)
                        {
                            
                            if (currentPieces[j].grid[x, y].HasValue)
                            {
                                bool foundMergePosition = false;

                                if (CheckForTileNextToCurrentTile(contained, x,y))
                                {
                                    foundMergePosition = true;
                                }
                                
                                else if (CheckForTileNextToCurrentTile(contained, x+1,y))
                                {
                                    foundMergePosition = true;
                                }
                                else if (CheckForTileNextToCurrentTile(contained, x-1,y))
                                {
                                    foundMergePosition = true;
                                }
                                else if (CheckForTileNextToCurrentTile(contained, x,y+1))
                                {
                                    foundMergePosition = true;
                                }
                                
                                else if (CheckForTileNextToCurrentTile(contained, x+1,y+1))
                                {
                                    foundMergePosition = true;
                                }
                                else if (CheckForTileNextToCurrentTile(contained, x-1,y+1))
                                {
                                    foundMergePosition = true;
                                }
                                else if (CheckForTileNextToCurrentTile(contained, x,y-1))
                                {
                                    foundMergePosition = true;
                                }
                                
                                else if (CheckForTileNextToCurrentTile(contained, x+1,y-1))
                                {
                                    foundMergePosition = true;
                                }
                                else if (CheckForTileNextToCurrentTile(contained, x-1,y-1))
                                {
                                    foundMergePosition = true;
                                }


                                if (foundMergePosition)
                                {
                                    //mergePieces(currentPieces[i], currentPieces[j]);
                                    return;
                                }

                            }
                        }
                    }
                }
            }*/
            
        }
        
        

        private bool CheckForTileNextToCurrentTile(int x, int y)
        {
            if (x < 0 || y < 0 || x >= board.GetLength(0) || y >= board.GetLength(1))
                return false;

            return board[x, y].HasValue;
        }

        
        
        private void HandleBreakPhase()
        {
            
            //look through the grid and see if there's anything we can break

            bool[,] breaks = new bool[boardTilesWidth,boardTilesHeight];
            int? priorColor = null;
            int count = 0;
            List<(int x, int y)> priorMatches = new List<(int x, int y)>();
            
            
            //first vertically
            for (int x = 0; x < board.GetLength(0); x++)
            {
                for (int y = 0; y < board.GetLength(1); y++)
                {
                    if (board[x, y]?.Color == priorColor)
                    {
                        if (!(priorColor is null))
                        {
                            count++;
                            priorMatches.Add((x,y));
                        }
                    }
                    else
                    {
                        if (count >= neededToBreak - 1)
                        {
                            foreach (var match in priorMatches)
                            {
                                breaks[match.x, match.y] = true;
                            }
                        }
                        priorColor = board[x, y]?.Color;
                        count = 0;
                        priorMatches = new List<(int x, int y)>();
                        priorMatches.Add((x,y));
                    }
                    
                }
                if (count >= neededToBreak - 1)
                {
                    foreach (var match in priorMatches)
                    {
                        breaks[match.x, match.y] = true;
                    }
                }
                priorColor = null;
                count = 0;
                priorMatches = new List<(int x, int y)>();
                
            }
            
            
            
            //then horizontally
            priorColor = null;
            count = 0;
            priorMatches = new List<(int x, int y)>();

            for (int y = 0; y < board.GetLength(1); y++)
            {
                for (int x = 0; x < board.GetLength(0); x++)
                {
                    if (board[x, y]?.Color == priorColor)
                    {
                        if (!(priorColor is null))
                        {
                            count++;
                            priorMatches.Add((x,y));
                        }
                    }
                    else
                    {
                        if (count >= neededToBreak - 1)
                        {
                            foreach (var match in priorMatches)
                            {
                                breaks[match.x, match.y] = true;
                            }
                        }
                        priorColor = board[x, y]?.Color;
                        count = 0;
                        priorMatches = new List<(int x, int y)>();
                        priorMatches.Add((x,y));
                    }
                    
                }
                if (count >= neededToBreak - 1)
                {
                    foreach (var match in priorMatches)
                    {
                        breaks[match.x, match.y] = true;
                    }
                }
                priorColor = null;
                count = 0;
                priorMatches = new List<(int x, int y)>();
                
            }



            List<(DropPuzzlePieceSegment segment,int x, int y)> tempBreakingPieces = new List<(DropPuzzlePieceSegment segment,int x, int y)>();
            int numberOfBreaks = 0;
            //remove all broken items
            for (int x = 0; x < board.GetLength(0); x++)
            {
                for (int y = 0; y < board.GetLength(1); y++)
                {
                    if (breaks[x,y])
                    {
                        DropPuzzlePieceSegment tempSegment = board[x, y]!.Value;
                        tempSegment.ConnectsDown = false;
                        tempSegment.ConnectsLeft = false;
                        tempSegment.ConnectsRight = false;
                        tempSegment.ConnectsUp = false;
                        tempBreakingPieces.Add((tempSegment,x,y));
                        board[x, y] = null;
                        BreakConnectionOfTouchingPiece(x, y, Direction.Up);
                        BreakConnectionOfTouchingPiece(x, y, Direction.Down);
                        BreakConnectionOfTouchingPiece(x, y, Direction.Left);
                        BreakConnectionOfTouchingPiece(x, y, Direction.Right);
                        numberOfBreaks++;
                    }
                }
            }

            comboCount += new Fix64(numberOfBreaks)/new Fix64(3); //this is an int, so breaking 6 or more counts as a combo
            damageBlockCount += numberOfBreaks;

            if (numberOfBreaks > 0)
            {
                currentlyBreakingPieces = tempBreakingPieces;
                currentBreakTimerRemaining = breakTimerSetTime;
                phase = Phase.BreakAnimation;
            }
            else
            {
                phase = Phase.Collapse;
            }

        }
        
        
        void HandleBreakAnimationPhase()
        {
            currentBreakTimerRemaining--;
            if (currentBreakTimerRemaining <= 0)
            {
                currentlyBreakingPieces = null;
                phase = Phase.Collapse;
            }
        }

        void HandleCollapsePhase()
        {
            //find anything that is floating and merge them into pieces
            bool[,] isTouchingBottom = new bool[boardTilesWidth,boardTilesHeight];

            
            //go across the bottom and flag every piece and recursively flag everything it touches
            for (int x = 0; x < board.GetLength(0); x++)
            {
                FlagLocationTouching(x, boardTilesHeight - 1,isTouchingBottom);
            }

            
            //go through every piece and flag any pieces that are anchor pieces
            for (int x = 0; x < board.GetLength(0); x++)
            {
                for (int y = 0; y < board.GetLength(1); y++)
                {
                    if (board[x, y] is {Anchored: true})
                    {
                        FlagLocationTouching(x, y,isTouchingBottom);
                    }
                }
            }

            
            
            //go through the unflagged locations that have board entries and form pieces for them
            for (int x = 0; x < board.GetLength(0); x++)
            {
                for (int y = 0; y < board.GetLength(1); y++)
                {
                    //this function will check if there is a piece there and if it needs to be added
                    AddPieceAt(x, y, isTouchingBottom);
                }
            }
            
            


            if (currentPieces.Count == 0)
            {
                FinalizeDamage();
                if (nextPiece is null)
                {
                    Die();
                    return;
                }
                currentPieces.Add(nextPiece!);
                //DropPuzzlePiece clone = (DropPuzzlePiece)nextPiece!.Clone();
                //clone.X = 0;
                //clone.pieceId = ++currentPieceId;
                //currentPieces.Add(clone);

                nextPiece = null;
                phase = Phase.Damage;
            }
            else
            {
                phase = Phase.Drop;
            }

        }
        
        

        int CalculateDamageCounterTarget()
        {
            return 1200 - (incomingDamage * 10);
        }
        
        void HandleDamagePhase()
        {
            //if no damage i want it to be 10 seconds
            //if 100 damage i want it to tick basically instantly
            if (damageCounter > CalculateDamageCounterTarget())
            //if (damageCounter > 5)
            {
                if (incomingDamage >= 8) //this should never be false but i'll doublecheck to be sure
                {
                    //remove a damage row
                    incomingDamage -= 8;

                    //check for death
                    for (int x = 0; x < board.GetLength(0); x++)
                    {
                        if (board[x, 0].HasValue)
                        {
                            Die();
                            return;
                        }
                    }

                    //move all pieces up 1
                    for (int x = 0; x < board.GetLength(0); x++)
                    {
                        for (int y = 1; y < board.GetLength(1); y++)
                        {
                            board[x, y - 1] = board[x, y];
                        }
                    }
                    
                    
                    //add in new damage and clear bottom row
                    for(int x = 0; x < damagePrediction!.grid.GetLength(0);x++)
                    {

                            //move piece horizontally based on center point 

                            int xpos = x;
                            int ypos = boardTilesHeight - 1;


                            board[xpos, ypos] = damagePrediction.grid[x, 0];
                    }
                    
                    GenerateNextDamage();
                }
            }



            phase = Phase.Break;
        }





        
        


        void Die()
        {
            needsReportDeath = true;
            phase = Phase.Dead;
        }
        
        
        
        void FinalizeDamage()
        {
            if (damageBlockCount > 0)
            {
                if (damageBlockCount > 3) //reset no matter what, but damage only happens in cases beyond a single tripple
                {

                        Fix64 rawDamage = ((Fix64.Pow(comboCount, new Fix64(9) / new Fix64(5)) +
                                            Fix64.Pow(new Fix64(damageBlockCount), new Fix64(6) / new Fix64(5)) /
                                            new Fix64(2)));
                        
                        //do counter damage
                        if (totalPlayers != 1 && incomingDamage > 0)
                        {
                            Fix64 tempIncomingDamage = new Fix64(incomingDamage);
                            if (rawDamage >= tempIncomingDamage)
                            {
                                rawDamage = rawDamage - tempIncomingDamage;
                                incomingDamage = 0;
                            }
                            else
                            {
                                tempIncomingDamage = tempIncomingDamage - rawDamage;
                                rawDamage = new Fix64(0);
                                incomingDamage = (int) tempIncomingDamage;
                            }
                        }

                        if (rawDamage == new Fix64(0))
                        {
                            damageToSend = null;
                        }
                        else
                        {
                            if (numberOfEnemies <= 0) //avoid div by zero
                                numberOfEnemies = 1;
                            damageToSend = (int)(rawDamage / new Fix64(numberOfEnemies));
                            if (numberOfEnemies > 1)
                                damageToSend += 1;

                            if (totalPlayers == 1) //practice mode should send the damage you send to yourself
                            {
                                damageToAdd = damageToSend;
                            }
                        }

                    
                }
                damageBlockCount = 0;
                comboCount = new Fix64(0);
            }
        }
        
        void FlagLocationTouching(int x, int y, bool[,] isTouchingBottom)
        {
            if (x < 0 || x >= boardTilesWidth || y < 0 || y >= boardTilesHeight)
                return;
            if (isTouchingBottom[x, y])
                return;
            if (board[x, y].HasValue)
            {
                isTouchingBottom[x, y] = true;
                FlagLocationTouching(x,y-1,isTouchingBottom);

                DropPuzzlePieceSegment value = board[x, y]!.Value;
                if (value.ConnectsRight)
                    FlagLocationTouching(x+1,y,isTouchingBottom);
                if (value.ConnectsLeft)
                    FlagLocationTouching(x-1,y,isTouchingBottom);
                if(value.ConnectsDown)
                    FlagLocationTouching(x,y+1,isTouchingBottom);
            }
        }
        
        
        void AddPieceAt(int x, int y, bool[,] isTouchingBottom)
        {
            if (x < 0 || x >= boardTilesWidth || y < 0 || y >= boardTilesHeight)
                return;
            if (isTouchingBottom[x, y])
                return;
            if (!board[x, y].HasValue)
                return;
            
            DropPuzzlePieceSegment?[,] segments = new DropPuzzlePieceSegment?[boardTilesWidth,boardTilesHeight];
            
            fillSegments(x, y, segments, isTouchingBottom);


            int minX = int.MaxValue;
            int minY = int.MaxValue;
            int maxX = int.MinValue;
            int maxY = int.MinValue;
            //connect all blocks in piece, also find the min and max x values
            for (int x2 = 0; x2 < board.GetLength(0); x2++)
            {
                for (int y2 = 0; y2 < board.GetLength(1); y2++)
                {
                    if (!segments[x2, y2].HasValue)
                        continue;

                    if (x2 < minX)
                        minX = x2;
                    if (x2 > maxX)
                        maxX = x2;
                    if (y2 < minY)
                        minY = y2;
                    if (y2 > maxY)
                        maxY = y2;
                    
                    DropPuzzlePieceSegment newValue = segments[x2, y2]!.Value;
                    
                    int x3 = x2+1;
                    int y3 = y2;

                    if (!(x3 < 0 || x3 >= boardTilesWidth || y3 < 0 ||
                        y3 >= boardTilesHeight) && segments[x3, y3].HasValue)
                    {
                        if (segments[x3, y3].HasValue)
                        {
                            
                            newValue.ConnectsRight = true;
                        }
                    }

                    x3 = x2-1;
                    y3 = y2;

                    if (!(x3 < 0 || x3 >= boardTilesWidth || y3 < 0 ||
                          y3 >= boardTilesHeight) && segments[x3, y3].HasValue)
                    {
                        if (segments[x3, y3].HasValue)
                        {
                            newValue.ConnectsLeft = true;
                        }
                    }
                    
                    x3 = x2;
                    y3 = y2-1;

                    if (!(x3 < 0 || x3 >= boardTilesWidth || y3 < 0 ||
                          y3 >= boardTilesHeight) && segments[x3, y3].HasValue)
                    {
                        if (segments[x3, y3].HasValue)
                        {
                            newValue.ConnectsUp = true;
                        }
                    }
                    
                    x3 = x2;
                    y3 = y2+1;

                    if (!(x3 < 0 || x3 >= boardTilesWidth || y3 < 0 ||
                          y3 >= boardTilesHeight) && segments[x3, y3].HasValue)
                    {
                        if (segments[x3, y3].HasValue)
                        {
                            newValue.ConnectsDown = true;
                        }
                    }
                    

                    segments[x2, y2] = newValue;
                }
            }
            
            
            //shrink the segments board to be as small as possible
            DropPuzzlePieceSegment?[,] shrunkSegments = new DropPuzzlePieceSegment?[maxX-minX+1,maxY-minY+1];

            for (int x2 = minX; x2 <= maxX; x2++)
            {
                for (int y2 = minY; y2 <= maxY; y2++)
                {
                    shrunkSegments[x2 - minX, y2 - minY] = segments[x2, y2];
                }
            }
            

            DropPuzzlePiece newPiece = new DropPuzzlePiece(++currentPieceId,shrunkSegments,minX * TileSize,minY*TileSize,false);
            currentPieces.Add(newPiece);
            


        }

        void fillSegments(int x, int y, DropPuzzlePieceSegment?[,] segments, bool[,] isTouchingBottom)
        {
            if (x < 0 || x >= boardTilesWidth || y < 0 || y >= boardTilesHeight)
                return;
            if (isTouchingBottom[x, y])
                return;
            if (!board[x, y].HasValue)
                return;
            
            segments[x, y] = new DropPuzzlePieceSegment(board[x, y]!.Value.Color, false, false, false, false, false);
            board[x, y] = null;

            //addsegment right
            fillSegments(x+1, y, segments, isTouchingBottom);
            //addsegment left
            fillSegments(x-1,y, segments, isTouchingBottom);
            //addsegment up
            fillSegments(x,y-1, segments, isTouchingBottom);
            //addsegment down
            fillSegments(x,y+1, segments, isTouchingBottom);

        }
        
        
        
        void BreakConnectionOfTouchingPiece(int x, int y, Direction direction)
        {
            switch (direction)
            {
                case Direction.Up:
                    y = y - 1;
                    break;
                case Direction.Down:
                    y = y + 1;
                    break;
                case Direction.Left:
                    x = x - 1;
                    break;
                case Direction.Right:
                    x = x + 1;
                    break;
                case Direction.None:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }


            if (x < 0 || x >= boardTilesWidth || y < 0 || y >= boardTilesHeight)
                return;

            if (board[x, y].HasValue)
            {
                DropPuzzlePieceSegment value = board[x, y]!.Value;
                    
                switch (direction)
                {
                    case Direction.Up:
                        value.ConnectsDown = false;
                        break;
                    case Direction.Down:
                        value.ConnectsUp = false;
                        break;
                    case Direction.Left:
                        value.ConnectsRight = false;
                        break;
                    case Direction.Right:
                        value.ConnectsLeft = false;
                        break;
                    case Direction.None:
                        throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
                    default:
                        throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
                }

                board[x, y] = value;

            }
        }

        private DropPuzzlePiece? FindFittingPiecePosition(DropPuzzlePiece rotatedPiece, DropPuzzlePiece unrotatedPiece)
        {
            if (rotatedPiece.X < 0)
                rotatedPiece.X = 0;
            if (rotatedPiece.X + TileSize * rotatedPiece.GetPieceTileWidth() > TileSize * boardTilesWidth)
                rotatedPiece.X = TileSize * (boardTilesWidth - rotatedPiece.GetPieceTileWidth());

            if (rotatedPiece.Y + TileSize * rotatedPiece.GetPieceTileHeight()> TileSize * boardTilesHeight)
                rotatedPiece.Y = TileSize * (boardTilesHeight - rotatedPiece.GetPieceTileHeight());


            if (!CheckPieceCollisionWithBoard(rotatedPiece, Direction.None,false).isCollided)
                return rotatedPiece;
            
            //see if piece is taller or wider
            if (rotatedPiece.GetPieceTileWidth() > unrotatedPiece.GetPieceTileWidth())
            {
                DropPuzzlePiece alligned = (DropPuzzlePiece)rotatedPiece.Clone();
                alligned.X -= alligned.X % TileSize;
                int allignedStartX = alligned.X;
                int allignedStartY = alligned.Y;

                for (int i = 0; i <= rotatedPiece.GetPieceTileWidth() / 2; i++)
                {
                    //check moving left
                    alligned.X = allignedStartX - i * TileSize;
                    if (alligned.X < 0)
                        alligned.X = 0;
                    if (!CheckPieceCollisionWithBoard(alligned, Direction.None,false).isCollided)
                        return alligned;
                    
                    //check at height brought to normal
                    alligned.Y += TileSize - (alligned.Y % TileSize);

                    if (alligned.Y + TileSize * alligned.GetPieceTileHeight() > TileSize * boardTilesHeight)
                        alligned.Y = TileSize * (boardTilesHeight - alligned.GetPieceTileHeight());

                    if (!CheckPieceCollisionWithBoard(alligned, Direction.None,false).isCollided)
                        return alligned;
                    
                    alligned.Y += TileSize;
                    if (alligned.Y + TileSize * alligned.GetPieceTileHeight()> TileSize * boardTilesHeight)
                        alligned.Y = TileSize * (boardTilesHeight - alligned.GetPieceTileHeight());

                    if (!CheckPieceCollisionWithBoard(alligned, Direction.None,false).isCollided)
                        return alligned;
                    alligned.Y = allignedStartY;

                    
                    //check moving right
                    alligned.X = allignedStartX + i * TileSize;
                    
                    if (alligned.X + TileSize * alligned.GetPieceTileWidth() > TileSize * boardTilesWidth)
                        alligned.X = TileSize * (boardTilesWidth - alligned.GetPieceTileWidth());

                    if (!CheckPieceCollisionWithBoard(alligned, Direction.None,false).isCollided)
                        return alligned;
                    
                    //check at height brought to normal
                    alligned.Y += TileSize - (alligned.Y % TileSize);

                    if (alligned.Y + TileSize * alligned.GetPieceTileHeight() > TileSize * boardTilesHeight)
                        alligned.Y = TileSize * (boardTilesHeight - alligned.GetPieceTileHeight());

                    if (!CheckPieceCollisionWithBoard(alligned, Direction.None,false).isCollided)
                        return alligned;
                    
                    alligned.Y += TileSize;
                    if (alligned.Y + TileSize * alligned.GetPieceTileHeight() > TileSize * boardTilesHeight)
                        alligned.Y = TileSize * (boardTilesHeight - alligned.GetPieceTileHeight());

                    if (!CheckPieceCollisionWithBoard(alligned, Direction.None,false).isCollided)
                        return alligned;
                    alligned.Y = allignedStartY;


                }
            }
            else
            {
                DropPuzzlePiece movedUpOne = (DropPuzzlePiece)rotatedPiece.Clone();
                movedUpOne.Y -= movedUpOne.Y % TileSize;
                var collisionDetails = CheckPieceCollisionWithBoard(movedUpOne, Direction.None,false);
                if (!collisionDetails.isCollided && !collisionDetails.withPiece)
                    return movedUpOne;
                movedUpOne.Y -= TileSize;
                collisionDetails = CheckPieceCollisionWithBoard(movedUpOne, Direction.None,false);
                if (!collisionDetails.isCollided && !collisionDetails.withPiece)
                    return movedUpOne;
            }
            
            

            return null;
        }

        private bool AddPieceToBoard(DropPuzzlePiece piece, bool meldWithConnected)
        {
            if (piece.X % TileSize > TileSize / 2)
            {
                piece.X += (TileSize - piece.X % TileSize);
            }
            else
            {
                piece.X -= piece.X % TileSize;
            }

            
            if (!meldWithConnected && !(CheckPieceCollisionWithBoard(piece, Direction.Down,false).isCollided || piece.Y + piece.GetPieceTileHeight() * TileSize > TileSize * boardTilesHeight - 1))
                return false;
            
            for(int x = 0; x < piece.grid.GetLength(0);x++)
            {
                for(int y = 0; y < piece.grid.GetLength(1);y++)
                {
                    if (piece.grid[x, y].HasValue)
                    {
                        //move piece horizontally based on center point 

                        int xpos = piece.X / TileSize + x;
                        int ypos = piece.Y / TileSize + y;
                        if (meldWithConnected)
                        {
                            ypos = (piece.Y + 15)/ TileSize + y;

                        }
                        if (ypos < 0)
                        {
                            Die();
                            return false;
                        }

                        DropPuzzlePieceSegment segment = piece.grid[x, y]!.Value;
                        
                        if (meldWithConnected)
                        {
                            if (CheckForTileNextToCurrentTile(xpos + 1, ypos))
                            {
                                segment.ConnectsRight = true;
                                DropPuzzlePieceSegment modified = board[xpos + 1, ypos]!.Value;
                                modified.ConnectsLeft = true;
                                board[xpos + 1, ypos] = modified;
                            }
                            if (CheckForTileNextToCurrentTile(xpos - 1, ypos))
                            {
                                segment.ConnectsLeft = true;
                                DropPuzzlePieceSegment modified = board[xpos - 1, ypos]!.Value;
                                modified.ConnectsRight = true;
                                board[xpos - 1, ypos] = modified;
                            }
                            if (CheckForTileNextToCurrentTile(xpos, ypos+1))
                            {
                                segment.ConnectsDown = true;
                                DropPuzzlePieceSegment modified = board[xpos, ypos+1]!.Value;
                                modified.ConnectsUp = true;
                                board[xpos, ypos+1] = modified;
                            }
                            if (CheckForTileNextToCurrentTile(xpos, ypos-1))
                            {
                                segment.ConnectsUp = true;
                                DropPuzzlePieceSegment modified = board[xpos, ypos-1]!.Value;
                                modified.ConnectsDown = true;
                                board[xpos, ypos-1] = modified;
                            }
                        }
                        
                        board[xpos, ypos] = segment;

                        
                    }
                }
            }

            return true;
        }
        
        
        private (bool isCollided,bool withPiece) CheckPieceCollisionWithBoard(DropPuzzlePiece piece, Direction direction, bool meldCheck)
        {
            //find all piece locations on grid
            for (int x = 0; x < piece.grid.GetLength(0); x++)
            {
                for (int y = 0; y < piece.grid.GetLength(1); y++)
                {
                    //check for piece below
                    if (piece.grid[x, y].HasValue)
                    {
                        int xpos;
                        int ypos;
                        int xpos2;
                        int ypos2;
                        bool checkpos2 = false;

                        switch (direction)
                        {
                            case Direction.Down:
                                xpos = piece.X / TileSize + x;
                                ypos = piece.Y / TileSize + y + 1;
                                xpos2 = piece.X / TileSize + x + 1;
                                ypos2 = piece.Y / TileSize + y + 1;
                                checkpos2 = piece.X % TileSize > 6;
                                break;
                            case Direction.Left:
                                xpos = piece.X / TileSize + x - 1;
                                ypos = piece.Y / TileSize + y;
                                xpos2 = piece.X / TileSize + x - 1;
                                ypos2 = piece.Y / TileSize + y + 1;
                                checkpos2 = piece.Y % TileSize > 6;
                                break;
                            case Direction.Right:
                                xpos = piece.X / TileSize + x + 1;
                                ypos = piece.Y / TileSize + y;
                                xpos2 = piece.X / TileSize + x + 1;
                                ypos2 = piece.Y / TileSize + y + 1;
                                checkpos2 = piece.Y % TileSize > 6;
                                break;
                            case Direction.None:
                                xpos = piece.X / TileSize + x;
                                ypos = piece.Y / TileSize + y;
                                xpos2 = piece.X / TileSize + x;
                                ypos2 = piece.Y / TileSize + y + 1;
                                checkpos2 = (piece.Y % TileSize > 6);
                                break;
                            case Direction.Up:
                                xpos = piece.X / TileSize + x;
                                ypos = piece.Y / TileSize + y - 1;
                                xpos2 = piece.X / TileSize + x + 1;
                                ypos2 = piece.Y / TileSize + y + 1;
                                checkpos2 = piece.X % TileSize > 6;
                                break;

                            default:
                                throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
                        }


                        if (meldCheck)
                        {
                            checkpos2 = false;
                            /*if ((piece.Y % TileSize > 16 && piece.Y % TileSize <= TileSize - 12))
                            {
                                return (false,false);
                            }*/
                            /*if ((piece.Y % TileSize > TileSize - 6 || piece.Y % TileSize <= 6))
                            {
                                return (false,false);
                            }*/
                            if (((piece.X + 16) % TileSize >= TileSize - 14 || (piece.X + 16) % TileSize <= 14))
                            {
                                return (false,false);
                            }

                            /*if ((direction == Direction.Left || direction == Direction.Right))
                            {
                                
                            
                                /*if ((piece.Y % TileSize > TileSize - 6 || piece.Y % TileSize <= 6))
                                {
                                    return (false,false);
                                }

                            }*/
                        }

                        if (((xpos >= 0 && ypos >= 0 && xpos < board.GetLength(0) && ypos < board.GetLength(1)) && board[xpos, ypos].HasValue)
                            || (checkpos2 && xpos2 >= 0 && ypos2 >= 0 && xpos2 < board.GetLength(0) && ypos2 < board.GetLength(1)) && board[xpos2, ypos2].HasValue)
                        {
                            return (true,false);
                        }
                    }
                }
            }

            if (meldCheck)
                return (false,false);
            //check against all other pieces
            foreach (DropPuzzlePiece comparedPiece in currentPieces)
            {
                if (comparedPiece.pieceId == piece.pieceId)
                {
                    continue;
                }
                
                
                //find all piece locations on grid
                for (int x = 0; x < piece.grid.GetLength(0); x++)
                {
                    for (int y = 0; y < piece.grid.GetLength(1); y++)
                    {
                        //check for piece below
                        if (piece.grid[x, y].HasValue)
                        {
                            int xpos;
                            int ypos;

                            switch (direction)
                            {
                                case Direction.Down:
                                    xpos = piece.X + x * TileSize;
                                    ypos = piece.Y + y * TileSize;
                                    break;
                                case Direction.Left:
                                    xpos = piece.X + x * TileSize - 4;
                                    ypos = piece.Y + y * TileSize;
                                    break;
                                case Direction.Right:
                                    xpos = piece.X + x * TileSize + 2;
                                    ypos = piece.Y + y * TileSize;
                                    break;
                                case Direction.None:
                                    xpos = piece.X + x * TileSize;
                                    ypos = piece.Y + y * TileSize;
                                    break;
                                case Direction.Up:
                                    xpos = piece.X + x * TileSize;
                                    ypos = piece.Y + y * TileSize;
                                    break;
                                default:
                                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
                            }
                            


                            for (int x2 = 0; x2 < comparedPiece.grid.GetLength(0); x2++)
                            {
                                for (int y2 = 0; y2 < comparedPiece.grid.GetLength(1); y2++)
                                {
                                    if (comparedPiece.grid[x2, y2].HasValue)
                                    {
                                        //find the other pieces location
                                        int cxpos = comparedPiece.X + x2 * TileSize;
                                        int cypos = comparedPiece.Y + y2 * TileSize;

                                        //the +4 on y shrinks the top of the checking piece so a piece jammed against it from the top doesn't report collisions
                                        if (xpos + TileSize > cxpos && xpos < cxpos + TileSize &&
                                            ypos + TileSize > cypos && ypos + 4 < cypos + TileSize)
                                        {
                                            return (true, true);
                                        }
                                    }
                                }
                            }

                            
                        }
                    }
                }
                
                
            }
            
            

            return (false,false);
        }

        void IGameObject.PlaySounds(NetSoundManager soundManager, int currentFrame)
        {
            
        }

        void IGameObject.Draw(PaletteEffectSpriteBatch context, float percentToNextFrame, IGameObject? nextFrame)
        {
            const int width = 32 * 8;
            const int height = 32 * 18;
            
            int totalBuffer = 1280 - (totalPlayers * width);
            int numberOfBuffers = 1 + totalPlayers;

            int bufferSize = totalBuffer/numberOfBuffers;
            
            
            
            int startX = bufferSize + (player * (bufferSize + width));
            int startY = 73;

            
            context.DrawRectangle(context.WhiteTexture(), new Vector2(startX, startY), width, height, Color.White, 1f,
                0f);

            for (int x = 0; x < width; x += 32)
            {
                for (int y = -66; y < height - 32; y += 32)
                {
                    context.DrawRectangle(context.WhiteTexture(), new Vector2(startX+x, startY+y), 32, 32, Color.Red, 1f,
                        100f);
                }

            }
            
            
            if (!(nextPiece is null))
            {
                nextPiece.drawInterpolated(context, startX, startY, false, percentToNextFrame, ((DropPuzzlePlayer?)nextFrame)?.nextPiece);
            }


            //if(totalPlayers > 1) //damage prediction block
            { 
                int damagePredictionY = (boardTilesHeight + 1) * (TileSize);

                
                context.DrawString(drawFont, $"{incomingDamage} -- {damageCounter} / {CalculateDamageCounterTarget()}",
                    new Ultraviolet.Vector2(startX, startY + damagePredictionY - 32), Color.White);

                for (int x = 0; x < width; x += 32)
                {
                    context.DrawRectangle(context.WhiteTexture(), new Vector2(startX + x, startY + damagePredictionY),
                        32, 32, Color.Red, 1f,
                        100f);
                }

                if (!(damagePrediction is null))
                {
                    damagePrediction.drawInterpolated(context, startX, startY + damagePredictionY, false, percentToNextFrame,
                        ((DropPuzzlePlayer?)nextFrame)?.damagePrediction);
                }
            }

            for (int i = 0; i < currentPieces.Count; i++)
            {
                DropPuzzlePiece piece = currentPieces[i];
                if (!(piece is null))  //i don't know why this is possible but on death i get a null pointer exception, may have to do with threading
                {
                    bool drawSticky = isMelding;
                    bool didDraw = false;
                    if (currentPieces.Count == ((DropPuzzlePlayer?) nextFrame)?.currentPieces.Count)
                    {
                        DropPuzzlePiece? nextFrameCurrentPiece = ((DropPuzzlePlayer?) nextFrame)?.currentPieces[i];
                        if (!(nextFrameCurrentPiece is null))
                        {
                            if (currentPieces[i].pieceId == nextFrameCurrentPiece.pieceId)
                                piece.drawInterpolated(context, startX, startY, drawSticky, percentToNextFrame,
                                    nextFrameCurrentPiece);
                            didDraw = true;
                        }

                    }

                    if (!didDraw)
                    {
                        piece.drawInterpolated(context, startX, startY, drawSticky, percentToNextFrame, null);
                    }
                }
            }

            //drawGameBoard
            for(int x = 0; x < board.GetLength(0);x++)
            {
                for(int y = 0; y < board.GetLength(1);y++)
                {
                    if (board[x, y].HasValue)
                    {
                        DropPuzzlePieceSegment segment = board[x, y]!.Value;
                        segment.Draw(context, startX+x*TileSize, startY+y*TileSize, false,null);
                    }
                }
            }
            
            //draw breaking pieces
            if (currentlyBreakingPieces != null)
            {
                foreach (var piece in currentlyBreakingPieces)
                {
                    piece.segment.Draw(context,startX+piece.x*TileSize,startY+piece.y*TileSize, false,(float)currentBreakTimerRemaining/breakTimerSetTime);
                }
            }


        }

        internal static void LoadContent(ContentManager content)
        {
            drawFont ??= content.Load<UltravioletFont>("Fonts\\Aileron-Regular");
            

        }

        (bool isColliding, ICollisionDetails? details) IGameObject.CheckCollision(IGameObject otherObject)
        {
            return (true, null);
        }

        void ReceiveDamage(int damage)
        {
            if (damageToAdd is null)
            {
                damageToAdd = 0;
            }
            damageToAdd += damage;
        }
        
        void IGameObject.AddCollisionCommand(IGameObject otherObject, ICollisionDetails? details, bool performedCollisionCheck)
        {
            if (otherObject.GetType() == typeof(DropPuzzlePlayer))
            {
                if (damageToSend is null) return;
                DropPuzzlePlayer otherPlayer = (DropPuzzlePlayer) otherObject;
                otherPlayer.ReceiveDamage(damageToSend.Value);
            }
            else if (otherObject.GetType() == typeof(DropPuzzleGameManager))
            {
                DropPuzzleGameManager gameManager = (DropPuzzleGameManager) otherObject;
                updatedNumEnemies = gameManager.LivingPlayers - 1;
                if (needsReportDeath)
                {
                    gameManager.ReportDeath();
                    needsReportDeath = false;
                }
            }
        }
        
        
        

        void IGameObject.ExecuteCollisionCommands(OnlineGameLoop caller)
        {
            if (!(damageToAdd is null))
            {
                incomingDamage += damageToAdd.Value;
            }

            if (!(updatedNumEnemies is null))
            {
                numberOfEnemies = updatedNumEnemies.Value;
            }
        }

        ulong IGameObject.CalculateDesynchronizationCheckValue(bool verbose)
        {
            return (ulong)currentPieceId;
        }

        static HashSet<Type>? typeList;
        HashSet<Type> IGameObject.GetCollidableClasses()
        {
            if (typeList is null)
            {
                typeList = new HashSet<Type>
                {
                    typeof(DropPuzzleGameManager),
                    typeof(DropPuzzlePlayer)
                };
            }
            return typeList;
        }

        void IPlayerGameObject.SetInput(InputObject input)
        {
            this.input = input;
        }
    }
}