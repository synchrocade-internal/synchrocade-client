using System;
using System.Drawing.Imaging;
using Ultraviolet;

namespace Synchrocade.GameObjects.UniqueGameObjects.DropPuzzleObjects
{

    internal class DropPuzzlePiece : ICloneable
    {
        private const int tileWidth = DropPuzzleConstants.tileSize;
        private const int pieceForgivness = DropPuzzleConstants.pieceForgivness;

        
        public DropPuzzlePieceSegment?[,] grid;
        internal int X;
        internal int Y;
        internal int forgiveness;
        //internal bool anchored;
        internal int pieceId;
        
        internal int desireLeft = 0;
        internal int desireRight = 0;
        internal bool rotateLeftPressed = false;
        internal bool rotateRightPressed = false;

        internal DropPuzzlePiece(int pieceId, DropPuzzlePieceSegment?[,] grid, int x, int y, bool anchored)
        {
            this.pieceId = pieceId;
            this.grid = grid;
            this.forgiveness = pieceForgivness;
            this.X = x;
            this.Y = y;
            //this.anchored = anchored;
        }
        
        internal DropPuzzlePiece(int pieceId, DropPuzzlePieceSegment?[,] grid, int x, int y, int forgiveness, int desireLeft, int desireRight, bool rotateLeftPressed, bool rotateRightPressed)
        {
            this.pieceId = pieceId;
            this.grid = grid;
            this.forgiveness = forgiveness;
            this.X = x;
            this.Y = y;
            //this.anchored = anchored;
            
            this.desireLeft = desireLeft;
            this.desireRight = desireRight;
            this.rotateLeftPressed = rotateLeftPressed;
            this.rotateRightPressed = rotateRightPressed;
        }
        
        internal DropPuzzlePiece(int pieceId, DropPuzzlePieceSegment?[,] grid, int x, int y, bool anchored, int forgiveness)
        {
            this.pieceId = pieceId;
            this.grid = grid;
            this.forgiveness = forgiveness;
            this.X = x;
            this.Y = y;
            //this.anchored = anchored;
            
            this.desireLeft = 0;
            this.desireRight = 0;
            this.rotateLeftPressed = false;
            this.rotateRightPressed = false;
        }
        
        internal static DropPuzzlePiece ProduceSingle(int pieceId, int color1, int x, int y, bool anchored)
        {
            DropPuzzlePieceSegment?[,] grid = new DropPuzzlePieceSegment?[1,1];
            grid[0, 0] = new DropPuzzlePieceSegment(color1, false, false, false, false, anchored);
            return new DropPuzzlePiece(pieceId, grid, x, y, anchored, pieceForgivness);
        }
        
        internal static DropPuzzlePiece ProduceDouble(int pieceId, int color1, int color2, int x, int y)
        {
            DropPuzzlePieceSegment?[,] grid = new DropPuzzlePieceSegment?[2,1];
            grid[0, 0] = new DropPuzzlePieceSegment(color1, true, false, false, false, false);
            grid[1, 0] = new DropPuzzlePieceSegment(color2, false, true, false, false, false);
            return new DropPuzzlePiece(pieceId, grid, x, y, false, pieceForgivness);
        }
        
        internal static DropPuzzlePiece ProduceLinearTriple(int pieceId, int color1, int color2, int color3, int x, int y)
        {
            DropPuzzlePieceSegment?[,] grid = new DropPuzzlePieceSegment?[3,1];
            grid[0, 0] = new DropPuzzlePieceSegment(color1, true, false, false, false, false);
            grid[1, 0] = new DropPuzzlePieceSegment(color2, true, true, false, false, false);
            grid[2, 0] = new DropPuzzlePieceSegment(color3, false, true, false, false, false);
            return new DropPuzzlePiece(pieceId, grid, x, y, false, pieceForgivness);
        }
        
        internal static DropPuzzlePiece ProduceL(int pieceId, int color1, int color2, int color3, int x, int y)
        {
            DropPuzzlePieceSegment?[,] grid = new DropPuzzlePieceSegment?[2,2];
            grid[0, 0] = new DropPuzzlePieceSegment(color1, false, false, false, true, false);
            grid[0, 1] = new DropPuzzlePieceSegment(color2, true, false, true, false, false);
            grid[1, 1] = new DropPuzzlePieceSegment(color3, false, true, false, false, false);
            return new DropPuzzlePiece(pieceId, grid, x, y, false, pieceForgivness);
        }
        
        internal static DropPuzzlePiece ProduceInverseL(int pieceId, int color1, int color2, int color3, int x, int y)
        {
            DropPuzzlePieceSegment?[,] grid = new DropPuzzlePieceSegment?[2,2];
            grid[1, 0] = new DropPuzzlePieceSegment(color1, false, false, false, true, false);
            grid[0, 1] = new DropPuzzlePieceSegment(color2, true, false, false, false, false);
            grid[1, 1] = new DropPuzzlePieceSegment(color3, false, true, true, false, false);
            return new DropPuzzlePiece(pieceId, grid, x, y, false, pieceForgivness);
        }
        
        internal static DropPuzzlePiece ProduceDamageRow(bool isEvenRow, int color1, int color2, int color3, int color4)
        {
            DropPuzzlePieceSegment?[,] grid = new DropPuzzlePieceSegment?[8,1];
            int offset = 0;
            if (isEvenRow)
                offset = 1;
            
            grid[0+offset, 0] = new DropPuzzlePieceSegment(color1, false, false, false, false, true);
            grid[2+offset, 0] = new DropPuzzlePieceSegment(color2, false, false, false, false, true);
            grid[4+offset, 0] = new DropPuzzlePieceSegment(color3, false, false, false, false, true);
            grid[6+offset, 0] = new DropPuzzlePieceSegment(color4, false, false, false, false, true);

            return new DropPuzzlePiece(-1, grid, 0, 0, true, pieceForgivness);
        }
        
        
        public object Clone()
        {
            DropPuzzlePiece clonedPiece = (DropPuzzlePiece) this.MemberwiseClone();
            clonedPiece.grid = (DropPuzzlePieceSegment?[,]) this.grid.Clone();
            return clonedPiece;
        }

        internal DropPuzzlePiece RotateRight()
        {
            DropPuzzlePieceSegment?[,] rotatedGrid = new DropPuzzlePieceSegment?[grid.GetLength(1), grid.GetLength(0)];
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    DropPuzzlePieceSegment? nonRotatedPiece = grid[i,grid.GetLength(1) - j - 1];

                    if (!(nonRotatedPiece is null))
                    {
                        DropPuzzlePieceSegment rotatedPiece = new DropPuzzlePieceSegment(nonRotatedPiece.Value.Color,
                            nonRotatedPiece.Value.ConnectsUp, nonRotatedPiece.Value.ConnectsDown, nonRotatedPiece.Value.ConnectsLeft,
                            nonRotatedPiece.Value.ConnectsRight, nonRotatedPiece.Value.Anchored);
                        rotatedGrid[j, i] = rotatedPiece;
                    }
                    
                }
            }

            //find the value the width has changed
            int gridChange = grid.GetLength(0) - rotatedGrid.GetLength(0);
            //move it half of that
            int newX = this.X + (gridChange / 2)*tileWidth;
            
            //find the value the height has changed
            int heightChange = grid.GetLength(1) - rotatedGrid.GetLength(1);
            //move it half of that
            int newY = this.Y + (heightChange / 2)*tileWidth;
            
            return new DropPuzzlePiece(pieceId, rotatedGrid, newX, newY, this.forgiveness, desireLeft,desireRight,rotateLeftPressed,rotateRightPressed);
        }
        
        
        internal DropPuzzlePiece RotateLeft()
        {
            DropPuzzlePieceSegment?[,] rotatedGrid = new DropPuzzlePieceSegment?[grid.GetLength(1), grid.GetLength(0)];
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    DropPuzzlePieceSegment? nonRotatedPiece = grid[grid.GetLength(0) - i - 1, j];

                    if (!(nonRotatedPiece is null))
                    {
                        DropPuzzlePieceSegment rotatedPiece = new DropPuzzlePieceSegment(nonRotatedPiece.Value.Color,
                            nonRotatedPiece.Value.ConnectsDown, nonRotatedPiece.Value.ConnectsUp, nonRotatedPiece.Value.ConnectsRight,
                            nonRotatedPiece.Value.ConnectsLeft, nonRotatedPiece.Value.Anchored);
                        rotatedGrid[j, i] = rotatedPiece;
                    }
                    
                }
            }

            
            //find the value the width has changed
            int withChange = grid.GetLength(0) - rotatedGrid.GetLength(0);
            //move it half of that
            int newX = this.X + (withChange / 2)*tileWidth;
            
            
            //find the value the height has changed
            int heightChange = grid.GetLength(1) - rotatedGrid.GetLength(1);
            //move it half of that
            int newY = this.Y + (heightChange / 2)*tileWidth;
            

            return new DropPuzzlePiece(pieceId, rotatedGrid, newX, newY, this.forgiveness,desireLeft,desireRight,rotateLeftPressed,rotateRightPressed);
        }

        internal int GetPieceTileWidth()
        {
            return grid.GetLength(0);
        }
        internal int GetPieceTileHeight()
        {
            return grid.GetLength(1);
        }
        
        internal void drawInterpolated(PaletteEffectSpriteBatch context, int startX, int startY, bool isMelding, float percentToNextFrame, DropPuzzlePiece? nextFrame)
        {
            float interpolatedX;
            float interpolatedY;
            if (!(nextFrame is null))
            {
                interpolatedX = Ultraviolet.Core.MathUtil.Lerp((float) this.X, (float) nextFrame.X, percentToNextFrame);
                interpolatedY = Ultraviolet.Core.MathUtil.Lerp((float) this.Y, (float) nextFrame.Y, percentToNextFrame);
            }
            else
            {
                interpolatedX = this.X;
                interpolatedY = this.Y;
            }

            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    if (!(grid[i, j] is null))
                    {
                        DropPuzzlePieceSegment segment = grid[i, j]!.Value;
                        segment.Draw(context, startX+interpolatedX+i*32,startY+interpolatedY+j*32, isMelding,null);
                    }
                }
            }
            
            

        }
    }
    
    internal struct DropPuzzlePieceSegment
    {
        internal int Color;
        internal bool ConnectsRight;
        internal bool ConnectsLeft;
        internal bool ConnectsUp;
        internal bool ConnectsDown;
        internal bool Anchored;
        
        


        public DropPuzzlePieceSegment(int color, bool connectsRight, bool connectsLeft, bool connectsUp, bool connectsDown, bool anchored)
        {
            Color = color;
            this.ConnectsRight = connectsRight;
            this.ConnectsLeft = connectsLeft;
            this.ConnectsUp = connectsUp;
            this.ConnectsDown = connectsDown;
            Anchored = anchored;
            
        }

        internal readonly void Draw(PaletteEffectSpriteBatch context, float X, float Y, bool isMelding, float? percent)
        {
            Ultraviolet.Color color;
            switch (Color)
            {
                case 1:
                    color = Ultraviolet.Color.Red;
                    break;
                case 2:
                    color = Ultraviolet.Color.Blue;
                    break;
                case 3:
                    color = Ultraviolet.Color.Green;
                    break;
                case 4:
                    color = Ultraviolet.Color.Purple;
                    break;
                case 5:
                    color = Ultraviolet.Color.Orange;
                    break;
                default:
                    color = Ultraviolet.Color.White;
                    break;
            }

            if (!(percent is null))
            {
                context.DrawFilledRectangle(context.WhiteTexture(),new Vector2(X+(1-percent.Value)*16,Y+(1-percent.Value)*16),32*percent.Value,32*percent.Value,color,3f);

            }
            else
            {
                Ultraviolet.Color borderColor = Ultraviolet.Color.White;
                if (isMelding)
                {
                    borderColor = Ultraviolet.Color.Yellow;
                }
                context.DrawFilledRectangle(context.WhiteTexture(),new Vector2(X,Y),32,32,color,3f);
                if(!ConnectsRight)
                    context.DrawLine(context.WhiteTexture(),X+32,Y+0,X+32,Y+32,borderColor,4f,2f);
                if(!ConnectsDown)
                    context.DrawLine(context.WhiteTexture(),X+0,Y+32,X+32,Y+32,borderColor,4f,2f);
                if(!ConnectsLeft)
                    context.DrawLine(context.WhiteTexture(),X+0,Y+0,X+0,Y+32,borderColor,4f,2f);
                if(!ConnectsUp)
                    context.DrawLine(context.WhiteTexture(),X+0,Y+0,X+32,Y+0,borderColor,4f,2f);

                if (Anchored)
                    context.DrawPoint(context.WhiteTexture(), new Vector2(X + 16, Y + 16), Ultraviolet.Color.White, 4, 2f,
                        true);
            }

        }
 
        
        
    }
    
    
}