using System;
using System.Collections.Generic;
using System.Linq;
using FixMath.NET;
using Serilog;
using Synchrocade.GameObjects.Sound;
using Synchrocade.ReusableFunctions;
using Ultraviolet;

namespace Synchrocade.GameObjects.UniqueGameObjects.DropPuzzleObjects
{
    internal class DropPuzzleGameManager : IManagerGameObject
    {
        private readonly int players;
        private long[] currentScores;
        public int LivingPlayers;
        
        private int reportedDeathCount = 0;
        
        internal DropPuzzleGameManager(int players)
        {
            
            this.players = players;
            this.LivingPlayers = players;
            currentScores = new long[players];
        }

        public bool IsGameFinished()
        {
            return false;
        }

        public void ReportDeath()
        {
            reportedDeathCount++;
        }
        
        

        public long[] ReturnScores()
        {
            return currentScores;
        }


        public void Update(OnlineGameLoop caller)
        {

        }
        
        
        public (bool isColliding, ICollisionDetails? details) CheckCollision(IGameObject otherObject)
        {
            return (true, null);
        }

        public void AddCollisionCommand(IGameObject otherObject, ICollisionDetails? details, bool performedCollisionCheck)
        {
        }

        public void ExecuteCollisionCommands(OnlineGameLoop caller)
        {
            LivingPlayers -= reportedDeathCount;
            reportedDeathCount = 0;
            //don't need to do anything here because order literally doesn't matter in this case as this object has no
            //physicality
        }

        public ulong CalculateDesynchronizationCheckValue(bool verbose)
        {

            ulong allScores = 0;
            foreach (long score in currentScores)
            {
                allScores += (ulong)score;
            }

            for (int i = 0; i < currentScores.Length; i++)
            {
                if(verbose)
                    Log.Error($"The game manager thinks Player {i} has a score of {currentScores[i]}");

                allScores += (ulong)currentScores[i];
            }
            
            if(verbose)
                Log.Error($"The game manager returns a check value of {allScores}");

            return allScores;
        }

        
        static HashSet<Type>? typeList;
        HashSet<Type> IGameObject.GetCollidableClasses()
        {
            if (typeList is null)
            {
                typeList = new HashSet<Type>
                {
                    typeof(DropPuzzlePlayer)
                };
            }
            return typeList;
        }

        public object Clone()
        {
            DropPuzzleGameManager clone = (DropPuzzleGameManager) this.MemberwiseClone();
            clone.currentScores = (long[])currentScores.Clone();

            return clone;
        }

        public int DeletedFrame()
        {
            return -1;
        }

        public void PlaySounds(NetSoundManager soundManager, int currentFrame)
        {
        }

        public void Draw(PaletteEffectSpriteBatch context, float percentToNextFrame, IGameObject? nextFrame)
        {
        }

    }
}