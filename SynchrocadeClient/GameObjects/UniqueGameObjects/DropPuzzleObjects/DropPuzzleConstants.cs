namespace Synchrocade.GameObjects.UniqueGameObjects.DropPuzzleObjects
{
    public static class DropPuzzleConstants
    {
        public const int tileSize = 32;
        public const int pieceForgivness = 30;
    }
}