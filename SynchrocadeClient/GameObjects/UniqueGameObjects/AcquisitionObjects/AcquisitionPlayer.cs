using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using FixMath.NET;
using Microsoft.CodeAnalysis;
using Serilog;
using Synchrocade.GameObjects.Input;
using Synchrocade.GameObjects.Sound;
using Synchrocade.ReusableFunctions;
using Ultraviolet;
using Ultraviolet.Audio;
using Ultraviolet.Content;
using Ultraviolet.Core.Text;
using Ultraviolet.FreeType2;
using Ultraviolet.Graphics.Graphics2D;
using Ultraviolet.Graphics.Graphics2D.Text;
using Ultraviolet.Presentation.Uvss.Syntax;
using Color = Ultraviolet.Color;
using Vector2 = System.Numerics.Vector2;




namespace Synchrocade.GameObjects.UniqueGameObjects.AcquisitionObjects
{
    enum AcquisitionPlayerHeading
    {
        Stopped,
        Up,
        Down,
        Left,
        Right
    }


    internal struct Turn
    {
        internal int nodeX;
        internal int nodeY;

        internal Turn(int nodeX, int nodeY)
        {
            this.nodeX = nodeX;
            this.nodeY = nodeY;
        }
    }

    internal struct Territory
    {
        public ImmutableArray<Turn> walls;
        public ImmutableArray<(bool isInWall, bool isEntryOrExit)> wallData;
        public int[,] capturedNodes;
        public int captureSize;
        public int turnsLength; //used just to tell apart two cases of entry/exit in wall when dealing with captured territory


    }
    
    internal class AcquisitionPlayer : IPlayerGameObject
    {
        private bool isFrozenForEndgame = false;
        private int endGameWinner = -1;
        private int frameOfEndgame = 0;
        
        
        public Territory? territoryScoredThisFrame = null;
        private List<(Territory territory,int player)> opponentsTerritoriesScoredThisFrame = new List<(Territory territory,int player)>(); //doesn't need saved because it happens within a single frame
        
        internal Fix64 X;
        internal Fix64 Y;

        private int nodeX;
        private int nodeY;
        
        private bool needsRespawn = false;
        private bool respawnedThisFrame = false;
        int? playerToGiveKillBonus = null;

        
        internal readonly int Player;
        private InputObject input;

        private int[,] nodes = new int[(nodesWidth*2+1), (nodesHeight*2 + 1)];  //0 is empty, 1 is inaccessible nodes (wall already exists around it)
        private int[,] myTerritory = new int[(nodesWidth*2+1), (nodesHeight*2 + 1)];

        private const int nodesWidth = AcquisitionSharedFunctions.nodesWidth;
        private const int nodesHeight = AcquisitionSharedFunctions.nodesHeight;
        
        private AcquisitionPlayerHeading heading = AcquisitionPlayerHeading.Stopped;

        private static UltravioletFont? drawFont = null;

        //private int[,] nodes = new int[63, 33];  //0 is empty, 1 is edge, 2 is territory i now own
        internal List<Turn> Walls;
        private List<Turn> turns = new List<Turn>();
        private List<Territory> territories = new List<Territory>();
        public int allTerritorySize = 0;
        public long score = 0;

        private int bonusPointsToScoreInExecution = 0;
        
        //this is all stuff that is passed from the pre-collision phase to the post collision phase
        private bool desireUp = false;
        private bool desireDown = false;
        private bool desireLeft = false;
        private bool desireRight = false;
        private bool onNode = false;
        Fix64 postNodeMovement = new Fix64(0);



        //collision related variables
        private bool canMoveUp = true;
        private bool canMoveDown = true;
        private bool canMoveLeft = true;
        private bool canMoveRight = true;

        private string playerName;


        private bool linesCrossing = false;
        
        
        private static SoundEffect? drawLoop;
        private NetSound drawLoopDetails;
        private static SoundEffect? playerRespawn;
        private NetSound playerRespawnDetails;
        private static SoundEffect? territoryCapture;
        private NetSound territoryCaptureDetails;
        private static SoundEffect? victorySound;
        private NetSound victorySoundDetails;

        public AcquisitionPlayer(string playerName, int nodeX, int nodeY, int player, OnlineGameLoop owner)
        {
            this.owner = owner;
            this.playerName = playerName;
            this.Player = player;
            (int x, int y) coors = AcquisitionSharedFunctions.GetNodeCoordinates(nodeX, nodeY);
            this.X = new Fix64(coors.x);
            this.Y = new Fix64(coors.y);
            Walls = AcquisitionSharedFunctions.GetStartingWalls();
            
            drawLoopDetails = new NetSound(drawLoop!,owner); 
            drawLoopDetails.volume = .4f;
            drawLoopDetails.looping = true;
            
            playerRespawnDetails = new NetSound(playerRespawn!,owner); 
            playerRespawnDetails.volume = .3f;
            playerRespawnDetails.looping = false;
            
            territoryCaptureDetails = new NetSound(territoryCapture!,owner); 
            territoryCaptureDetails.volume = .3f;
            territoryCaptureDetails.looping = false;
            
            victorySoundDetails = new NetSound(victorySound!,owner); 
            victorySoundDetails.volume = .3f;
            victorySoundDetails.looping = false;
        }
        


        public (bool found, bool corner, (Turn lineStart, Turn lineEnd)[]? containedLines) isPointOnWall(int pointX, int pointY)
        {
            return AcquisitionSharedFunctions.IsPointInTurnList(Walls, pointX, pointY);
        }

        public (bool canMoveUp, bool canMoveDown, bool canMoveLeft, bool canMoveRight)
            CheckForNodeCollisionsAroundNode(int checkPointX, int checkPointY)
        {
            bool up = true;
            bool down = true;
            bool left = true;
            bool right = true;
            (int nodeX, int nodeY) checkPointUp = (checkPointX, checkPointY - 1);
            (int nodeX, int nodeY) checkPointDown = (checkPointX, checkPointY + 1);
            (int nodeX, int nodeY) checkPointLeft = (checkPointX - 1, checkPointY);
            (int nodeX, int nodeY) checkPointRight = (checkPointX + 1, checkPointY);

            if((checkPointUp.nodeX >= 0 && checkPointUp.nodeX <= nodesWidth) && (checkPointUp.nodeY >= 0 && checkPointUp.nodeY <= nodesHeight))
            {
                if (nodes[checkPointUp.nodeX*2, checkPointUp.nodeY*2] == 1)
                {
                    up = false;
                }
                if (nodes[checkPointUp.nodeX*2, checkPointUp.nodeY*2+1] == 1)
                {
                    up = false;
                }
            }

            if ((checkPointDown.nodeX >= 0 && checkPointDown.nodeX <= nodesWidth) &&
                (checkPointDown.nodeY >= 0 && checkPointDown.nodeY <= nodesHeight))
            {
                if (nodes[checkPointDown.nodeX*2, checkPointDown.nodeY*2] == 1)
                {
                    down = false;
                }
                if (nodes[checkPointDown.nodeX*2, checkPointDown.nodeY*2-1] == 1)
                {
                    down = false;
                }
            }

            if ((checkPointLeft.nodeX >= 0 && checkPointLeft.nodeX <= nodesWidth) &&
                (checkPointLeft.nodeY >= 0 && checkPointLeft.nodeY <= nodesHeight))
            {
                if (nodes[checkPointLeft.nodeX*2, checkPointLeft.nodeY*2] == 1)
                {
                    left = false;
                }
                if (nodes[checkPointLeft.nodeX*2+1, checkPointLeft.nodeY*2] == 1)
                {
                    left = false;
                }
            }

            if ((checkPointRight.nodeX >= 0 && checkPointRight.nodeX <= nodesWidth) &&
                (checkPointRight.nodeY >= 0 && checkPointRight.nodeY <= nodesHeight))
            {
                if (nodes[checkPointRight.nodeX*2, checkPointRight.nodeY*2] == 1)
                {
                    right = false;
                }
                if (nodes[checkPointRight.nodeX*2-1, checkPointRight.nodeY*2] == 1)
                {
                    right = false;
                }
            }

            return (up,down,left,right);

        }

        public (bool canMoveUp, bool canMoveDown, bool canMoveLeft, bool canMoveRight) CheckForPlayerCollisionsAroundNode(int checkPointX, int checkPointY,bool isThisPlayer)
        {
            bool up = true;
            bool down = true;
            bool left = true;
            bool right = true;
            (int nodeX, int nodeY) checkPointUp = (checkPointX, checkPointY - 1);
            (int nodeX, int nodeY) checkPointDown = (checkPointX, checkPointY + 1);
            (int nodeX, int nodeY) checkPointLeft = (checkPointX - 1, checkPointY);
            (int nodeX, int nodeY) checkPointRight = (checkPointX + 1, checkPointY);

            
            Turn? priorTurn = null;
            Turn? twoTurnsAgo = null;
            foreach (Turn turn in turns)
            {
                if (priorTurn is null || twoTurnsAgo is null)
                {
                    twoTurnsAgo = priorTurn;
                    priorTurn = turn;
                    continue;
                }


                if (AcquisitionSharedFunctions.IsPointInLine(twoTurnsAgo.Value.nodeX, twoTurnsAgo.Value.nodeY, priorTurn.Value.nodeX,
                    priorTurn.Value.nodeY, checkPointUp.nodeX, checkPointUp.nodeY)) 
                    up = false;
                if (AcquisitionSharedFunctions.IsPointInLine(twoTurnsAgo.Value.nodeX, twoTurnsAgo.Value.nodeY, priorTurn.Value.nodeX,
                    priorTurn.Value.nodeY, checkPointDown.nodeX, checkPointDown.nodeY)) 
                    down = false;
                if (AcquisitionSharedFunctions.IsPointInLine(twoTurnsAgo.Value.nodeX, twoTurnsAgo.Value.nodeY, priorTurn.Value.nodeX,
                    priorTurn.Value.nodeY, checkPointLeft.nodeX, checkPointLeft.nodeY)) 
                    left = false;
                if (AcquisitionSharedFunctions.IsPointInLine(twoTurnsAgo.Value.nodeX, twoTurnsAgo.Value.nodeY, priorTurn.Value.nodeX,
                    priorTurn.Value.nodeY, checkPointRight.nodeX, checkPointRight.nodeY)) 
                    right = false;
                
                twoTurnsAgo = priorTurn;
                priorTurn = turn;
            }


            //if i'm not the player then add the currently explored line
            if (!isThisPlayer && !(priorTurn is null))
            {
                if (!(twoTurnsAgo is null))
                {
                    if (AcquisitionSharedFunctions.IsPointInLine(twoTurnsAgo.Value.nodeX, twoTurnsAgo.Value.nodeY, priorTurn.Value.nodeX,
                        priorTurn.Value.nodeY, checkPointUp.nodeX, checkPointUp.nodeY))
                        up = false;
                    if (AcquisitionSharedFunctions.IsPointInLine(twoTurnsAgo.Value.nodeX, twoTurnsAgo.Value.nodeY, priorTurn.Value.nodeX,
                        priorTurn.Value.nodeY, checkPointDown.nodeX, checkPointDown.nodeY))
                        down = false;
                    if (AcquisitionSharedFunctions.IsPointInLine(twoTurnsAgo.Value.nodeX, twoTurnsAgo.Value.nodeY, priorTurn.Value.nodeX,
                        priorTurn.Value.nodeY, checkPointLeft.nodeX, checkPointLeft.nodeY))
                        left = false;
                    if (AcquisitionSharedFunctions.IsPointInLine(twoTurnsAgo.Value.nodeX, twoTurnsAgo.Value.nodeY, priorTurn.Value.nodeX,
                        priorTurn.Value.nodeY, checkPointRight.nodeX, checkPointRight.nodeY))
                        right = false;
                }

                if (AcquisitionSharedFunctions.IsPointInLine(priorTurn.Value.nodeX, priorTurn.Value.nodeY, nodeX,
                    nodeY, checkPointUp.nodeX, checkPointUp.nodeY)) 
                    up = false;
                if (AcquisitionSharedFunctions.IsPointInLine(priorTurn.Value.nodeX, priorTurn.Value.nodeY, nodeX,
                    nodeY, checkPointDown.nodeX, checkPointDown.nodeY)) 
                    down = false;
                if (AcquisitionSharedFunctions.IsPointInLine(priorTurn.Value.nodeX, priorTurn.Value.nodeY, nodeX,
                    nodeY, checkPointLeft.nodeX, checkPointLeft.nodeY)) 
                    left = false;
                if (AcquisitionSharedFunctions.IsPointInLine(priorTurn.Value.nodeX, priorTurn.Value.nodeY, nodeX,
                    nodeY, checkPointRight.nodeX, checkPointRight.nodeY)) 
                    right = false;
            }

            
            //here's where we need to add the hacks to remove specific nodes i SHOULD be able to enter
            if (isThisPlayer && !(twoTurnsAgo is null))
            {
                //i need to check for the last node of the prior turn so i can return
                if (twoTurnsAgo.Value.nodeX == checkPointUp.nodeX && twoTurnsAgo.Value.nodeY == checkPointUp.nodeY)
                    up = true;
                if (twoTurnsAgo.Value.nodeX == checkPointDown.nodeX && twoTurnsAgo.Value.nodeY == checkPointDown.nodeY)
                    down = true;
                if (twoTurnsAgo.Value.nodeX == checkPointLeft.nodeX && twoTurnsAgo.Value.nodeY == checkPointLeft.nodeY)
                    left = true;
                if (twoTurnsAgo.Value.nodeX == checkPointRight.nodeX && twoTurnsAgo.Value.nodeY == checkPointRight.nodeY)
                    right = true;
            }
            
            if (!isThisPlayer)
            {
                //wall nodes also should also not be considered collisions, you should be able to use the wall to go around your opponent
                if (isPointOnWall(checkPointUp.nodeX, checkPointUp.nodeY).found)
                    up = true;
                if (isPointOnWall(checkPointDown.nodeX, checkPointDown.nodeY).found)
                    down = true;
                if (isPointOnWall(checkPointLeft.nodeX, checkPointLeft.nodeY).found)
                    left = true;
                if (isPointOnWall(checkPointRight.nodeX, checkPointRight.nodeY).found)
                    right = true;
            }

            return (up,down,left,right);


        }

        
        public static void LoadContent(ContentManager contentManager)
        {
            drawFont ??= contentManager.Load<UltravioletFont>("Fonts\\Aileron-Regular");
            if(drawLoop is null)
                drawLoop = contentManager.Load<SoundEffect>("Unique/Acquisition/Sound/drawloop.ogg", false);
            if(playerRespawn is null)
                playerRespawn = contentManager.Load<SoundEffect>("Unique/Acquisition/Sound/playerrespawn.ogg", false);
            if(territoryCapture is null)
                territoryCapture = contentManager.Load<SoundEffect>("Unique/Acquisition/Sound/territorycapture.ogg", false);

            if(victorySound is null)
                victorySound = contentManager.Load<SoundEffect>("Unique/Acquisition/Sound/victorysound.ogg", false);

        }

        private void SetNodeLocation()
        {
            //20,60 - 1260,700
            
            //find x node location
            nodeX = nodesWidth - (((1280 - 20) - (int) X) / 20);

                
            //find y node location
            //700-60
            nodeY = nodesHeight - ((720 - 20) - (int) Y) / 20;
        }


        
        
        
        public object Clone()
        {
            AcquisitionPlayer returnObject = (AcquisitionPlayer)this.MemberwiseClone();
            
            //the inside of each doesn't need to be deep copied because i made sure to keep them all as data types
            returnObject.turns = returnObject.turns.ToList();
            returnObject.Walls = returnObject.Walls.ToList();
            returnObject.territories = returnObject.territories.ToList();
            returnObject.opponentsTerritoriesScoredThisFrame = opponentsTerritoriesScoredThisFrame.ToList();
            returnObject.nodes = (int[,])nodes.Clone();
            returnObject.myTerritory = (int[,])myTerritory.Clone();

            
            
            
            

            return returnObject;
        }

        public int DeletedFrame()
        {
            return -1;
        }



        private OnlineGameLoop? owner;

        private bool hasPlayedEndingSound = false;
        public void Update(OnlineGameLoop caller)
        {
            drawLoopDetails.update();
            playerRespawnDetails.update();
            territoryCaptureDetails.update();
            victorySoundDetails.update();


            if (isFrozenForEndgame)
            {
                if (Player == 0 && !hasPlayedEndingSound && frameOfEndgame > AcquisitionSharedFunctions.framesOfPreFanfare)
                {
                    victorySoundDetails.play();
                    hasPlayedEndingSound = true;
                }
                drawLoopDetails.stop();
                frameOfEndgame++;
                return;
            }
            
            opponentsTerritoriesScoredThisFrame = new List<(Territory, int)>();
            territoryScoredThisFrame = null;
            
            bonusPointsToScoreInExecution = 0;
            respawnedThisFrame = false;
            needsRespawn = false;
            
            desireUp = false;
            desireDown = false;
            desireLeft = false;
            desireRight = false;

            linesCrossing = false;

            //check desired heading
            if (input.GetDirectionPressed(InputObject.Direction.up,InputObject.DType.Move))
            {
                desireUp = true;
                desireDown = false;
            }
            else if (input.GetDirectionPressed(InputObject.Direction.down,InputObject.DType.Move))
            {
                desireUp = false;
                desireDown = true;
            }
                
            if (input.GetDirectionPressed(InputObject.Direction.left,InputObject.DType.Move))
            {
                desireLeft = true;
                desireRight = false;
            }
            else if (input.GetDirectionPressed(InputObject.Direction.right,InputObject.DType.Move))
            {
                desireLeft = false;
                desireRight = true;
            }
                
            //reverse heading if can

            if (desireDown && (heading == AcquisitionPlayerHeading.Up))
            {
                heading = AcquisitionPlayerHeading.Down;
            }
            else if (desireUp && (heading == AcquisitionPlayerHeading.Down))
            {
                heading = AcquisitionPlayerHeading.Up;
            }
            else if (desireLeft && (heading == AcquisitionPlayerHeading.Right))
            {
                heading = AcquisitionPlayerHeading.Left;
            }
            else if (desireRight && (heading == AcquisitionPlayerHeading.Left))
            {
                heading = AcquisitionPlayerHeading.Right;
            }
                    

                
            //move towards next node in direction of heading
            //20,60 - 1260,700

            Fix64 preNodeMovement = new Fix64(4)/new Fix64(3)+new Fix64(1)/new Fix64(10);
            onNode = false;

            if (heading == AcquisitionPlayerHeading.Stopped)
            {
                onNode = true;
            }
            else if (heading == AcquisitionPlayerHeading.Left)
            {
                if (X > new Fix64(20))
                {
                    Fix64 mod = Fix64.fmod(X, new Fix64(20));
                    if (mod >= preNodeMovement)
                    {
                        X = X - preNodeMovement;
                    }
                    else
                    {
                        X = X - mod;
                        preNodeMovement = preNodeMovement - mod;
                        onNode = true;
                    }
                }
                else
                {
                    onNode = true;
                }
            } else if (heading == AcquisitionPlayerHeading.Right)
            {
                if (X < new Fix64(1260))
                {
                    Fix64 mod = new Fix64(20)-Fix64.fmod(X, new Fix64(20));;
                    if (mod == new Fix64(20))
                        mod = new Fix64(0);
                    if (mod >= preNodeMovement)
                    {
                        X = X + preNodeMovement;
                    }
                    else
                    {
                        X = X + mod;
                        preNodeMovement = preNodeMovement - mod;
                        onNode = true;
                    }
                }
                else
                {
                    onNode = true;
                }
            } 
            else if (heading == AcquisitionPlayerHeading.Up)
            {
                if (Y >= new Fix64(60))
                {
                    Fix64 mod = Fix64.fmod(Y, new Fix64(20));
                    if (mod >= preNodeMovement)
                    {
                        Y = Y - preNodeMovement;
                    }
                    else
                    {
                        Y = Y - mod;
                        preNodeMovement = preNodeMovement - mod;
                        onNode = true;
                    }
                }
                else
                {
                    onNode = true;
                }
            } 
            else if (heading == AcquisitionPlayerHeading.Down)
            {
                if (Y < new Fix64(700))
                {
                    Fix64 mod = new Fix64(20)-Fix64.fmod(Y, new Fix64(20));
                    if (mod == new Fix64(20))
                        mod = new Fix64(0);
                    if (mod >= preNodeMovement)
                    {
                        Y = Y + preNodeMovement;
                    }
                    else
                    {
                        Y = Y + mod;
                        preNodeMovement = preNodeMovement - mod;
                        onNode = true;
                    }
                }
                else
                {
                    onNode = true;
                }
            }

            if (onNode)
            {
                SetNodeLocation();

                postNodeMovement = preNodeMovement;
                    
                    
                    
                //handle the closing of space
                if(turns.Count > 0)
                {
                    if (isPointOnWall(nodeX, nodeY).found) //if the place i have gone is a wall
                    {
                        turns.Add(new Turn(nodeX, nodeY));
                        if (turns.Count > 2)
                        {
                            //here's where the magic happens
                            claimTerritory();

                        }
                        else
                        {
                            if (!turns.First().Equals(turns.Last()))
                            {
                                claimTerritory();
                            }
                        }

                        turns = new List<Turn>();
                        drawLoopDetails.stop();
                    }
                }

                (bool canMoveUp, bool canMoveDown, bool canMoveLeft, bool canMoveRight) personalCollisions =
                    CheckForPlayerCollisionsAroundNode(nodeX, nodeY, true);
                canMoveUp = personalCollisions.canMoveUp;
                canMoveDown = personalCollisions.canMoveDown;
                canMoveLeft = personalCollisions.canMoveLeft;
                canMoveRight = personalCollisions.canMoveRight;
            }
            

        }


        public void claimTerritory()
        {

            //this is not the final thing, we need to be able to pass the territory to the other players
            //we also need to be able to deal with the same frame capture of a bigger chunk of territory fully containing a smaller chunk on the same frame
            territoryScoredThisFrame = FindCompletedTerritory();
        }

        
        public Territory FindCompletedTerritory()
        {
            //first find the shortest wall path to insert the completed territory to the wall
            //clone our wall for safe edits
            List<Turn> tempWall = Walls.ToList();
            List<(bool isInWall, bool isEntryOrExit)> tempWallData = new List<(bool isInWall, bool isEntryOrExit)>();
            
            
            foreach (Turn _ in tempWall)
            {
                tempWallData.Add((true,false));
            }
            //find the entry and exit point on our wall  
            Turn entry = turns.First();
            Turn exit = turns.Last();
            //add those two points to our wall

                
            (bool found, bool corner, (Turn lineStart, Turn lineEnd)[]? containedLines) exitNodes = AcquisitionSharedFunctions.IsPointInTurnList(tempWall,exit.nodeX, exit.nodeY);
            int exitStartIndex = tempWall.FindIndex(x => x.Equals(exitNodes.containedLines![0].lineStart));
            int exitEndIndex = tempWall.FindIndex(x => x.Equals(exitNodes.containedLines![0].lineEnd));
            int exitInsertationPoint = 0;
            if (Math.Abs(exitStartIndex - exitEndIndex) == 1)
            {
                exitInsertationPoint = Math.Max(exitStartIndex,exitEndIndex); //add at the highest point, which puts you between them
            }
            
            if (!(exitNodes.containedLines![0].lineStart.Equals(exit) || exitNodes.containedLines![0].lineEnd.Equals(exit)))
            {
                tempWall.Insert(exitInsertationPoint,exit);
                tempWallData.Insert(exitInsertationPoint, (true, true));
            }
            
            (bool found, bool corner, (Turn lineStart, Turn lineEnd)[]? containedLines) entryNodes = AcquisitionSharedFunctions.IsPointInTurnList(tempWall,entry.nodeX, entry.nodeY);
            int startIndex = tempWall.FindIndex(x => x.Equals(entryNodes.containedLines![0].lineStart));
            int endIndex = tempWall.FindIndex(x => x.Equals(entryNodes.containedLines![0].lineEnd));
            int entryInsertationPoint = 0;
            if (Math.Abs(startIndex - endIndex) == 1)
            {
                entryInsertationPoint = Math.Max(startIndex,endIndex); //add at the highest point, which puts you between them
            }

            if (!(entryNodes.containedLines![0].lineStart.Equals(entry) || entryNodes.containedLines![0].lineEnd.Equals(entry)))
            {
                tempWall.Insert(entryInsertationPoint,entry);
                tempWallData.Insert(entryInsertationPoint, (true, true));
            }


            //unwrap the inner and outer paths into lines
            //refind the indexes as adding may have caused drifts, i could do this better but laziness
            entryInsertationPoint = tempWall.FindIndex(x => x.Equals(entry));
            exitInsertationPoint = tempWall.FindIndex(x => x.Equals(exit));

            //reverse if the list is flipped from our natural turn direction
            if (entryInsertationPoint > exitInsertationPoint)
            {
                turns.Reverse();
                int temp = entryInsertationPoint;
                entryInsertationPoint = exitInsertationPoint;
                exitInsertationPoint = temp;
            }
            //check the inside length, inside plus outside should add to 188 on an empty grid
            int innerLength = 0;
            for (int x = entryInsertationPoint + 1; x <= exitInsertationPoint; x++)
            {
                innerLength += MeasureNodeLine(tempWall[x - 1].nodeX, tempWall[x - 1].nodeY, tempWall[x].nodeX, tempWall[x].nodeY);
            }

                
            //check the outside length inside + outside should add to 188 on an empty grid
            int outerLength = 0;
            for (int x = 0 + 1; x <= entryInsertationPoint; x++)
            {
                outerLength += MeasureNodeLine(tempWall[x - 1].nodeX, tempWall[x - 1].nodeY, tempWall[x].nodeX, tempWall[x].nodeY);
            }
            for (int x = exitInsertationPoint + 1; x < tempWall.Count; x++)
            {
                outerLength += MeasureNodeLine(tempWall[x - 1].nodeX, tempWall[x - 1].nodeY, tempWall[x].nodeX, tempWall[x].nodeY);
            }
            outerLength += MeasureNodeLine(tempWall[^1].nodeX, tempWall[^1].nodeY, tempWall[0].nodeX, tempWall[0].nodeY);

                
            List<(bool isInWall, bool isEntryOrExit)> turnsData =
                new List<(bool isInWall, bool isEntryOrExit)>();
            foreach (Turn turn in turns)
            {
                //entry/exits are the only things on the turn that are part of the wall
                bool isEntryExit = entry.Equals(turn) || exit.Equals(turn);
                turnsData.Add((isEntryExit,isEntryExit));
            }
                
            //take the shortest length path
            if (outerLength < innerLength)
            {
                //remove the longer inner path
                tempWall.RemoveRange(entryInsertationPoint,exitInsertationPoint-entryInsertationPoint+1);
                tempWallData.RemoveRange(entryInsertationPoint,exitInsertationPoint-entryInsertationPoint+1);

                //add our captured path in its place


                    
                tempWall.InsertRange(entryInsertationPoint,turns);
                tempWallData.InsertRange(entryInsertationPoint,turnsData);
            }
            else
            {
                //remove the longer outer path
                tempWall.RemoveRange(exitInsertationPoint, tempWall.Count - exitInsertationPoint);
                tempWallData.RemoveRange(exitInsertationPoint, tempWallData.Count - exitInsertationPoint);
                tempWall.RemoveRange(0,entryInsertationPoint+1);
                tempWallData.RemoveRange(0,entryInsertationPoint+1);

                turns.Reverse();
                //add our captured path in its place
                tempWall.InsertRange(0,turns);
                tempWallData.InsertRange(0,turnsData);
            }
                
                
            //turn this into a completed territory
            Territory returnTerritory = new Territory();
            returnTerritory.walls = tempWall.ToImmutableArray();
            returnTerritory.wallData = tempWallData.ToImmutableArray();
            returnTerritory.turnsLength = turns.Count;
            
            //find the nodes inside the territory
            int capturedTerritorySize;
            (returnTerritory.capturedNodes, capturedTerritorySize) = findCapturedTerritoryControlledNodes(returnTerritory);
            
            
            //score the territory
            returnTerritory.captureSize = capturedTerritorySize;
            
            

            if (tempWallData.Count != tempWall.Count)
            {
                throw new Exception("wall and wall data lenghts aren't the same");
            }

            return returnTerritory;

        }


        private (int[,] blockedTerritoryNodes, int capturedNodes) findCapturedTerritoryControlledNodes(Territory territory)
        {
            int[,] blockedTerritoryNodes = new int[(nodesWidth*2+1), (nodesHeight*2 + 1)];
            int[,] wallNodes = new int[(nodesWidth*2+1), (nodesHeight*2 + 1)];
            
            //loop through all the horizontal walls

            Turn? priorTurn = null;
            (bool isInWall, bool isEntryOrExit)? priorTurnData = null;
            for (int x = 0; x < territory.walls.Length; x++)
            {
                Turn turn = territory.walls[x];
                (bool isInWall, bool isEntryOrExit) turnData = territory.wallData[x];
                if (priorTurn is null || priorTurnData is null)
                {
                    priorTurn = turn;
                    priorTurnData = turnData;
                    continue;
                }

                (blockedTerritoryNodes,wallNodes) = findCapturedTerritoryForSingleWallSegment(turn, turnData, priorTurn.Value, priorTurnData.Value, (blockedTerritoryNodes,wallNodes), territory.turnsLength);
                
                priorTurn = turn;
                priorTurnData = turnData;

            }
            

            if (!(priorTurn is null || priorTurnData is null) && !priorTurn.Equals(territory.walls[0]))
            {
                (blockedTerritoryNodes,wallNodes) = findCapturedTerritoryForSingleWallSegment(priorTurn.Value, priorTurnData.Value, territory.walls[0], territory.wallData[0], (blockedTerritoryNodes,wallNodes), territory.turnsLength);
            }


            for (int x = 0; x <= (nodesWidth*2); x++)
            {
                for (int y = 0; y <= (nodesHeight*2); y++)
                {
                    if (blockedTerritoryNodes[x,y] % 2 == 0)
                    {
                        blockedTerritoryNodes[x,y] = 0;
                    }
                    else
                    {
                        blockedTerritoryNodes[x,y] = 1;
                    }


                    if (wallNodes[x, y] == -2)
                    {
                        blockedTerritoryNodes[x, y] = 0;
                    }
                    else if (wallNodes[x, y] == -1)
                    {
                        blockedTerritoryNodes[x, y] = 1;
                    }
                    
                }
            }


            int capturedNodes = 0;
            for (int x = 0; x <= (nodesWidth*2); x++)
            {
                for (int y = 0; y <= (nodesHeight*2); y++)
                {
                    if (blockedTerritoryNodes[x,y] == 1)
                    {
                        capturedNodes++;
                    }

                }
            }
            
            

            return (blockedTerritoryNodes, capturedNodes);
        }


        public (int[,] blockedTerritory, int[,] wallNodes) findCapturedTerritoryForSingleWallSegment(Turn sourceNode, (bool isInWall, bool isEntryOrExit) sourceNodeData, Turn destinationNode, (bool isInWall, bool isEntryOrExit) destinationNodeData, (int[,] blockedTerritory, int[,] wallNodes) priorNodes, int turnsLength)
        {
            //go through every horizontal node
            if (sourceNode.nodeY == destinationNode.nodeY)
            {
                int min = Math.Min(sourceNode.nodeX*2, destinationNode.nodeX*2);
                int max = Math.Max(sourceNode.nodeX*2, destinationNode.nodeX*2);

                for (int x = min; x < max; x++)
                {
                    for (int y = sourceNode.nodeY*2; y <= nodesHeight*2; y++)
                    {

                            priorNodes.blockedTerritory[x, y]++;
                        
                    }
                    
                }
            }


            if (sourceNode.nodeY == destinationNode.nodeY)
            {
                int min = Math.Min(sourceNode.nodeX*2, destinationNode.nodeX*2);
                int max = Math.Max(sourceNode.nodeX*2, destinationNode.nodeX*2);

                for (int x = min; x <= max; x++)
                {
                    if (sourceNodeData.isInWall && destinationNodeData.isInWall && !(turnsLength == 2 && (sourceNodeData.isEntryOrExit && destinationNodeData.isEntryOrExit))  && priorNodes.wallNodes[x, sourceNode.nodeY * 2] != -2)
                    {
                        priorNodes.wallNodes[x, sourceNode.nodeY * 2] = -1;
                    }
                    else
                    {
                        priorNodes.wallNodes[x, sourceNode.nodeY * 2] = -2;
                    }
                }
            }
            else if (sourceNode.nodeX == destinationNode.nodeX)
            {
                int min = Math.Min(sourceNode.nodeY*2, destinationNode.nodeY*2);
                int max = Math.Max(sourceNode.nodeY*2, destinationNode.nodeY*2);

                for (int y = min; y <= max; y++)
                {
                    if ((sourceNodeData.isInWall && destinationNodeData.isInWall) && !(turnsLength == 2 && (sourceNodeData.isEntryOrExit && destinationNodeData.isEntryOrExit)) && priorNodes.wallNodes[sourceNode.nodeX * 2, y] != -2)
                    {
                        priorNodes.wallNodes[sourceNode.nodeX * 2, y] = -1;
                    }
                    else
                    {
                        priorNodes.wallNodes[sourceNode.nodeX * 2, y] = -2;
                    }
                }
            }
            
            

            //also see if this is a wall node, and if it is a wall node we should mark each item as confirmed territory somehow
            return priorNodes;
        }
        
        
        
        
        
        public void mergeTerritoryWithWall(Territory territory, bool isMyTerritory)
        {

            //find all of the nodes that are inside the current wall, there could be many pairs of them
            List<Turn> tempWall = Walls.ToList();
            //do not use the insertation point in logic other then to find the direction of the list in comparison
            List<(int Index, Turn turn)> turnsInWall = new List<(int Index, Turn turn)>();

            for (int x = 0; x < territory.wallData.Length; x++)
            {
                if (territory.wallData[x].isEntryOrExit)
                {
                    Turn turn = territory.walls[x];
                    (bool found, bool corner, (Turn lineStart, Turn lineEnd)[]? containedLines) result = AcquisitionSharedFunctions.IsPointInTurnList(tempWall, turn.nodeX, turn.nodeY);
                    if (result.found)
                    {
                        if (result.containedLines is null)
                        {
                            continue;
                        }

                    
                        int startIndex = tempWall.FindIndex(x => x.Equals(result.containedLines[0].lineStart));
                        int endIndex = tempWall.FindIndex(x => x.Equals(result.containedLines[0].lineEnd));
                        int insertationPoint = 0;
                        
                        if (Math.Abs(startIndex - endIndex) == 1)
                        {
                            insertationPoint = Math.Max(startIndex,endIndex); //add at the highest point, which puts you between them
                        }

                    
                        if (!territory.wallData[x].isInWall||territory.wallData[x].isEntryOrExit)
                        {
                            tempWall.Insert(insertationPoint,turn);
                        }
                        turnsInWall.Add((insertationPoint,turn));

                    }

                }
            }
            

            List<Turn> territoryWalls = territory.walls.ToList();
            

            
            //go between entry and exit
            {

                int territorySegmentEntry = territoryWalls.FindIndex(y => y.Equals(turnsInWall[0].turn));
                int territorySegmentExit = territoryWalls.FindIndex(y => y.Equals(turnsInWall[1].turn));

                
                if (territorySegmentEntry > territorySegmentExit)
                {
                    int temp = territorySegmentEntry;
                    territorySegmentEntry = territorySegmentExit;
                    territorySegmentExit = temp;
                }
                
                

                
                List<Turn> stuffToAdd =
                    territoryWalls.GetRange(territorySegmentEntry, territorySegmentExit - territorySegmentEntry + 1);
                List<Turn> stuffToRemove = territoryWalls.ToList();
                
                foreach(Turn turn in stuffToAdd)
                {
                    stuffToRemove.Remove(turn);
                }

                stuffToRemove.Remove(turnsInWall[0].turn);
                stuffToRemove.Remove(turnsInWall[1].turn);

                foreach(Turn turn in stuffToRemove)
                {
                    tempWall.Remove(turn);
                }
                
                

                int stuffToAddEntry = stuffToAdd.FindIndex(y => y.Equals(turnsInWall[0].turn));
                int stuffToAddExit = stuffToAdd.FindIndex(y => y.Equals(turnsInWall[1].turn));

                int entryPoint = tempWall.FindIndex(y => y.Equals(turnsInWall[0].turn));
                int exitPoint = tempWall.FindIndex(y => y.Equals(turnsInWall[1].turn));

                if (turnsInWall[1].turn.Equals(tempWall.Last()))
                {
                    int temp = entryPoint;
                    entryPoint = exitPoint;
                    exitPoint = temp;
                }
                
                if (stuffToAddEntry > stuffToAddExit && entryPoint <= exitPoint)
                {
                    //only one list is reversed so i need to reverse the list
                    stuffToAdd.Reverse();
                } else if (stuffToAddEntry <= stuffToAddExit && entryPoint > exitPoint)
                {
                    stuffToAdd.Reverse();
                }
                
                
                tempWall.InsertRange(entryPoint,stuffToAdd);


            }

            
            tempWall = tempWall.Distinct().ToList();
            
            
            //remove the entry/exit points if both connecting lines go the same direction 
            int entryPoint2 = tempWall.FindIndex(y => y.Equals(turnsInWall[0].turn));

            if (FindIfSurroundingPointsFlowInSameDirection(entryPoint2, tempWall))
            {
                tempWall.RemoveAt(entryPoint2);
            }
            
            
            int exitPoint2 = tempWall.FindIndex(y => y.Equals(turnsInWall[1].turn));

            if (FindIfSurroundingPointsFlowInSameDirection(exitPoint2, tempWall))
            {
                tempWall.RemoveAt(exitPoint2);
            }

            Walls = tempWall;

            
            for (int x = 0; x <= nodesWidth * 2; x++)
            {
                for (int y = 0; y <= nodesHeight * 2; y++)
                {
                    if (territory.capturedNodes[x, y] == 1)
                    {
                        nodes[x, y] = 1;
                        if (isMyTerritory)
                        {
                            myTerritory[x, y] = 1;
                        }
                    }
                }
            }
            
        }

        private bool FindIfSurroundingPointsFlowInSameDirection(int indexToTest, List<Turn>textList)
        {
            //get all my points
            Turn? firstPoint = null;
            Turn pointAtIndex = textList[indexToTest];
            Turn? secondPoint = null;
            if (indexToTest == 0)
            {
                firstPoint = textList.Last();
            }
            else
            {
                firstPoint = textList[indexToTest - 1];
            }
            
            if (indexToTest < textList.Count-1)
            {
                secondPoint = textList[indexToTest + 1];
            }
            else
            {                
                secondPoint = textList[0];
            }

            if (firstPoint.Value.nodeX == pointAtIndex.nodeX && secondPoint.Value.nodeX == pointAtIndex.nodeX)
                return true;
            
            if (firstPoint.Value.nodeY == pointAtIndex.nodeY && secondPoint.Value.nodeY == pointAtIndex.nodeY)
                return true;
            
            
            return false;
        }

        
        //this only measures straight lines
        private int MeasureNodeLine(int p1x, int p1y, int p2x, int p2y)
        {
            //since this is only for straight lines, we can shortcut this by just subtracting one from the other
            //the equal parts are going to end up zero, meaning the other side gives the value
            return Math.Abs((p1x - p2x) + (p1y - p2y));
        }
        
        public (bool isColliding, ICollisionDetails? details) CheckCollision(IGameObject otherObject)
        {
            return (true, null);
        }

        private void GainBonusPointsForPlayerCapture()
        {
            bonusPointsToScoreInExecution += 200 * 2;
        }
        
        
        
        
        
        
        public void AddCollisionCommand(IGameObject otherObject, ICollisionDetails? details, bool performedCollisionCheck)
        {
            if (isFrozenForEndgame) return;

            if (otherObject.GetType() == typeof(AcquisitionPlayer))
            {
                AcquisitionPlayer otherPlayer = (AcquisitionPlayer)otherObject;

                if (otherPlayer.Player == playerToGiveKillBonus)
                {
                    otherPlayer.GainBonusPointsForPlayerCapture();
                    playerToGiveKillBonus = null;
                }
                (bool canMoveUp, bool canMoveDown, bool canMoveLeft, bool canMoveRight) OtherPLayerCollisions =
                    otherPlayer.CheckForPlayerCollisionsAroundNode(nodeX, nodeY, false);
                
                //i can't advance to a node my opponent has already made advancement to
                (bool canMoveUp, bool canMoveDown, bool canMoveLeft, bool canMoveRight) OtherPlayerAdvancementCollision = isOpponentAdvancingOnTile(otherPlayer, nodeX,nodeY);
                

                
                canMoveUp = OtherPlayerAdvancementCollision.canMoveUp && OtherPLayerCollisions.canMoveUp && canMoveUp;
                canMoveDown = OtherPlayerAdvancementCollision.canMoveDown && OtherPLayerCollisions.canMoveDown && canMoveDown;
                canMoveLeft = OtherPlayerAdvancementCollision.canMoveLeft && OtherPLayerCollisions.canMoveLeft && canMoveLeft;
                canMoveRight = OtherPlayerAdvancementCollision.canMoveRight && OtherPLayerCollisions.canMoveRight && canMoveRight;
                if (!(otherPlayer.territoryScoredThisFrame is null))
                {
                    opponentsTerritoriesScoredThisFrame.Add((otherPlayer.territoryScoredThisFrame.Value,otherPlayer.Player));
                }
                

                if (turns.Count > 0 && otherPlayer.turns.Count > 0)
                {
                    Turn myLastTurn = turns.Last();
                    Turn opponentsLastTurn = otherPlayer.turns.Last();

                    
                    (int nodeX, int nodeY) myTurnCords = AcquisitionSharedFunctions.GetNodeCoordinates(myLastTurn.nodeX, myLastTurn.nodeY);
                    (int nodeX, int nodeY) opponentTurnCoordinates = AcquisitionSharedFunctions.GetNodeCoordinates(opponentsLastTurn.nodeX, opponentsLastTurn.nodeY);

                    if (AcquisitionSharedFunctions.DoLinesIntersect(myTurnCords.nodeX, myTurnCords.nodeY, (int) X,
                        (int) Y,
                        opponentTurnCoordinates.nodeX, opponentTurnCoordinates.nodeY, (int) otherPlayer.X,
                        (int) otherPlayer.Y))
                    {
                        linesCrossing = true;
                    }
                }

            }
            else if (otherObject.GetType() == typeof(AcquisitionGameBall))
            {
                AcquisitionGameBall ball = (AcquisitionGameBall)otherObject;
                if (myTerritory[ball.nodeX * 2, ball.nodeY * 2] == 1)
                {
                    Log.Debug($"scored ball on {owner!.GetCurrentFrame()}");
                    ball.PlayerScoredBall();
                    bonusPointsToScoreInExecution += 50 * 2;
                }

                if (turns.Count <= 0) return;

                List<Turn> finalTurnList = turns.ToList();
                if(!(finalTurnList.Last().nodeX == nodeX && finalTurnList.Last().nodeY == nodeY))
                    finalTurnList.Add(new Turn(nodeX, nodeY));

                if (finalTurnList.Count <= 1) return;
                var ballWallCollision = AcquisitionSharedFunctions.IsPointInTurnList(finalTurnList, ball.nodeX, ball.nodeY);
                if (ballWallCollision.found)
                {
                    ball.BallBouncedOffPlayer(ballWallCollision);
                    needsRespawn = true;
                }
            }

        }

        public (bool canMoveUp, bool canMoveDown, bool canMoveLeft, bool canMoveRight) isOpponentAdvancingOnTile(AcquisitionPlayer otherPlayer, int checkPointX, int checkPointY)
        {
            bool up = true;
            bool down = true;
            bool left = true;
            bool right = true;
            (int nodeX, int nodeY) checkPointUp = (checkPointX, checkPointY - 1);
            (int nodeX, int nodeY) checkPointDown = (checkPointX, checkPointY + 1);
            (int nodeX, int nodeY) checkPointLeft = (checkPointX - 1, checkPointY);
            (int nodeX, int nodeY) checkPointRight = (checkPointX + 1, checkPointY);
            
            (int x, int y) otherPlayerCoordinates = AcquisitionSharedFunctions.GetNodeCoordinates(otherPlayer.nodeX, otherPlayer.nodeY);

            (int nodeX, int nodeY) otherPlayersDesiredNode = (otherPlayer.nodeX, otherPlayer.nodeY);
            if (otherPlayer.heading == AcquisitionPlayerHeading.Down)
            {
                otherPlayersDesiredNode = (otherPlayer.nodeX, otherPlayer.nodeY + 1);
            }
            else if (otherPlayer.heading == AcquisitionPlayerHeading.Up)
            {
                otherPlayersDesiredNode = (otherPlayer.nodeX, otherPlayer.nodeY - 1);
            }
            else if (otherPlayer.heading == AcquisitionPlayerHeading.Left)
            {
                otherPlayersDesiredNode = (otherPlayer.nodeX - 1, otherPlayer.nodeY);
            }
            else if (otherPlayer.heading == AcquisitionPlayerHeading.Right)
            {
                otherPlayersDesiredNode = (otherPlayer.nodeX + 1, otherPlayer.nodeY);
            }

            bool hasmoved = !(new Fix64(otherPlayerCoordinates.x) == otherPlayer.X) && (new Fix64(otherPlayerCoordinates.y) == otherPlayer.Y);
            if(hasmoved)
            {
                if (otherPlayersDesiredNode.Equals(checkPointUp))
                {
                    up = false;
                }
                else if (otherPlayersDesiredNode.Equals(checkPointDown))
                {
                    down = false;
                }
                else if (otherPlayersDesiredNode.Equals(checkPointLeft))
                {
                    left = false;
                }
                else if (otherPlayersDesiredNode.Equals(checkPointRight))
                {
                    right = false;
                }
            }




            return (up, down, left, right);

        }

        public void ExecuteCollisionCommands(OnlineGameLoop caller)
        {
            score += (long)bonusPointsToScoreInExecution * 1000;

            if (isFrozenForEndgame)
            {
                bonusPointsToScoreInExecution = 0;
                return;
            }

            //handle the merging of territories with walls
            //first check to see that none of the territories contain any of the other territories
            if (opponentsTerritoriesScoredThisFrame.Count > 0)
            {
                List<(Territory,int)> territoriesToRemove = new List<(Territory,int)>();
                //check between all opponents in list, this will never actually run in a 2p game so it won't be tested for a bit
                for (int x = 0; x < opponentsTerritoriesScoredThisFrame.Count; x++)
                {
                    for (int y = 0; y < opponentsTerritoriesScoredThisFrame.Count; y++)
                    {
                        if (x == y)
                            continue;
                        //see if y contains x
                        if (DoesPathContainAnother(opponentsTerritoriesScoredThisFrame[x].territory.walls,
                            opponentsTerritoriesScoredThisFrame[y].territory.walls))
                        {
                            territoriesToRemove.Add(opponentsTerritoriesScoredThisFrame[y]);
                        }//see if x contains y
                        else if (DoesPathContainAnother(opponentsTerritoriesScoredThisFrame[y].territory.walls,
                            opponentsTerritoriesScoredThisFrame[x].territory.walls))
                        {
                            territoriesToRemove.Add(opponentsTerritoriesScoredThisFrame[x]);
                        }
                    }
                }
                
                //if i scored
                if (!(territoryScoredThisFrame is null))
                {
                    //see if i contain anything in the list

                    for (int x = 0; x < opponentsTerritoriesScoredThisFrame.Count; x++)
                    {
                            //see if anything in the list contains me
                            if (DoesPathContainAnother(opponentsTerritoriesScoredThisFrame[x].territory.walls,
                                territoryScoredThisFrame.Value.walls))
                            {
                                territoryScoredThisFrame = null;
                                break;
                            }//see if i contain anything in the list
                            else if (DoesPathContainAnother(territoryScoredThisFrame.Value.walls,
                                opponentsTerritoriesScoredThisFrame[x].territory.walls))
                            {
                                territoriesToRemove.Add(opponentsTerritoriesScoredThisFrame[x]);
                            }
                    }
                }

                foreach ((Territory,int) removed in territoriesToRemove)
                {
                    opponentsTerritoriesScoredThisFrame.Remove(removed);
                }
                
                //merge opponents territories with walls
                foreach ((Territory territory ,int player) territory  in opponentsTerritoriesScoredThisFrame)
                {
                    mergeTerritoryWithWall(territory.territory,false);
                    if (territory.territory.capturedNodes[nodeX*2, nodeY*2] == 1)
                    {
                        needsRespawn = true;
                        playerToGiveKillBonus = territory.player;
                    }
                    
                }
                //add a fix for cases where i got stuck in a wall
                if (nodes[nodeX*2, nodeY*2] == 1)
                {
                    needsRespawn = true;
                }
                
                

            }

            if (!(territoryScoredThisFrame is null))
            {
                //merge my territory with walls
                mergeTerritoryWithWall(territoryScoredThisFrame.Value,true);
                int capturedSize = territoryScoredThisFrame.Value.captureSize;
                allTerritorySize += capturedSize;
                //Fix64 capture64 = new Fix64(capturedSize);

                long captureLong = capturedSize * 1000L;
                score += captureLong + (capturedSize * 100000L / AcquisitionSharedFunctions.totalCaptureNodes) / 2L;
                
                //score += (long)(capture64 + (capture64 * ((capture64 / new Fix64(AcquisitionSharedFunctions.totalCaptureNodes) / new Fix64(2)))));

                //and add to my territory list
                territories.Add(territoryScoredThisFrame.Value);
                territoryCaptureDetails.play();
            }



            (bool canMoveUp, bool canMoveDown, bool canMoveLeft, bool canMoveRight) nodeCollisions =
                CheckForNodeCollisionsAroundNode(nodeX, nodeY);
            canMoveUp = nodeCollisions.canMoveUp && canMoveUp;
            canMoveDown = nodeCollisions.canMoveDown && canMoveDown;
            canMoveLeft = nodeCollisions.canMoveLeft && canMoveLeft;
            canMoveRight = nodeCollisions.canMoveRight && canMoveRight;

            if (linesCrossing)
            {
                int testNodeX = nodeX;
                int testNodeY = nodeY;

                if (heading == AcquisitionPlayerHeading.Up)
                    testNodeY++;
                else if (heading == AcquisitionPlayerHeading.Down)
                    testNodeY--;
                else if (heading == AcquisitionPlayerHeading.Right)
                    testNodeX--;
                else if (heading == AcquisitionPlayerHeading.Left)
                    testNodeX++;

                if (testNodeX >= 0 && testNodeX <= nodesWidth && testNodeY >= 0 && testNodeY <= nodesHeight)
                {
                    (int coordx, int coordy) nodeCoordinates = AcquisitionSharedFunctions.GetNodeCoordinates(testNodeX, testNodeY);
                    X = new Fix64(nodeCoordinates.coordx);
                    Y = new Fix64(nodeCoordinates.coordy);
                    SetNodeLocation();
                }

                heading = AcquisitionPlayerHeading.Stopped;
                return;
            }
            
            //handle after node movement
            if (onNode)
            {

                (bool found, bool corner, (Turn lineStart, Turn lineEnd)[]? containedLines) nodePositionOnWall =
                    isPointOnWall(nodeX, nodeY);
                bool isNodeOnWall = nodePositionOnWall.found;
                AcquisitionPlayerHeading priorHeading = heading;
                if (heading == AcquisitionPlayerHeading.Stopped)
                {
                    if (desireRight && canMoveRight)
                        heading = AcquisitionPlayerHeading.Right;
                    else if (desireLeft && canMoveLeft)
                        heading = AcquisitionPlayerHeading.Left;
                    else if (desireUp && canMoveUp)
                        heading = AcquisitionPlayerHeading.Up;
                    else if (desireDown && canMoveDown)
                        heading = AcquisitionPlayerHeading.Down;
                }
                else if (heading == AcquisitionPlayerHeading.Up || heading == AcquisitionPlayerHeading.Down)
                {
                    if (desireRight && canMoveRight)
                        heading = AcquisitionPlayerHeading.Right;
                    else if (desireLeft && canMoveLeft)
                        heading = AcquisitionPlayerHeading.Left;
                }
                else if (heading == AcquisitionPlayerHeading.Left || heading == AcquisitionPlayerHeading.Right)
                {
                    if (desireUp && canMoveUp)
                        heading = AcquisitionPlayerHeading.Up;
                    else if (desireDown && canMoveDown)
                        heading = AcquisitionPlayerHeading.Down;
                }

                bool didTurn = (heading != priorHeading);

                //check to see if my new turn takes me from wall node to a non wall node
                if (isNodeOnWall)
                {
                    int testNodeX = nodeX;
                    int testNodeY = nodeY;
                    if (heading == AcquisitionPlayerHeading.Up)
                        testNodeY--;
                    else if (heading == AcquisitionPlayerHeading.Down)
                        testNodeY++;
                    else if (heading == AcquisitionPlayerHeading.Right)
                        testNodeX++;
                    else if (heading == AcquisitionPlayerHeading.Left)
                        testNodeX--;
                    
                        
                    if (testNodeX >= 0 && testNodeX <= nodesWidth && testNodeY >= 0 && testNodeY <= nodesHeight && nodes[testNodeX*2, testNodeY*2] != 1)
                    {
                        //if the point isn't found, or if the point is found check to see if our shared line shares a point with our current line
                        (bool found, bool corner, (Turn lineStart, Turn lineEnd)[]? containedLines) testNodePositionOnWall =
                            isPointOnWall(testNodeX, testNodeY);
                        
                        if (testNodePositionOnWall.found)
                        {
                            bool foundLineMatch = false;
                            foreach ((Turn lineStart, Turn lineEnd) testLine  in testNodePositionOnWall.containedLines!)
                            {
                                foreach ((Turn lineStart, Turn lineEnd) wallLine  in nodePositionOnWall.containedLines!)
                                {
                                    if (((wallLine.lineStart.Equals(testLine.lineStart)) && (wallLine.lineEnd.Equals(testLine.lineEnd)) || ((wallLine.lineStart.Equals(testLine.lineEnd)) && (wallLine.lineEnd.Equals(testLine.lineStart)))))
                                    {
                                        foundLineMatch = true;
                                        break;
                                    }
                                }

                                if (foundLineMatch)
                                {
                                    break;
                                }
                            }

                            if (!foundLineMatch)
                            {
                                turns = new List<Turn>();
                                turns.Add(new Turn(nodeX,nodeY));
                                drawLoopDetails.play();
                            }
                            
                        }
                        else
                        {
                            turns = new List<Turn>();
                            turns.Add(new Turn(nodeX,nodeY));
                            drawLoopDetails.play();

                        }
                    }
                }  
                
                if (didTurn)
                {


                   
                    if (!isNodeOnWall)
                    {

                        Turn? lastTurn = null;
                        if(turns.Count > 0)
                            lastTurn = turns.Last();
                        if (!(lastTurn is null) && lastTurn!.Value.nodeX == nodeX && lastTurn!.Value.nodeY == nodeY)
                        {
                                turns.RemoveAt(turns.Count - 1);
                        }
                        else
                        {
                            turns.Add(new Turn(nodeX,nodeY));
                        }
                        
                    }
                }
                

                //bring us back to our node before moving out, also means we can move if we're colliding
                (int coordx, int coordy) nodeCoordinates = AcquisitionSharedFunctions.GetNodeCoordinates(nodeX, nodeY);
                X = new Fix64(nodeCoordinates.coordx);
                Y = new Fix64(nodeCoordinates.coordy);

                if (heading == AcquisitionPlayerHeading.Left && canMoveLeft)
                {
                    if (X - postNodeMovement > new Fix64(20))
                    {
                        X = X - postNodeMovement;
                    }
                    else
                    {
                        X = new Fix64(20);
                    }
                } 
                else if (heading == AcquisitionPlayerHeading.Right && canMoveRight)
                {
                    if (X + postNodeMovement < new Fix64(1260))
                    {
                        X = X + postNodeMovement;
                    }
                    else
                    {
                        X = new Fix64(1260);
                    }
                }
                else if (heading == AcquisitionPlayerHeading.Up && canMoveUp) 
                {
                    if (Y - postNodeMovement > new Fix64(60))
                    {
                        Y = Y - postNodeMovement;
                    }
                    else
                    {
                        Y = new Fix64(60);
                    }
                }
                else if (heading == AcquisitionPlayerHeading.Down && canMoveDown)
                {
                    if (Y + postNodeMovement < new Fix64(700))
                    {
                        Y = Y + postNodeMovement;
                    }
                    else
                    {
                        Y = new Fix64(700);
                    }
                }
            }


            
            
            
            if (needsRespawn)
            {
                Respawn(AcquisitionSharedFunctions.FindClosestRespawnLocation(nodeX,nodeY,Walls));
                needsRespawn = false;
                respawnedThisFrame = true;
            }
            
        }
        
        
        void IGameObject.PlaySounds(NetSoundManager soundManager, int currentFrame)
        {
            drawLoopDetails.doPlayback(soundManager);
            territoryCaptureDetails.doPlayback(soundManager);
            playerRespawnDetails.doPlayback(soundManager);
            victorySoundDetails.doPlayback(soundManager);
        }

        public void Respawn((int nodeX, int nodeY, bool isCorner, (Turn lineStart, Turn lineEnd)[] containedLines) position)
        {
            drawLoopDetails.stop();
            playerRespawnDetails.play();
            turns = new List<Turn>();
            (nodeX, nodeY, _, _) = position;
            (int X, int Y) coord = AcquisitionSharedFunctions.GetNodeCoordinates(nodeX, nodeY);
            heading = AcquisitionPlayerHeading.Stopped;
            X = new Fix64(coord.X);
            Y = new Fix64(coord.Y);

        }



        public bool DoesPathContainAnother(IEnumerable<Turn> containingPath, IEnumerable<Turn> containedPath)
        {
            Turn? priorTurn = null;
            Turn? first = null;

            //draw lines
            foreach (Turn turn in containingPath)
            {
                if (first is null)
                {
                    first = turn;
                }
                if (priorTurn is null)
                {
                    priorTurn = turn;
                    continue;
                }

                foreach (Turn contained in containedPath)
                {
                    if (AcquisitionSharedFunctions.IsPointInLine(turn.nodeX, turn.nodeY, priorTurn.Value.nodeX, priorTurn.Value.nodeY,
                        contained.nodeX, contained.nodeY))
                        return true;
                }
            }

            if (!(first is null) && !(priorTurn is null))
            {
                foreach (Turn contained in containedPath)
                {
                    if (AcquisitionSharedFunctions.IsPointInLine(first.Value.nodeX, first.Value.nodeY, priorTurn.Value.nodeX, priorTurn.Value.nodeY,
                        contained.nodeX, contained.nodeY))
                        return true;
                }
            }

            return false;
        }
        
        


        

        public void Draw(PaletteEffectSpriteBatch context, float percentToNextFrame, IGameObject? nextFrame)
        {
            int drawWidth = context.Ultraviolet.GetGraphics().GetViewport().Width;
            int drawHeight = context.Ultraviolet.GetGraphics().GetViewport().Height;
            (int nativeValue, int scaleValue) hScale = (1280, drawWidth);
            (int nativeValue, int scaleValue) vScale = (720, drawHeight);




            
            
            Color playerColor = new Color(255, 191, 0);
            if (this.Player == 1)
            {
                playerColor = Color.Green;
            }
            else if (this.Player == 2)
            {
                playerColor = Color.Fuchsia;
            }
            else if (this.Player == 3)
            {
                playerColor = Color.Cyan;
            }
            
            string scoreString =
                $"{allTerritorySize * 100 / AcquisitionSharedFunctions.totalCaptureNodes}% - {string.Format("{0:#,##0.##}", score * 1000)}";
            Size2 scoreSize = drawFont!.GetFace(UltravioletFontStyle.Regular).MeasureString(scoreString);

            float nameHeight = (DrawHelpers.GetScaledValue(vScale, 60 - 5) - 2 * scoreSize.Height);
            float scoreHeight = (DrawHelpers.GetScaledValue(vScale, 60 - 5) - scoreSize.Height);

            Size2 nameSize = drawFont!.GetFace(UltravioletFontStyle.Regular).MeasureString(playerName);

            //draw scoring
            if (Player == 0)
            {
                
                context.DrawString(drawFont,playerName , new Ultraviolet.Vector2(DrawHelpers.GetScaledValue(hScale, 20)
                    , nameHeight), playerColor);

                context.DrawString(drawFont,scoreString , new Ultraviolet.Vector2(DrawHelpers.GetScaledValue(hScale, 20)
                    , scoreHeight), playerColor);
            }
            else if (Player == 1)
            {
                context.DrawString(drawFont, playerName, new Ultraviolet.Vector2(DrawHelpers.GetScaledValue(hScale, 1260)-nameSize.Width
                    , nameHeight), playerColor);

                context.DrawString(drawFont, scoreString, new Ultraviolet.Vector2(DrawHelpers.GetScaledValue(hScale, 1260)-scoreSize.Width
                    , scoreHeight), playerColor);
            }
            else if (Player == 2)
            {
                context.DrawString(drawFont,playerName , new Ultraviolet.Vector2(DrawHelpers.GetScaledValue(hScale, 20+400)
                    , nameHeight), playerColor);

                context.DrawString(drawFont,scoreString , new Ultraviolet.Vector2(DrawHelpers.GetScaledValue(hScale, 20+400)
                    , scoreHeight), playerColor);
            }
            else if (Player == 3)
            {
                context.DrawString(drawFont, playerName, new Ultraviolet.Vector2(DrawHelpers.GetScaledValue(hScale, 1260-400)-nameSize.Width,
                    nameHeight), playerColor);

                context.DrawString(drawFont, scoreString, new Ultraviolet.Vector2(DrawHelpers.GetScaledValue(hScale, 1260-400)-scoreSize.Width,
                    scoreHeight), playerColor);
            }

            
            //draw debug info
            /*if (Player == 0)
            {
                context.DrawString(drawFont, $"{nodeX} , {nodeY} , {isPointOnWall(nodeX, nodeY).found},  {nodes[nodeX/2, nodeY/2]}", new Ultraviolet.Vector2(0, 0), Color.White);

                foreach (Turn wallPoint in Walls)
                {
                    (int nodeX, int nodeY) turnCoords = AcquisitionSharedFunctions.GetNodeCoordinates(wallPoint.nodeX, wallPoint.nodeY);

                    context.DrawPoint(context.WhiteTexture(),new Vector2(turnCoords.nodeX,turnCoords.nodeY),Color.Aqua,4,layerDepth:2,centered:true);

                }
            }*/
            
            
            //draw player dot

            
            //draw the endgame fanfare
            if (frameOfEndgame > AcquisitionSharedFunctions.framesOfPreFanfare)
            {
                DrawVictory(context, playerColor);

                return;
            }
            
            AcquisitionPlayer nextPlayer;
            if(!(nextFrame is null))
                nextPlayer = (AcquisitionPlayer) nextFrame;
            else 
                nextPlayer = (AcquisitionPlayer) this;

            float interpolatedX;
            float interpolatedY;
            if (!(respawnedThisFrame || nextPlayer.respawnedThisFrame))
            {
                interpolatedX = Ultraviolet.Core.MathUtil.Lerp((float) this.X, (float) nextPlayer.X, percentToNextFrame);
                interpolatedY = Ultraviolet.Core.MathUtil.Lerp((float) this.Y, (float) nextPlayer.Y, percentToNextFrame);
            }
            else
            {
                interpolatedX = (float)X;
                interpolatedY = (float)Y;
            }
            
            
            context.DrawPoint(context.WhiteTexture(),new Vector2(DrawHelpers.GetScaledValue(hScale, interpolatedX),DrawHelpers.GetScaledValue(vScale, interpolatedY)),playerColor,9,layerDepth:1,centered:true);


            if (nextPlayer.turns.Count > 0)
            {
                Turn? priorTurn = null;
                //draw lines
                foreach (Turn turn in nextPlayer.turns)
                {
                    if (priorTurn is null)
                    {
                        priorTurn = turn;
                        continue;
                    }
                    
                    (int nodeX, int nodeY) priorTurnCoords = AcquisitionSharedFunctions.GetNodeCoordinates(priorTurn.Value.nodeX, priorTurn.Value.nodeY);
                    (int nodeX, int nodeY) currentTurnCoords = AcquisitionSharedFunctions.GetNodeCoordinates(turn.nodeX, turn.nodeY);
                    
                    
                    context.DrawLine(context.WhiteTexture(),DrawHelpers.GetScaledValue(hScale, priorTurnCoords.nodeX),
                        DrawHelpers.GetScaledValue(vScale, priorTurnCoords.nodeY),
                        DrawHelpers.GetScaledValue(hScale, currentTurnCoords.nodeX),
                        DrawHelpers.GetScaledValue(vScale, currentTurnCoords.nodeY), playerColor,2,2);
                    priorTurn = turn;
                }
                
                (int nodeX, int nodeY) priorTurnCoords2 = AcquisitionSharedFunctions.GetNodeCoordinates(priorTurn!.Value.nodeX, priorTurn!.Value.nodeY);

                
                context.DrawLine(context.WhiteTexture(),DrawHelpers.GetScaledValue(hScale, priorTurnCoords2.nodeX),
                    DrawHelpers.GetScaledValue(vScale, priorTurnCoords2.nodeY),
                    DrawHelpers.GetScaledValue(hScale, interpolatedX),
                    DrawHelpers.GetScaledValue(vScale, interpolatedY), playerColor,2,2);

            }


            if (Player == 0)
            {
                DrawTerritory(Walls, context, Color.Gray, 2, 75);

            }


            for (int x = 0; x < (nodesWidth) * 2 + 1; x++)
            {
                for (int y = 0; y < (nodesHeight) * 2 + 1; y++)
                {
                    (int nodeX, int nodeY) nodeCoordinates = AcquisitionSharedFunctions.GetNodeCoordinates(x, y);
                    int adjustedXPos = nodeCoordinates.nodeX / 2 + 10;
                    int adjustedYPos = nodeCoordinates.nodeY / 2 + 30;


                    if (myTerritory[x, y] == 1)
                    {
                        
                        
                        context.DrawPoint(context.WhiteTexture(), new Vector2(DrawHelpers.GetScaledValue(hScale, adjustedXPos),
                                DrawHelpers.GetScaledValue(vScale, adjustedYPos)), playerColor,
                            4, layerDepth: 5, centered: true);
                    }

                    if (Player == 0 && x % 2 == 0 && y % 2 == 0 && nodes[x,y]!=1)
                    {
                        
                        context.DrawPoint(context.WhiteTexture(),new Vector2(DrawHelpers.GetScaledValue(hScale, adjustedXPos),
                            DrawHelpers.GetScaledValue(vScale, adjustedYPos)),
                            new Color(120, 120, 120),4,layerDepth:90,centered:true);
                    }
                    
                }
            }


        }


        private void DrawVictory(PaletteEffectSpriteBatch context, Color playerColor)
        {
            if (Player != endGameWinner)
                return;
            int framesFanfareFallsAcross = AcquisitionSharedFunctions.totalFramesOfEndGameFreeze -
                                           AcquisitionSharedFunctions.framesOfPreFanfare -
                                           AcquisitionSharedFunctions.framesOfPostFanfare;

            int frameIntoFanfare = frameOfEndgame - AcquisitionSharedFunctions.framesOfPreFanfare;

            int percentIntoFanfare = frameIntoFanfare * 100000 / framesFanfareFallsAcross;

            if (percentIntoFanfare > 100000)
                percentIntoFanfare = 100000;
            //loop through the possible nodes but only to a certain x


                for (int x = 0; x < (((nodesWidth) * 2 + 1) * percentIntoFanfare) / 100000; x++)
                {
                    for (int y = 0; y < (nodesHeight) * 2 + 1; y++)
                    {
                        if (Player % 2 == 0)
                        {
                            DrawSingleVictoryDot(context, x, y, playerColor);
                        }
                        else
                        {
                            DrawSingleVictoryDot(context, ((nodesWidth) * 2 + 1) - x - 1, y, playerColor);
                        }
                    }
                }
            

        }


        private void DrawSingleVictoryDot(PaletteEffectSpriteBatch context, int x, int y, Color playerColor)
        {
            
            int drawWidth = context.Ultraviolet.GetGraphics().GetViewport().Width;
            int drawHeight = context.Ultraviolet.GetGraphics().GetViewport().Height;
            (int nativeValue, int scaleValue) hScale = (1280, drawWidth);
            (int nativeValue, int scaleValue) vScale = (720, drawHeight);
            
            if (y >= 23 && y <= 25)
            {
                if (x >= 30 && x <= 32)
                {
                    return;
                }
                if (x >= 54 && x <= 56)
                {
                    return;
                }
                if (x >= 64 && x <= 70)
                {
                    return;
                }
                if (x >= 77 && x <= 82)
                {
                    return;
                }
                if (x >= 88 && x <= 90)
                {
                    return;
                }
            }
            else if (y >= 26 && y <= 28)
            {
                if (x >= 31 && x <= 33)
                {
                    return;
                }
                if (x >= 42 && x <= 44)
                {
                    return;
                }
                if (x >= 53 && x <= 55)
                {
                    return;
                }
                if (x >= 81 && x <= 83)
                {
                    return;
                }
                
                if (x >= 66 && x <= 68)
                {
                    return;
                }
                if (x >= 77 && x <= 79)
                {
                    return;
                }
                if (x >= 88 && x <= 90)
                {
                    return;
                }
                
            }
            else if (y >= 29 && y <= 31)
            {
                if (x >= 32 && x <= 34)
                {
                    return;
                }
                if (x >= 41 && x <= 45)
                {
                    return;
                }
                if (x >= 52 && x <= 54)
                {
                    return;
                }
                if (x >= 82 && x <= 84)
                {
                    return;
                }
                if (x >= 66 && x <= 68)
                {
                    return;
                }
                if (x >= 77 && x <= 79)
                {
                    return;
                }
                if (x >= 88 && x <= 90)
                {
                    return;
                }
                
            }
            else if (y >= 32 && y <= 34)
            {
                if (x >= 33 && x <= 35)
                {
                    return;
                }
                if (x >= 40 && x <= 42)
                {
                    return;
                }
                if (x >= 44 && x <= 46)
                {
                    return;
                }
                if (x >= 51 && x <= 53)
                {
                    return;
                }
                
                
                if (x >= 83 && x <= 85)
                {
                    return;
                }
                
                
                if (x >= 66 && x <= 68)
                {
                    return;
                }
                if (x >= 77 && x <= 79)
                {
                    return;
                }
                if (x >= 88 && x <= 90)
                {
                    return;
                }
                
            }
            else if (y >= 35 && y <= 37)
            {
                if (x >= 34 && x <= 36)
                {
                    return;
                }
                if (x >= 39 && x <= 41)
                {
                    return;
                }
                if (x >= 45 && x <= 47)
                {
                    return;
                }
                if (x >= 50 && x <= 52)
                {
                    return;
                }
                if (x >= 84 && x <= 86)
                {
                    return;
                }
                if (x >= 66 && x <= 68)
                {
                    return;
                }
                if (x >= 77 && x <= 79)
                {
                    return;
                }
                if (x >= 88 && x <= 90)
                {
                    return;
                }
            }
            else if (y >= 38 && y <= 40)
            {

                if (x >= 35 && x <= 40)
                {
                    return;
                }
                if (x >= 46 && x <= 51)
                {
                    return;
                }
                if (x >= 64 && x <= 70)
                {
                    return;
                }
                if (x >= 85 && x <= 90)
                {
                    return;
                }
                if (x >= 77 && x <= 79)
                {
                    return;
                }
            }
            (int nodeX, int nodeY) nodeCoordinates = AcquisitionSharedFunctions.GetNodeCoordinates(x, y);
            int adjustedXPos = nodeCoordinates.nodeX / 2 + 10;
            int adjustedYPos = nodeCoordinates.nodeY / 2 + 30;
            
            
            context.DrawPoint(context.WhiteTexture(), new Vector2(DrawHelpers.GetScaledValue(hScale, adjustedXPos),
                    DrawHelpers.GetScaledValue(vScale, adjustedYPos)), playerColor,
                4, layerDepth: 5, centered: true);
        }
        
        
        private void DrawTerritory(IEnumerable<Turn> list, PaletteEffectSpriteBatch context, Color color, float thickness, float layerDepth)
        {
            
                        
            int drawWidth = context.Ultraviolet.GetGraphics().GetViewport().Width;
            int drawHeight = context.Ultraviolet.GetGraphics().GetViewport().Height;
            (int nativeValue, int scaleValue) hScale = (1280, drawWidth);
            (int nativeValue, int scaleValue) vScale = (720, drawHeight);


            Turn? priorTurn = null;
            Turn? first = null;

            //draw lines
            foreach (Turn turn in list)
            {
                if (first is null)
                {
                    first = turn;
                }
                if (priorTurn is null)
                {
                    priorTurn = turn;
                    continue;
                }
                
                (int nodeX, int nodeY) priorTurnCoords = AcquisitionSharedFunctions.GetNodeCoordinates(priorTurn.Value.nodeX, priorTurn.Value.nodeY);
                (int nodeX, int nodeY) currentTurnCoords = AcquisitionSharedFunctions.GetNodeCoordinates(turn.nodeX, turn.nodeY);
                
                
                
                context.DrawLine(context.WhiteTexture(),DrawHelpers.GetScaledValue(hScale, priorTurnCoords.nodeX),
                    DrawHelpers.GetScaledValue(vScale, priorTurnCoords.nodeY),
                    DrawHelpers.GetScaledValue(hScale, currentTurnCoords.nodeX),
                    DrawHelpers.GetScaledValue(vScale, currentTurnCoords.nodeY),
                    color,thickness,layerDepth);
                priorTurn = turn;
            }

            if (!(first is null))
            {
                (int nodeX, int nodeY) priorTurnCoords2 =
                    AcquisitionSharedFunctions.GetNodeCoordinates(priorTurn!.Value.nodeX, priorTurn!.Value.nodeY);
                (int nodeX, int nodeY) firstTurnCords =
                    AcquisitionSharedFunctions.GetNodeCoordinates(first.Value.nodeX, first.Value.nodeY);


                
                context.DrawLine(context.WhiteTexture(), DrawHelpers.GetScaledValue(hScale, priorTurnCoords2.nodeX),
                    DrawHelpers.GetScaledValue(vScale, priorTurnCoords2.nodeY),
                    DrawHelpers.GetScaledValue(hScale, firstTurnCords.nodeX),
                    DrawHelpers.GetScaledValue(vScale, firstTurnCords.nodeY), color, thickness, layerDepth);
            }
            

        }




        internal void FreezeAllDataAndDisplayWinner(int winningIndex)
        {
            isFrozenForEndgame = true;
            endGameWinner = winningIndex;
        }
        


        public ulong CalculateDesynchronizationCheckValue(bool verbose)
        {
            ulong checkValue =  (ulong)(Player + 1) * (ulong)nodeX + (ulong)(Player + 1) * (ulong)nodeY;

            if(verbose)
                Log.Error($"Player {Player} returns a check value of {checkValue}, it is at node {nodeX}, {nodeY}, absolute position {X.ToString()}, {Y.ToString()}");
            return checkValue;
        }

        static HashSet<Type>? typeList;
        public HashSet<Type> GetCollidableClasses()
        {
            if (typeList is null)
            {
                typeList = new HashSet<Type>
                {
                    typeof(AcquisitionPlayer),
                    typeof(AcquisitionGameBall),
                    typeof(AcquisitionGameManager)
                };
            }
            return typeList;
        }

        public void SetInput(InputObject input)
        {
            this.input = input;
        }
    }
}