using System;
using System.Collections.Generic;
using System.Linq;
using FixMath.NET;
using Serilog;
using Synchrocade.GameObjects.Sound;
using Synchrocade.ReusableFunctions;
using Ultraviolet;

namespace Synchrocade.GameObjects.UniqueGameObjects.AcquisitionObjects
{
    internal class AcquisitionGameManager : IManagerGameObject
    {
        private readonly int players;
        private long[] currentScores;
        private int[] currentlyControlledTerritory;

        private const int totalCaptureNodes = AcquisitionSharedFunctions.totalCaptureNodes;

        private bool gameEnding;
        private int gameEndingTimer = AcquisitionSharedFunctions.totalFramesOfEndGameFreeze;
        //private bool firstFrameOfEndGame = true;
        
        internal AcquisitionGameManager(int players)
        {
            this.players = players;
            currentScores = new long[players];
            currentlyControlledTerritory = new int[players];
        }

        public bool IsGameFinished()
        {
            return gameEndingTimer < 0;
        }
        
        
        

        public long[] ReturnScores()
        {
            return currentScores;
        }


        public void Update(OnlineGameLoop caller)
        {
            if (!gameEnding)
            {
                int controlledNodes = currentlyControlledTerritory.Sum();
                gameEnding = controlledNodes * 100 / totalCaptureNodes >= 90;
                
            }
            else
            {
                gameEndingTimer--;
                //firstFrameOfEndGame = false;
            }
        }
        
        private bool respawnTimerRunning = false;
        
        public (bool isColliding, ICollisionDetails? details) CheckCollision(IGameObject otherObject)
        {
            return (true, null);
        }

        public void AddCollisionCommand(IGameObject otherObject, ICollisionDetails? details, bool performedCollisionCheck)
        {
            if (otherObject.GetType() == typeof(AcquisitionPlayer))
            {
                AcquisitionPlayer player = (AcquisitionPlayer) otherObject;
                currentlyControlledTerritory[player.Player] = player.allTerritorySize;
                currentScores[player.Player] = player.score * 1000; //for now

                if (gameEnding)
                {
                    int winningIndex = -1;
                    long winningScore = -1;
                    for (int index = 0; index < currentScores.Length; index++)
                    {
                        if (currentScores[index] > winningScore)
                        {
                            winningIndex = index;
                            winningScore = currentScores[index];
                        }
                    }

                    player.FreezeAllDataAndDisplayWinner(winningIndex);
                }
            }
            if (gameEnding && otherObject.GetType() == typeof(AcquisitionGameBall))
            {
                AcquisitionGameBall ball = (AcquisitionGameBall) otherObject;
                ball.FreezeForEndGame();
                
            }
        }

        public void ExecuteCollisionCommands(OnlineGameLoop caller)
        {
            //don't need to do anything here because order literally doesn't matter in this case as this object has no
            //physicality
        }

        public ulong CalculateDesynchronizationCheckValue(bool verbose)
        {

            ulong allScores = 0;
            foreach (long score in currentScores)
            {
                allScores += (ulong)score;
            }

            for (int i = 0; i < currentScores.Length; i++)
            {
                if(verbose)
                    Log.Error($"The game manager thinks Player {i} has a score of {currentScores[i]}");

                allScores += (ulong)currentScores[i];
            }
            
            if(verbose)
                Log.Error($"The game manager returns a check value of {allScores}");

            return allScores;
        }

        
        static HashSet<Type>? typeList;
        HashSet<Type> IGameObject.GetCollidableClasses()
        {
            if (typeList is null)
            {
                typeList = new HashSet<Type>
                {
                    typeof(AcquisitionPlayer),
                    typeof(AcquisitionGameBall)
                };
            }
            return typeList;
        }

        public object Clone()
        {
            AcquisitionGameManager clone = (AcquisitionGameManager) this.MemberwiseClone();
            clone.currentScores = (long[])currentScores.Clone();
            clone.currentlyControlledTerritory = (int[])currentlyControlledTerritory.Clone();

            return clone;
        }

        public int DeletedFrame()
        {
            return -1;
        }

        public void PlaySounds(NetSoundManager soundManager, int currentFrame)
        {
        }

        public void Draw(PaletteEffectSpriteBatch context, float percentToNextFrame, IGameObject? nextFrame)
        {
            int drawWidth = context.Ultraviolet.GetGraphics().GetViewport().Width;
            int drawHeight = context.Ultraviolet.GetGraphics().GetViewport().Height;
            (int nativeValue, int scaleValue) hScale = (1280, drawWidth);
            (int nativeValue, int scaleValue) vScale = (720, drawHeight);

            

            context.DrawRectangle(context.WhiteTexture(),new Vector2(DrawHelpers.GetScaledValue(hScale, 20)
                ,DrawHelpers.GetScaledValue(vScale, 60))
                ,DrawHelpers.GetScaledValue(hScale, 1240)
                ,DrawHelpers.GetScaledValue(vScale, 640)
                ,new Color(64, 64, 64),4,layerDepth:100);
            
            
            /*for (int y = 60; y <= 700; y+=20)
            {
                for (int x = 20; x <= 1260; x+=20)
                {
                    context.DrawPoint(context.WhiteTexture(),new Vector2(x,y),new Color(120, 120, 120),4,layerDepth:90,centered:true);
                }
            }*/
        }

    }
}