using System;
using System.Collections.Generic;
using System.Linq;
using Serilog;
using Serilog.Core;

namespace Synchrocade.GameObjects.UniqueGameObjects.AcquisitionObjects
{
    internal static class AcquisitionSharedFunctions
    {
        
        public const int nodesWidth = 62;
        public const int nodesHeight = 32;
        public const int totalCaptureNodes = (nodesWidth * 2 + 1) * (nodesHeight * 2 + 1);

        public const int framesOfPreFanfare = 240;
        public const int framesOfPostFanfare = 120;
        public const int framesOfFanfare = 520;

        
        public const int totalFramesOfEndGameFreeze = framesOfPreFanfare+framesOfPostFanfare+framesOfFanfare; 


        
        internal static (int x, int y) GetNodeCoordinates(int nodeX, int nodeY)
        {
            return (nodeX * 20 + 20, nodeY * 20 + 60);
        }

        internal static List<Turn> GetStartingWalls()
        {
            List<Turn> walls = new List<Turn>();
            walls.Add(new Turn(0,0));
            walls.Add(new Turn(62,0));
            walls.Add(new Turn(62,32));
            walls.Add(new Turn(0,32));
            return walls;
        }
        
        
        internal static (int nodeX, int nodeY, bool isCorner, (Turn lineStart, Turn lineEnd)[] containedLines) FindClosestRespawnLocation(int nodeX, int nodeY, List<Turn> walls)
        {
            int closestPointX = -1;
            int closestPointY = -1;
            int closestDistance = -1;
            int nodeAdditionToResolveOrderIssuesOnTie = 0;
            (Turn lineStart, Turn lineEnd)[]? closestWallList = null;
            bool closestIsCorner = false;

            //check the points themselves
            for (int i = 0; i < walls.Count; i++)
            {
                int nodeDistance =
                    CheckDistanceBetweenTwoPoints(walls[i].nodeX, walls[i].nodeY, nodeX, nodeY);
                if (closestPointX == -1 || nodeDistance <= 
                    closestDistance)
                {
                    if (nodeDistance < closestDistance || walls[i].nodeX+walls[i].nodeY > nodeAdditionToResolveOrderIssuesOnTie)
                    {
                        nodeAdditionToResolveOrderIssuesOnTie = walls[i].nodeX + walls[i].nodeY;
                        closestPointX = walls[i].nodeX;
                        closestPointY = walls[i].nodeY;
                        closestDistance = nodeDistance;
                        closestIsCorner = true;
                        Turn firstPoint;
                        if (i > 0)
                        {
                            firstPoint = walls[i - 1];
                        }
                        else
                        {
                            firstPoint = walls.Last();
                        }
                        
                        Turn secondPoint;
                        if (i < walls.Count - 1)
                        {
                            secondPoint = walls[i + 1];
                        }
                        else
                        {
                            secondPoint = walls[0];
                        }

                        closestWallList = new[] {(firstPoint, walls[i]), (walls[i], secondPoint)};
                    }

                }
            }
            

            
            
            //check any lines perpendicular to me
            for (int i = 0; i < walls.Count - 1; i++)
            {
                if (i == 0)
                {
                    Turn node1 = walls[^1];
                    Turn node2 = walls[0];
                    (int distance, int nodeX, int nodeY)? result = CheckDistanceBetweenPointAndLine(nodeX, nodeY, node1.nodeX, node1.nodeY, node2.nodeX,
                        node2.nodeY);
                    if (!(result is null))
                    {
                        if (closestPointX == -1 || result.Value.distance <= 
                            closestDistance)
                        {
                            if (result.Value.distance < closestDistance || result.Value.nodeX + result.Value.nodeY >
                                nodeAdditionToResolveOrderIssuesOnTie)
                            {
                                nodeAdditionToResolveOrderIssuesOnTie = result.Value.nodeX + result.Value.nodeY;
                                closestPointX = result.Value.nodeX;
                                closestPointY = result.Value.nodeY;
                                closestDistance = result.Value.distance;
                                closestIsCorner = false;
                                closestWallList = new[] {(node1, node2)};
                            }

                        }

                    }
                }
                
                Turn nodeA = walls[i];
                Turn nodeB = walls[i+1];
                (int distance, int nodeX, int nodeY)? result2 = CheckDistanceBetweenPointAndLine(nodeX, nodeY, nodeA.nodeX, nodeA.nodeY, nodeB.nodeX,
                    nodeB.nodeY);
                if (!(result2 is null))
                {
                    if (closestPointX == -1 || result2.Value.distance <= 
                        closestDistance)
                    {
                        if (result2.Value.distance < closestDistance || result2.Value.nodeX + result2.Value.nodeY >
                            nodeAdditionToResolveOrderIssuesOnTie)
                        {
                            nodeAdditionToResolveOrderIssuesOnTie = result2.Value.nodeX + result2.Value.nodeY;
                            closestPointX = result2.Value.nodeX;
                            closestPointY = result2.Value.nodeY;
                            closestDistance = result2.Value.distance;
                            closestIsCorner = false;
                            closestWallList = new[] {(nodeA, nodeA)};
                        }

                    }

                }

            }

            return (closestPointX, closestPointY,closestIsCorner,closestWallList!);
        }

        //this function will only check distances in square paths
        internal static int CheckDistanceBetweenTwoPoints(int firstNodeX, int firstNodeY, int secondNodeX, int secondNodeY)
        {
            return (Math.Abs(firstNodeX - secondNodeX) + Math.Abs(firstNodeY - secondNodeY));
        }


        //i will ignore the points in this function as i handle them with the point function
        internal static  (int distance, int nodeX, int nodeY)? CheckDistanceBetweenPointAndLine(int nodeX, int nodeY, int lineStartX, int lineStartY, int lineEndX,
            int lineEndY)
        {
            //find the lines orientation
            if (lineStartX == lineEndX)
            {
                int minY = Math.Min(lineStartY, lineEndY);
                int maxY = Math.Max(lineStartY, lineEndY);

                if (nodeY > minY && nodeY < maxY)
                {
                    return (Math.Abs(lineStartX - nodeX),lineStartX,nodeY);
                }
            }
            else if (lineStartY == lineEndY)
            {
                int minX = Math.Min(lineStartX, lineEndX);
                int maxX = Math.Max(lineStartX, lineEndX);


                if (nodeX > minX && nodeX < maxX)
                {
                    return (Math.Abs(lineStartY - nodeY), nodeX,lineStartY);
                }
            }
            return null;
        }
        
        public static (bool found, bool corner, (Turn lineStart, Turn lineEnd)[]? containedLines) IsPointInTurnList(List<Turn> turnList, int pointX,
            int pointY)
        {
            for (int i = 0; i < turnList.Count; i++)
            {
                //check after only if first is true
                //if first element check both before and after
                if (i + 1 < turnList.Count)
                {
                    if (IsPointInLine(turnList[i].nodeX, turnList[i].nodeY, turnList[i + 1].nodeX,
                        turnList[i + 1].nodeY,
                        pointX,
                        pointY))
                    {
                        if (i == 0 && turnList.Count > 2)
                        {
                            if (IsPointInLine(turnList.Last().nodeX, turnList.Last().nodeY, turnList[0].nodeX,
                                turnList[0].nodeY,
                                pointX,
                                pointY))
                            {
                                return (true, true,
                                    new[] {(turnList.Last(), turnList[0]), (turnList[0], turnList[1])});
                            }

                        }
                        if (i + 2 < turnList.Count)
                        {
                            if (IsPointInLine(turnList[i+1].nodeX, turnList[i+1].nodeY, turnList[i + 2].nodeX,
                                turnList[i + 2].nodeY,
                                pointX,
                                pointY))
                            {
                                return (true, true,
                                    new[] {(turnList[i], turnList[i + 1]), (turnList[i + 1], turnList[i + 2])});
                            }
                        }
                        else if(turnList.Count > 2)
                        {
                            if (IsPointInLine(turnList[i+1].nodeX, turnList[i+1].nodeY, turnList[0].nodeX,
                                turnList[0].nodeY,
                                pointX,
                                pointY))
                            {
                                return (true, true,
                                    new[] {(turnList[i], turnList[i + 1]), (turnList[i + 1], turnList[0])});
                            }
                        }

                        return (true, false,
                            new[] {(turnList[i], turnList[i + 1])});
                    }
                }
                else
                {
                    if (turnList.Count > 1)
                    {
                        if (IsPointInLine(turnList[i].nodeX, turnList[i].nodeY, turnList[0].nodeX,
                            turnList[0].nodeY,
                            pointX,
                            pointY))
                        {
                            return (true, false,
                                new[] {(turnList[i], turnList[0])});
                        }
                    }

                }
            }
            
            return (false, false, null);
        }


        
        internal static bool IsPointInLine(int lineP1X, int lineP1Y, int lineP2X, int lineP2Y, int checkPointX, int checkPointY)
        {
            if (lineP1X == lineP2X) //horizontal line
            {
                if (checkPointX == lineP1X &&
                    checkPointY >= Math.Min(lineP1Y, lineP2Y) &&
                    checkPointY <= Math.Max(lineP1Y, lineP2Y))
                {
                    return true;
                }
            }
            else if (lineP1Y == lineP2Y)//vertical line
            {
                if (checkPointY == lineP1Y &&
                    checkPointX >= Math.Min(lineP1X, lineP2X) &&
                    checkPointX <= Math.Max(lineP1X, lineP2X))
                {
                    return true;
                }
            }

            return false;
        }


        public static bool DoLinesIntersect(int line1P1X, int line1P1Y, int line1P2X, int line1P2Y,int line2P1X, int line2P1Y, int line2P2X, int line2P2Y)
        {
            bool line1Horizontal = false;
            bool line1Vertical = false;
            bool line2Horizontal = false;
            bool line2Vertical = false;
            if (line1P1X == line1P2X)
                line1Vertical = true;
            if (line1P1Y == line1P2Y)
                line1Horizontal = true;
            if (line2P1X == line2P2X)
                line2Vertical = true;
            if (line2P1Y == line2P2Y)
                line2Horizontal = true;

            if ((line1Horizontal && line2Horizontal))
            {
                if(line1P1Y != line2P1Y)
                    return false;

                int line1MinX = Math.Min(line1P1X, line1P2X);
                int line1MaxX = Math.Max(line1P1X, line1P2X);
                int line2MinX = Math.Min(line2P1X, line2P2X);
                int line2MaxX = Math.Max(line2P1X, line2P2X);

                return (line1MinX >= line2MinX && line1MinX <= line2MaxX)
                    || (line1MaxX >= line2MinX && line1MaxX <= line2MaxX)
                    || (line2MinX >= line1MinX && line2MinX <= line1MaxX)
                    || (line2MaxX >= line1MinX && line2MaxX <= line1MaxX);
            }
            else if ((line1Vertical && line2Vertical))
            {
                if(line1P1X != line2P1X)
                    return false;
                
                int line1MinY = Math.Min(line1P1Y, line1P2Y);
                int line1MaxY = Math.Max(line1P1Y, line1P2Y);
                int line2MinY = Math.Min(line2P1Y, line2P2Y);
                int line2MaxY = Math.Max(line2P1Y, line2P2Y);

                return (line1MinY >= line2MinY && line1MinY <= line2MaxY)
                       || (line1MaxY >= line2MinY && line1MaxY <= line2MaxY)
                       || (line2MinY >= line1MinY && line2MinY <= line1MaxY)
                       || (line2MaxY >= line1MinY && line2MaxY <= line1MaxY);
                
            }
                

            int horizontalMinX;
            int horizontalMaxX;
            int horizontalY;
            int verticalMinY;
            int verticalMaxY;
            int verticalX;

            if (line1Horizontal)
            {
                horizontalMinX = Math.Min(line1P1X, line1P2X);
                horizontalMaxX = Math.Max(line1P1X, line1P2X);
                horizontalY = line1P1Y;
                
                verticalMinY = Math.Min(line2P1Y, line2P2Y);
                verticalMaxY = Math.Max(line2P1Y, line2P2Y);
                verticalX = line2P1X;
            }
            else
            {
                verticalMinY = Math.Min(line1P1Y, line1P2Y);
                verticalMaxY = Math.Max(line1P1Y, line1P2Y);
                verticalX = line1P1X;
                horizontalMinX = Math.Min(line2P1X, line2P2X);
                horizontalMaxX = Math.Max(line2P1X, line2P2X);
                horizontalY = line2P1Y;
            }

            return horizontalY >= verticalMinY && horizontalY <= verticalMaxY && verticalX >= horizontalMinX &&
                   verticalX <= horizontalMaxX;
        }

    }
    
    
    
}