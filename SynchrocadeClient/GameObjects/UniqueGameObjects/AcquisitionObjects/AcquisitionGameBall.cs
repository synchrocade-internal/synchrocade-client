using System;
using System.Collections.Generic;
using System.Linq;
using FixMath.NET;
using Serilog;
using Synchrocade.GameObjects.Sound;
using Synchrocade.ReusableFunctions;
using Ultraviolet;
using Ultraviolet.Audio;
using Ultraviolet.Content;
using Ultraviolet.Graphics.Graphics2D;
using Vector2 = System.Numerics.Vector2;

namespace Synchrocade.GameObjects.UniqueGameObjects.AcquisitionObjects
{
    
    enum AcquisitionBallHeading
    {
        UpRight,
        UpLeft,
        DownRight,
        DownLeft
    }
    
    
    
    internal class AcquisitionGameBall : IGameObject
    {

        private bool isGameFrozen = false;
        
        internal Fix64 X;
        internal Fix64 Y;
        
        
        
        internal int nodeX;
        internal int nodeY;
        private bool needsRespawn = false;
        private bool onNode = false;
        private bool respawnedThisFrame = false;
        AcquisitionBallHeading heading;
        
        Fix64 postNodeMovement = new Fix64(0); 
        
        private List<Turn> walls;
        
        
        private const int nodesWidth = AcquisitionSharedFunctions.nodesWidth;
        private const int nodesHeight = AcquisitionSharedFunctions.nodesHeight;


        private (bool found, bool corner, (Turn lineStart, Turn lineEnd)[]? containedLines)? playerCollisionDetail = null;

        
        
        private static SoundEffect? bounce;
        private NetSound bounceDetails;
        private static SoundEffect? ballRespawn;
        private NetSound ballRespawnDetails;

        internal AcquisitionGameBall(int nodeX, int nodeY, AcquisitionBallHeading startDirection, OnlineGameLoop owner)
        {
            this.nodeX = nodeX;
            this.nodeY = nodeY;
            (int x, int y) coors = AcquisitionSharedFunctions.GetNodeCoordinates(nodeX, nodeY);
            this.X = new Fix64(coors.x);
            this.Y = new Fix64(coors.y);
            this.heading = startDirection;
            walls = AcquisitionSharedFunctions.GetStartingWalls();
            
            
            bounceDetails = new NetSound(bounce!,owner); 
            bounceDetails.volume = .02f;
            bounceDetails.looping = false;
            
            ballRespawnDetails = new NetSound(ballRespawn!,owner); 
            ballRespawnDetails.volume = .2f;
            ballRespawnDetails.looping = false;

        }
        
        public object Clone()
        {
            AcquisitionGameBall returnObject = (AcquisitionGameBall)this.MemberwiseClone();
            returnObject.walls = returnObject.walls.ToList();
            return returnObject;
        }

        public int DeletedFrame()
        {
            return -1;
        }

        public void Update(OnlineGameLoop caller)
        {
            bounceDetails.update();
            ballRespawnDetails.update();
            
            
            if (isGameFrozen)
                return;
            
            respawnedThisFrame = false;
            needsRespawn = false;
            Fix64 preNodeMovement = (new Fix64(4)/new Fix64(3) + new Fix64(1)/new Fix64(10));
            onNode = false;
            
            if (heading == AcquisitionBallHeading.DownLeft)
            {
                if (X > new Fix64(20))
                {
                    Fix64 mod = Fix64.fmod(X, new Fix64(20));
                    if (mod >= preNodeMovement)
                    {
                        X = X - preNodeMovement;
                        Y = Y + preNodeMovement;
                    }
                    else
                    {
                        X = X - mod;
                        Y = Y + mod;
                        preNodeMovement = preNodeMovement - mod;
                        onNode = true;
                    }
                }
                else
                {
                    onNode = true;
                }
            }
            else if (heading == AcquisitionBallHeading.DownRight)
            {
                if (X < new Fix64(1260))
                {
                    Fix64 mod = new Fix64(20)-Fix64.fmod(X, new Fix64(20));;
                    if (mod == new Fix64(20))
                        mod = new Fix64(0);
                    if (mod >= preNodeMovement)
                    {
                        X = X + preNodeMovement;
                        Y = Y + preNodeMovement;

                    }
                    else
                    {
                        X = X + mod;
                        Y = Y + mod;
                        preNodeMovement = preNodeMovement - mod;
                        onNode = true;
                    }
                }
                else
                {
                    onNode = true;
                }

            }
            else if (heading == AcquisitionBallHeading.UpLeft)
            {
                if (X > new Fix64(20))
                {
                    Fix64 mod = Fix64.fmod(X, new Fix64(20));
                    if (mod >= preNodeMovement)
                    {
                        X = X - preNodeMovement;
                        Y = Y - preNodeMovement;
                    }
                    else
                    {
                        X = X - mod;
                        Y = Y - mod;
                        preNodeMovement = preNodeMovement - mod;
                        onNode = true;
                    }
                }
                else
                {
                    onNode = true;
                }
            }
            else if (heading == AcquisitionBallHeading.UpRight)
            {
                if (X < new Fix64(1260))
                {
                    Fix64 mod = new Fix64(20)-Fix64.fmod(X, new Fix64(20));;
                    if (mod == new Fix64(20))
                        mod = new Fix64(0);
                    if (mod >= preNodeMovement)
                    {
                        X = X + preNodeMovement;
                        Y = Y - preNodeMovement;

                    }
                    else
                    {
                        X = X + mod;
                        Y = Y - mod;
                        preNodeMovement = preNodeMovement - mod;
                        onNode = true;
                    }
                }
                else
                {
                    onNode = true;
                }

            }
            


            if (onNode)
            {
                SetNodeLocation();
            }
            postNodeMovement = preNodeMovement;
        }

        
        private void SetNodeLocation()
        {
            //20,60 - 1260,700
            
            //find x node location
            nodeX = nodesWidth - (((1280 - 20) - (int) X) / 20);

                
            //find y node location
            //700-60
            nodeY = nodesHeight - ((720 - 20) - (int) Y) / 20;
        }

        
        public void PlaySounds(NetSoundManager soundManager, int currentFrame)
        {
            bounceDetails.doPlayback(soundManager);
            ballRespawnDetails.doPlayback(soundManager);
        }
        
        public static void LoadContent(ContentManager contentManager)
        {
            if(bounce is null)
                bounce = contentManager.Load<SoundEffect>("Unique/Acquisition/Sound/ballbounce.ogg", false);

            if(ballRespawn is null)
                ballRespawn = contentManager.Load<SoundEffect>("Unique/Acquisition/Sound/ballrespawn.ogg", false);

        }

        public void Draw(PaletteEffectSpriteBatch context, float percentToNextFrame, IGameObject? nextFrame)
        {
            if (isGameFrozen)
                return;
            
            int drawWidth = context.Ultraviolet.GetGraphics().GetViewport().Width;
            int drawHeight = context.Ultraviolet.GetGraphics().GetViewport().Height;
            (int nativeValue, int scaleValue) hScale = (1280, drawWidth);
            (int nativeValue, int scaleValue) vScale = (720, drawHeight);
            
            Color ballColor = Color.Red;
            AcquisitionGameBall nextBall;
            if(!(nextFrame is null))
                nextBall = (AcquisitionGameBall) nextFrame;
            else 
                nextBall = (AcquisitionGameBall) this;

            float interpolatedX;
            float interpolatedY;
            if (!(respawnedThisFrame || nextBall.respawnedThisFrame))
            {
                interpolatedX = Ultraviolet.Core.MathUtil.Lerp((float) this.X, (float) nextBall.X, percentToNextFrame);
                interpolatedY = Ultraviolet.Core.MathUtil.Lerp((float) this.Y, (float) nextBall.Y, percentToNextFrame);
            }
            else
            {
                interpolatedX = (float)X;
                interpolatedY = (float)Y;
            }
            
            context.DrawPoint(context.WhiteTexture(),new Vector2(DrawHelpers.GetScaledValue(hScale, interpolatedX),
                DrawHelpers.GetScaledValue(vScale, interpolatedY)),ballColor,9,layerDepth:1,centered:true);

        }

        public (bool isColliding, ICollisionDetails? details) CheckCollision(IGameObject otherObject)
        {
            return (true, null);
        }

        public void AddCollisionCommand(IGameObject otherObject, ICollisionDetails? details, bool performedCollisionCheck)
        {
            if (isGameFrozen)
                return;
            
            if (otherObject.GetType() == typeof(AcquisitionPlayer))
            {
                AcquisitionPlayer player = (AcquisitionPlayer) otherObject;
                //if the player is p0 grab their wall
                if (player.Player == 0)
                {
                    walls = player.Walls.ToList();
                }
                //we'll let the player handle collisions themselves
            }
        }

        public void PlayerScoredBall()
        {
            needsRespawn = true;
        }

        public void BallBouncedOffPlayer((bool found, bool corner, (Turn lineStart, Turn lineEnd)[]? containedLines) collisionDetail)
        {
            playerCollisionDetail = collisionDetail;
        }
        

        public void ExecuteCollisionCommands(OnlineGameLoop caller)
        {
            if (isGameFrozen)
                return;
            
            if (needsRespawn)
            {
                Respawn(AcquisitionSharedFunctions.FindClosestRespawnLocation(nodeX,nodeY,walls));
                needsRespawn = false;
            }
            else
            {
                if (onNode)
                {
                    //bring us back to our node before moving out, also means we can move if we're colliding
                    (int coordx, int coordy) nodeCoordinates = AcquisitionSharedFunctions.GetNodeCoordinates(nodeX, nodeY);
                    X = new Fix64(nodeCoordinates.coordx);
                    Y = new Fix64(nodeCoordinates.coordy);

                    
                    
                    (bool found, bool corner, (Turn lineStart, Turn lineEnd)[]? containedLines) result =
                        AcquisitionSharedFunctions.IsPointInTurnList(walls, nodeX, nodeY);
                    
                    if (!(playerCollisionDetail is null))
                    {
                        if(!result.found) //if i collide with both the wall and the player on the same frame, prefer the walls details
                            result = playerCollisionDetail.Value;
                        playerCollisionDetail = null;
                    }
                    
                    if (result.found)
                    {
                        bounceDetails.play();
                        
                        if (result.corner)
                        {
                            bool handledCorner = false;

                            AcquisitionBallHeading cornerDirection = FindCornerDirection(result.containedLines!);
                            if (heading == AcquisitionBallHeading.UpRight || heading == AcquisitionBallHeading.DownLeft)
                            {
                                if (cornerDirection == AcquisitionBallHeading.DownRight)
                                {
                                    heading = AcquisitionBallHeading.UpLeft;
                                    handledCorner = true;
                                }
                                else if (cornerDirection == AcquisitionBallHeading.UpLeft)
                                {
                                    heading = AcquisitionBallHeading.DownRight;
                                    handledCorner = true;
                                }
                            }
                            else if (heading == AcquisitionBallHeading.DownRight ||
                                     heading == AcquisitionBallHeading.UpLeft)
                            {
                                if (cornerDirection == AcquisitionBallHeading.DownLeft)
                                {
                                    heading = AcquisitionBallHeading.UpRight;
                                    handledCorner = true;

                                }

                                if (cornerDirection == AcquisitionBallHeading.UpRight)
                                {
                                    heading = AcquisitionBallHeading.DownLeft;
                                    handledCorner = true;

                                }

                            }

                            if (!handledCorner)
                            {
                                heading = heading switch
                                {
                                    AcquisitionBallHeading.DownLeft => AcquisitionBallHeading.UpRight,
                                    AcquisitionBallHeading.UpRight => AcquisitionBallHeading.DownLeft,
                                    AcquisitionBallHeading.DownRight => AcquisitionBallHeading.UpLeft,
                                    AcquisitionBallHeading.UpLeft => AcquisitionBallHeading.DownRight,
                                    _ => throw new ArgumentOutOfRangeException()
                                };
                            }

                        }
                        else
                        {
                            //if horizontal line
                            if (result.containedLines![0].lineEnd.nodeX == result.containedLines![0].lineStart.nodeX)
                            {
                                heading = heading switch
                                {
                                    AcquisitionBallHeading.DownLeft => AcquisitionBallHeading.DownRight,
                                    AcquisitionBallHeading.UpRight => AcquisitionBallHeading.UpLeft,
                                    AcquisitionBallHeading.DownRight => AcquisitionBallHeading.DownLeft,
                                    AcquisitionBallHeading.UpLeft => AcquisitionBallHeading.UpRight,
                                    _ => throw new ArgumentOutOfRangeException()
                                };

                            }
                            else //if vertical
                            {
                                heading = heading switch
                                {
                                    AcquisitionBallHeading.DownLeft => AcquisitionBallHeading.UpLeft,
                                    AcquisitionBallHeading.UpRight => AcquisitionBallHeading.DownRight,
                                    AcquisitionBallHeading.DownRight => AcquisitionBallHeading.UpRight,
                                    AcquisitionBallHeading.UpLeft => AcquisitionBallHeading.DownLeft,
                                    _ => throw new ArgumentOutOfRangeException()
                                };
                            }
                        }
                    }


                    bool moveDown = false;
                    bool moveLeft = false;
                    bool moveRight = false;
                    bool moveUp = false;

                    if (heading == AcquisitionBallHeading.DownLeft)
                    {
                        moveLeft = true;
                        moveDown = true;
                    }
                    else if (heading == AcquisitionBallHeading.DownRight)
                    {
                        moveRight = true;
                        moveDown = true;
                    }
                    else if (heading == AcquisitionBallHeading.UpLeft)
                    {
                        moveLeft = true;
                        moveUp = true;
                    }
                    else if (heading == AcquisitionBallHeading.UpRight)
                    {
                        moveRight = true;
                        moveUp = true;
                    }

                    if (moveLeft)
                    {
                        if (X - postNodeMovement > new Fix64(20))
                        {
                            X = X - postNodeMovement;
                        }
                        else
                        {
                            X = new Fix64(20);
                        }
                        
                    }
                    else if (moveRight)
                    {
                        if (X + postNodeMovement < new Fix64(1260))
                        {
                            X = X + postNodeMovement;
                        }
                        else
                        {
                            X = new Fix64(1260);
                        }
                    }
                    if (moveUp)
                    {
                        if (Y - postNodeMovement > new Fix64(60))
                        {
                            Y = Y - postNodeMovement;
                        }
                        else
                        {
                            Y = new Fix64(60);
                        }
                    }
                    else if (moveDown)
                    {
                        if (Y + postNodeMovement < new Fix64(700))
                        {
                            Y = Y + postNodeMovement;
                        }
                        else
                        {
                            Y = new Fix64(700);
                        }
                    }

                }
            }
        }
        
        //returns a corners direction 
        private AcquisitionBallHeading FindCornerDirection((Turn lineStart, Turn lineEnd)[] turns)
        {
            Turn cornerPoint;
            if (turns[0].lineStart.Equals(turns[1].lineEnd) || turns[0].lineStart.Equals(turns[1].lineStart))
            {
                cornerPoint = turns[0].lineStart;
            }
            else
            {
                cornerPoint = turns[0].lineEnd;
            }

            Turn line1OtherPoint;
            line1OtherPoint = turns[0].lineStart.Equals(cornerPoint) ? turns[0].lineEnd : turns[0].lineStart;
            
            Turn line2OtherPoint;
            line2OtherPoint = turns[1].lineStart.Equals(cornerPoint) ? turns[1].lineEnd : turns[1].lineStart;
            
            //find if line 1 is horizontal or vertical
            bool up = false;
            bool down = false;
            bool left = false;
            bool right = false;
            if (line1OtherPoint.nodeX == cornerPoint.nodeX)
            {
                if (line1OtherPoint.nodeY - cornerPoint.nodeY > 0)
                {
                    down = true;
                }
                else
                {
                    up = true;
                }
                
            }
            else
            {
                if (line1OtherPoint.nodeX - cornerPoint.nodeX > 0)
                {
                    right = true;
                }
                else
                {
                    left = true;
                }
            }
            
            
            if (line2OtherPoint.nodeX == cornerPoint.nodeX)
            {
                if (line2OtherPoint.nodeY - cornerPoint.nodeY > 0)
                {
                    down = true;
                }
                else
                {
                    up = true;
                }
                
            }
            else
            {
                if (line2OtherPoint.nodeX - cornerPoint.nodeX > 0)
                {
                    right = true;
                }
                else
                {
                    left = true;
                }
            }

            if (up && left)
                return AcquisitionBallHeading.UpLeft;
            if (down && right)
                return AcquisitionBallHeading.DownRight;
            if (down && left)
                return AcquisitionBallHeading.DownLeft;
            if (up && right)
                return AcquisitionBallHeading.UpRight;

            throw new Exception("Corner check in ball return wasn't provided a valid corner?");

        }
        
        public void Respawn((int nodeX, int nodeY, bool isCorner, (Turn lineStart, Turn lineEnd)[] containedLines) position)
        {
            ballRespawnDetails.play();
            respawnedThisFrame = true;
            bool isCorner;
            (Turn lineStart, Turn lineEnd)[] containedLines;
            int oldNodeX = nodeX;
            int oldnodey = nodeY;
            (nodeX, nodeY, isCorner, containedLines) = position;
            (int X, int Y) coord = AcquisitionSharedFunctions.GetNodeCoordinates(nodeX, nodeY);
            Log.Logger.Debug($"respawned ball at {nodeX},{nodeY},{coord.X},{coord.Y}");
            //heading = AcquisitionPlayerHeading.Stopped; do i need to set a heading?

            int XMovement = oldNodeX - nodeX;
            int YMovement = oldnodey - nodeY;

            bool up = false;
            bool down = false;
            bool left = false;
            bool right = false;

            
            if (XMovement == 0)
            {
                if (isCorner)
                {
                    //this is moving us directly right or left
                    AcquisitionBallHeading cornerDirection = FindCornerDirection(containedLines);
                    if (cornerDirection == AcquisitionBallHeading.UpLeft || cornerDirection == AcquisitionBallHeading.DownLeft)
                    {
                        right = true;
                    }
                    else
                    {
                        left = true;
                    }
                }
                else
                {
                    if (heading == AcquisitionBallHeading.DownRight || heading == AcquisitionBallHeading.UpRight)
                    {
                        right = true;
                    }
                    else
                    {
                        left = true;
                    }
                }
            }
            else if (XMovement < 0)
            {
                left = true;
            }
            else
            {
                right = true;
            }
            
            if (YMovement == 0)
            {
                if (isCorner)
                {
                    //this is moving us directly right or left
                    AcquisitionBallHeading cornerDirection = FindCornerDirection(containedLines);
                    if (cornerDirection == AcquisitionBallHeading.UpLeft || cornerDirection == AcquisitionBallHeading.UpRight)
                    {
                        down = true;
                    }
                    else
                    {
                        up = true;
                    }
                }
                else
                {
                    if (heading == AcquisitionBallHeading.UpLeft || heading == AcquisitionBallHeading.UpRight)
                    {
                        up = true;
                    }
                    else
                    {
                        down = true;
                    }
                }
            }
            else if (YMovement < 0)
            {
                up = true;
            }
            else
            {
                down = true;
            }

            if (up && left)
                heading = AcquisitionBallHeading.UpLeft;
            else if (down && right)
                heading = AcquisitionBallHeading.DownRight;
            else if (down && left)
                heading = AcquisitionBallHeading.DownLeft;
            else if (up && right)
                heading = AcquisitionBallHeading.UpRight;
            
            
            X = new Fix64(coord.X);
            Y = new Fix64(coord.Y);
        }

        public void FreezeForEndGame()
        {
            isGameFrozen = true;
        }
        
        
        public ulong CalculateDesynchronizationCheckValue(bool verbose)
        {
            ulong checkValue = ((ulong)nodeX + (ulong)nodeY)*5;
            if(verbose)
                Log.Error($"A game ball returns a check value of {checkValue}, it is at node {nodeX}, {nodeY}, absolute position {X.ToString()}, {Y.ToString()}");
            return checkValue;
        }

        
        static HashSet<Type>? typeList;
        public HashSet<Type> GetCollidableClasses()
        {
            if (typeList is null)
            {
                typeList = new HashSet<Type>
                {
                    typeof(AcquisitionPlayer),
                    typeof(AcquisitionGameManager)
                };
            }
            return typeList;
        }

    }
}