using System;
using System.Collections.Generic;
using System.Xml.Linq;
using FixMath.NET;
using Synchrocade.Content.Text.Global;
using Synchrocade.GameObjects.PaletteEffect;
using Synchrocade.GameObjects.Sound;
using Synchrocade.ReusableFunctions;
using Ultraviolet;
using Ultraviolet.Audio;
using Ultraviolet.Content;
using Ultraviolet.Graphics;
using Ultraviolet.Graphics.Graphics2D;

namespace Synchrocade.GameObjects.UniqueGameObjects.SkidstarObjects
{
    internal class SkidstarExplosionEffect : IGameObject

    {
        private const int xyscale = 1;
        private const int rotationscale = 1;


        private readonly Fix64 x;
        private readonly Fix64 y;
        private readonly Fix64 size;
        private int delay;
        private Fix64 northX;
        private Fix64 northY;
        private Fix64 eastX;
        private Fix64 eastY;
        private Fix64 southX;
        private Fix64 southY;
        private Fix64 westX;
        private Fix64 westY;
        private int deletedFrame = -1;
        
        private static Texture2D? sprite;
        private const float layerDepth = .8f;
        private int lifetime = 50;

        private Fix64 travelrate;
        private int player;
        internal SkidstarExplosionEffect(int player, Fix64 size, Fix64 x, Fix64 y, int delay, OnlineGameLoop owner)
        {
            this.player = player;
            this.delay = delay;
            this.x = x;
            this.northX = x;
            this.southX = x;
            this.eastX = x;
            this.westX = x;
            this.y = y;
            this.northY = y;
            this.southY = y;
            this.eastY = y;
            this.westY = y;
            this.size = size;
            this.delay = delay;
            travelrate = (new Fix64(3)*size)/new Fix64(2);
            
            

            //hit1details.volume = .25f;
            //hit1details.looping = true;
            //1/2, 1, 3/2, 2
            if (size >= new Fix64(2))
            {
                hitdetails = new NetSound(hit4!,owner);
                hitdetails.volume = .30f;

            }
            else if (size >= new Fix64(3)/new Fix64(2))
            {
                hitdetails = new NetSound(hit3!,owner);
                hitdetails.volume = .29f;
            }
            else if (size >= new Fix64(1))
            {
                hitdetails = new NetSound(hit2!,owner);
                hitdetails.volume = .28f;
            }
            else
            {
                hitdetails = new NetSound(hit1!,owner);
                hitdetails.volume = .27f;
            }


        }


        public int DeletedFrame()
        {
            return deletedFrame;
        }


        private bool soundstarted = false;
        public void Update(OnlineGameLoop caller)
        {
            if (deletedFrame > 0)
            {
                return;
            }
            if (delay > 0)
            {
                delay--;
                return;
            }

            if (!soundstarted)
            {
                hitdetails.play();
                soundstarted = true;
            }
            
            northY -= travelrate;
            southY += travelrate;
            eastX += travelrate;
            westX -= travelrate;

            lifetime--;
            if (lifetime <= 0)
                deletedFrame = caller.GetCurrentFrame();
            
            hitdetails.update();
            
        }

        
        private static SoundEffect? hit1;
        private static SoundEffect? hit2;
        private static SoundEffect? hit3;
        private static SoundEffect? hit4;
        private NetSound hitdetails;

        public void PlaySounds(NetSoundManager soundManager, int currentFrame)
        {
            
            hitdetails.doPlayback(soundManager);
            //throw new NotImplementedException();
        }

        public const float northRotation = 0;
        public const float eastRotation = (float) 1.570796;
        public const float southRotation = (float) 3.141593;
        public const float westRotation = (float) 4.712389;
            
        public void Draw(PaletteEffectSpriteBatch context, float percentToNextFrame, IGameObject? nextFrame)
        {
            if (deletedFrame > 0||delay>0)
            {
                return;
            }
            if (percentToNextFrame == 0 || nextFrame is null)
            {
               
                
                //draw north east south and west
                DrawInternal(context, (float)northX, (float)northY, (float)northRotation,(float)size);
                DrawInternal(context, (float)southX, (float)southY, (float)southRotation,(float)size);
                DrawInternal(context, (float)eastX, (float)eastY, (float)eastRotation,(float)size);
                DrawInternal(context, (float)westX, (float)westY, (float)westRotation,(float)size);

            }
            else
            {

                SkidstarExplosionEffect nextExplosionFrame = (SkidstarExplosionEffect)nextFrame;


                //interpolate all xs and ys
                float inorthX = ScreenWrap.WrapLerp((float)northX, (float)nextExplosionFrame.northX, percentToNextFrame, 1280 * xyscale);
                float inorthY = ScreenWrap.WrapLerp((float)northY, (float)nextExplosionFrame.northY, percentToNextFrame, 720 * xyscale);


                float isouthX = ScreenWrap.WrapLerp((float)southX, (float)nextExplosionFrame.southX, percentToNextFrame, 1280 * xyscale);
                float isouthY = ScreenWrap.WrapLerp((float)southY, (float)nextExplosionFrame.southY, percentToNextFrame, 720 * xyscale);

                float ieastX = ScreenWrap.WrapLerp((float)eastX, (float)nextExplosionFrame.eastX, percentToNextFrame, 1280 * xyscale);
                float ieastY = ScreenWrap.WrapLerp((float)eastY, (float)nextExplosionFrame.eastY, percentToNextFrame, 720 * xyscale);

                float iwestX = ScreenWrap.WrapLerp((float)westX, (float)nextExplosionFrame.westX, percentToNextFrame, 1280 * xyscale);
                float iwestY = ScreenWrap.WrapLerp((float)westY, (float)nextExplosionFrame.westY, percentToNextFrame, 720 * xyscale);

                
                //draw
                DrawInternal(context, (float)inorthX, (float)inorthY, (float)northRotation,(float)size);
                DrawInternal(context, (float)isouthX, (float)isouthY, (float)southRotation,(float)size);
                DrawInternal(context, (float)ieastX, (float)ieastY, (float)eastRotation,(float)size);
                DrawInternal(context, (float)iwestX, (float)iwestY, (float)westRotation,(float)size);


            }
        }

        void DrawInternal(PaletteEffectSpriteBatch context, float x, float y, float rotation, float scale)
        {
            PaletteSpriteData paletteId = new PaletteSpriteData((UInt16)(player),true);
            if (deletedFrame > 0)
            {
                return;
            }
            if(sprite is null)
            {
                throw new InvalidOperationException(GlobalExceptionText.GameAssetNotLoaded);
            }


            
                //PaletteSpriteData paletteId = new PaletteSpriteData((UInt16)(player),true);
                //draw the sprite
                context.Draw(sprite, new Vector2(x / xyscale, y / xyscale), new Rectangle(0, 0, sprite.Width, sprite.Height), Color.Transparent, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), scale, SpriteEffects.None, layerDepth, paletteId);

                //draw one of the up to 4 clones
                bool touchingLeftWall = (x - sprite.Width * xyscale / 2) < 0;
                bool touchingRightWall = (x + sprite.Width * xyscale / 2) > 1280;

                bool touchingTopWall = (y - sprite.Height * xyscale / 2) < 0;
                bool touchingBottomWall = (y + sprite.Height * xyscale / 2) > 720; ;
                if (touchingLeftWall)
                {
                    context.Draw(sprite, new Vector2((x / xyscale) + 1280, y / xyscale), new Rectangle(0, 0, sprite.Width, sprite.Height), Color.Transparent, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), scale, SpriteEffects.None, layerDepth, paletteId);

                    if (touchingBottomWall)
                    {
                        context.Draw(sprite, new Vector2((x / xyscale) + 1280, (y / xyscale) - 720), new Rectangle(0, 0, sprite.Width, sprite.Height), Color.Transparent, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), scale, SpriteEffects.None, layerDepth);
                        context.Draw(sprite, new Vector2((x / xyscale), (y / xyscale) - 720), new Rectangle(0, 0, sprite.Width, sprite.Height), Color.Transparent, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), scale, SpriteEffects.None, layerDepth);
                    }
                    else if (touchingTopWall)
                    {
                        context.Draw(sprite, new Vector2((x / xyscale) + 1280, (y / xyscale) + 720), new Rectangle(0, 0, sprite.Width, sprite.Height), Color.Transparent, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), scale, SpriteEffects.None, layerDepth);
                        context.Draw(sprite, new Vector2((x / xyscale), (y / xyscale) + 720), new Rectangle(0, 0, sprite.Width, sprite.Height), Color.Transparent, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), scale, SpriteEffects.None, layerDepth);
                    }
                }
                else if (touchingRightWall)
                {
                    context.Draw(sprite, new Vector2((x / xyscale) - 1280, y / xyscale), new Rectangle(0, 0, sprite.Width, sprite.Height), Color.Transparent, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), scale, SpriteEffects.None, layerDepth);

                    if (touchingBottomWall)
                    {
                        context.Draw(sprite, new Vector2((x / xyscale) - 1280, (y / xyscale) - 720), new Rectangle(0, 0, sprite.Width, sprite.Height), Color.Transparent, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), scale, SpriteEffects.None, layerDepth);
                        context.Draw(sprite, new Vector2((x / xyscale), (y / xyscale) - 720), new Rectangle(0, 0, sprite.Width, sprite.Height), Color.Transparent, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), scale, SpriteEffects.None, layerDepth);
                    }
                    else if (touchingTopWall)
                    {
                        context.Draw(sprite, new Vector2((x / xyscale) - 1280, (y / xyscale) + 720), new Rectangle(0, 0, sprite.Width, sprite.Height), Color.Transparent, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), scale, SpriteEffects.None, layerDepth);
                        context.Draw(sprite, new Vector2((x / xyscale), (y / xyscale) + 720), new Rectangle(0, 0, sprite.Width, sprite.Height), Color.Transparent, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), scale, SpriteEffects.None, layerDepth);
                    }

                }
                else if (touchingTopWall)
                {
                    context.Draw(sprite, new Vector2((x / xyscale), (y / xyscale) + 720), new Rectangle(0, 0, sprite.Width, sprite.Height), Color.Transparent, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), scale, SpriteEffects.None, layerDepth);

                }
                else if (touchingBottomWall)
                {
                    context.Draw(sprite, new Vector2((x / xyscale), (y / xyscale) - 720), new Rectangle(0, 0, sprite.Width, sprite.Height), Color.Transparent, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), scale, SpriteEffects.None, layerDepth);

                }
            

        }

        
        
        
        public (bool isColliding, ICollisionDetails? details) CheckCollision(IGameObject otherObject)
        {
            throw new NotImplementedException();//this should never be hit because this doesn't collide with things
        }

        public void AddCollisionCommand(IGameObject otherObject, ICollisionDetails? details, bool performedCollisionCheck)
        {
            throw new NotImplementedException();//this should never be hit because this doesn't collide with things
        }

        public void ExecuteCollisionCommands(OnlineGameLoop caller)
        {
        }

        public ulong CalculateDesynchronizationCheckValue(bool verbose)
        {
            return 0;
        }
        public object Clone()
        {
            SkidstarExplosionEffect clone = (SkidstarExplosionEffect)this.MemberwiseClone();
            clone.hitdetails = (hitdetails.Clone() as NetSound)!;
            return clone;
        }
        static HashSet<Type> typeList = new HashSet<Type>();
        HashSet<Type> IGameObject.GetCollidableClasses()
        {
            return typeList;
        }
        
        internal static void LoadContent(ContentManager contentManager)
        {
            if(hit1 is null)
                hit1 = contentManager.Load<SoundEffect>("Unique/Skidstar/Sound/hit1.wav", false);
            if(hit2 is null)
                hit2 = contentManager.Load<SoundEffect>("Unique/Skidstar/Sound/hit2.wav", false);
            if(hit3 is null)
                hit3 = contentManager.Load<SoundEffect>("Unique/Skidstar/Sound/hit3.wav", false);
            if(hit4 is null)
                hit4 = contentManager.Load<SoundEffect>("Unique/Skidstar/Sound/hit4.wav", false);



            if(sprite is null)
                sprite = contentManager.Load<Texture2D>("Unique/Skidstar/Graphics/explosion",false);
        }
    }
}