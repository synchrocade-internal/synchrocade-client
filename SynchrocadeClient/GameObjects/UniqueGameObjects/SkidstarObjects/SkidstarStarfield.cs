using System;
using System.Collections.Generic;
using System.Numerics;
using Synchrocade.GameObjects.Sound;
using Synchrocade.ReusableFunctions;
using Ultraviolet;
using Ultraviolet.Content;
using Ultraviolet.Graphics;
using Ultraviolet.Graphics.Graphics2D;
using Vector2 = Ultraviolet.Vector2;

namespace Synchrocade.GameObjects.UniqueGameObjects.SkidstarObjects
{
    class SkidstarStarfield : IGameObject
    {
        private readonly int RandomSeed;
        internal ClonableRandom rand;
        
        private static Texture2D? background;
        private readonly Vector2[] stars;
        private (int color,int size, int variation)[] starColors;

        private const int numberOfStars = 250;

        
        //stars should be in an array, the positions are static but their colors will change periodically
        //colors should be defined in sets of 3
        //lets impliment this without colors first
        
        private const float layerDepth = 2f;
        
            
        public SkidstarStarfield(int randomSeed, int width, int height)
        {
            rand = new ClonableRandom(randomSeed);
            stars = new Vector2[numberOfStars];
            starColors = new (int, int, int)[numberOfStars];

            PopulateStarfield(width, height);
            
            
            RandomSeed = randomSeed;
        }

        void PopulateStarfield(int width, int height)
        {
            int[] starxcoords = new int[numberOfStars];
            int[] starycoords = new int[numberOfStars];
            float loopXGranularity = (width - 10) / (float)numberOfStars;
            float loopYGranularity = (height - 10) / (float)numberOfStars;

            for (int i = 0; i < numberOfStars; i++)
            {
                starxcoords[i] = (int)(i * loopXGranularity) + 5;
                starycoords[i] = (int)(i * loopYGranularity) + 5;
                int color = rand.Next(1, 4);
                int size = rand.Next(1, 4);
                int variation = rand.Next(1, 12);
                starColors[i] = (color, size, variation);
            }

            Shuffle.ShuffleArray<int>(starycoords, rand);
            
            for (int i = 0; i < numberOfStars; i++)
            {
                stars[i] = new Vector2(starxcoords[i],starycoords[i]);
            }
        }
        
        public void Update(OnlineGameLoop caller)
        {
            starColors[rand.Next(0,numberOfStars)].variation=rand.Next(1, 12);
            //throw new NotImplementedException();
        }
        
        public void Draw(PaletteEffectSpriteBatch context, float percentToNextFrame, IGameObject? nextFrame)
        {
            context.Draw(background, new Vector2(0, 0), null, Color.Transparent, 0, new Vector2(0, 0), 1,
                SpriteEffects.None, layerDepth);
            
            for (int i = 0; i < numberOfStars; i++)
            {
                
                context.DrawPoint(context.WhiteTexture(), stars[i], getColor(starColors[i].color, starColors[i].variation), starColors[i].size, layerDepth - .1f, true);
                //stars[i] = new Vector2(starxcoords[i],starycoords[i]);
            }
            //throw new NotImplementedException();
        }

        private Color getColor(int color, int variation)
        {
            //color 1: blue, color 2: red, color 3: gray
            Color returnColor;
            int r;
            int g;
            int b;

            if (color == 1)
            {
                r = 50;
                g = 150;
                b = 200;

            }
            else if (color == 2)
            {
                r = 200;
                g = 100;
                b = 100;

            }
            else 
            {
                r = 133;
                g = 133;
                b = 133;

            }

            //variation 1 is just as it is
            if (variation == 2)
                r += 30;
            else if (variation == 3)
                g += 30;
            else if (variation == 4)
                b += 30;
            else if (variation == 5)
                r -= 30;
            else if (variation == 6)
                g -= 30;
            else if (variation == 7)
                b -= 30;
            else if (variation == 8)
            {
                r += 10;
                g += 10;
                b += 10;
            }
            else if (variation == 9)
            {
                r -= 10;
                g -= 10;
                b -= 10;
            }
            else if (variation == 10)
            {
                r += 20;
                g += 20;
                b += 20;
            }
            else if (variation == 11)
            {
                r -= 20;
                g -= 20;
                b -= 20;
            }
            
            returnColor = new Color(r,g,b);
            return returnColor;
        }
        
        internal static void LoadContent(ContentManager contentManager)
        {
            if (background is null)
                background = contentManager.Load<Texture2D>("Unique/Skidstar/Graphics/skidstarbackground",false);
        }
        
        
        public object Clone()
        {
            SkidstarStarfield returnfield = (SkidstarStarfield)this.MemberwiseClone();
            returnfield.rand = rand.Clone();
            (int color, int size, int variation)[] array = new (int color, int size, int variation)[numberOfStars];
            Array.Copy(returnfield.starColors, array,numberOfStars);
            returnfield.starColors = array;
            return returnfield;
        }

        
        
        
        
        
        
        
        
        
        /*
         *
         * everything below is defaulted out because it's not an object that interacts with anything
         * 
         */
        public int DeletedFrame()
        {
            return -1;
        }

        public void PlaySounds(NetSoundManager soundManager, int currentFrame)
        {
            return;
        }
        
        public ulong CalculateDesynchronizationCheckValue(bool verbose) //background desyncs aren't worth tracking in the slightest
        {
            return 0;
        }
        

        static HashSet<Type> typeList = new HashSet<Type>();
        HashSet<Type> IGameObject.GetCollidableClasses()
        {
            return typeList;
        }
        
        //collisions can't happen because the object doesn't collide with anything, so i'll leave these all as exceptions
        public (bool isColliding, ICollisionDetails? details) CheckCollision(IGameObject otherObject)
        {
            throw new NotImplementedException();
        }
        
        public void AddCollisionCommand(IGameObject otherObject, ICollisionDetails? details, bool performedCollisionCheck)
        {
            throw new NotImplementedException();
        }

        public void ExecuteCollisionCommands(OnlineGameLoop caller)
        {
            return; //this is called every frame to execute anything that happened on a prior one
        }
    }
}