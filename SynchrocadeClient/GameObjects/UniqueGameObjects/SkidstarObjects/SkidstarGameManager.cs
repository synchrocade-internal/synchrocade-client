using System;
using System.Collections.Generic;
using Synchrocade.GameObjects.Sound;

namespace Synchrocade.GameObjects.UniqueGameObjects.SkidstarObjects
{
    internal class SkidstarGameManager : IManagerGameObject
    {
        private readonly int players;
        private readonly int numberOfRounds;

        private int respawnTimer = 0;
        private const int respawnTimerMax = 600;
        private long[] currentScores;

        
        internal SkidstarGameManager(int players, int numberOfRounds)
        {
            this.players = players;
            this.numberOfRounds = numberOfRounds;
            currentScores = new long[players];
        }

        private bool gameFinished = false;
        public bool IsGameFinished()
        {
            return gameFinished;
        }

        private bool doesScoreDeclareAWinner()
        {
            foreach (int score in currentScores)
            {
                if (score == numberOfRounds)
                    return true;
            }
            return false;
        }

        public long[] ReturnScores()
        {
            return currentScores;
        }


        public void Update(OnlineGameLoop caller)
        {
            if (respawnTimerRunning && respawnTimer > 0)
            {
                respawnTimer--;
            }

            
        }
        
        private bool needsRespawn = false;
        private bool respawnTimerRunning = false;
        
        public (bool isColliding, ICollisionDetails? details) CheckCollision(IGameObject otherObject)
        {
            if (respawnTimerRunning && respawnTimer == 0)
            {
                if (doesScoreDeclareAWinner())
                    gameFinished = true;
                else 
                    needsRespawn = true;
            }
            
            if (otherObject.GetType() == typeof(SkidstarShip))
            {
                SkidstarShip otherShip = (SkidstarShip)otherObject;
                if (otherShip.isDead())
                {
                    //leave this to only support 2p for now
                    if (otherShip.player == 1)
                    {
                        currentScores[0]++;
                    }
                    else
                    {
                        currentScores[1]++;
                    }

                    respawnTimer = respawnTimerMax;
                    respawnTimerRunning = true;
                }
                return (true, null);
            }
            throw new NotImplementedException(); //this should never be hit because nothing else should ever be called by code as colliding with this
        }

        public void AddCollisionCommand(IGameObject otherObject, ICollisionDetails? details, bool performedCollisionCheck)
        {
            if (otherObject.GetType() == typeof(SkidstarShip))
            {
                if (needsRespawn)
                {
                    SkidstarShip otherShip = (SkidstarShip)otherObject;
                    otherShip.setRespawn();
                }

                return;
            }
            
            throw new NotImplementedException();
        }

        public void ExecuteCollisionCommands(OnlineGameLoop caller)
        {
            if (respawnTimerRunning && respawnTimer == 0)
            {
                needsRespawn = false;
                respawnTimerRunning = false;
            }
        }

        
        
        
        
        
        
        public ulong CalculateDesynchronizationCheckValue(bool verbose)
        {
            long allScores = 0;
            foreach (int score in currentScores)
            {
                allScores += score;
            }

            if (allScores >= 0)
                return (uint) allScores;
            return 0;
        }

        
        static HashSet<Type>? typeList;
        HashSet<Type> IGameObject.GetCollidableClasses()
        {
            if (typeList is null)
            {
                typeList = new HashSet<Type>
                {
                    typeof(SkidstarShip),
                };
            }
            return typeList;
        }

        public object Clone()
        {
            SkidstarGameManager clone = (SkidstarGameManager) this.MemberwiseClone();
            clone.currentScores = (long[])currentScores.Clone();
            return clone;
        }

        public int DeletedFrame()
        {
            return -1;
        }

        public void PlaySounds(NetSoundManager soundManager, int currentFrame)
        {
        }

        public void Draw(PaletteEffectSpriteBatch context, float percentToNextFrame, IGameObject? nextFrame)
        {
        }

    }
}