﻿using Ultraviolet.Graphics.Graphics2D;
using Ultraviolet.Graphics;
using Ultraviolet;
using Synchrocade.ReusableFunctions;
using System;
using System.Collections.Generic;
using FixMath.NET;
using Newtonsoft.Json.Serialization;
using Ultraviolet.Content;
using Synchrocade.GameObjects.Input;
using static Synchrocade.GameObjects.Input.InputObject;
using Synchrocade.GameObjects;
using Synchrocade.GameObjects.PaletteEffect;
using Synchrocade.GameObjects.CommonGameObjects;
using Serilog;
using Synchrocade.Content.Text.Global;
using Synchrocade.GameObjects.Sound;
using Ultraviolet.Audio;

namespace Synchrocade.GameObjects.UniqueGameObjects.SkidstarObjects
{
    class SkidstarShip : IPlayerGameObject
    {
        private const int xyscale = 1;
        private const int rotationscale = 1;

        private int incomingDamage = 0;

        private int health = 10000;
        private int currentChargeValue = 0;
        private bool fireButtonCurrentlyPressed = false;
        private bool dead = false;



        private readonly Fix64 startX;
        private readonly Fix64 startY;
        private readonly Fix64 startRotation;
        
        internal Fix64 x;
        internal Fix64 y;
        private readonly Fix64 collisionradius = new Fix64(35 * xyscale);

        internal Fix64 rotation = new Fix64(0);
        Fix64 rotationVelocity = new Fix64(0);

        Fix64 movementVectorX = new Fix64(0);
        Fix64 movementVectorY = new Fix64(0);

        private Fix64 collisionPositionModx = new Fix64(0);
        private Fix64 collisionPositionMody = new Fix64(0);
        private Fix64 movementVectorModx = new Fix64(0);
        private Fix64 movementVectorMody = new Fix64(0);
        private Fix64 rotationVelosityMod = new Fix64(0);


        public readonly int player;

        InputObject input;
        private static Texture2D? sprite;
        private static Texture2D? thrust;

        private static Texture2D? spriteturnleft;

        private static Texture2D? barFrame;
        private static Texture2D? barInternal;





        //private bool chargeSoundPlaying = false;
        private static SoundEffect? chargeSound1;
        private static SoundEffect? chargeSound2;
        private static SoundEffect? chargeSound3;
        private static SoundEffect? chargeSound4;



        //private readonly object chargeSoundHandle = new object();
        private NetSound chargeSoundDetails1;
        private NetSound chargeSoundDetails2;
        private NetSound chargeSoundDetails3;
        private NetSound chargeSoundDetails4;




        //private bool chargeNotificationPlaying = false;
        private static SoundEffect? chargeNotification;
        //private readonly object chargeNotificationHandle = new object();
        private NetSound chargeNotificationDetails;

        private static SoundEffect? overchargeSound;
        private NetSound overchargeSoundDetails;
        private NetSound crashSoundDetails;


        private static SoundEffect? engineLoop;
        private NetSound engineLoopDetails;

        

        private const float layerDepth = 1f;
        private const float spriteScale = 1;

        private bool rotatingLeft = false;
        private bool rotatingRight = false;

        private bool thrusting = false;

        private int chargeGraphicModifier = 0;
        private const int maxChargeModifer = 200;

        
        
        private bool shouldRespawn = false;
        private void respawn()
        {
            dead = false;
            x = startX;
            y = startY;
            rotation = startRotation;
            health = 10000;
            currentChargeValue = 0;
            movementVectorX = new Fix64(0);
            movementVectorY = new Fix64(0);
            shouldRespawn = false;
            
            this.collisionPositionModx = new Fix64(0);
            this.collisionPositionMody = new Fix64(0);
            this.movementVectorModx = new Fix64(0);
            this.movementVectorMody = new Fix64(0);
            this.rotationVelosityMod = new Fix64(0);
            this.rotationVelocity = new Fix64(0);
            hasReportedDeath = false;
        }

        internal void setRespawn()
        {
            shouldRespawn = true;
        }

        private bool hasReportedDeath = false;
        internal bool isDead()
        {
            if (hasReportedDeath)
                return false;
            else if (dead)
            {
                hasReportedDeath = true;
                return true;
            }

            return false;
        }

        internal SkidstarShip(int x, int y, int startingAngle, int player, OnlineGameLoop owner)
        {
             

            this.player = player;
            this.x = new Fix64(x * xyscale);
            this.y = new Fix64(y * xyscale);
            rotation = new Fix64(startingAngle * rotationscale);

            this.startX = this.x;
            this.startY = this.y;
            this.startRotation = rotation;

            
            chargeSoundDetails1 = new NetSound(chargeSound1!,owner); 
            chargeSoundDetails1.volume = .60f;
            chargeSoundDetails1.looping = true;
            
            chargeSoundDetails2 = new NetSound(chargeSound2!,owner); 
            chargeSoundDetails2.volume = .65f;
            chargeSoundDetails2.looping = true;
            
            chargeSoundDetails3 = new NetSound(chargeSound3!,owner); 
            chargeSoundDetails3.volume = .70f;
            chargeSoundDetails3.looping = true;
            
            chargeSoundDetails4 = new NetSound(chargeSound4!,owner); 
            chargeSoundDetails4.volume = .75f;
            chargeSoundDetails4.looping = true;
            
            
            //chargeSoundDetails.pitch = -.5f; pitch seems currently fucked with bass

            chargeNotificationDetails = new NetSound(chargeNotification!, owner);

            overchargeSoundDetails = new NetSound(overchargeSound!, owner);
            overchargeSoundDetails.volume = .30f;
            
            crashSoundDetails = new NetSound(overchargeSound!, owner);
            crashSoundDetails.volume = .30f;
            
            engineLoopDetails = new NetSound(engineLoop!, owner);
            engineLoopDetails.volume = .07f;
            engineLoopDetails.looping = true;
        }


        void IGameObject.AddCollisionCommand(IGameObject otherObject, ICollisionDetails? details, bool performedCollisionCheck)
        {
            if (dead||otherObject.GetType() == typeof(SkidstarGameManager))
                return;
            //const int spinRatio = 50;
            BouncingCircleCollisionDetails? collisionDetails = (BouncingCircleCollisionDetails?)details; //if i'm colliding with anything that doesn't provide me with details within the context of skidstar that is exceptional
            if (otherObject.GetType() == typeof(SkidstarShip) || otherObject.GetType() == typeof(SkidstarBullet))
            {
                if (!(collisionDetails is null))
                {

                    
                    if (performedCollisionCheck)
                    {

                        this.collisionPositionModx -= collisionDetails.positionMods.modx;
                        this.collisionPositionMody -= collisionDetails.positionMods.mody;
                        if (!(collisionDetails.velocityMods is null))
                        {
                            this.movementVectorModx += collisionDetails.velocityMods.Value.c1VelModX;
                            this.movementVectorMody += collisionDetails.velocityMods.Value.c1VelModY;
                        }

                        if (otherObject.GetType() == typeof(SkidstarShip) && !(collisionDetails.velocityMods is null))
                            this.incomingDamage += (int)(Fix64.Abs(collisionDetails.velocityMods.Value.c1VelModX) + Fix64.Abs(collisionDetails.velocityMods.Value.c1VelModY)) / xyscale * 50;
                    }
                    else
                    {
                        this.collisionPositionModx += collisionDetails.positionMods.modx;
                        this.collisionPositionMody += collisionDetails.positionMods.mody;
                        if (!(collisionDetails.velocityMods is null))
                        {
                            this.movementVectorModx += collisionDetails.velocityMods.Value.c2VelModX;
                            this.movementVectorMody += collisionDetails.velocityMods.Value.c2VelModY;
                        }
                        
                        if (otherObject.GetType() == typeof(SkidstarShip) && !(collisionDetails.velocityMods is null))
                            this.incomingDamage += (int)(Fix64.Abs(collisionDetails.velocityMods.Value.c2VelModX) + Fix64.Abs(collisionDetails.velocityMods.Value.c2VelModY)) / xyscale * 50;
                    }
                }
                if (otherObject.GetType() == typeof(SkidstarBullet))
                {
                    SkidstarBullet bullet = (SkidstarBullet)otherObject;
                    switch (bullet.chargeLevel)
                    {
                        case 1:
                            incomingDamage += 200;
                            break;
                        case 2:
                            incomingDamage += 500;
                            break;
                        case 3:
                            incomingDamage += 1250;
                            break;
                        case 4:
                            incomingDamage += 3125;
                            break;
                    }
                }
                else if (otherObject.GetType() == typeof(SkidstarShip))
                {
                    if (player < ((SkidstarShip) otherObject).player)
                    {
                        crashSoundDetails.play();
                    }
                }


            }
        }

        (bool isColliding, ICollisionDetails? details) IGameObject.CheckCollision(IGameObject otherObject)
        {
            if (otherObject.GetType() == typeof(SkidstarGameManager))
            {
                return otherObject.CheckCollision(this);
            }
            if (dead)
            {
                return (false,null);
            }
            if((this as IGameObject).DeletedFrame() > 0)
            {
                return (false,null);
            }
            if (otherObject.GetType() == typeof(SkidstarShip))
            {

                SkidstarShip othership = (SkidstarShip)otherObject;
                if (othership.dead)
                {
                    return (false,null);
                }
                Fix64 myAdjX = this.x;
                Fix64 myAdjY = this.y;
                Fix64 theirAdjX = othership.x;
                Fix64 theirAdjY = othership.y;
                if(Fix64.Abs(myAdjX - theirAdjX) >= new Fix64(1280 * xyscale / 2))
                {
                    if(myAdjX < theirAdjX)
                    {
                        myAdjX += new Fix64(1280 * xyscale);
                    }
                    else
                    {
                        theirAdjX += new Fix64(1280 * xyscale);
                    }
                }
                if (Fix64.Abs(myAdjY - theirAdjY) >= new Fix64(720 * xyscale / 2))
                {
                    if (myAdjY < theirAdjY)
                    {
                        myAdjY += new Fix64(720 * xyscale);
                    }
                    else
                    {
                        theirAdjY += new Fix64(720 * xyscale);
                    }
                }


                bool isColliding = Collisions.isCircleColliding(myAdjX, myAdjY, this.collisionradius, theirAdjX, theirAdjY, othership.collisionradius);
                if (!isColliding)
                    return (false, null);

                (Fix64 modx, Fix64 mody) correctionDistance = Collisions.correctionDistanceBetweenCircles(myAdjX, myAdjY, this.collisionradius, theirAdjX, theirAdjY, othership.collisionradius);
                (Fix64 c1VelModX, Fix64 c1VelModY, Fix64 c2VelModX, Fix64 c2VelModY)? velocityMods = Collisions.calculateBouncesForPhysicsCircles(myAdjX, myAdjY, this.movementVectorX, this.movementVectorY,new Fix64(100), theirAdjX, theirAdjY, othership.movementVectorX,othership.movementVectorY,new Fix64(100));
                
                return (true, new BouncingCircleCollisionDetails(correctionDistance, velocityMods));
            }
            if (otherObject.GetType() == typeof(SkidstarBullet))
            {
                SkidstarBullet bullet = (SkidstarBullet)otherObject;
                Fix64 myAdjX = this.x;
                Fix64 myAdjY = this.y;
                Fix64 theirAdjX = bullet.x;
                Fix64 theirAdjY = bullet.y;
                if (Fix64.Abs(myAdjX - theirAdjX) >= new Fix64(1280 * xyscale / 2))
                {
                    if (myAdjX < theirAdjX)
                    {
                        myAdjX += new Fix64(1280 * xyscale);
                    }
                    else
                    {
                        theirAdjX += new Fix64(1280 * xyscale);
                    }
                }
                if (Fix64.Abs(myAdjY - theirAdjY) >= new Fix64(720 * xyscale / 2))
                {
                    if (myAdjY < theirAdjY)
                    {
                        myAdjY += new Fix64(720 * xyscale);
                    }
                    else
                    {
                        theirAdjY += new Fix64(720 * xyscale);
                    }
                }



                if (bullet.player == this.player)
                {
                    return (false,null);
                }
                else if (Collisions.isCircleColliding(myAdjX, myAdjY, this.collisionradius, theirAdjX, theirAdjY, bullet.collisionradius))
                { 
                    (Fix64 modx, Fix64 mody) correctionDistance = (new Fix64(0), new Fix64(0));
                    (Fix64 c1VelModX, Fix64 c1VelModY, Fix64 c2VelModX, Fix64 c2VelModY)? velocityMods = Collisions.calculateBouncesForPhysicsCircles(myAdjX, myAdjY, this.movementVectorX, this.movementVectorY, new Fix64(100), theirAdjX, theirAdjY, bullet.movementVectorX, bullet.movementVectorY, new Fix64(bullet.chargeLevel * 25));

                return (true, new BouncingCircleCollisionDetails(correctionDistance, velocityMods));

                }
                else
                    {
                        return (false,null);
                    }
                }

            throw new NotImplementedException();
        }

        void IGameObject.Draw(PaletteEffectSpriteBatch context, float percentToNextFrame, IGameObject? nextFrame)
        {
            if (dead)
                return;

            if (percentToNextFrame<=0 || nextFrame is null)
            {
                DrawInternal(context, (float)x, (float)y, (float)rotation);
            }
            else
            {
                SkidstarShip nextShip = (SkidstarShip)nextFrame;

                //interpolate x and y

                long ix = (long)ScreenWrap.WrapLerp((float)x, (float)nextShip.x, percentToNextFrame,1280*xyscale);
                long iy = (long)ScreenWrap.WrapLerp((float)y, (float)nextShip.y, percentToNextFrame,720*xyscale);

                //long ix = (long)ScreenWrap.WrapLerp(nextShip.x, x, percentToNextFrame, 1280 * xyscale);
                //long iy = (long)ScreenWrap.WrapLerp(nextShip.y, y, percentToNextFrame, 720 * xyscale);

                //interpolate rotation
                float irotation = ScreenWrap.WrapLerp((float)rotation, (float)nextShip.rotation, percentToNextFrame, (float)6.2831 * (float)rotationscale); ;
                //draw
                DrawInternal(context, ix, iy, irotation);

            }

        }


        private const int charge1Length = 120;
        private const int charge2Length = 240;
        private const int charge3Length = 480;
        private const int charge4Length = 960;


        private int currentChargeLevelByChargeValue()
        {
            if (currentChargeValue >= charge4Length) return 4;
            else if (currentChargeValue >= charge3Length) return 3;
            else if (currentChargeValue >= charge2Length) return 2;
            else if (currentChargeValue >= charge1Length) return 1;
            else return 0;
        }

        private float ratioInCurrentCharge()
        {
            if (currentChargeValue >= charge3Length) return ((float)currentChargeValue - charge3Length) / (charge4Length - charge3Length);
            else if (currentChargeValue >= charge2Length) return ((float)currentChargeValue - charge2Length) / (charge3Length - charge2Length);
            else if (currentChargeValue >= charge1Length) return ((float)currentChargeValue - charge1Length) / (charge2Length - charge1Length);
            else if (currentChargeValue >= 0) return (float)currentChargeValue / charge1Length;
            else return 0;
        }


        void DrawInternal(PaletteEffectSpriteBatch context, float x, float y, float rotation)
        {
            Texture2D? sprite = SkidstarShip.sprite;
            SpriteEffects effects = SpriteEffects.None;
            if (rotatingLeft||rotatingRight)
                sprite = SkidstarShip.spriteturnleft;
            if (rotatingRight)
                effects = SpriteEffects.FlipHorizontally;
            
            if (sprite is null)
            {
                throw new InvalidOperationException(GlobalExceptionText.GameAssetNotLoaded);
            }
            
            Color chargeModificationColor;
            if (currentChargeLevelByChargeValue() >= 4)
            {
                
                chargeModificationColor= new Color(255,0,0,(int) (chargeGraphicModifier / 2));
                float currentCharge = chargeGraphicModifier / 50f;
                if (currentCharge <= 1)
                {
                    x = x - 5;
                }
                else if (currentCharge > 1 && currentCharge <= 2)
                {
                    y = y + 5;
                }
                else if (currentCharge > 2 && currentCharge <= 3)
                {
                    x = x + 5;
                }
                else if (currentCharge > 3 && currentCharge <= 4)
                {
                    y = y - 5;
                }
            }
            else
                chargeModificationColor= new Color(200,255,255,(int) (chargeGraphicModifier / 4));
            
            
            

            float fx = (float)x/xyscale;
            float fy = (float)y/xyscale;

            PaletteSpriteData paletteId = new PaletteSpriteData((UInt16)(player),true);




            //draw the sprite new Rectangle(0, 0, drawSprite.Width, drawSprite.Height)
            drawShipAndHud(sprite,context,fx, fy,rotation,chargeModificationColor,effects,paletteId);

            //draw one of the up to 4 clones

            
            int hudCorrectionDistance = barInternal!.Width * 2 + 10;
            
            bool touchingLeftWall = (x - (sprite.Width + hudCorrectionDistance) * xyscale / 2) < 0;
            bool touchingRightWall = (x + (sprite.Width + hudCorrectionDistance)  * xyscale / 2) > 1280;

            bool touchingTopWall = (y - sprite.Height * xyscale / 2) < 0;
            bool touchingBottomWall = (y + sprite.Height * xyscale / 2) > 720; ;
            if (touchingLeftWall)
            {
                drawShipAndHud(sprite,context,fx + 1280, fy,rotation,chargeModificationColor,effects,paletteId);
                
                if (touchingBottomWall)
                {
                    drawShipAndHud(sprite,context,fx + 1280, fy - 720,rotation,chargeModificationColor,effects,paletteId);
                    drawShipAndHud(sprite,context, fx, fy - 720,rotation,chargeModificationColor,effects,paletteId);
                }
                else if (touchingTopWall)
                {
                    drawShipAndHud(sprite,context,fx + 1280, fy + 720,rotation,chargeModificationColor,effects,paletteId);
                    drawShipAndHud(sprite,context, fx, fy + 720,rotation,chargeModificationColor,effects,paletteId);
                }
            }
            else if (touchingRightWall)
            {
                drawShipAndHud(sprite,context,fx - 1280, fy,rotation,chargeModificationColor,effects,paletteId);
                if (touchingBottomWall)
                {
                    drawShipAndHud(sprite,context,fx - 1280, fy - 720,rotation,chargeModificationColor,effects,paletteId);
                    drawShipAndHud(sprite,context, fx, fy - 720,rotation,chargeModificationColor,effects,paletteId);
                }
                else if (touchingTopWall)
                {
                    drawShipAndHud(sprite,context,fx - 1280, fy + 720,rotation,chargeModificationColor,effects,paletteId);
                    drawShipAndHud(sprite,context,fx, fy + 720,rotation,chargeModificationColor,effects,paletteId);
                }

            }
            else if (touchingTopWall)
            {
                drawShipAndHud(sprite,context,fx,fy+720,rotation,chargeModificationColor,effects,paletteId);
            }
            else if (touchingBottomWall)
            {
                drawShipAndHud(sprite, context,fx,fy-720,rotation,chargeModificationColor,effects,paletteId);
            }
        }


        private void drawShipAndHud(Texture2D sprite, PaletteEffectSpriteBatch context, float fx, float fy, float rotation, Color chargeModificationColor, SpriteEffects effects, PaletteSpriteData paletteId)
        {
            Color frameColor = new Color(29, 177, 226);
            //draw ship
            context.Draw(sprite, new Vector2(fx, fy), new Rectangle(0, 0, sprite!.Width, sprite!.Height), chargeModificationColor, rotation / (float)rotationscale + 1.5f * (float)Math.PI, new Vector2(sprite!.Width / 2f, sprite!.Height / 2f), spriteScale, effects, layerDepth, paletteId);
            
            //draw thrust animation (just recycling the bullet image back because lazy)
            if(thrusting)
                context.Draw(thrust, new Vector2(fx, fy), new Rectangle(0, 0, thrust!.Width, thrust!.Height), chargeModificationColor, rotation / (float)rotationscale + 2f * (float)Math.PI, new Vector2(sprite!.Width / 2f - thrust.Width * 2 - 14, sprite!.Height / 2f - thrust.Height), 1f, SpriteEffects.None, layerDepth + .05f, paletteId);

            
            
            //draw health
            float healthratio = health / 10000f;
            float healthcolorElement = healthratio * 510; 
            //1 should be 255 green 0 red
            //.5 should be 255 green 255 red
            //0 should be 255 red 0 green
            Color healthColor = new Color((int)Math.Min((510f - healthcolorElement),255f),(int)Math.Min(healthcolorElement,255f),0);
            float healthPositionMod = (32 * (1 - healthratio));
            context.Draw(barFrame, new Vector2(fx-sprite!.Width/2, fy), new Rectangle(0, 0, barFrame!.Width, barFrame!.Height), frameColor, 0, new Vector2(barFrame!.Width / 2f, barFrame!.Height / 2f), 2f, SpriteEffects.FlipHorizontally, layerDepth +.2f, new PaletteSpriteData((UInt16)(player),false));
            context.Draw(barInternal, new Vector2(fx-sprite!.Width/2, fy+(int)(healthPositionMod*2)), new Rectangle(0, (int)healthPositionMod, barInternal!.Width, barInternal!.Height - (int)healthPositionMod), healthColor, 0, new Vector2(barInternal!.Width / 2f, barInternal!.Height / 2f), 2f, SpriteEffects.FlipHorizontally, layerDepth +.3f, new PaletteSpriteData((UInt16)(player),false));
            
            
            
            //draw charge
            //draw frame
            context.Draw(barFrame, new Vector2(fx+sprite!.Width/2, fy), new Rectangle(0, 0, barFrame!.Width, barFrame!.Height), frameColor, 0, new Vector2(barFrame!.Width / 2f, barFrame!.Height / 2f), 2f, SpriteEffects.None, layerDepth +.2f, new PaletteSpriteData((UInt16)(player),false));

            //charge needs two bars
            //the background bar for the current charge level
            Color charge1Color = new Color(0,48,68);
            Color charge2Color = new Color(0,137,20);
            Color charge3Color = new Color(192,206,0);
            Color charge4Color = new Color(255,0,0);
            int currentChargeLevel = currentChargeLevelByChargeValue();
            Color priorBarColor = charge1Color;
            Color nextBarColor = charge1Color;
            
            switch (currentChargeLevel)
            {
                case 0:
                    nextBarColor = charge1Color;
                    break;
                case 1:
                    priorBarColor = charge1Color;
                    nextBarColor = charge2Color;
                    break;
                case 2:
                    priorBarColor = charge2Color;
                    nextBarColor = charge3Color;
                    break;
                case 3:
                    priorBarColor = charge3Color;
                    nextBarColor = charge4Color;
                    break;
                case 4:
                    priorBarColor = charge4Color;
                    break;
            }

            if (currentChargeLevel != 0)
            {
                context.Draw(barInternal, new Vector2(fx+sprite!.Width/2, fy), new Rectangle(0, 0, barInternal!.Width, barInternal!.Height), priorBarColor, 0, new Vector2(barInternal!.Width / 2f, barInternal!.Height / 2f), 2f, SpriteEffects.None, layerDepth +.4f, new PaletteSpriteData((UInt16)(player),false));
            }
            //the currently charging bar
            if (currentChargeLevel != 4)
            {
                float chargeRatio = ratioInCurrentCharge();
                float chargePositionMod = (32 * (1 - chargeRatio));

                context.Draw(barInternal, new Vector2(fx+sprite!.Width/2, fy+(int)(chargePositionMod*2)), new Rectangle(0, (int)chargePositionMod, barInternal!.Width, barInternal!.Height - (int)chargePositionMod), nextBarColor, 0, new Vector2(barInternal!.Width / 2f, barInternal!.Height / 2f), 2f, SpriteEffects.None, layerDepth +.3f, new PaletteSpriteData((UInt16)(player),false));

            }
            
            


        }
        
        void IGameObject.ExecuteCollisionCommands(OnlineGameLoop caller)
        {
            if (shouldRespawn)
            {
                respawn();
                return;
            }
            if (dead)
                return;
            this.x += this.collisionPositionModx;
            this.y += this.collisionPositionMody;
            this.rotationVelocity += this.rotationVelosityMod;
            this.movementVectorX += this.movementVectorModx;
            this.movementVectorY += this.movementVectorMody;
            this.collisionPositionModx = new Fix64(0);
            this.collisionPositionMody = new Fix64(0);
            this.movementVectorModx = new Fix64(0);
            this.movementVectorMody = new Fix64(0);
            this.rotationVelosityMod = new Fix64(0);
            health -= incomingDamage;
            if (health <= 0)
            {
                health = 0;
                dead = true;
                caller.QueueAddObject(new SkidstarExplosionEffect(player,new Fix64(3),x,y,0,caller));
                caller.QueueAddObject(new SkidstarExplosionEffect(player,new Fix64(3),x - new Fix64(100),y,30,caller));
                caller.QueueAddObject(new SkidstarExplosionEffect(player,new Fix64(3),x,y + new Fix64(100),60,caller));
                caller.QueueAddObject(new SkidstarExplosionEffect(player,new Fix64(3),x + new Fix64(100),y,90,caller));
                caller.QueueAddObject(new SkidstarExplosionEffect(player,new Fix64(3),x ,y - new Fix64(100),120,caller));
                chargeSoundDetails1.stop();
                chargeSoundDetails2.stop();
                chargeSoundDetails3.stop();
                chargeSoundDetails4.stop();

                engineLoopDetails.stop();

            }
            incomingDamage = 0;
            //throw new NotImplementedException();
        }


        void IGameObject.Update(OnlineGameLoop caller)
        {
            if (dead)
                return;
            Fix64 speedcap = (Fix64)(7);
            int currentChargeLevel = currentChargeLevelByChargeValue();
            if (this.input.GetDirectionPressed(Direction.up,DType.Move))
            {
                engineLoopDetails.play();
                thrusting = true;
                if (currentChargeLevel == 4)
                {
                    AddForceToShipMomentum((Fix64)(.015));
                }
                else
                {
                    AddForceToShipMomentum((Fix64)(.055));
                } 
            }
            else
            {
                engineLoopDetails.stop();
                thrusting = false;
            }




            x = ScreenWrap.WrapFix64(x, new Fix64(1280 * xyscale));
            y = ScreenWrap.WrapFix64(y, new Fix64(720 * xyscale));
            
            bool capped = false;
            Fix64 value = (movementVectorX*movementVectorX + movementVectorY*movementVectorY);
            Fix64 currentspeed = Fix64.Sqrt(value);
            if (currentspeed > speedcap)
                capped = true;
            if(capped)
            {
                movementVectorX = (movementVectorX * (Fix64).95);
                movementVectorY = (movementVectorY * (Fix64).95);
            }

            bool breaking = false;
            if (this.input.GetButtonPressed(InputObject.Button.but2)||this.input.GetDirectionPressed(InputObject.Direction.down,DType.Move))
            {
                movementVectorX = (movementVectorX * (Fix64).985);
                movementVectorY = (movementVectorY * (Fix64).985);

                breaking = true;
                //movementVectorX = 0;
                //movementVectorY = 0;
            }

            Fix64 rotationBreakSpeed = (Fix64).0006;
            Fix64 rotationSpeed = (Fix64).0004;
            

            if(breaking)
            {
                rotationSpeed = (Fix64)4 * currentspeed / (Fix64)150;
                rotationBreakSpeed = (Fix64)6 * currentspeed / (Fix64)150;

                if (rotationBreakSpeed < (Fix64).0003)
                    rotationBreakSpeed = (Fix64).0003;
                if (rotationSpeed < (Fix64).0002)
                    rotationSpeed = (Fix64).0002;

                if (rotationBreakSpeed > (Fix64).0009)
                    rotationBreakSpeed = (Fix64).0009;
                if (rotationSpeed > (Fix64).0006)
                    rotationSpeed = (Fix64).0006;





            }
            if (currentChargeLevel == 4)
            {
                rotationBreakSpeed = (Fix64).00032;
                rotationSpeed = (Fix64).00022;
            }
            
            if (this.input.GetDirectionPressed(Direction.left,DType.Move))
            {
                if (rotationVelocity >= rotationBreakSpeed)
                    rotationVelocity -= rotationBreakSpeed;
                else
                    rotationVelocity -= rotationSpeed;
                
                rotatingLeft = true;
                rotatingRight = false;
            }
            else if (this.input.GetDirectionPressed(Direction.right,DType.Move))
            {
                if (rotationVelocity <= -rotationBreakSpeed)
                    rotationVelocity += rotationBreakSpeed;
                else
                    rotationVelocity += rotationSpeed;
                rotatingLeft = false;
                rotatingRight = true;
            }
            else if (rotationVelocity != (Fix64)0 && !this.input.GetButtonPressed(InputObject.Button.but3))
            {
                if (rotationVelocity > rotationBreakSpeed)
                    rotationVelocity -= rotationBreakSpeed;
                else if (rotationVelocity < -rotationBreakSpeed)
                    rotationVelocity += rotationBreakSpeed;
                else
                    rotationVelocity = (Fix64)0;
                rotatingLeft = false;
                rotatingRight = false;
            }
            else
            {
                rotatingLeft = false;
                rotatingRight = false;
            }
            //rotation speed cap
            if (rotationVelocity > (Fix64).5)
                rotationVelocity = (Fix64).5;
            if (rotationVelocity < (Fix64)(-.5))
                rotationVelocity = (Fix64)(-.5);

            rotation += rotationVelocity;

            rotation = ScreenWrap.WrapFix64(rotation, (Fix64)6.2831 * (Fix64)rotationscale);



            x += movementVectorX;
            y += movementVectorY;



            if (this.input.GetButtonPressed(InputObject.Button.but1))
            {
                fireButtonCurrentlyPressed = true;
                if (currentChargeValue < charge1Length)
                {
                    chargeSoundDetails2.stop();
                    chargeSoundDetails3.stop();
                    chargeSoundDetails4.stop();

                    chargeSoundDetails1.play();
                    incrimentChargeLevel();

                }
                else if (currentChargeValue < charge2Length)
                {
                    chargeSoundDetails1.stop();
                    chargeSoundDetails3.stop();
                    chargeSoundDetails4.stop();

                    chargeSoundDetails2.play();
                    incrimentChargeLevel();

                }
                else if (currentChargeValue < charge3Length)
                {
                    chargeSoundDetails1.stop();
                    chargeSoundDetails2.stop();
                    chargeSoundDetails4.stop();

                    chargeSoundDetails3.play();
                    incrimentChargeLevel();

                }
                else if (currentChargeValue < charge4Length)
                {
                    chargeSoundDetails1.stop();
                    chargeSoundDetails2.stop();
                    chargeSoundDetails3.stop();

                    chargeSoundDetails4.play();
                    incrimentChargeLevel();

                }
                else if (currentChargeValue >= charge4Length)
                {
                    IncrimentChargeGraphicModifier(currentChargeLevel);
                    health -= 2;
                    chargeSoundDetails1.stop();
                    chargeSoundDetails2.stop();
                    chargeSoundDetails3.stop();
                    chargeSoundDetails4.stop();
                    overchargeSoundDetails.play();
                }

            }
            else
            {
                if (fireButtonCurrentlyPressed)
                {
                    fireButtonCurrentlyPressed = false;
                    //fire
                    if (currentChargeLevel > 0)
                    {
                        
                        caller.QueueAddObject(new SkidstarBullet(x, y, currentChargeLevel, movementVectorX / (Fix64)2, movementVectorY / (Fix64)2, (Fix64)4 + (Fix64)currentChargeLevel, rotation, player,caller));
                        AddForceToShipMomentum((Fix64)(-1.3) * (Fix64)currentChargeLevel);
                        currentChargeValue = 0;
                        chargeSoundDetails1.play();
                        chargeSoundDetails2.stop();
                        chargeSoundDetails3.stop();
                        chargeSoundDetails4.stop();
                    }
                }

                else
                {
                    
                    if (currentChargeValue == 0)
                    {
                        chargeSoundDetails1.play();
                    }
                    if (currentChargeValue < charge1Length)
                    {
                        incrimentChargeLevel();
                    }
                    else
                    {
                        chargeGraphicModifier = 0;
                        chargeSoundDetails1.stop();
                    }
                }
            }

            chargeSoundDetails1.update();
            chargeSoundDetails2.update();
            chargeSoundDetails3.update();
            chargeSoundDetails4.update();



            chargeNotificationDetails.update();
            overchargeSoundDetails.update();
            crashSoundDetails.update();
            engineLoopDetails.update();

        }

        private void incrimentChargeLevel()
        {
            currentChargeValue++;
            if (currentChargeValue == charge4Length)
            {
                chargeNotificationDetails.play();
            }
            else if (currentChargeValue == charge3Length)
            {
                chargeNotificationDetails.play();
            }
            else if (currentChargeValue == charge2Length)
            {
                chargeNotificationDetails.play();
            }
            else if (currentChargeValue == charge1Length)
            {
                chargeNotificationDetails.play();
            }
            IncrimentChargeGraphicModifier(currentChargeLevelByChargeValue());

        }


        private void IncrimentChargeGraphicModifier(int currentChargeLevel)
        {
            chargeGraphicModifier += (currentChargeLevel * 2) + 1;
            if (chargeGraphicModifier > maxChargeModifer)
                chargeGraphicModifier = 0;
        }
        void AddForceToShipMomentum(Fix64 magnitude)
        {
            Fix64 cosOfAngle = Fix64.Cos(rotation / new Fix64(rotationscale));
            Fix64 sinOfAngle = Fix64.Sin(rotation / new Fix64(rotationscale));

            Fix64 x = ((magnitude) * cosOfAngle);
            Fix64 y = ((magnitude) * sinOfAngle);


            //check to see if the new vector is too large

            movementVectorX -= x;
            movementVectorY -= y;


        }

        void IPlayerGameObject.SetInput(InputObject input)
        {
            this.input = input;
        }

        static HashSet<Type>? typeList;
        HashSet<Type> IGameObject.GetCollidableClasses()
        {
            if (typeList is null)
            {
                typeList = new HashSet<Type>
                {
                    typeof(SkidstarShip),
                    typeof(SkidstarBullet),
                    typeof(SkidstarGameManager)
                };
            }
            return typeList;
        }
        internal object Clone()
        {
            return this.MemberwiseClone();
        }


        internal static void LoadContent(ContentManager contentManager)
        {
            if (sprite is null)
                sprite = contentManager.Load<Texture2D>("Unique/Skidstar/Graphics/ship",false);
            if (spriteturnleft is null)
                spriteturnleft = contentManager.Load<Texture2D>("Unique/Skidstar/Graphics/turnship",false);
            if (chargeSound1 is null)
                chargeSound1 = contentManager.Load<SoundEffect>("Unique/Skidstar/Sound/chargeloop1.wav", false);
            if (chargeSound2 is null)
                chargeSound2 = contentManager.Load<SoundEffect>("Unique/Skidstar/Sound/chargeloop2.wav", false);
            if (chargeSound3 is null)
                chargeSound3 = contentManager.Load<SoundEffect>("Unique/Skidstar/Sound/chargeloop3.wav", false);
            if (chargeSound4 is null)
                chargeSound4 = contentManager.Load<SoundEffect>("Unique/Skidstar/Sound/chargeloop4.wav", false);
            if (chargeNotification is null)
                chargeNotification = contentManager.Load<SoundEffect>("Unique/Skidstar/Sound/chargenotification.wav", false);
            if(overchargeSound is null)
                overchargeSound = contentManager.Load<SoundEffect>("Unique/Skidstar/Sound/overcharge.wav", false);
            if(barFrame is null)
                barFrame = contentManager.Load<Texture2D>("Unique/Skidstar/Graphics/barframe",false);
            if(barInternal is null)
                barInternal = contentManager.Load<Texture2D>("Unique/Skidstar/Graphics/barinternal",false);
            if(thrust is null)
                thrust = contentManager.Load<Texture2D>("Unique/Skidstar/Graphics/bullet",false);
            if(engineLoop is null)
                engineLoop = contentManager.Load<SoundEffect>("Unique/Skidstar/Sound/engineloop.wav", false);


            

        }

        object ICloneable.Clone()
        {
            object newObject = this.MemberwiseClone();
            (newObject as SkidstarShip)!.chargeSoundDetails1 = (chargeSoundDetails1.Clone() as NetSound)!;
            (newObject as SkidstarShip)!.chargeSoundDetails2 = (chargeSoundDetails2.Clone() as NetSound)!;
            (newObject as SkidstarShip)!.chargeSoundDetails3 = (chargeSoundDetails3.Clone() as NetSound)!;
            (newObject as SkidstarShip)!.chargeSoundDetails4 = (chargeSoundDetails4.Clone() as NetSound)!;

            (newObject as SkidstarShip)!.chargeNotificationDetails = (chargeNotificationDetails.Clone() as NetSound)!;
            (newObject as SkidstarShip)!.overchargeSoundDetails = (overchargeSoundDetails.Clone() as NetSound)!;
            (newObject as SkidstarShip)!.crashSoundDetails = (crashSoundDetails.Clone() as NetSound)!;
            (newObject as SkidstarShip)!.engineLoopDetails = (engineLoopDetails.Clone() as NetSound)!;

            return newObject;
        }

        int IGameObject.DeletedFrame()
        {
            return -1;
        }

        ulong IGameObject.CalculateDesynchronizationCheckValue(bool verbose)
        {
            Log.Verbose($"player:{player},x:{(int)x / xyscale},y:{(int)y / xyscale}");
            return ((ulong)((int)x / xyscale) + (ulong)((int)y / xyscale) + (ulong)((int)rotation / rotationscale)) * ((ulong)player + 1);
        }


        
        void IGameObject.PlaySounds(NetSoundManager soundManager, int currentFrame)
        {
            chargeSoundDetails1.doPlayback(soundManager);
            chargeSoundDetails2.doPlayback(soundManager);
            chargeSoundDetails3.doPlayback(soundManager);
            chargeSoundDetails4.doPlayback(soundManager);

            chargeNotificationDetails.doPlayback(soundManager);
            overchargeSoundDetails.doPlayback(soundManager);
            crashSoundDetails.doPlayback(soundManager);
            engineLoopDetails.doPlayback(soundManager);
        }
    }
}
