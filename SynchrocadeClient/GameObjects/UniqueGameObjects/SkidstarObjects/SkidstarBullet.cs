﻿using Ultraviolet.Graphics.Graphics2D;
using Ultraviolet.Graphics;
using Ultraviolet;
using Synchrocade.Content.Text.Global;
using Synchrocade.ReusableFunctions;
using System;
using System.Collections.Generic;
using FixMath.NET;
using Ultraviolet.Content;
using Synchrocade.GameObjects;
using Synchrocade.GameObjects.PaletteEffect;
using Synchrocade.GameObjects.Sound;
using Ultraviolet.Audio;

namespace Synchrocade.GameObjects.UniqueGameObjects.SkidstarObjects
{
    class SkidstarBullet : IGameObject
    {
        private const int xyscale = 1;
        private const int rotationscale = 1;

        private int timeToLive = 480;


        internal Fix64 x;
        internal Fix64 y;
        internal readonly Fix64 collisionradius;

        int deletedFrame = -1;

        private readonly Fix64 rotation = new Fix64(0);

        internal Fix64 movementVectorX = new Fix64(0);
        internal Fix64 movementVectorY = new Fix64(0);
        internal int player = 0;
        private static Texture2D? sprite;
        bool collidedWithLargeObject = false;
        private const float layerDepth = .75f;
        private float spriteScale = 1;

        internal int chargeLevel;
        
        
        private int chargeGraphicModifier = 0;
        private const int maxChargeModifer = 200;
        
        
        private static SoundEffect? fire1;
        private static SoundEffect? fire2;
        private static SoundEffect? fire3;
        private static SoundEffect? fire4;
        private NetSound firedetails;



        internal SkidstarBullet(Fix64 x, Fix64 y, int chargeLevel, Fix64 movementVectorX, Fix64 movementVectorY, Fix64 speed, Fix64 angle, int player, OnlineGameLoop owner)
        {
            this.timeToLive = 480 / chargeLevel;
            this.collisionradius = new Fix64(8 * xyscale * chargeLevel);
            this.chargeLevel = chargeLevel;
            this.spriteScale = chargeLevel;
            this.player = player;
            this.x = x;
            this.y = y;
            this.movementVectorX = movementVectorX;
            this.movementVectorY = movementVectorY;
            this.rotation = angle;
            AddForceToShipMomentum(speed + speed / new Fix64(10) * new Fix64(chargeLevel - 1));

            if (chargeLevel == 1)
            {
                firedetails = new NetSound(fire1!,owner);
            }
            else if (chargeLevel == 2)
            {
                firedetails = new NetSound(fire2!,owner);
            }
            else if (chargeLevel == 3)
            {
                firedetails = new NetSound(fire3!,owner);
            }
            else
            {
                firedetails = new NetSound(fire4!,owner);
            }
            firedetails.volume = .50f;
            firedetails.play();

        }

        void IGameObject.AddCollisionCommand(IGameObject otherObject, ICollisionDetails? details, bool performedCollisionCheck)
        {
            if (otherObject.GetType() == typeof(SkidstarShip))
            {
                collidedWithLargeObject = true;
            }
            else if (otherObject.GetType() == typeof(SkidstarBullet))
            {
                SkidstarBullet bullet = (SkidstarBullet)otherObject;
                if (bullet.chargeLevel >= this.chargeLevel)
                {
                    collidedWithLargeObject = true;
                }
            }

        }

        (bool isColliding, ICollisionDetails? details) IGameObject.CheckCollision(IGameObject otherObject)
        {
            if (deletedFrame > 0)
            {
                return (false, null);
            }
            //code for collision checks is handled in skidstar ship
            if (otherObject.GetType() == typeof(SkidstarShip))
            {
                return otherObject.CheckCollision(this);
            }
            if (otherObject.GetType() == typeof(SkidstarBullet))
            {
                SkidstarBullet bullet = (SkidstarBullet)otherObject;
                if (bullet.player == this.player)
                {
                    return (false,null);
                }
                if (Collisions.isCircleColliding(this.x, this.y, this.collisionradius, bullet.x, bullet.y, bullet.collisionradius))
                {
                    return (true,null);
                }
                else
                {
                    return (false,null);
                }
            }
            throw new NotImplementedException();
        }

        void IGameObject.Draw(PaletteEffectSpriteBatch context, float percentToNextFrame, IGameObject? nextFrame)
        {
            if (deletedFrame > 0)
            {
                return;
            }
            if (percentToNextFrame == 0 || nextFrame is null)
            {
                DrawInternal(context, (float)x, (float)y, (float)rotation);
            }
            else
            {

                SkidstarBullet nextBullet = (SkidstarBullet)nextFrame;


                //interpolate x and y
                float ix = ScreenWrap.WrapLerp((float)x, (float)nextBullet.x, percentToNextFrame, 1280 * xyscale);
                float iy = ScreenWrap.WrapLerp((float)y, (float)nextBullet.y, percentToNextFrame, 720 * xyscale);

                //interpolate rotation
                float irotation = ScreenWrap.WrapLerp((float)rotation, (float)nextBullet.rotation, percentToNextFrame, (float)6.2831 * (float)rotationscale); ;
                //draw
                DrawInternal(context, ix, iy, irotation);

            }

        }

        void DrawInternal(PaletteEffectSpriteBatch context, float x, float y, float rotation)
        {
            if (deletedFrame > 0)
            {
                return;
            }
            if(sprite is null)
            {
                throw new InvalidOperationException(GlobalExceptionText.GameAssetNotLoaded);
            }

                Color chargeModificationColor;
                if (chargeLevel >= 4)
                {
                    
                    chargeModificationColor= new Color(255,0,0,(int) (chargeGraphicModifier / 2));
                    float currentCharge = chargeGraphicModifier / 50f;
                    if (currentCharge <= 1)
                    {
                        x = x - 5;
                    }
                    else if (currentCharge > 1 && currentCharge <= 2)
                    {
                        y = y + 5;
                    }
                    else if (currentCharge > 2 && currentCharge <= 3)
                    {
                        x = x + 5;
                    }
                    else if (currentCharge > 3 && currentCharge <= 4)
                    {
                        y = y - 5;
                    }
                }
                else
                    chargeModificationColor= new Color(200,255,255,(int) (chargeGraphicModifier / 4));

            
                PaletteSpriteData paletteId = new PaletteSpriteData((UInt16)(player),true);
                //draw the sprite
                context.Draw(sprite, new Vector2(x / xyscale, y / xyscale), new Rectangle(0, 0, sprite.Width, sprite.Height), chargeModificationColor, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), spriteScale, SpriteEffects.None, layerDepth, paletteId);

                //draw one of the up to 4 clones
                bool touchingLeftWall = (x - sprite.Width * xyscale / 2) < 0;
                bool touchingRightWall = (x + sprite.Width * xyscale / 2) > 1280;

                bool touchingTopWall = (y - sprite.Height * xyscale / 2) < 0;
                bool touchingBottomWall = (y + sprite.Height * xyscale / 2) > 720; ;
                if (touchingLeftWall)
                {
                    context.Draw(sprite, new Vector2((x / xyscale) + 1280, y / xyscale), new Rectangle(0, 0, sprite.Width, sprite.Height), chargeModificationColor, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), spriteScale, SpriteEffects.None, layerDepth, paletteId);

                    if (touchingBottomWall)
                    {
                        context.Draw(sprite, new Vector2((x / xyscale) + 1280, (y / xyscale) - 720), new Rectangle(0, 0, sprite.Width, sprite.Height), chargeModificationColor, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), spriteScale, SpriteEffects.None, layerDepth, paletteId);
                        context.Draw(sprite, new Vector2((x / xyscale), (y / xyscale) - 720), new Rectangle(0, 0, sprite.Width, sprite.Height), chargeModificationColor, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), spriteScale, SpriteEffects.None, layerDepth, paletteId);
                    }
                    else if (touchingTopWall)
                    {
                        context.Draw(sprite, new Vector2((x / xyscale) + 1280, (y / xyscale) + 720), new Rectangle(0, 0, sprite.Width, sprite.Height), chargeModificationColor, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), spriteScale, SpriteEffects.None, layerDepth, paletteId);
                        context.Draw(sprite, new Vector2((x / xyscale), (y / xyscale) + 720), new Rectangle(0, 0, sprite.Width, sprite.Height), chargeModificationColor, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), spriteScale, SpriteEffects.None, layerDepth, paletteId);
                    }
                }
                else if (touchingRightWall)
                {
                    context.Draw(sprite, new Vector2((x / xyscale) - 1280, y / xyscale), new Rectangle(0, 0, sprite.Width, sprite.Height), chargeModificationColor, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), spriteScale, SpriteEffects.None, layerDepth, paletteId);

                    if (touchingBottomWall)
                    {
                        context.Draw(sprite, new Vector2((x / xyscale) - 1280, (y / xyscale) - 720), new Rectangle(0, 0, sprite.Width, sprite.Height), chargeModificationColor, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), spriteScale, SpriteEffects.None, layerDepth, paletteId);
                        context.Draw(sprite, new Vector2((x / xyscale), (y / xyscale) - 720), new Rectangle(0, 0, sprite.Width, sprite.Height), chargeModificationColor, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), spriteScale, SpriteEffects.None, layerDepth, paletteId);
                    }
                    else if (touchingTopWall)
                    {
                        context.Draw(sprite, new Vector2((x / xyscale) - 1280, (y / xyscale) + 720), new Rectangle(0, 0, sprite.Width, sprite.Height), chargeModificationColor, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), spriteScale, SpriteEffects.None, layerDepth, paletteId);
                        context.Draw(sprite, new Vector2((x / xyscale), (y / xyscale) + 720), new Rectangle(0, 0, sprite.Width, sprite.Height), chargeModificationColor, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), spriteScale, SpriteEffects.None, layerDepth, paletteId);
                    }

                }
                else if (touchingTopWall)
                {
                    context.Draw(sprite, new Vector2((x / xyscale), (y / xyscale) + 720), new Rectangle(0, 0, sprite.Width, sprite.Height), chargeModificationColor, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), spriteScale, SpriteEffects.None, layerDepth, paletteId);

                }
                else if (touchingBottomWall)
                {
                    context.Draw(sprite, new Vector2((x / xyscale), (y / xyscale) - 720), new Rectangle(0, 0, sprite.Width, sprite.Height), chargeModificationColor, rotation / (float)rotationscale, new Vector2(sprite.Width / 2, sprite.Height / 2), spriteScale, SpriteEffects.None, layerDepth, paletteId);

                }
            

        }

        void IGameObject.ExecuteCollisionCommands(OnlineGameLoop caller)
        {
            if(collidedWithLargeObject)
            {
                
                caller.QueueAddObject(new SkidstarExplosionEffect(player,new Fix64(chargeLevel)/new Fix64(2),x,y,0,caller));
                deletedFrame = caller.GetCurrentFrame();

            }
        }
        

        void IGameObject.Update(OnlineGameLoop caller)
        {
            if (deletedFrame < 0)
            {
                
                chargeGraphicModifier += (chargeLevel * 2) + 1;
                if (chargeGraphicModifier > maxChargeModifer)
                    chargeGraphicModifier = 0;

                
                x = ScreenWrap.WrapFix64(x, new Fix64(1280 * xyscale));
                y = ScreenWrap.WrapFix64(y, new Fix64(720 * xyscale));

                x += movementVectorX;
                y += movementVectorY;


                timeToLive--;
                if (timeToLive < 0)
                {
                    deletedFrame = caller.GetCurrentFrame();
                }


                //need to solve deletes
            }
            firedetails.update();

        }


        void AddForceToShipMomentum(Fix64 magnitude)
        {
            Fix64 cosOfAngle = Fix64.Cos(rotation / new Fix64(rotationscale));
            Fix64 sinOfAngle = Fix64.Sin(rotation / new Fix64(rotationscale));

            Fix64 x = magnitude * cosOfAngle;
            Fix64 y = magnitude * sinOfAngle;


            //check to see if the new vector is too large

            movementVectorX -= x;
            movementVectorY -= y;
        }



        static HashSet<Type>? typeList;
        HashSet<Type> IGameObject.GetCollidableClasses()
        {
            if (typeList is null)
            {
                typeList = new HashSet<Type>
                {
                    typeof(SkidstarShip), typeof(SkidstarBullet)
                };
            }
            return typeList;
        }

        internal static void LoadContent(ContentManager contentManager)
        {
            if(fire1 is null)
                fire1 = contentManager.Load<SoundEffect>("Unique/Skidstar/Sound/fire1.wav", false);
            if(fire2 is null)
                fire2 = contentManager.Load<SoundEffect>("Unique/Skidstar/Sound/fire2.wav", false);
            if(fire3 is null)
                fire3 = contentManager.Load<SoundEffect>("Unique/Skidstar/Sound/fire3.wav", false);
            if(fire4 is null)
                fire4 = contentManager.Load<SoundEffect>("Unique/Skidstar/Sound/fire4.wav", false);

            if(sprite is null)
                sprite = contentManager.Load<Texture2D>("Unique/Skidstar/Graphics/bullet",false);
        }

        object System.ICloneable.Clone()
        {
            SkidstarBullet clone = (SkidstarBullet) this.MemberwiseClone();
            clone.firedetails = (NetSound)firedetails.Clone();
            return clone;
        }

        int IGameObject.DeletedFrame()
        {
            return deletedFrame;
        }

        ulong IGameObject.CalculateDesynchronizationCheckValue(bool verbose)
        {
            return ((ulong)((int)x / xyscale) + (ulong)((int)y / xyscale) + (ulong)((int)rotation / rotationscale)) * ((ulong)player + 1);
        }

        void IGameObject.PlaySounds(NetSoundManager soundManager, int currentFrame)
        {
            firedetails.doPlayback(soundManager);

        }
    }
}