﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synchrocade.GameObjects
{
    internal enum DrawMode {
        Interpolation, Extrapolation, Projection, Normal, Comparison
    };
}
