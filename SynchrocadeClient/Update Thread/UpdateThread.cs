﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Synchrocade.UpdateThread
{
    internal static class NativeMethods
    {
        [DllImport("ntdll.dll", SetLastError = true)]
        internal static extern int NtQueryTimerResolution(out uint MinimumResolution, out uint MaximumResolution, out uint CurrentResolution);
        [DllImport("ntdll.dll", SetLastError = true)]
        internal static extern int NtSetTimerResolution(in uint DesiredResolution, in bool SetResolution, out uint CurrentResolution);
    }

    internal class UpdateThread
    {
        readonly object _memoryIntegrityLock;
        private Thread? workerThread;
        private bool loopRunning;
        long nextFrameTime = 0; //frame time in nanoseconds, because why not
        private SpinWait spinner = new SpinWait();
        public bool IsStalled { get; set; }
        private double targetUps;
        private long nanosecondsPerUpdate;
        private readonly Stopwatch elapsedTime = new Stopwatch();
        private readonly Action<bool> calledFunction;
        internal UpdateThread(Action<bool> calledFunction, double targetUps, object memoryIntegrityLock)
        {
            IsStalled = false;
            _memoryIntegrityLock = memoryIntegrityLock;
            UpdateSystemTimerResolution();

            this.SetUPS(targetUps);
            this.calledFunction = calledFunction;
            
            //this.host = host;
            //this.tickTimer.Start();
        }
        
        internal bool IsRunning()
        {
            return loopRunning;
        }
        internal float PercentToNextFrame()
        {
            //get current time
            long currentNanoseconds = (long)(elapsedTime.ElapsedTicks / (double)Stopwatch.Frequency * 1000000000);
            //find the differecne between the two
            long timeIntoFrame = nextFrameTime - currentNanoseconds;
            //get the difference between those two numbers, and the time per frame
            float returnTime = 1f - (float)timeIntoFrame / nanosecondsPerUpdate;

            /*if (returnTime < 0)
                return 0;
            if (returnTime > 1)
                return 1;*/

            return returnTime;
        }

        [DebuggerNonUserCode()]
        private void ThreadMain()
        {
            try
            {
                this.elapsedTime.Start();
                long framecount = 0;
                while (loopRunning)
                {
                    lock (_memoryIntegrityLock)
                    {
                        calledFunction.Invoke(IsStalled);
                        nextFrameTime += nanosecondsPerUpdate;
                        framecount++;
                    }

                    long currentNanoseconds = (long)(elapsedTime.ElapsedTicks / (double)Stopwatch.Frequency * 1000000000);
                    long timeToSleep = nextFrameTime - currentNanoseconds;


                    int msToSleep = (int)(timeToSleep / 1000000);
                    if (msToSleep > 1)
                    {
                        Thread.Sleep(msToSleep - 1);
                    }
                    do
                    {
                        spinner.SpinOnce();
                        currentNanoseconds = (long)(elapsedTime.ElapsedTicks / (double)Stopwatch.Frequency * 1000000000);
                    } while (currentNanoseconds < nextFrameTime);
                }
            }
            catch(Exception e)
            {

                Log.Fatal(e, "Exception Passed To Update Thread");
                throw;
            }
        }

         
        internal void SetUPS(double newUpdates)
        {
            this.targetUps = newUpdates;
            this.nanosecondsPerUpdate = (uint)(1.0 / (double)newUpdates * 1000000000);
        }

        internal double GetUPS()
        {
            return this.targetUps;
        }

        //timing
        //aim for targetUps operations per second, let's actually ignore spinwaits for now, because this isn't draw, and slight shifting sholud be fine
        //otherwise tell the timer to go exactly to the right time

        internal void Start()
        {
            loopRunning = true;
            workerThread = new Thread(new ThreadStart(ThreadMain));
            workerThread.Start();
        }

        internal void Stop()
        {
            loopRunning = false;

        }


        private bool UpdateSystemTimerResolution()
        {

            if (!System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                //systemTimerPeriod = 10000;
                return false;
            }




            uint desired = 5000;

            bool setresolution = true;

            _ = NativeMethods.NtQueryTimerResolution(out _, out uint max, out _);
            if (max > desired)
                desired = max;
            //System.Console.WriteLine(max);
            _ = NativeMethods.NtSetTimerResolution(in max, in setresolution, out _);

            return true;
        }

    }
}
