namespace Synchrocade.ReusableFunctions
{
    public static class DrawHelpers
    {
        public static float GetScaledValue((int nativeValue, int scaleValue) scale,
            float valueToConvert)
        {
            float scaleRatio = scale.scaleValue / (float)scale.nativeValue ;

            return scaleRatio * valueToConvert;
        }
    }
}