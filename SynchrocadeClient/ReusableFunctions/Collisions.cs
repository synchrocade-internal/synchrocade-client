﻿using Synchrocade.Content.Text.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixMath.NET;

namespace Synchrocade.ReusableFunctions
{
    class Collisions
    {
        internal static bool isCircleColliding(Fix64 circle1x, Fix64 circle1y, Fix64 circle1r, Fix64 circle2x, Fix64 circle2y, Fix64 circle2r)
        {
            Fix64 collisionDistance = circle1r+circle2r;
            return ((circle1x - circle2x) * (circle1x - circle2x) + (circle1y - circle2y) * (circle1y - circle2y)) <= collisionDistance * collisionDistance;
        }


        internal static (Fix64 c1VelModX, Fix64 c1VelModY, Fix64 c2VelModX, Fix64 c2VelModY) calculateBouncesForPhysicsCircles(Fix64 c1x, Fix64 c1y, Fix64 c1velx, Fix64 c1vely, Fix64 c1mass, Fix64 c2x, Fix64 c2y, Fix64 c2velx, Fix64 c2vely, Fix64 c2mass)
        {
            //specific code inspired by https://www.youtube.com/watch?v=LPzyNOHY3A4 
            Fix64 distanceBetweenPoints = Fix64.Sqrt((c1x - c2x) * (c1x - c2x) + (c1y - c2y) * (c1y - c2y));
            Fix64 nx = (c2x - c1x) / distanceBetweenPoints;
            Fix64 ny = (c2y - c1y) / distanceBetweenPoints;

            Fix64 kx = (c1velx - c2velx);
            Fix64 ky = (c1vely - c2vely);
            Fix64 p = new Fix64(2) * (nx * kx + ny * ky) / (c1mass + c2mass);
            (Fix64 c1VelModX, Fix64 c1VelModY, Fix64 c2VelModX, Fix64 c2VelModY) returnValue;
            returnValue.c1VelModX = (new Fix64(-1) * p * (c2mass) * nx);
            returnValue.c1VelModY = (new Fix64(-1) * p * (c2mass) * ny);
            returnValue.c2VelModX = (p * (c1mass) * nx);
            returnValue.c2VelModY = (p * (c1mass) * ny);
            return returnValue;


        }
        internal static (Fix64 modx, Fix64 mody) correctionDistanceBetweenCircles(Fix64 circle1x, Fix64 circle1y, Fix64 circle1r, Fix64 circle2x, Fix64 circle2y, Fix64 circle2r)
        {
            //specific code inspired by https://www.youtube.com/watch?v=LPzyNOHY3A4 


            Fix64 distanceBetweenPoints = Fix64.Sqrt((circle1x - circle2x) * (circle1x - circle2x) + (circle1y - circle2y) * (circle1y - circle2y));
            Fix64 overlap = (distanceBetweenPoints - circle1r - circle2r) / new Fix64(2) ;

            (Fix64 modx, Fix64 mody) returnValue;

            returnValue.modx = Fix64.NFloor(overlap * (circle1x - circle2x) / distanceBetweenPoints);
            if(returnValue.modx > (Fix64)0) //produce some very tiny extra space between the two due to moving from float to int
            {
                returnValue.modx = returnValue.modx + (Fix64)1;
            }
            else if (returnValue.modx < (Fix64)0)
            {
                returnValue.modx = returnValue.modx - (Fix64)1;
            }

            returnValue.mody = Fix64.NFloor(overlap * (circle1y - circle2y) / distanceBetweenPoints);
            if (returnValue.mody > (Fix64)0) //produce some very tiny extra space between the two due to moving from float to int
            {
                returnValue.mody = returnValue.mody + (Fix64)1;
            }
            else if (returnValue.mody < (Fix64)0)
            {
                returnValue.mody = returnValue.mody - (Fix64)1;
            }
#if DEBUG
            Fix64 checkdistanceBetweenPoints = Fix64.Sqrt((circle2x - circle1x) * (circle2x - circle1x) + (circle2y - circle1y) * (circle2y - circle1y));
            Fix64 checkoverlap = (checkdistanceBetweenPoints - circle2r - circle1r) / new Fix64(2);

            (Fix64 modx, Fix64 mody) checkVAlue;

            checkVAlue.modx = Fix64.NFloor(overlap * (circle2x - circle1x) / distanceBetweenPoints);
            if (checkVAlue.modx > (Fix64)0) //produce some very tiny extra space between the two due to moving from float to int
            {
                checkVAlue.modx = checkVAlue.modx + (Fix64)1;
            }
            else if (checkVAlue.modx < (Fix64)0)
            {
                checkVAlue.modx = checkVAlue.modx - (Fix64)1;
            }

            checkVAlue.mody = Fix64.NFloor(overlap * (circle2y - circle1y) / distanceBetweenPoints);
            if (checkVAlue.mody > (Fix64)0) //produce some very tiny extra space between the two due to moving from float to int
            {
                checkVAlue.mody = checkVAlue.mody + (Fix64)1;
            }
            else if (checkVAlue.mody < (Fix64)0)
            {
                checkVAlue.mody = checkVAlue.mody - (Fix64)1;
            }
            
            if (checkVAlue.modx != new Fix64(-1) * returnValue.modx || checkVAlue.mody != new Fix64(-1) * returnValue.mody)
                throw new InvalidOperationException(GlobalExceptionText.CollisionNonSymetrical);
#endif

            return returnValue;
        }
    }
}
