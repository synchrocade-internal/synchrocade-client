
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Synchrocade.ReusableFunctions
{
    public static class LinkHandler
    {
        public static bool ExecuteLink(String target)
        {
            //https://stackoverflow.com/questions/4580263/how-to-open-in-default-browser-in-c-sharp
            target = target.Replace("\"", "");
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                target = target.Replace("&", "^&");
                Process.Start(new ProcessStartInfo("cmd", $"/c start {target}") { CreateNoWindow = true });
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                Process.Start("xdg-open", target);
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                Process.Start("open", target);
            }
            else
            {
                throw new Exception("Tried to handle links on an unsupported system.");
            }
            return true;
        }
    }
}