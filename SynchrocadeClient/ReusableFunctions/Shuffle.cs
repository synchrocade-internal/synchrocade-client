namespace Synchrocade.ReusableFunctions
{
    public class Shuffle
    {
        public static void ShuffleArray<T>(T[] array, ClonableRandom rnd)
        {
            for(var i=array.Length; i > 0; i--)
                Swap<T>(array,i-1, rnd.Next(0, i));
        }

        private static void Swap<T>(T[] array, int i, int j)
        {
            T temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        
    }
}