﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Synchrocade.ReusableFunctions
{
    internal static class BitShift
    {
        //this is a side effect function, and it doesn't check for space, so manage the space yourself
        //bits to add should be the rightmost bits
        //highorder means add from the left (higher) side, loworder means add from the right (lower) side
        public static void AddToArray(ref byte[] byteArray, uint currentBitPosition, byte byteToAdd, uint numBitsToAdd, bool highOrder)
        {
            //find byte refreence
            uint byteInString = currentBitPosition / 8;
            uint bitInByte = currentBitPosition % 8;

            if(bitInByte + numBitsToAdd <= 8)
            {
                //if this operation doesn't break the bit boundery and can be done in one operation
                //shift to the correct bit
                byte shiftedByte;
                if (!highOrder)
                {
                    shiftedByte = (byte)(byteToAdd << (byte)((8 - bitInByte) - (numBitsToAdd)));
                }
                else
                {
                    shiftedByte = (byte)(byteToAdd >> (byte)bitInByte);
                }
                byteArray[byteInString] = (byte)(byteArray[byteInString] | shiftedByte);
            }
            else
            {
                if (!highOrder)
                {

                    //fill out the current block
                    uint bitsToShift = (numBitsToAdd) - (8 - bitInByte);
                    byte shiftedByte = (byte)(byteToAdd >> (byte)bitsToShift);
                    byteArray[byteInString] = (byte)(byteArray[byteInString] | shiftedByte);
                    //spawn an operation to fill the next block
                    //3 is the correct number of bits to trash

                    byte truncatedByte = (byte)(byteToAdd << (byte)(8 - bitsToShift));
                    truncatedByte = (byte)(truncatedByte >> (byte)(8 - bitsToShift));
                    AddToArray(ref byteArray, currentBitPosition + (8 - bitsToShift), truncatedByte, numBitsToAdd - (8 - bitsToShift), false);
                }
                else
                {
                    byte shiftedByte = (byte)(byteToAdd >> (byte)bitInByte);
                    byteArray[byteInString] = (byte)(byteArray[byteInString] | shiftedByte);

                    byte truncatedByte = (byte)(byteToAdd << (byte)(8 - bitInByte));
                    AddToArray(ref byteArray, currentBitPosition + (8 - bitInByte), truncatedByte, numBitsToAdd - (8 - bitInByte), true);

                    //addToArray(ref byteArray, currentBitPosition, byteToAdd, numBitsToAdd, false);
                }

            }
            //shift the first bit to the position we need
        }

        //where the other byte is backloaded, we want THIS to be frontloaded, so we start from the leftmost bits
        internal static void AddToArray(ref byte[] byteArray, uint currentBitPosition, byte[] bytesToAdd, uint numBitsToAdd)
        {
            uint bitsStillNeedingAdded = numBitsToAdd;
            uint bitsProcessed = 0;
            while(bitsStillNeedingAdded > 0)
            {
                if(bitsStillNeedingAdded >= 8)
                {
                    
                    AddToArray(ref byteArray, currentBitPosition + bitsProcessed, bytesToAdd[bitsProcessed / 8], 8,true);
                    bitsStillNeedingAdded -= 8;
                    bitsProcessed += 8;
                }
                else
                {
                    //now onto final byte
                    AddToArray(ref byteArray, currentBitPosition + bitsProcessed, bytesToAdd[bitsProcessed / 8], bitsStillNeedingAdded,true);
                    bitsStillNeedingAdded = 0;
                }
            }
        }

        internal static void FillByteEnd(ref byte fillByte, uint startingPosition, bool ones = true)
        {
            uint shiftDistance = startingPosition % 8;
            if (ones)
            {
                byte shiftedByte = (byte)(0b1111_1111 >> (byte)shiftDistance);
                fillByte = (byte)(fillByte | shiftedByte);
            }
            else
            {
                byte shiftedByte = (byte)(0b1111_1111 << (byte)(8 - shiftDistance));
                fillByte = (byte)(fillByte & shiftedByte);
            }
        }

        internal static void FillFinalArrayByteEnd(ref byte[] fillByte, uint startingPosition, bool ones = true)
        {
            uint byteStartingPosition = startingPosition % 8;
            uint index = ((startingPosition + 1) / 8);
            FillByteEnd(ref fillByte[index], byteStartingPosition, ones);
        }

        internal static Boolean ReadBitFromArray(byte[] byteArray, uint position)
        {
            uint shiftDistance = position % 8;
            byte compareByte = (byte)(byteArray[position / 8] << (byte)shiftDistance);
            compareByte = (byte)(compareByte & (byte) 0b1000_0000);
            return (compareByte > 0);
        }
        internal static byte ReadSingleByteFromArray(byte[] byteArray, uint startPosition, uint bitsToRead)
        {
            return ReadFromArray(byteArray, startPosition, bitsToRead)[0];
        }
        internal static byte[] ReadFromArray(byte[] byteArray, uint startPosition, uint bitsToRead)
        {
            uint size = bitsToRead / 8 + 1;
            byte[] returnArray = new byte[size];
            uint shiftDistance = startPosition % 8;
            uint currentReturnArrayPosition = 0;
            if (shiftDistance == 0)
            {
                //copy the array exactly to the last bit
                Array.Copy(byteArray, startPosition / 8, returnArray, 0, bitsToRead / 8);

                //truncate the last bit

                byte truncateSpace = (byte)(8 - bitsToRead % 8);
                byte truncatedByte = (byte)((byte)(byteArray[(startPosition / 8) + (bitsToRead / 8)] >> truncateSpace) << truncateSpace);
                returnArray[bitsToRead / 8] = truncatedByte;

            }
            else
            {
                uint startIndex = startPosition / 8;
                uint bytesToRead = (bitsToRead / 8) + 1;
                for (uint x = startIndex; x < startIndex + bytesToRead; x++)
                {
                    //shift all bits in range left to get to proper left shift poing
                    returnArray[currentReturnArrayPosition] = (byte)(byteArray[x] << (byte)shiftDistance);
                    //shift the sources to the right to have only the remainder bits
                    if (byteArray.Length > x + 1)
                    {
                        byte shiftedByte = (byte)(byteArray[x + 1] >> (byte)(8 - shiftDistance));
                        returnArray[currentReturnArrayPosition] = (byte)(returnArray[currentReturnArrayPosition] | shiftedByte);
                    }
                    currentReturnArrayPosition++;
                }
                uint truncateBits = 8 - (bitsToRead % 8);
                returnArray[currentReturnArrayPosition - 1] = (byte)((byte)(returnArray[currentReturnArrayPosition - 1] >> (byte)truncateBits) << (byte)truncateBits);
                //final bit

                //first truncate

                //then place into position


            }



            return returnArray;
        }


    }
}
