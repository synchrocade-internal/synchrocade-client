﻿using FixMath.NET;

namespace Synchrocade.ReusableFunctions
{
    class ScreenWrap
    {
        internal static Fix64 WrapFix64(Fix64 num, Fix64 wrapPoint)
        {
            if (num < new Fix64(0))
                return wrapPoint + num;
            else
                return num % wrapPoint;
        }
        internal static float WrapFloat(float num, float wrapPoint)
        {
            if (num < 0)
                return wrapPoint + num;
            else
                return num % wrapPoint;
        }

        internal static float WrapLerp(float a, float b, float scaleFactor, float wrapPoint)
        {
            //this crossover comes when moving from the largest to smallest point, or the smallest to largest point
            if(a > wrapPoint *.75 && b < wrapPoint *.25)
            {
                b = b + wrapPoint;
            }
            else if (a < wrapPoint * .25 && b > wrapPoint * .75)
            {
                a = a + wrapPoint;
            }

            //
            return WrapFloat(Ultraviolet.Core.MathUtil.Lerp(a,b, scaleFactor),wrapPoint);
        }
    }
}
