using System;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using Serilog;


namespace Synchrocade
{
    class Program
    {
        static void Main(string[] args)
        {
            Environment.CurrentDirectory = AppContext.BaseDirectory;
            Log.Logger = new LoggerConfiguration().Enrich.WithProcessId().Enrich.FromLogContext().MinimumLevel.Verbose().WriteTo.Console(Serilog.Events.LogEventLevel.Verbose).WriteTo.File("../Logs/log.txt", rollingInterval: RollingInterval.Day, retainedFileCountLimit: 3,fileSizeLimitBytes: 5242880,shared:true).CreateLogger();
            Log.Information("Starting Synchrocade Executable");

            bool compile = false;
            bool run = false;
            bool ignorebadssl = false;

            bool lastp = false;
            bool lasta = false;

            string address = "synchrocade.com";
            int port = 443;
            foreach (string arg in args)
            {
                if (lasta)
                {
                    lasta = false;
                    if (Uri.CheckHostName(arg) != UriHostNameType.Unknown)
                        address = arg;
                    continue;
                }
                if (lastp)
                {
                    lastp = false;
                    int.TryParse(arg, out port);
                    if (port < 0 || port > 49151)
                        port = 443;
                    continue;
                }
                switch (arg)
                {
                    case "-compile":
                        compile = true;
                        break;
                    case "-run":
                        run = true;
                        break;
                    case "-a":
                        lasta = true;
                        break;
                    case "-p":
                        lastp = true;
                        break;
                    case "-ignorebadssl":
                        ignorebadssl = true;
                        break;
                }
            }

            //don't allow us to disable ssl when targeting our normal server
            if (ignorebadssl && address == "synchrocade.com")
            {
                ignorebadssl = false;
            }
            if (!compile && !run)
                run = true;

            try
            {
                using (var game = new OnlineGame(compile, run, ignorebadssl, address, port))
                    game.Run();
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Exception returned to main.");
            }

            
            Log.Information("Ending Synchrocade Executable");
            Log.CloseAndFlush();

        }
    }
}
