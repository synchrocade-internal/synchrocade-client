﻿using Synchrocade.Content.Text.Global;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synchrocade.TemporalContainer
{
    internal class CircularArray<T>
    {
        //a circular array object
        private readonly T[] buffer;
        private long head = -1; //the position in the array of the first element, -1 if no first element
        private long startindex = -1; //how far in the indacies head is 
        private long count = 0; //how many elements exist past the head
        private readonly long startingFrame = -1;
        private T extrapolatedFrame;
        private T firstElementCache;

        

        internal CircularArray(ulong size, long startingFrame, T firstElementCache, T extrapolatedFrame)
        {
            buffer = new T[size];
            startindex = startingFrame - 1;
            this.startingFrame = startingFrame;
            this.firstElementCache = firstElementCache;
            this.extrapolatedFrame = extrapolatedFrame;
        }

        internal T GetFirstElementCache()
        {
            return firstElementCache;
        }

        internal void SetFirstElementCache(T objectToCache)
        {
            firstElementCache = objectToCache;
        }

        internal long GetStartingFrame()
        {
            return this.startingFrame;
        }

        //this doesn't do much of real use, it can be called to clear memory a bit faster
        internal void RemoveItemsFromStart(long newFirstElementIndex)
        {

            long index = CalculateIndexLocation(newFirstElementIndex);
            long travel = newFirstElementIndex - startindex;
            long newstartindex = startindex + travel;
            head = index;
            count -= travel;
            startindex = newstartindex;
        }

        //this is key to our rollback system
        internal void RemoveItemsFromEnd(long newLastElementIndex)
        {
            extrapolatedFrame = default;
            long travel = startindex + count - 1 - newLastElementIndex;
            this.count -= travel;
        }
        //this will be special, as index will be total, so if we're 1200 in, with a buffer of 200, 0-999 will be inaccessable, and 1000-1200 will be acessable
        internal T this[long index]
        {
            get
            {
                long calculatedIndex = CalculateIndexLocation(index);
                if (calculatedIndex == -1)
                {
                    throw new ArgumentOutOfRangeException(string.Format(CultureInfo.CurrentCulture,GlobalExceptionText.TemporalIndexOutOfRange, index, startindex, startindex + count - 1));
                }
                return buffer[calculatedIndex];
            }
            set
            {
                extrapolatedFrame = default;
                long calculatedIndex = CalculateIndexLocation(index);
                if (calculatedIndex == -1)
                {
                    throw new ArgumentOutOfRangeException(string.Format(CultureInfo.CurrentCulture, GlobalExceptionText.TemporalIndexOutOfRange, index, startindex, startindex + count));
                }
                buffer[calculatedIndex] = value;
            }
        }

        //the array index given the full length index, or -1 if the index is invalid
        private long CalculateIndexLocation(long index)
        {
            if (index < startindex || index > startindex + count - 1)
            {
                return -1;
            }
            else
            {

                return (head + index - startindex) % buffer.Length;
            }
        }

        internal void Push(T objectToAdd)
        {
            extrapolatedFrame = default;
            if (count < buffer.Length)
            {
                count++;
                if (head == -1)
                {
                    head++;
                    startindex++;
                }
            }
            else
            {
                head++;
                if (head >= buffer.Length)
                {
                    head = 0;
                }
                startindex++;
            }
            buffer[CalculateIndexLocation(startindex + count - 1)] = objectToAdd;
        }


        internal T GetExtrapolatedFrame()
        {
            return extrapolatedFrame;
        }

        internal void SetExtrapolatedFrame(T extrapoloation)
        {
            extrapolatedFrame = extrapoloation;
        }
    }
}
